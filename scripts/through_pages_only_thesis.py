import urllib2
import requests
from requests.exceptions import HTTPError
from HTMLParser import HTMLParser
import re
from string import punctuation
from subprocess import call
import time
import argparse
import sys
reload(sys)  
sys.setdefaultencoding('utf8')

class MyHTMLParser(HTMLParser):

  def __init__(self):
    HTMLParser.__init__(self)
    self.recording = 0
    self.data = []
  def handle_starttag(self, tag, attrs):
    if tag == 'p':
      for name, value in attrs:
        if name == 'class' and value == 'sb':
            self.recording = 1


  def handle_endtag(self, tag):
    if tag == 'required_tag':
      self.recording -=1


  def handle_data(self, data):
    if self.recording:
        self.data.append(data)



""" 
Thesis languages: 

EN
ET
QU
RO 
SW
TA
DE

"""

languages = ['DE'] #['EN','ET','QU','RO','SW',"TA",'DE']

pageIds = [2016000,        2016002,2016003,2016004,2016005,2016006,2016007,2016008,2016009,\
                           2016162,2016163,2016164,2016165,2016166,2016167,2016168,               2016172,\
                           2016082,        2016084,2016085,2016086,2016087,2016088,2016089,2016090,2016091,2016092,2016095,\
                           2016242,        2016244,2016245,2016246,2016247,2016248,2016249,2016250,        2016252,2016253,2016256,2016257,\
                           2016322,        2016324,2016325,2016326,2016327,2016328,        2016330,2016333,2016334,\
           2016400,        2016402,2016403,2016404,2016405,2016406,2016407,                                2016412,\
                   2016041,2016042,2016043,        2016045,2016046,2016047,\
                   2016121,2016122,2016123,2016124,        2016126,2016127,2016128,\
                   2016201,2016202,                2016205,2016206,2016207,2016208,\
                   2016281,2016282,2016283,        2016285,2016286,2016287,2016288,\
                   2016361,2016362,        2016364,        2016366,2016367,2016368,\
                   2016441,2016442,2016443,        2016445,        2016447,2016448,\
                   2016521,2016522,2016523,                2016526,2016527,2016528,\
                   2016601,2016602,2016603,                        2016607,2016608,2016609,         2016611,\
                   2016681,2016682,2016683,2016684,        2016686,2016687,2016688,\
                   2016761,2016762,2016763,        2016765,2016766,2016767,2016768,\
                   2016841,2016842,2016843,2016844,2016845,        2016847,2016848,2016849,\
                   2016921,2016922,2016923,2016924,2016925,2016926,        2016928,\
                           2017002,                2017005,2017006,2017007,2017008,2017009,2017010,2017011,2017012,\
                   2017241,2017242,2017243,2017244,        2017246,2017247,2017248, \
                   2017281,2017282,2017283,                2017286,2017287,2017288]


for pid in pageIds:

    pageId = str(pid)
    print (pageId)

    if pageId.startswith('20'):
        call(['mkdir','-p',pageId[:4]+'/'+pageId])
        folder = pageId[:4]+'/'+pageId +'/'
    else:
        # 402016522
        year =  pageId[2:6]
        print (year)
        call(['mkdir','-p',year+'/'+pageId])
        folder = year+'/'+pageId+'/'




    p = MyHTMLParser()


    for language in languages:
        time.sleep(.05) # put a pause in because  system thinks its a DoS attack
        


        web_page = "http://m.wol.jw.org/" + language + "/wol/d/r37/lp-" + language + "/" + pageId

        try:
            r = requests.get(web_page)
            r.raise_for_status()
        except HTTPError:
            print ("Could not load the page", language)
        else:
            print ("Loaded the page for ", language)
            f = urllib2.urlopen(web_page)
            print ("Scraping the web..")
            html = f.read()
            p.feed(html)
            p.close()

            file_name = language + pageId + ".txt"
            print ("Creating the file..", file_name)
            file = open(folder+file_name, "w")
            for word in p.data:
                file.write(re.sub(r'(?<=['+punctuation+'])\s+(?=[A-Z])', '\n', word))
            p.data = []
            p.recording = 0
            p.close()