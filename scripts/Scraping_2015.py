import urllib2
import requests
from requests.exceptions import HTTPError
from HTMLParser import HTMLParser
import re
from string import punctuation
from subprocess import call
import time

class MyHTMLParser(HTMLParser):

  def __init__(self):
    HTMLParser.__init__(self)
    self.recording = 0
    self.data = []
  def handle_starttag(self, tag, attrs):
    if tag == 'p':
      for name, value in attrs:
        if name == 'class' and value == 'sb':
            self.recording = 1


  def handle_endtag(self, tag):
    if tag == 'required_tag':
      self.recording -=1


  def handle_data(self, data):
    if self.recording:
        self.data.append(data)


languages = ["ACH", "AF", "SQ", "AM", "AR", "HY", "ATI", "AY", "AZ-LATN", "BCI", "BAS", "BN", "BCL", "BI", "BS", "BUM", "BG", "CAK", "KM", "CAT", "CEB", "NY", "CMN-HANS", "CMN-HANT", "TOI", "TOG", "TUM", "YAO", "CTU", "BEM", "NYA", "HR", "CS", "DA", "DUA", "DHV", "NL", "EFI", "EN", "ET", "EE", "FJ", "FI", "FR", "GAA", "CAB", "KA", "DE", "EL", "KL", "GUG", "GU", "AS", "BTG", "GUW", "GXX", "HT", "HE", "HZ", "HIL", "HI", "HO", "HMN", "HU", "IS", "IG", "ILO", "ID", "IT", "JA", "KN", "KK-CYRL", "KEK", "KAM", "KQN", "KWY", "KI", "KMB", "RW", "KY", "GIL", "RUN", "SOP", "KO", "KRI", "KMR", "KWN", "KJ", "LAM", "LO", "LV", "KOO", "LN", "LT", "PDT", "LG", "LUN", "LUO", "LUE", "MK", "MG", "ZLM", "ML", "MT", "MAM", "ARN", "MR", "YUA", "MAU", "MIQ", "MCO", "KHK", "MOS", "MY", "NCX", "NGU", "NCH", "NCJ", "NK", "NR", "NG", "NE", "NO", "NYK", "NZI", "OM", "OS", "PAG", "JW-PAA", "PAP", "FA", "WES", "PL", "PON", "PT", "PA", "QUE", "QUY", "QU", "QUZ", "QUC", "QUG", "QVI", "RO", "NYN", "RU", "SM", "SG", "NSO", "SR-CYRL", "ST", "TN", "SN", "SID", "LOZ", "SI", "SK", "SL", "PIS", "SO", "ES", "SRN", "SW", "SWC", "SS", "SV", "TL", "TY", "TG", "TA", "TDX", "XMV", "TT", "TE", "TDT", "TH", "TI", "TPI", "TO", "TOP", "TS", "TR", "TK-CYRL", "TVL", "TW", "TZH", "TZO", "UK", "UMB", "UR", "UZ-CYRL", "VE", "JW-VZ", "VI", "WLS", "WAR", "GUC", "CY", "WLV", "WAL", "XH", "YO", "ZAI", "ZPA", "ZU"]

""" 
ALZ     (2013-2016)
AS      (2000-2016) 
DJK     (2011-2016)
AZ-CYRL (2011-2016)

"""
new_lang  = ["ALZ","AS","DJK","AZ-CYRL"]

pageIds = [2015000,         2015002,2015003,2015004,2015005,          2015007,2015008,\
           2015080,         2015082,2015083,2015084,2015085,2015086,\
           #2015160,         2015162,2015163,2015164,2015165,2015166,2015167,                             2015172,\
           #2015240,         2015242,2015243,2015244,2015245,2015246,2015247,\
           #2015320,         2015322,2015323,2015324,2015325,2015326,2015327,2015328,\
           #2015400,         2015402,2015403,2015404,2015405,2015406,2015407,\
           #2015480,         2015482,2015483,2015484,2015485,2015486,2015487,2015488,\
           #2015560,         2015562,2015563,2015564,2015565,2015566,2015567,\
           #                 2015642,        2015644,2015645,2015646,2015647,2015648,2015649,2015650,2015651,\
           #                 2015722,        2015724,2015725,2015726,2015727,        2015729,2015730,                       2015734,\
           #                 2015802,        2015804,2015805,2015806,2015807,2015808,2015809,\
           #                 2015882,        2015884,2015885,2015886,2015887,        2015889,2015890,\
           #         2015041,2015042,2015043,        2015045,2015046,2015047,\
                            2015122,2015123,2015124,2015125,2015126,2015127,2015128,\
                    2015201,2015202,2015203,2015204,        2015206,2015207,2015208,\
                    2015281,2015282,2015283,        2015285,2015286,2015287,2015288,\
                    2015361,2015362,2015363,                2015366,2015367,2015368,\
                    2015441,2015442,        2015444,2015445,2015446,2015447,2015448,\
                    2015521,2015522,2015523,        2015525,2015526,2015527,2015528,\
                    2015601,2015602,2015603,2015604,2015605,2015606,2015607,\
                    2015681,2015682,2015683,2015684,2015685,2015686,\
                    2015761,2015762,2015763,        2015765,2015766,2015767,2015768,                 2015771,\
                    2015841,2015842,2015843,        2015845,2015846,2015847,2015848,        2015850,\
                    2015921,2015922,2015923,2015924,2015925,2015926]
for pid in pageIds:

    pageId = str(pid)
    print pageId

    if pageId.startswith('20'):
        call(['mkdir','-p',pageId[:4]+'/'+pageId])
        folder = pageId[:4]+'/'+pageId +'/'
    else:
        # 402016522
        year =  pageId[2:6]
        print year
        call(['mkdir','-p',year+'/'+pageId])
        folder = year+'/'+pageId+'/'




    p = MyHTMLParser()


    for language in languages:
        time.sleep(.05) # put a pause in because  system thinks its a DoS attack
        


        web_page = "http://m.wol.jw.org/" + language + "/wol/d/r37/lp-" + language + "/" + pageId

        try:
            r = requests.get(web_page)
            r.raise_for_status()
        except HTTPError:
            print "Could not load the page", language
        else:
            print "Loaded the page for ", language
            f = urllib2.urlopen(web_page)
            print "Scraping the web.."
            html = f.read()
            p.feed(html)
            p.close()

            file_name = language + pageId + ".txt"
            print "Creating the file..", file_name
            file = open(folder+file_name, "w")
            for word in p.data:
                file.write(re.sub(r'(?<=['+punctuation+'])\s+(?=[A-Z])', '\n', word))
            p.data = []
            p.recording = 0
            p.close()