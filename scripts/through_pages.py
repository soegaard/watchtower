import urllib2
import requests
from requests.exceptions import HTTPError
from HTMLParser import HTMLParser
import re
from string import punctuation
from subprocess import call
import time
import argparse

class MyHTMLParser(HTMLParser):

  def __init__(self):
    HTMLParser.__init__(self)
    self.recording = 0
    self.data = []
  def handle_starttag(self, tag, attrs):
    if tag == 'p':
      for name, value in attrs:
        if name == 'class' and value == 'sb':
            self.recording = 1


  def handle_endtag(self, tag):
    if tag == 'required_tag':
      self.recording -=1


  def handle_data(self, data):
    if self.recording:
        self.data.append(data)


languages = ["ACH", "AF", "SQ", "AM", "AR", "HY", "ATI", "AY", "AZ-LATN", "BCI", "BAS", "BN", "BCL", "BI", "BS", "BUM", "BG", "CAK", "KM", "CAT", "CEB", "NY", "CMN-HANS", "CMN-HANT", "TOI", "TOG", "TUM", "YAO", "CTU", "BEM", "NYA", "HR", "CS", "DA", "DUA", "DHV", "NL", "EFI", "EN", "ET", "EE", "FJ", "FI", "FR", "GAA", "CAB", "KA", "DE", "EL", "KL", "GUG", "GU", "AS", "BTG", "GUW", "GXX", "HT", "HE", "HZ", "HIL", "HI", "HO", "HMN", "HU", "IS", "IG", "ILO", "ID", "IT", "JA", "KN", "KK-CYRL", "KEK", "KAM", "KQN", "KWY", "KI", "KMB", "RW", "KY", "GIL", "RUN", "SOP", "KO", "KRI", "KMR", "KWN", "KJ", "LAM", "LO", "LV", "KOO", "LN", "LT", "PDT", "LG", "LUN", "LUO", "LUE", "MK", "MG", "ZLM", "ML", "MT", "MAM", "ARN", "MR", "YUA", "MAU", "MIQ", "MCO", "KHK", "MOS", "MY", "NCX", "NGU", "NCH", "NCJ", "NK", "NR", "NG", "NE", "NO", "NYK", "NZI", "OM", "OS", "PAG", "JW-PAA", "PAP", "FA", "WES", "PL", "PON", "PT", "PA", "QUE", "QUY", "QU", "QUZ", "QUC", "QUG", "QVI", "RO", "NYN", "RU", "SM", "SG", "NSO", "SR-CYRL", "ST", "TN", "SN", "SID", "LOZ", "SI", "SK", "SL", "PIS", "SO", "ES", "SRN", "SW", "SWC", "SS", "SV", "TL", "TY", "TG", "TA", "TDX", "XMV", "TT", "TE", "TDT", "TH", "TI", "TPI", "TO", "TOP", "TS", "TR", "TK-CYRL", "TVL", "TW", "TZH", "TZO", "UK", "UMB", "UR", "UZ-CYRL", "VE", "JW-VZ", "VI", "WLS", "WAR", "GUC", "CY", "WLV", "WAL", "XH", "YO", "ZAI", "ZPA", "ZU"]

""" 
ALZ     (2013-2016)
AS      (2000-2016) 
DJK     (2011-2016)
AZ-CYRL (2011-2016)

"""
new_lang  = ["ALZ","AS","DJK","AZ-CYRL"]

pageIds = [2001000,2001001,2001002,2001003,2001004,2001005,2001006]
           #2001040,2001041,2001042,                       2001046,            2001048,2001049,\
           #2001050,2001051,2001052,2001053,\
           #2001080,2001081,2001082,2001083,2001084,2001085,2001086,2001087,
           #2001088,\
           #2001120,2001121,2001122,2001123,2001124,2001125,        2001127,2001128,\
           #2001160,2001161,2001162,2001163,2001164,2001165,2001166,2001167,\
           #2001200,2001201,2001202,2001203,2001204,2001205,2001206,2001207,        2001209,\
           #2001240,2001241,2001242,2001243,2001244,2001245,2001246,2001247,\
           #2001280,2001281,2001282,2001283,2001284,2001285,2001286,2001287,2001288,\
           #2001320,2001321,2001322,2001323,2001324,2001325,2001326,        2001328,2001329,2001330,\
           #2001360,2001361,2001362,2001363,2001364,2001365,2001366,2001367,2001368,\
           #2001400,2001401,2001402,2001403,2001404,2001405,2001406,2001407,2001408,\
           #2001440,2001441,2001442,2001443,2001444,2001445,2001446,        2001448,\
           #2001480,2001481,2001482,2001483,2001484,2001485,2001486,                2001489,2001490,\
           #2001520,2001521,2001522,2001523,2001524,2001525,2001526,                2001529,2001530,\
           #2001560,2001561,2001562,2001563,2001564,2001565,2001566,2001567,        2001569,\
           #2001600,2001601,2001602,2001603,2001604,2001605,2001606,        2001608,2001609,\
           #2001640,2001641,2001642,2001643,2001644,2001645,2001646,2001647,        2001649,\
           #2001680,2001681,2001682,2001683,2001684,2001685,2001686,        2001688,2001689,\
           #2001720,2001721,2001722,2001723,2001724,2001725,2001726,        2001728,\
           #2001760,2001761,2001762,2001763,2001764,2001765,2001766,        2001768,\
           #2001800,2001801,2001802,2001803,2001804,2001805,2001806,        2001808,\
           #2001840,2001841,2001842,2001843,2001844,2001845,2001846,        2001848,2001849,\
           #2001880,2001881,2001882,2001883,2001884,2001885,2001886,        2001888,\
           #2001920,2001921,2001922,2001923,2001924,2001925,                2001928,2001929]


for pid in pageIds:

    pageId = str(pid)
    print pageId

    if pageId.startswith('20'):
        call(['mkdir','-p',pageId[:4]+'/'+pageId])
        folder = pageId[:4]+'/'+pageId +'/'
    else:
        # 402016522
        year =  pageId[2:6]
        print year
        call(['mkdir','-p',year+'/'+pageId])
        folder = year+'/'+pageId+'/'




    p = MyHTMLParser()


    for language in languages:
        #time.sleep(.05) # put a pause in because  system thinks its a DoS attack
        


        web_page = "http://m.wol.jw.org/" + language + "/wol/d/r37/lp-" + language + "/" + pageId

        try:
            r = requests.get(web_page)
            r.raise_for_status()
        except HTTPError:
            print "Could not load the page", language
        else:
            print "Loaded the page for ", language
            f = urllib2.urlopen(web_page)
            print "Scraping the web.."
            html = f.read()
            p.feed(html)
            p.close()

            file_name = language + pageId + ".txt"
            print "Creating the file..", file_name
            file = open(folder+file_name, "w")
            for word in p.data:
                file.write(re.sub(r'(?<=['+punctuation+'])\s+(?=[A-Z])', '\n', word))
            p.data = []
            p.recording = 0
            p.close()