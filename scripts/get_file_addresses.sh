#!/bin/bash/

lid=(ACH AF SQ AM AR HY ATI AY AZ-LATN BCI BAS BN BCL BI BS BUM BG CAK KM CAT CEB NY CMN-HANS CMN-HANT TOI TOG TUM YAO CTU BEM NYA HR CS DA DUA DHV NL EFI EN ET EE FJ FI FR GAA CAB KA DE EL KL GUG GU AS BTG GUW GXX HT HE HZ HIL HI HO HMN HU IS IG ILO ID IT JA KN KK-CYRL KEK KAM KQN KWY KI KMB RW KY GIL RUN SOP KO KRI KMR KWN KJ LAM LO LV KOO LN LT PDT LG LUN LUO LUE MK MG ZLM ML MT MAM ARN MR YUA MAU MIQ MCO KHK MOS MY NCX NGU NCH NCJ NK NR NG NE NO NYK NZI OM OS PAG JW-PAA PAP FA WES PL PON PT PA QUE QUY QU QUZ QUC QUG QVI RO NYN RU SM SG NSO SR-CYRL ST TN SN SID LOZ SI SK SL PIS SO ES SRN SW SWC SS SV TL TY TG TA TDX XMV TT TE TDT TH TI TPI TO TOP TS TR TK-CYRL TVL TW TZH TZO UK UMB UR UZ-CYRL VE JW-VZ VI WLS WAR GUC CY WLV WAL XH YO ZAI ZPA ZU)

tLen=${#lid[@]}


mkdir -p ~/watchtower/Language_Locations/


for (( ind1=0; ind1<${tLen}; ind1++ ));
do

echo ${lid[ind1]}
find ~/watchtower/ -name *${lid[ind1]}'2'* -print > ~/watchtower/Language_Locations/${lid[ind1]}.txt
done