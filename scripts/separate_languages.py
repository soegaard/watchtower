import os
from subprocess import call

lang_loc_dir = '/home/sarah/watchtower/Language_Locations/'
location_files = os.listdir('/home/sarah/watchtower/Language_Locations/')
where_loc = "/home/sarah/watchtower/Monolingual/"
for loc_f in location_files:
	lang = loc_f.split('.txt')[0]
	print lang
	call(['mkdir','-p', where_loc+lang])
	with open(lang_loc_dir+loc_f) as lf:
		for line in lf:
			orig_address  = line.strip()
			new_address   = where_loc+lang+'/'+orig_address.split('/')[-1]
			
			#print 'cp ' + orig_address +' ' +new_address
			call(['cp',orig_address,new_address])

