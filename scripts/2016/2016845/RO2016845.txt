„VESTITORI ai Regatului din Marea Britanie, treziți-vă!” Acest titlu, apărut în numărul din decembrie 1937 al publicației Informatorul* (ediția londoneză), a răsunat ca o trompetă ce anunța deșteptarea.
Subtitlul „Nicio creștere remarcabilă în ultimii zece ani” le dădea de gândit cititorilor.
Pe prima pagină, un raport de serviciu din perioada 1928-1937 vorbea de la sine.



PREA MULȚI PIONIERI?

De ce nu progresa lucrarea de predicare în Marea Britanie?
După cât se pare, congregațiile își pierduseră zelul față de lucrare, predicând în ritmul adoptat cu mulți ani mai înainte. În plus, filiala stabilise că, pentru a se parcurge teritoriul, erau suficienți 200 de pionieri, iar aceștia lucrau în teritorii izolate, nu împreună cu congregațiile.
Astfel, filiala anunțase că în Marea Britanie nu mai era teritoriu pentru alți pionieri și îi îndemnase pe cei care doreau să înceapă pionieratul să meargă să slujească în alte țări europene.
Un val de pionieri zeloși din Marea Britanie s-au dus în alte țări, cum ar fi Franța, deși știau puțin limba locală sau nu o știau deloc.



UN „APEL MOBILIZATOR”

În 1937, articolul din Informatorul propunea un obiectiv îndrăzneț pentru anul 1938: un milion de ore în lucrarea de predicare!
Obiectivul putea fi atins cu ușurință dacă vestitorii petreceau în lucrare 15 ore pe lună, iar pionierii, 110 ore.
Au fost oferite mai multe sugestii, printre care organizarea de grupuri de predicare de cinci ore pe zi și efectuarea de vizite ulterioare, îndeosebi în serile din cursul săptămânii.





Pionieri entuziaști participând cu zel la lucrarea de predicare




Acest imbold dat lucrării de predicare i-a reînsuflețit pe mulți vestitori.
Hilda Padgett* își amintește: „Cu toții așteptam de mult acest apel mobilizator de la sediul mondial, iar rezultatele n-au întârziat să apară”.
Sora E. Wallis a spus: „Sugestia de a petrece în predicare cinci ore pe zi a fost extraordinară.
Nu este bucurie mai mare decât să petreci o zi întreagă în lucrarea Domnului! . . .
Uneori ne întorceam obosiți, dar întotdeauna fericiți”.
Tânărul Stephen Miller a înțeles că trebuia să treacă imediat la acțiune și a răspuns apelului. Întrucât situația îi permitea, a vrut să profite de lucrul acesta.
El își amintește cum grupuri de frați și de surori pe biciclete petreceau în lucrare zile întregi, iar în serile de vară puneau cuvântări înregistrate.
Plini de zel, ei luau parte la marșuri de informare cu pancarte și ofereau reviste pe stradă.
Informatorul a lansat încă un apel mobilizator: „Avem nevoie de o armată de 1 000 de pionieri”.
Potrivit noilor îndrumări, pionierii trebuiau să lucreze cu congregațiile, susținându-le și întărindu-le. „Mulți frați au înțeles atunci că trebuiau să înceapă pionieratul”, își amintește Joyce Ellis (născută Barber).
Ea spune: „Deși aveam doar 13 ani, voiam să fac pionierat”. În iulie 1940, la vârsta de 15 ani, ea și-a atins obiectivul. Și Peter, care mai târziu a devenit soțul lui Joyce, a reacționat la acest apel de „trezire” și „a început să se gândească la pionierat”. În iunie 1940, la vârsta de 17 ani, el a mers cu bicicleta 105 kilometri până la Scarborough, unde fusese repartizat ca pionier.
Cyril și Kitty Johnson s-au numărat printre noii pionieri care au dat dovadă de spirit de sacrificiu.
Ei și-au vândut casa și alte bunuri pentru a se putea întreține în serviciul cu timp integral.
Cyril a renunțat la locul de muncă, iar, după o lună, el și soția lui erau pregătiți să înceapă pionieratul.
El spune: „Eram siguri că luaserăm o decizie bună.
Am făcut totul de bunăvoie și cu bucurie”.



SUNT ÎNFIINȚATE CASE DE PIONIERI

Pe măsură ce numărul pionierilor creștea vertiginos, frații cu răspundere au luat măsuri practice pentru a susține această armată de pionieri.
Conform îndrumărilor primite,
Jim Carr, care în 1938 era serv de zonă (în prezent se numește supraveghetor de circumscripție), a urmat sugestia de a înființa case de pionieri în mai multe orașe.
Grupuri de pionieri erau îndemnați să locuiască și să lucreze teritoriul împreună pentru a reduce cheltuielile. În Sheffield, pionierii au închiriat o casă mare, de care s-a îngrijit un frate cu răspundere.
Congregația locală a donat bani și mobilă.
Jim își amintește: „Toți au contribuit la bunul mers al lucrării”.
Aici au locuit zece pionieri harnici, care aveau un program spiritual riguros. „În fiecare dimineață, la micul dejun, era analizat textul zilei”, iar „pionierii mergeau zilnic în teritoriu în diverse părți ale orașului”.





Un val de noi pionieri au început să lucreze teritoriul din Marea Britanie




Nu numai pionierii, ci și vestitorii au răspuns acelui apel de „trezire”, astfel că, în 1938, obiectivul de un milion de ore a fost atins.
De fapt, potrivit rapoartelor, s-au înregistrat creșteri în toate aspectele lucrării de predicare. În cinci ani, numărul vestitorilor din Marea Britanie a crescut de aproape trei ori.
Noul imbold dat lucrării i-a pregătit pe membrii poporului lui Iehova pentru anii de război care se profilau la orizont.
Pe măsură ce războiul Armaghedonului se apropie, numărul pionierilor din Marea Britanie continuă să crească. În ultimii zece ani s-au înregistrat mai multe recorduri în ce privește numărul pionierilor. În octombrie 2015, numărul lor a fost de 13 224.
Acești pionieri sunt convinși că serviciul cu timp integral este cel mai bun mod de viață.



Intitulat ulterior Serviciul pentru Regat.
Turnul de veghe din 1 octombrie 1995, paginile 19-24, conține relatarea autobiografică a surorii Hilda Padgett.




    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Publicații în limba română (2000-2017)
			
		

		
			
				
				Preferințe
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Termeni de utilizare
			
			
				Politică de confidențialitate
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



