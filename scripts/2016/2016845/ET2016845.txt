SEE kõlas justkui trompetihüüd: „Suurbritannia kuningriigikuulutajad, ärgake!” (Teataja*, detsember 1937,
Londoni väljaanne).
Sellele järgnes kainestav alapealkiri „Kümme aastat pole olnud märkimisväärset kasvu”.
Seda mõtet kinnitas esilehel olev teenistusaruanne, mis andis ülevaate aastatest 1928–37.



KAS TÕESTI LIIGA PALJU PIONEERE?

Mis oli selle põhjuseks, et kuulutustöö hoog Suurbritannias oli raugenud?
Koguduste tegevus oli ilmselt muutunud liiga rutiinseks, tempo oli sama nagu palju aastaid tagasi.
Lisaks oli harubüroo otsustanud, et sealsel territooriumil võiks tegutseda vaid umbes 200 pioneeri, kes ei kuuluta mitte koos kogudusega, vaid eraldatud paikades.
Seepärast oli harubüroo öelnud neile, kel oli plaanis alustada pioneeritööd, et Suurbritannias pole enam vajadust, ning oli ergutanud neid minema kuulutama teistesse Euroopa riikidesse.
On kiiduväärne, et suur hulk pioneere läkski appi teistesse maadesse, nagu näiteks Prantsusmaale, kuigi nende prantsuse keele oskus oli vähene või suisa olematu.



ÜLESKUTSE VÄLJUDA RUTIINIST

Eelpool mainitud Teataja seadis 1938. aastaks eesmärgi: miljon tundi!
See eeldas, et kuulutajad pühendavad põlluteenistusele iga kuu 15 tundi ja pioneerid 110 tundi.
Soovitati ka moodustada kuulutustöögrupid, kes töötaksid viis tundi päevas ja keskenduksid korduskülastuste tegemisele, eriti argipäevaõhtuti.





Innukad pioneerid kuulutustööl




See uus eesmärk tekitas paljudes vaimustust. „See oli meie peakorteri üleskutse, mida enamik meist oli juba oodanud ja mis tõi peagi suurepäraseid tulemusi,” meenutas Hilda Padgett.* Õde Wallis teatas: „Soovitus teha viietunniseid päevi oli tõesti tore!
Mis võiks tuua veel suuremat rõõmu, kui olla kogu päev Issanda teenistuses? ...
Kas võiks öelda, et olime päeva lõpus väsinud?
Võib-olla.
Aga rõõmsad?
Seda kindlasti!” Noor Stephen Miller mõistis samuti selle üleskutse tähtsust ja andis oma panuse.
Ta tahtis seda teha ja tal oli selleks ka võimalus.
Tal on meeles, kuidas nad tegid jalgratastel grupiga pikki päevi kuulutustööd ja mängisid suveõhtutel ette salvestatud kõnesid.
Samuti jagasid nad tõesõnumit innukalt tänavatel, marssides plakatitega ja pakkudes ajakirju.
Selles Teatajas oli ka üleskutse „Meil on tarvis tuhat pioneeri”.
Uue korra järgi ei pidanud pioneerid enam töötama kogudustest eraldi, vaid koos nendega, olles neile toeks ja kosutuseks. „Paljusid vendi otsekui äratas üles tõsiasi, et neid vajatakse pioneeritööl,” meenutas Joyce Ellis (neiupõlvenimega Barber).
Ta lisas: „Kuigi olin tol ajal vaid 13-aastane, oli pioneeritöö just see, mida ma teha tahtsin.” Ta saavutas oma eesmärgi ja hakkas pioneeriks juulis 1940, kui ta oli 15-aastane.
Ka Peterit, kellest hiljem sai Joyce’i abikaasa, ajendas see üleskutse mõtlema pioneeritööle. 1940. aasta juunis, kui ta oli 17-aastane, sõitis ta jalgrattaga sadakond kilomeetrit Scarborough’ linna, et hakata täitma oma ülesannet pioneerteenistuses.
Nende seas, kes andsid ennastohverdavuses head eeskuju, olid ka Cyril ja Kitty Johnson.
Nad otsustasid müüa ära oma maja ja vara, et alustada täisajalist teenistust.
Cyril lahkus töölt ja kuu aja pärast olid nad valmis pioneeritööd tegema.
Ta meenutas: „Me olime oma otsuses täiesti kindlad.
Tegime seda kõike meeleldi ja rõõmuga.”



RAJATAKSE PIONEERIKODUD

Kuna pioneeride arv kasvas järsult, otsustasid vastutavad vennad neid praktilisel viisil aidata.
Jim Carr, kes teenis aastal 1938 vööndisulasena (praegune nimetus ringkonnaülevaataja), järgis soovitust rajada linnades pioneerikodud.
Pioneere ergutati elama ja töötama üheskoos, et kulusid kokku hoida.
Sheffieldis üüriti suur maja, mille elukorralduse eest hoolitses üks küps vend.
Kohalik kogudus annetas raha ja mööblit.
Jim meenutas: „Kõik pingutasid selle nimel, et pioneerikodu toimiks hästi.” Seal elas kümme tegusat pioneeri, kel oli hea vaimne tegevuskava.
Igal hommikul arutati söögilaua ääres päevateksti ja pioneerid olid iga päev kuulutamas oma territooriumil linna eri osades.





Suurbritannia kuulutustööpõllule lisandus suur hulk pioneere




Nii kuulutajad kui ka pioneerid pingutasid tublisti ja nii täidetigi 1938. aastaks seatud eesmärk teha kuulutustööd miljon tundi.
Aruanded näitasid, et tegelikult oli kasv igas teenistusvaldkonnas.
Viie aasta jooksul kuulutajate arv Suurbritannias peaaegu kolmekordistus.
Uued eesmärgid kuningriigitöös tugevdasid Jehoova teenijaid ja aitasid neil seista silmitsi eesootavate sõjaaja raskustega.
Praegu, kui on lähenemas Harmagedooni lahing, täienevad pioneeride read Suurbritannias taas.
Nende arv on viimase kümne aasta jooksul üha kasvanud. 2015. aasta oktoobris oli pioneere 13 224.
Need pioneerid on veendunud, et täisajaline teenistus on parim eluviis.



Hilisema nimega Meie Kuningriigiteenistus.
Hilda Padgetti eluloo võib leida ajakirjast Vahitorn, 1. oktoober 1995, lk 19–24.




    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Eestikeelsed väljaanded (2000-2017)
			
		

		
			
				
				Eelistused
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Kasutustingimused
			
			
				Privaatsus
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



