APOSTEL Paulus tunnistas ausalt: „Jumala armu tõttu olen ma see, kes ma olen.
Ja tema arm pole olnud asjatu.” (Loe 1. Korintlastele 15:9, 10.) Paulus teadis väga hästi, et ta ei vääri Jumala armu, sest ta oli varem olnud kristlaste tagakiusaja.
2 Elu lõpu poole kirjutas Paulus oma kaastöölisele Timoteosele: „Ma olen tänulik meie isandale Kristus Jeesusele, kes on andnud mulle jõudu, selle eest, et ta pidas mind usaldusväärseks ja andis mulle ülesande oma teenistuses.” (1. Tim. 1:12–14.)
Mis teenistus see oli?
See selgub tema sõnadest Efesose kogudusevanematele.
Ta ütles: „Siiski ei pea ma oma elu sugugi tähtsaks, vaid tahan lõpetada võidujooksu ja teenistuse, mille ma olen saanud meie isandalt Jeesuselt: anda põhjalikult tunnistust heast sõnumist Jumala armu kohta.” (Ap. t. 20:24.)
 3. Milline eriline ülesanne Paulusel oli? (Vaata pilti artikli alguses.)
3 Mis oli see hea sõnum, mida Paulus kuulutas, ja kuidas ilmnes selles Jumala arm?
Ta kirjutas Efesose kristlastele: „Küllap olete kuulnud, et teie aitamiseks on mulle antud Jumala armu majapidaja amet.” (Efesl. 3:1, 2.)
Paulusele oli antud ülesanne kuulutada mittejuutidele head sõnumit, et ka nemad võivad olla nende hulgas, keda kutsutakse Jumala taevasesse valitsusse, mida juhib Kristus. (Loe Efeslastele 3:5–8.) Oma innukusega teenistuses jättis Paulus tänapäeva kristlastele suurepärase eeskuju ja tõendas, et Jumala arm tema vastu pole olnud asjatu.



KAS JUMALA ARM AJENDAB KA SIND TEGUTSEMA?

4., 5. Miks võib öelda, et „hea sõnum kuningriigist” on ühtlasi „hea sõnum Jumala armu kohta”?
4 Praegusel lõpuajal on Jehoova rahval ülesanne kuulutada „head sõnumit kuningriigist ... kogu maailmas tunnistuseks kõigile rahvastele”. (Matt. 24:14.)
See sõnum on ühtlasi „hea sõnum Jumala armu kohta”, sest kõik õnnistused, mida Jumala kuningriik toob, saavad meile osaks tänu Jumala armule, mida ta väljendab Kristuse kaudu. (Efesl. 1:3.)
Kas sina näitad Pauluse eeskujul üles tänulikkust Jehoova armu eest, tehes innukalt kuulutustööd? (Loe Roomlastele 1:14–16.)
5 Eelmisest artiklist saime teada, kui palju õnnistusi meie, patused inimesed, tänu Jehoova armule oleme saanud.
Jumala armu tõttu oleme kõigi inimeste võlglased ja peaksime tegema oma parima, et anda neile teada, kuidas Jumal oma armastust väljendab ja kuidas nad võivad seda kogeda.
Aga mismoodi siis aidata teistel Jumala armu hindama hakata?



RÄÄGI HEAD SÕNUMIT LUNASTUSEST

6., 7. Miks võib öelda, et inimestele lunastusest rääkides räägime neile ühtlasi Jumala armust?
6 Praeguses kõikesallivas maailmas ei tunne paljud patustades mingit süütunnet ega näe ka vajadust lunastuse järele.
Samal ajal saab üha rohkem inimesi aru, et selline kõikelubavus ei too tõelist õnne.
Paljud ei mõista, enne kui Jehoova tunnistajad neile seda selgitavad, mis on patt, kuidas see neid mõjutab ja mida peaks tegema, et patu orjusest vabaneda.
Siirad inimesed on rõõmsad, kui saavad teada, et Jehoova saatis suurest armastusest ja lahkusest maa peale oma poja lunastusohvriks, et vabastada meid patust ja surmast. (1. Joh. 4:9, 10.)
7 Paulus kirjutas Jumala armsa poja kohta: „[Ta] on lunastanud meid oma verega, vabastanud meid, nii et me võime oma üleastumised andeks saada tänu Jumala rikkalikule armule.” (Efesl. 1:7.)
See, et Jehoova oli valmis oma poja meie eest ohvriks tooma, on suurim tõend tema armastusest meie vastu ja näitab, kui rikkalik on tema arm.
Kui usume Jeesuse ohvriveresse, siis saame oma patud andeks ja meil võib olla puhas südametunnistus. (Heebr. 9:14.)
Sõnum lunastusest on tõesti hea sõnum, mida teistele rääkida!



AITA TEISTEL JUMALAGA HÄID SUHTEID LUUA

 8. Miks vajavad patused inimesed Jumalaga lepitust?
8 Meil on kohustus rääkida kaasinimestele, et neil on võimalik Loojaga head suhted luua.
Seni, kuni inimene pole hakanud uskuma Jeesuse lunastusohvrisse, on ta Jumala vaenlane.
Apostel Johannes kirjutas: „Kes Pojasse usub, saab igavese elu, aga kes Pojale ei kuuletu, ei saa elu, vaid Jumala viha jääb tema peale.” (Joh. 3:36.) Õnneks teeb Kristuse lunastusohver võimalikuks lepituse Jumalaga.
Paulus ütles: „Teid, kes olite kunagi Jumalast kaugel ja tema vaenlased, sest teie mõtteis olid kurjad teod, on Jumal nüüd endaga lepitanud selle inimese keha kaudu, kes andis end surma.” (Kol. 1:21, 22.)
9., 10. a) Millise kohustuse on Kristus andnud oma võitud vendadele? b) Kuidas „teised lambad” võituid aitavad?
9 Kristus on andnud oma võitud vendadele maa peal lepitusteenistuse, nagu Paulus seda nimetab.
Paulus kirjutas selle kohta võitutele esimesel sajandil: „See kõik on Jumalalt, kes lepitas meid endaga Kristuse kaudu ja andis meile lepitusteenistuse.
Jumal lepitas Kristuse kaudu maailma endaga, panemata inimeste väärsamme neile süüks, ning usaldas meile lepitussõnumi.
Seega me oleme suursaadikud Kristuse asemel, otsekui paluks Jumal inimesi meie kaudu.
Me palume Kristuse asemel: „Laske end Jumalaga lepitada.”” (2. Kor. 5:18–20.)
10 „Teised lambad” peavad seda suureks auks, et saavad oma võitud vendi selles teenistuses aidata. (Joh. 10:16.)
Teenides Kristuse käskjalgadena, teevad nad suurema osa kuulutustööst, õpetades inimestele Piibli tõdesid ja aidates neil Jehoovaga lähedaseks saada.
See on väga tähtis osa meie tööst, mida me teeme, et anda põhjalikult tunnistust heast sõnumist Jumala armu kohta.



RÄÄGI INIMESTELE,
ET JUMAL KUULAB PALVEID

11., 12. Miks meil tuleks inimestele rääkida, et Jehoova kuulab palveid?
11 Paljud inimesed palvetavad sellepärast, et see annab neile parema enesetunde, kuid tegelikult nad ei usu, et Jumal nende palveid kuulab.
Me peame neile seda selgitama.
Laulik Taavet kirjutas Jehoova kohta: „Sina kuulad palvet, kõiksugused inimesed tulevad su juurde.
Mu süüteod on mind enda alla matnud, sina aga katad me üleastumised kinni.” (Laul 65:2, 3.)
12 Jeesus ütles oma jüngritele: „Kui te palute midagi minu nime kaudu, siis ma teen seda.” (Joh. 14:14.)
Muidugi peaks see „midagi” olema kooskõlas Jehoova tahtega.
Seda kinnitavad ka Johannese sõnad „Me oleme kindlad, et kui me palume Jumalalt midagi kooskõlas tema tahtega, siis ta kuulab meid”. (1. Joh. 5:14.)
Kas pole meeldiv inimestele selgitada, et palvetamine ei ole lihtsalt abinõu enesetunde parandamiseks, vaid see on võrratu võimalus minna Jehoova armutrooni ette? (Heebr. 4:16.)
Kui õpetame inimesi palvetama õigel viisil, õige isiku poole ja õigete asjade eest, siis saavad nad Jehoovaga lähedaseks ja võivad leida lohutust hädaajal. (Laul 4:1; 145:18.)



JUMALA ARM UUES MAAILMAS

13., 14. a) Milline au saab võitutele tulevikus osaks? b) Mida imelist võitud inimkonna heaks tulevikus teevad?
13 Jehoova väljendab oma armu ka pärast praeguse kurja maailma lõppu.
Rääkides suurest aust, mis saab osaks 144 000-le taevasesse valitsusse kutsutule, kirjutas Paulus: „Jumal, kes on väga halastav, on suurest armastusest meie vastu teinud meid, kes me olime väärsammude tõttu surnud, elavaks koos Kristusega.
Tänu armule on teid päästetud.
Jumal on äratanud meid üles koos Kristus Jeesusega ja pannud meid kui Kristuse jüngreid istuma taevastesse paikadesse ühes temaga, et näidata tulevastel ajastutel oma armu võrratut rikkust oma headusega meie vastu, kes me oleme Kristus Jeesuse jüngrid.” (Efesl. 2:4–7.)
14 Me ei suuda isegi ette kujutada, kui palju imelist on Jehooval varuks võitud kristlastele ajaks, kui nad istuvad taevastel troonidel ja valitsevad koos Kristusega. (Luuka 22:28–30; Filipl. 3:20, 21; 1. Joh. 3:2.)
Jehoova näitab võitute vastu tõesti eriliselt „oma armu võrratut rikkust”.
Neist saab „Uus Jeruusalemm”,
Kristuse pruut. (Ilm. 3:12; 17:14; 21:2, 9, 10.)
Nad osalevad koos Jeesusega rahvaste tervendamises, aidates Jumalale kuulekatel inimestel saada vabaks patust ja surmast ning jõuda täiuseni. (Loe Ilmutus 22:1, 2, 17.)
15., 16. Kuidas näitab Jehoova tulevikus oma armu „teiste lammaste” vastu?
15 Kirjakohast Efeslastele 2:7 lugesime, et Jumal näitab ka „tulevastel ajastutel oma armu võrratut rikkust”.
Pole mingit kahtlust, et uues maailmas näeme rohkelt tõendeid Jehoova armust. (Luuka 18:29, 30.) Üks imelisemaid asju, mis maa peal toimub, on surnute ülesäratamine. (Iiob 14:13–15; Joh. 5:28, 29.)
Ustavad mehed ja naised, kes surid enne Kristuse ohvrisurma, samuti need „teiste lammaste” hulgast, kes on surnud viimseil päevil, tuuakse ellu tagasi ja nad saavad Jehoovat edasi teenida.
16 Miljonid inimesed, kes surid, ilma et nad oleksid saanud Jumalat tundma õppida, äratatakse samuti üles.
Neile antakse võimalus näidata, kas nad soovivad alluda Jehoova ülemvõimule või mitte.
Johannes kirjutas: „Ma nägin ka surnuid, suuri ja väikseid, seismas trooni ees, ning rullraamatud avati.
Ja avati veel üks rullraamat, see on eluraamat.
Surnute üle mõisteti kohut nende tegude järgi, selle alusel, mis rullraamatutesse oli kirjutatud.
Meri andis tagasi surnud, kes temas olid, ning surm ja surmavald andsid tagasi surnud, kes neis olid, ning igaühe üle mõisteti kohut tema tegude järgi.” (Ilm. 20:12, 13.)
Muidugi ootab Jehoova neilt ülesäratatutelt, et nad õpiksid elama Piibli põhimõtete järgi.
Peale selle tuleb neil järgida seda, mis on kirjas neis rullraamatutes, mis me tulevikus saame ja mis sisaldavad Jehoova nõudeid eluks uues maailmas.
Kõik nendes rullraamatutes sisalduv peegeldab Jehoova armu.



KUULUTA INNUKALT HEAD SÕNUMIT

17. Mida meil tuleks kuulutustööd tehes meeles pidada?
17 Kuna lõpp on nii lähedal, pole meie ülesanne kuulutada head sõnumit Jumala kuningriigist olnud kunagi pakilisem kui praegu. (Mark. 13:10.)
Kahtlemata ilmneb sellest heast sõnumist Jehoova arm.
Seda tuleb meil kuulutustööd tehes meeles pidada.
Meie töö eesmärk on tuua au Jehoovale.
Me saame seda teha siis, kui selgitame inimestele, et kõik Jehoova tõotused uue maailma kohta on tema rikkaliku armu väljendused.





Teenigem innukalt kui „Jumala mitmeti väljenduva armu tublid majapidajad” (1. Peetr. 4:10) (vaata lõike 17–19)




18., 19. Kuidas me saame ülistada Jehoovat tema armu eest?
18 Kuulutustööd tehes saame inimestele selgitada, et Kristuse valitsuse all saab inimkond kätte kõik selle hea, mille lunastusohver võimalikuks teeb, ja jõuab aja jooksul täiuseni.
Piibel ütleb: „Loodu vabastatakse kaduvuse orjusest ja saab Jumala laste võrratu vabaduse.” (Rooml. 8:21.)
See kõik on võimalik vaid tänu Jehoova suurele armule.
19 Me soovime rääkida kõigile sellest imelisest tõotusest, mis on kirjas piiblitekstis Ilmutus 21:4, 5: „[Jumal] pühib ära kõik pisarad nende silmist ja enam ei ole surma ega leinamist, hädakisa ega valu.
Kõik endine on möödunud.” See, kes troonil istus, ütles: „Ma teen kõik uueks!” Samuti ütles ta: „Kirjuta, sest need sõnad on usaldusväärsed ja tõesed.”” Kui räägime innukalt seda head sõnumit, toob see Jehoovale tema armu eest suurt ülistust.






    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Eestikeelsed väljaanded (2000-2017)
			
		

		
			
				
				Eelistused
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Kasutustingimused
			
			
				Privaatsus
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



