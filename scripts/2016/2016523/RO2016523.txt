APOSTOLUL Pavel a putut să spună cu sinceritate: ‘Bunătatea nemeritată a lui Dumnezeu față de mine n-a fost zadarnică’. (Citește 1 Corinteni 15:9, 10.) Pavel știa bine că nu obținuse prin propriile eforturi marea îndurare a lui Dumnezeu și nici n-o meritase, întrucât fusese un persecutor al creștinilor.
2 Spre sfârșitul vieții sale,
Pavel i-a scris colaboratorului său Timotei: „Îi sunt recunoscător lui Cristos Isus,
Domnul nostru, care mi-a dat putere, pentru că m-a considerat fidel și m-a numit într-un serviciu” (1 Tim. 1:12-14). În ce a constat acel serviciu?
Pavel le-a spus bătrânilor congregației din Efes: „Eu nu pun preț pe sufletul meu, ca și cum mi-ar fi scump, numai să pot să-mi sfârșesc cursa și serviciul pe care l-am primit de la Domnul Isus: acela de a depune mărturie temeinică despre vestea bună referitoare la bunătatea nemeritată a lui Dumnezeu” (Fap. 20:24).
 3. Ce serviciu special a primit Pavel? (Vezi imaginea de la începutul articolului.)
3 Ce „veste bună” a predicat Pavel și cum a scos aceasta în evidență bunătatea nemeritată a lui Iehova?
Pavel le-a spus creștinilor din Efes: „Ați auzit de administrarea bunătății nemeritate a lui Dumnezeu, care mi-a fost dată pentru voi” (Ef. 3:1, 2).
Pavel primise misiunea de a le transmite oamenilor din națiuni vestea bună, pentru ca și ei să se poată număra printre cei chemați să domnească alături de Cristos în Regatul mesianic. (Citește Efeseni 3:5-8.) Pavel a predicat cu mult zel, lăsându-le un exemplu demn de urmat creștinilor din prezent.
El a dovedit că bunătatea nemeritată pe care i-o arătase Dumnezeu n-a fost „zadarnică”.



TE ÎNDEAMNĂ BUNĂTATEA NEMERITATĂ A LUI DUMNEZEU LA ACȚIUNE?

 4, 5. De ce putem spune că ‘vestea bună despre Regat’ este una și aceeași cu vestea bună referitoare la „bunătatea nemeritată a lui Dumnezeu”?
4 În acest timp al sfârșitului, slujitorii lui Iehova au misiunea de a predica ‘această veste bună despre Regat pe tot pământul locuit ca mărturie pentru toate națiunile’ (Mat. 24:14).
Vestea bună despre Regat este și „vestea bună referitoare la bunătatea nemeritată a lui Dumnezeu”.
De ce?
Deoarece toate binecuvântările pe care sperăm să le primim sub domnia Regatului vor fi rezultatul bunătății lui Iehova manifestate prin intermediul lui Cristos (Ef. 1:3).
Pavel și-a arătat recunoștința pentru bunătatea nemeritată a lui Iehova predicând cu zel.
Imităm și noi exemplul lui Pavel? (Citește Romani 1:14-16.)
5 În articolul precedent am văzut ce binecuvântări ne aduce bunătatea nemeritată a lui Iehova nouă, oamenilor păcătoși.
Prin urmare, avem responsabilitatea de a face tot posibilul să-i învățăm pe alții cum își arată Iehova iubirea și ce foloase le poate aduce aceasta.
Care sunt unele aspecte ale bunătății nemeritate a lui Dumnezeu pentru care îi putem ajuta pe alții să fie recunoscători?



SĂ RĂSPÂNDIM VESTEA BUNĂ DESPRE JERTFA DE RĂSCUMPĂRARE

 6, 7. Cum răspândim vestea bună despre bunătatea nemeritată a lui Dumnezeu când le vorbim oamenilor despre răscumpărare?
6 În această lume permisivă, mulți nu se mai simt vinovați când comit un păcat.
Prin urmare, ei nu înțeleg de ce oamenii au nevoie de răscumpărare. În același timp, din ce în ce mai mulți oameni își dau seama că un mod de viață permisiv nu aduce adevărata fericire.
Până nu vorbesc cu Martorii lui Iehova, mulți nu înțeleg ce este păcatul, cum ne afectează și ce trebuie să facem pentru a fi eliberați din sclavia păcatului.
Oamenii sinceri simt o mare ușurare când află că Iehova l-a trimis pe Fiul său pe pământ pentru a ne elibera de păcat și de consecința lui, moartea.
Iehova a făcut acest lucru datorită marii sale iubiri și a bunătății sale nemeritate (1 Ioan 4:9, 10).
7 Să observăm ce a spus Pavel despre Fiul iubit al lui Dumnezeu: ‘Prin el avem eliberarea prin răscumpărare, datorită sângelui său, da, iertarea greșelilor noastre, după bogăția bunătății nemeritate a lui Iehova’ (Ef. 1:7).
Jertfa de răscumpărare a lui Cristos este cea mai mare dovadă a iubirii lui Dumnezeu față de noi și a ‘bogăției’ bunătății sale nemeritate.
Câtă ușurare ne aduce faptul de a ști că, dacă manifestăm credință în jertfa lui Isus, păcatele noastre vor fi iertate și vom avea o conștiință curată! (Evr. 9:14)
Aceasta este, cu siguranță, o veste bună pe care merită să le-o împărtășim și altora!



SĂ-I AJUTĂM PE OAMENI SĂ DEVINĂ PRIETENI CU DUMNEZEU

 8. De ce trebuie oamenii păcătoși să se împace cu Dumnezeu?
8 Avem responsabilitatea de a le spune semenilor noștri că pot să devină prieteni cu Creatorul lor.
Dacă oamenii nu manifestă credință în jertfa lui Isus,
Dumnezeu îi consideră dușmani.
Apostolul Ioan a scris: „Cine manifestă credință în Fiul are viață veșnică.
Cine nu ascultă de Fiul nu va vedea viața, ci mânia lui Dumnezeu rămâne peste el” (Ioan 3:36).
Din fericire, jertfa lui Cristos face posibilă împăcarea cu Dumnezeu. În acest sens,
Pavel a spus: „Pe voi, care altădată erați înstrăinați și dușmani, pentru că mintea vă era îndreptată spre lucrări rele, acum el v-a împăcat din nou prin corpul de carne al aceluia, prin moartea lui” (Col. 1:21, 22).
 9, 10. a) Ce responsabilitate le-a încredințat Cristos fraților săi unși? b) Cum îi ajută cei dintre „alte oi” pe creștinii unși?
9 Cristos le-a încredințat fraților săi unși de pe pământ „misiunea împăcării”.
Pavel le-a scris creștinilor unși din secolul I: „Toate lucrurile vin de la Dumnezeu, care ne-a împăcat cu el prin Cristos și ne-a dat misiunea împăcării, și anume serviciul prin care anunțăm că Dumnezeu a împăcat lumea cu el prin Cristos, neluând în considerare greșelile oamenilor, și ne-a încredințat nouă cuvântul împăcării.
Noi suntem deci ambasadori în locul lui Cristos și, ca și cum Dumnezeu ar implora prin noi, vă rugăm, în locul lui Cristos: «Împăcați-vă cu Dumnezeu!»” (2 Cor. 5:18-20).
10 Cei dintre „alte oi” consideră că este un privilegiu să-i ajute pe creștinii unși în această lucrare (Ioan 10:16).
Ca mesageri ai lui Cristos, ei efectuează cea mai mare parte a lucrării de predicare, învățându-i pe oameni adevărul și ajutându-i să cultive o relație apropiată cu Iehova.
Aceasta este o parte importantă a lucrării prin care se depune o mărturie temeinică despre vestea bună referitoare la bunătatea nemeritată a lui Dumnezeu.



SĂ RĂSPÂNDIM VESTEA BUNĂ CĂ DUMNEZEU ASCULTĂ RUGĂCIUNILE

11, 12. De ce este o veste bună pentru oameni să afle că pot să i se roage lui Iehova?
11 Mulți oameni se roagă deoarece acest lucru îi face să se simtă bine, însă ei nu cred cu adevărat că Dumnezeu le ascultă rugăciunile.
Acești oameni trebuie să afle că Iehova este ‘Ascultătorul rugăciunii’.
Psalmistul David a scris: „O,
Ascultător al rugăciunii, la tine va veni orice carne.
Nelegiuirile mele m-au copleșit.
Dar tu vei acoperi fărădelegile noastre” (Ps. 65:2, 3).
12 Isus le-a spus discipolilor săi: „Dacă veți cere ceva în numele meu, voi face” (Ioan 14:14).
Fără îndoială, „ceva” înseamnă orice lucru care este în armonie cu voința lui Iehova.
Ioan ne dă asigurarea: „Aceasta este încrederea pe care o avem în El: că, indiferent ce am cere după voința sa,
El ne ascultă” (1 Ioan 5:14).
Este foarte important să-i învățăm pe oameni că rugăciunea nu este doar un ajutor pe plan psihologic.
Ea este o modalitate minunată de a se apropia de „tronul bunătății nemeritate” al lui Iehova (Evr. 4:16). Învățându-i să se roage cum trebuie, la Cine trebuie și pentru ce trebuie, îi putem ajuta să se apropie de Iehova și să găsească mângâiere când se confruntă cu necazuri (Ps. 4:1; 145:18).



BUNĂTATEA NEMERITATĂ ÎN NOUL SISTEM

13, 14. a) Ce privilegii deosebite vor avea cei unși în viitor? b) Ce lucrare minunată vor face cei unși pentru omenire?
13 Iehova își va manifesta bunătatea nemeritată și după sfârșitul actualului sistem rău.
Cum anume?
El le va acorda celor 144 000, care vor domni alături de Cristos în Regatul său ceresc, un privilegiu extraordinar.
Pavel a scris: „Dumnezeu, care este bogat în îndurare, în marea sa iubire cu care ne-a iubit ne-a dat viață cu Cristos chiar când eram morți în greșeli – prin bunătate nemeritată ați fost salvați – și ne-a ridicat împreună și ne-a așezat împreună în locurile cerești, în unitate cu Cristos Isus, pentru ca în sistemele viitoare să fie arătată neasemuita bogăție a bunătății sale nemeritate, în bunăvoința sa față de noi, în unitate cu Cristos Isus” (Ef. 2:4-7).
14 Este greu să ne imaginăm lucrurile minunate pe care Iehova le-a pregătit pentru creștinii unși când vor domni cu Cristos în cer (Luca 22:28-30; Filip. 3:20, 21; 1 Ioan 3:2).
Iehova își va arăta într-un mod cu totul special „neasemuita bogăție a bunătății sale nemeritate” față de cei unși.
Ei vor fi „Noul Ierusalim”, mireasa lui Cristos (Rev. 3:12; 17:14; 21:2, 9, 10).
Ei vor participa împreună cu Isus la „vindecarea națiunilor”, arătându-le oamenilor ascultători ce trebuie să facă pentru a fi eliberați de povara păcatului și a morții și ajutându-i să ajungă la perfecțiune. (Citește Revelația 22:1, 2, 17.)
15, 16. Cum își va arăta Iehova bunătatea nemeritată față de „alte oi” în viitor?
15 În Efeseni 2:7 se vorbește despre bunătatea nemeritată pe care Dumnezeu o va arăta în „sistemele viitoare”.
Atunci, toți oamenii de pe pământ vor avea parte de „neasemuita bogăție a bunătății sale nemeritate” (Luca 18:29, 30).
Una dintre cele mai mari dovezi ale bunătății nemeritate a lui Iehova față de omenire va fi învierea morților (Iov 14:13-15; Ioan 5:28, 29).
Oamenii fideli din vechime care au murit înainte ca Isus să-și jertfească viața, precum și cei dintre „alte oi” care mor fideli pe parcursul zilelor din urmă vor fi readuși la viață pentru a continua să-i slujească lui Iehova.
16 De asemenea, vor fi înviați milioane de oameni care au murit fără să-l cunoască pe Dumnezeu.
Ei vor avea ocazia de a se supune suveranității lui Iehova.
Ioan a scris: „Am văzut morții, mari și mici, stând în picioare înaintea tronului și au fost deschise niște suluri.
Dar a fost deschis și un alt sul: sulul vieții. Și morții au fost judecați după cele scrise în suluri, după faptele lor.
Marea i-a dat înapoi pe morții din ea, moartea și Hadesul i-au dat înapoi pe morții din ele. Și au fost judecați fiecare după faptele lui” (Rev. 20:12, 13).
Desigur, cei înviați vor trebui să învețe cum să aplice principiile biblice. În plus, ei vor trebui să respecte noile instrucțiuni din „suluri”, care vor conține cerințele lui Iehova pentru cei care vor trăi în noul sistem.
Dezvăluirea acelor instrucțiuni va fi o altă modalitate prin care Iehova își va arăta bunătatea nemeritată.



SĂ CONTINUĂM SĂ RĂSPÂNDIM VESTEA BUNĂ

17. Ce nu trebuie să uităm când participăm la lucrarea de predicare?
17 Având în vedere că sfârșitul se apropie, este mai important ca oricând să predicăm vestea bună despre Regat! (Mar. 13:10)
Fără îndoială, vestea bună scoate în evidență bunătatea nemeritată a lui Iehova.
Să nu uităm acest lucru când participăm la lucrarea de predicare!
Când predicăm, obiectivul nostru este să-l onorăm pe Iehova. În acest sens, trebuie să le arătăm oamenilor că toate binecuvântările de care vom avea parte în lumea nouă sunt dovezi ale bunătății extraordinare a lui Iehova.





Să slujim cu zel ca „buni administratori ai bunătății nemeritate a lui Dumnezeu” (1 Pet. 4:10) (Vezi paragrafele 17-19)




18, 19. Cum putem glorifica bunătatea nemeritată a lui Iehova?
18 Când depunem mărturie, le putem explica semenilor că, sub conducerea Regatului lui Cristos, oamenii se vor bucura de toate foloasele jertfei de răscumpărare și, treptat, vor deveni perfecți.
Biblia spune: „Creația va fi eliberată din sclavia stricăciunii și va avea glorioasa libertate a copiilor lui Dumnezeu” (Rom. 8:21).
Acest lucru va fi posibil numai datorită bunătății extraordinare a lui Iehova.
19 Avem privilegiul de a le vorbi tuturor oamenilor care vor să asculte despre promisiunea emoționantă din Revelația 21:4, 5: „[Dumnezeu] va șterge orice lacrimă din ochii lor și moartea nu va mai fi.
Nici jale, nici strigăt, nici durere nu vor mai fi.
Lucrurile de odinioară au trecut”.
Iar Iehova, cel care stă pe tron, spune: „Iată!
Eu fac toate lucrurile noi!”. Și mai spune: „Scrie, pentru că aceste cuvinte sunt demne de încredere și adevărate!”.
Când predicăm cu zel această veste bună, noi glorificăm cu adevărat bunătatea nemeritată a lui Iehova!






    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Publicații în limba română (2000-2017)
			
		

		
			
				
				Preferințe
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Termeni de utilizare
			
			
				Politică de confidențialitate
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



