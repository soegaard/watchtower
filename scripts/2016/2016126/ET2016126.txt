JOONATAN võis olla päris hämmastunud, kui vapralt noor Taavet hiigelkasvu Koljati vastu välja astus.
Nüüd seisis Taavet Iisraeli kuninga Sauli ees, käes selle vilisti pea. (1. Saam. 17:57.)
Sauli poeg Joonatan ilmselt imetles Taavetit tema julguse pärast.
Oli selge, et Jumal oli Taavetiga, ning sellest ajast alates „said Joonatan ja Taavet headeks sõpradeks” ning „Joonatan sõlmis Taavetiga lepingu, sest ta armastas teda nagu omaenese hinge”. (1. Saam. 18:1–3.)
Kogu ülejäänud elu oli Joonatan Taavetile truu sõber.
2 Joonatan jäi Taavetile truuks vaatamata sellele, et Jumal oli valinud Iisraeli järgmiseks kuningaks Taaveti, mitte tema.
Kui siis Saulil tekkis soov Taavet ära tappa, hakkas Joonatan Taaveti pärast muret tundma.
Et oma sõpra julgustada, läks Joonatan Juuda kõrbe Horese lähedale.
Joonatan aitas Taavetil „Jehoovalt jõudu ammutada” ning ütles: „Ära karda ...
Sina saad Iisraeli kuningaks ning mina olen tähtsuselt sinust järgmine.” (1. Saam. 23:16, 17.)
 3. Mis oli Joonatanile tähtsam kui ustavus Taavetile ja kust me seda teame? (Vaata pilti artikli alguses.)
3 Ustavust väärtustavad üldiselt kõik inimesed.
Ent meil jääks kõige tähtsam kahe silma vahele, kui me imetleks vaid Joonatani truudust Taavetile ega võtaks arvesse tema ustavust Jumalale.
Miks pidas Joonatan Taavetit oma sõbraks, mitte rivaaliks?
Ilmselt oli Joonatanile miski muu palju tähtsam kui tema enda positsioon.
Tuleta meelde, et Joonatan aitas Taavetil „Jehoovalt jõudu ammutada”.
Seega on selge, et Joonatani südames oli esikohal ustavus Jumalale.
Tõepoolest, just tänu sellele, et Joonatan oli ustav Jehoovale, oli ta ustav ka Taavetile.
Mõlemad mehed elasid kooskõlas vastastikku antud vandega: „Jehoova olgu igavesti tunnistajaks sinu ja minu vahel ning sinu järglaste ja minu järglaste vahel!” (1. Saam. 20:42.)
 4. a) Mis valmistab meile tõelist rõõmu ja rahulolu? b) Mida me selles artiklis arutame?
4 Kristlastena me ei imetle üksnes teiste ustavust, vaid oleme ka ise ustavad pereliikmed, sõbrad ja usukaaslased. (1. Tess. 2:10, 11.)
Ent kellele on kõige tähtsam olla ustav?
Muidugi temale, kes on andnud meile elu! (Ilm. 4:11.)
Ustavus Jumalale toob meile tõelist rõõmu ja rahulolu.
Ent kui tahame jääda talle ustavaks, tuleb meil hoida tema ligi ka kõige raskemates katsumustes.
Selles artiklis me arutame, kuidas Joonatani eeskuju aitab meil jääda Jehoovale ustavaks neljas olukorras: 1) kui meil on kellelegi raske alluda, 2) kui tuleb valida, kellele olla ustav, 3) kui meid mõistetakse valesti või koheldakse ebaõiglaselt ja 4) kui mingi lepingu või lubaduse täitmine osutub keeruliseks.



KUI MEIL ON KELLELEGI RASKE ALLUDA

 5. Miks oli kuningas Sauli valitsusajal iisraellastel raske Jumalale ustavaks jääda?
5 Kuigi Jumal oli võidnud Joonatani isa Sauli kuningaks, ei kuuletunud Saul hiljem talle ja Jumal hülgas ta. (1. Saam. 15:17–23.)
Kuna Jehoova ei kõrvaldanud Sauli troonilt kohe, pani Sauli väärkäitumine tema alamad ja talle lähedased inimesed proovile.
Neil oli raske jääda Jumalale ustavaks, sest kuningas, kes istus Jehoova troonil, toimis isepäiselt. (1. Ajar. 29:23.)
 6. Mis näitab, et Joonatan jäi Jehoovale ustavaks?
6 Mida tegi Joonatan, kui tema isa tegudes hakkas avalduma sõnakuulmatuse vaim? (1. Saam. 13:13, 14.)
Joonatan oli ikka ustav Jehoovale.
Prohvet Saamuel oli öelnud: „Jehoova ei hülga teid [oma rahvast] oma suure nime pärast”. (1. Saam. 12:22.)
Joonatan näitas, et ta uskus seda tõotust, kui Iisraeli ähvardas vilistite tohutu armee, kus oli 30 000 sõjavankrit.
Saulil oli kõigest 600 meest ning üksnes temal ja Joonatanil olid relvad!
Ent Joonatan läks vilistite valvesalga juurde vaid oma relvakandjaga. „Miski ei takista Jehooval meid päästa kas paljude või vähestega,” ütles Joonatan.
Need kaks iisraellast lõid maha umbes 20 valvesalga meest.
Siis „toimus maavärin ja Jumal pani kõigi südamesse õudustunde”.
Segaduses vilistid pöörasid oma mõõgad üksteise vastu.
Nii andis Joonatani usk Jumalasse iisraellastele võidu. (1. Saam. 13:5, 15, 22; 14:1, 2, 6, 14, 15, 20.)
 7. Kuidas suhtus Joonatan oma isasse?
7 Kuigi Saul ei kuuletunud enam Jumalale, tegi Joonatan oma isaga koostööd, kui see oli vähegi võimalik.
Näiteks võitlesid nad koos Jumala rahva eest. (1. Saam. 31:1, 2.)
8., 9. Kuidas see näitab meie ustavust Jumalale, kui me peame lugu neist, kellele ta on andnud juhtpositsiooni?
8 Meiegi võime Joonatani sarnaselt jääda Jehoovale ustavaks, alludes valitsemas olevatele võimudele, kuni see ei lähe vastuollu Jumala seadustega, ja seda isegi siis, kui meie arvates mõni võimukandja ei vääri lugupidamist.
Näiteks võib mõni valitsusametnik olla korrumpeerunud, kuid me austame teda siiski tema positsiooni pärast, sest „valitsemas olevad võimud on oma kohtadel Jumala seatuna”. (Loe Roomlastele 13:1, 2.) Me saame olla Jehoovale ustavad, kui peame lugu kõigist neist, kellele ta on andnud juhtpositsiooni. (1. Kor. 11:3; Heebr. 13:17.)





Üks viis jääda Jehoovale ustavaks on osutada lugupidamist uskmatu abikaasa vastu (vaata lõiku 9)




9 Lõuna-Ameerikas elav Olga[1] jäi Jumalale ustavaks sellega, et suhtus lugupidavalt oma abikaasasse, kuigi too kohtles teda väga halvasti.
Aastaid väljendas ta mees oma pahameelt selle üle, et naine on Jehoova tunnistaja.
Ta mõnitas ja solvas teda, keeldus temaga rääkimast ning ähvardas tema juurest ära minna ja lapsed kaasa võtta.
Olga aga ei tasunud halba halvaga.
Ta tegi oma parima, et olla hea naine, hoolitsedes kõigi pereliikmete eest ja vaadates, et mehel oleks toit laual ja riided korras. (Rooml. 12:17.)
Kui võimalik, läks ta koos mehega tema sugulastele või kolleegidele külla.
Ja kui nad pidid minema teise linna mehe isa matustele, sättis naine lapsed valmis ja tegi kõik reisiks vajalikud ettevalmistused.
Matusetalituse ajal ootas ta oma meest kiriku uksel, kuni tseremoonia lõppes.
Pärast paljusid aastaid hakkas mees tänu Olga kannatlikkusele ja lugupidavale suhtumisele leebuma.
Praegu viib ta oma naist kuningriigisaali ja isegi õhutab teda sinna minema ning käib ka ise aeg-ajalt koosolekutel. (1. Peetr. 3:1.)



KUI TULEB VALIDA,
KELLELE OLLA USTAV

10. Mille põhjal otsustas Joonatan, kellele jääda ustavaks?
10 Kuna Saul oli võtnud nõuks Taaveti ära tappa, tuli Joonatanil teha raske valik, kellele jääda ustavaks.
Kuigi ta oli teinud lepingu Taavetiga, allus ta ka oma isale.
Joonatan aga teadis, et Jumal on Taavetiga, mitte Sauliga.
Seepärast pidas Joonatan ustavust Taavetile tähtsamaks kui ustavust Saulile.
Ta hoiatas Taavetit, et see end varjaks, ja püüdis seejärel Sauli veenda Taavetit ellu jätma. (Loe 1. Saamueli 19:1–6.)
11., 12. Kuidas aitab armastus Jumala vastu meil otsustada jääda talle ustavaks?
11 Ustavus Jumalale aitas Austraalias elaval õel nimega Alice otsustada, milline on asjade õige tähtsusjärjekord.
Kui ta hakkas Piiblit uurima, rääkis ta oma perele kõigest sellest heast, mida ta õppis.
Hiljem ütles ta oma pereliikmetele, et ei pea enam koos nendega jõule.
Ta küll põhjendas oma seisukohta, kuid nende esialgne kerge pettumus muutus tasapisi ägedaks vihaks.
Nad tundsid, et ta on oma perele selja keeranud.
Alice jutustab: „Mu ema sõnas viimaks, et ta ei pea mind enam oma tütreks.
Olin šokeeritud ja tundsin suurt valu, sest ma armastasin oma perekonda väga.
Sellest hoolimata otsustasin, et Jehoova ja tema poeg on mulle tähtsamad, ning lasin end järgmisel kokkutulekul ristida.” (Matt. 10:37.)
12 Kui me pole valvsad, võib lojaalsus oma rahvusele, koolile või spordimeeskonnale tasapisi lämmatada meie ustavuse Jumalale.
Võtame näiteks Henry, kellele meeldib malet mängida.
Tema kooli malemeeskond oli võitnud mitmeid meistrivõistlusi ning ta soovis anda endast parima.
Henry räägib: „Aegamööda sai lojaalsus oma koolile mulle tähtsamaks kui ustavus Jumalale.
Nädalavahetustel malematšidel osalemise tõttu jäi mu teokraatlik tegevus unarusse.
Seega otsustasin lõpuks malemeeskonnast lahkuda.” (Matt. 6:33.)
13. Kuidas aitab ustavus Jumalale meil lahendada pereprobleeme?
13 Vahel võib olla raske otsustada, kuidas pereliikmete vahel oma tähelepanu jagada.
Näiteks Ken sõnab: „Ma soovisin oma eakat ema regulaarselt külastada ja tahtsin, et ta aeg-ajalt ka koos meiega oleks.
Ent minu ema ja mu naine ei saanud omavahel läbi.
Ma seisin dilemma ees, kuna ei suutnud ilma üht solvamata olla teisele meele järele.
Siis aga mõistsin, et säärases olukorras tuleb mul kõigepealt arvestada oma abikaasaga.
Seega püüdsin leida taktitundelise lahenduse, mis oleks talle vastuvõetav.” Ustavus Jumalale ja austus Piibli vastu andis Kenile julgust selgitada oma naisele, miks see peaks ämma lahkelt kohtlema, ning selgitada ka emale, miks tal tuleks miniast lugu pidada. (Loe 1. Moosese 2:24; 1. Korintlastele 13:4, 5.)



KUI MEID MÕISTETAKSE VALESTI VÕI KOHELDAKSE EBAÕIGLASELT

14. Kuidas Saul Joonatani kohtles?
14 Meie ustavuse Jehoovale võib panna proovile see, kui mõni vastutav vend kohtleb meid ebaõiglaselt.
Ka Joonatan seisis silmitsi sarnase probleemiga.
Kuningas Saul, kelle Jumal oli ametisse pannud, teadis oma poja sõprusest Taavetiga, kuid ta ei mõistnud seda.
Saul sai Joonatani peale maruvihaseks ja alandas teda.
Ent Joonatan ei vastanud samaga.
Ta jäi ustavaks nii Jumalale kui ka Taavetile, kellest sai Iisraeli järgmine kuningas. (1. Saam. 20:30–41.)
15. Kuidas me peaksime toimima, kui mõni vend kohtleb meid ebaõiglaselt?
15 Jehoova rahva kogudustes ei juhtu tänapäeval just sageli, et vastutavad vennad kohtleksid kedagi ebaõiglaselt.
Ent ka need vennad on ebatäiuslikud ja võivad meie tegusid vääriti mõista. (1. Saam. 1:13–17.)
Kui kunagi peaks juhtuma, et meid mõistetakse valesti või koheldakse ebaõiglaselt, siis jäägem ikka ustavaks Jehoovale.



KUI LEPINGU VÕI LUBADUSE TÄITMINE OSUTUB KEERULISEKS

16. Millistes olukordades nõuab ustavus Jumalale meilt ennastohverdavust?
16 Saul ärgitas Joonatani, et see tegutseks omaenda huvides. (1. Saam. 20:31.)
Ent ustavus Jumalale ajendas Joonatani hoidma sõprust Taavetiga, mitte kuningavõimu endale taotlema.
Meiegi saame jäljendada Joonatani isetut vaimu, kui peame meeles, et inimene, kes on Jehoovale ustav, „ei tagane oma tõotusest ka siis, kui see toob talle kahju”. (Laul 15:4.)
Joonatan ei taganenud Taavetile antud tõotusest ning ka meie ei peaks oma lubadusi murdma.
Näiteks kui ärilepingust kinnipidamine osutub raskemaks, kui me arvasime, siis ustavus Jumalale ja Piibli põhimõtete austamine peaks ajendama meid oma sõna pidama.
Või kui meil tekib abielus probleeme, mida me ei osanud oodata, siis ajendab armastus Jumala vastu meid oma abikaasale truuks jääma. (Loe Malaki 2:13–16.)





Ärilepingust kinnipidamine võib panna proovile meie ustavuse Jumalale (vaata lõiku 16)




17. Mida sa oled sellest artiklist õppinud?
17 Kui mõtiskleme Joonatani elu üle, siis soovime ilmselt ka ise olla Jumalale ustavad ja isetu meelelaadiga.
Olgem Joonatani sarnaselt Jehoovale ustavad ka selles mõttes, et oleme ustavad kaaslased tema teenijatele, isegi neile, kes võivad meile pettumust valmistada.
Kui jääme proovilepanevates olukordades Jehoovale ustavaks, siis rõõmustame tema südant ning see annab meile sügava rahuldustunde. (Õpet. 27:11.)
Võime olla kindlad, et meie ustavus Jehoovale saab lõpuks imeliselt tasutud.
Järgmises artiklis uurime, mida me võime õppida Taaveti kaasaegsetest, nii nendest, kes olid Jumalale ustavad, kui ka nendest, kes ei olnud.



[1] (9. lõik.)
Nimesid on muudetud.




    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Eestikeelsed väljaanded (2000-2017)
			
		

		
			
				
				Eelistused
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Kasutustingimused
			
			
				Privaatsus
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



