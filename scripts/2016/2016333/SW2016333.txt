ALIZALIWA: 1974

NCHI: MEXICO

HISTORIA: KIJANA MJEURI,
MPIGANAJI WA MTAANI










MAISHA YANGU YA ZAMANI:
 Nilizaliwa Ciudad Mante, katika eneo zuri la jiji la Tamaulipas, nchini Mexico.
Kwa kawaida, watu wa jiji hilo ni wakarimu na wenye fadhili.
Lakini kwa kusikitisha, eneo hilo lilikuwa hatari sana kwa sababu ya magenge ya uhalifu.
Nilikuwa mtoto wa pili katika familia yenye watoto wanne.
Kwa kuwa wazazi wangu walikuwa Wakatoliki nilibatizwa katika dini hiyo, na baadaye nikawa mwimbaji wa kwaya kanisani.
Nilitaka kumpendeza Mungu kwa kuwa niliogopa sana kuhukumiwa na kuteswa motoni milele.
Baba aliondoka nyumbani nilipokuwa na miaka mitano.
Hilo lilinihuzunisha sana na nilijihisi sifai.
Sikuelewa kwa nini baba alituacha ingawa tulimpenda sana.
Mama alihitaji kufanya kazi kwa muda mrefu ili kuitunza familia.
Hivyo, sikuenda shule kwa ukawaida na nilianza kushirikiana na vijana waliokuwa na umri mkubwa.
Walinifundisha matusi, kuvuta sigara, kuiba na kupigana.
Kwa kuwa nilipenda umaarufu, nilijifunza ndondi, mieleka, karate, na kutumia silaha.
Nikawa kijana mjeuri.
Mara nyingi nilipigana kwa kutumia silaha, na mara kadhaa nilikutwa mtaani nikiwa nimeumizwa vibaya karibu kufa.
Mama yangu aliumia sana aliponikuta katika hali hiyo na alilazimika kunipeleka hospitali haraka!
Nilipokuwa na umri wa miaka 16,
Jorge, rafiki yangu wa tangu utoto alitutembelea nyumbani.
Alituambia kwamba yeye ni Shahidi wa Yehova na alitaka kutueleza ujumbe muhimu.
Alianza kutueleza mambo aliyoamini kwa kutumia Biblia.
Sikuwahi kusoma Biblia, nilifurahi kujifunza jina la Mungu na makusudi yake.
Jorge alijitolea kujifunza Biblia pamoja nasi, na tulikubali.



JINSI BIBLIA ILIVYOBADILI MAISHA YANGU:
 Nilifarijika sana kujifunza ukweli kuhusu moto wa mateso, kwamba hilo si fundisho la Biblia. (Zaburi 146:4; Mhubiri 9:5)
Nilipojifunza jambo hilo, niliacha kumwogopa Mungu isivyofaa.
Nilianza kumwona Mungu kuwa Baba mwenye upendo ambaye huwatakia mema watoto wake.
Nilipoendelea kujifunza Biblia, nilitambua uhitaji wa kubadili utu wangu.
Nilihitaji kusitawisha unyenyekevu na kuacha jeuri.
Ushauri unaopatikana katika 1 Wakorintho 15:33 ulinisaidia.
Unasema hivi: “Mashirika mabaya huharibu tabia nzuri.” Nilitambua kwamba ili nibadili utu wangu, nilipaswa kuacha kushirikiana na watu ambao wangedhoofisha jitihada zangu.
Kwa hiyo niliacha kushirikiana na rafiki zangu wa zamani na nikaanza kushirikiana na Wakristo wa kweli, ambao hutatua kutoelewana kwa kutumia kanuni za Biblia badala ya kutumia ngumi au jeuri.
Andiko lingine lililonisaidia sana ni Waroma 12:17-19.
Linasema hivi: “Msimlipe yeyote uovu kwa uovu.  . .
Ikiwezekana, kwa kadiri inavyowategemea ninyi, iweni wenye kufanya amani na watu wote.
Wapendwa, msijilipizie kisasi wenyewe.  . . kwa maana imeandikwa: ‘Kisasi ni changu; mimi nitalipa, asema Yehova.’” Nilitambua kwamba Yehova atakomesha ukosefu wa haki kwa njia yake na kwa wakati wake.
Hatua kwa hatua niliacha kuwa mjeuri.
Siwezi kusahau jambo lililotokea jioni moja nilipokuwa ninarudi nyumbani.
Kundi la vijana wahalifu nililoshirikiana nalo zamani lilinishambulia, na kiongozi wao alinipiga mgongoni, huku akipaaza sauti, “Jilinde!” Nilitoa sala fupi kwa Yehova, nikimwomba anisaidie kujidhibiti.
Ingawa nilitamani sana kulipiza kisasi, nilifaulu kujidhibiti.
Siku iliyofuata nilikutana na kiongozi wa genge hilo akiwa peke yake.
Nilishikwa na hasira na nikataka kulipiza kisasi, lakini nilisali tena kimyakimya kwa Yehova anisaidie kujidhibiti.
Jambo la kushangaza ni kwamba kijana huyo aliniomba msamaha kwa kusema: “Nisamehe kwa yaliyotokea usiku wa jana.
Ukweli ni kwamba, ningependa kuwa kama wewe.
Nataka kujifunza Biblia.” Nilifurahi sana kwamba niliweza kudhibiti hasira yangu!
Matokeo ni kwamba, tulianza kujifunza Biblia pamoja.
Inasikitisha kwamba washiriki wengine wa familia yetu hawakuendelea kujifunza Biblia wakati huo.
Hata hivyo, nilifanya uamuzi thabiti wa kuendelea kujifunza na kutoruhusu mtu au kitu chochote kinizuie.
Nilijua kwamba kushirikiana kwa ukawaida na watu wa Mungu kungeponya maumivu yangu ya kihisia na ningepata familia niliyohitaji.
Niliendelea kufanya maendeleo, na mwaka 1991, nikabatizwa na kuwa Shahidi wa Yehova.








JINSI AMBAVYO NIMEFAIDIKA:
 Nilikuwa mwenye uchungu moyoni, mkandamizaji, na mjeuri.
Lakini Biblia imebadili kabisa maisha yangu.
Sasa ninahubiri ujumbe wa Biblia wa amani kwa mtu yeyote anayetaka kusikiliza.
Nimetumikia nikiwa mtumishi wa wakati wote kwa miaka 23.
Kwa muda fulani nilijitolea kufanya kazi katika ofisi ya tawi ya Mashahidi wa Yehova nchini Mexico.
Nilipokuwa huko, nilikutana na Claudia, dada Mkristo mwenye bidii, na tukafunga ndoa mwaka 1999.
Ninamshukuru sana Yehova kwa kunipa mwenzi mshikamanifu!
Tulitumikia pamoja katika kutaniko la Lugha ya Ishara ya Mexico, tukiwasaidia viziwi kujifunza kumhusu Yehova.
Baadaye tuliombwa kuhamia nchini Belize ili kuwafundisha watu Biblia.
Ingawa tunaishi maisha rahisi, tuna kila kitu tunachohitaji ili kuwa na furaha.
Hatutamani kubadili maisha yetu.
Baada ya muda, mama yangu alianza tena kujifunza Biblia na akabatizwa.
Pia, kaka yangu, mke wake, na watoto wao watatu ni Mashahidi wa Yehova.
Baadhi ya rafiki zangu wa zamani ambao niliwahubiria ujumbe wa Ufalme wanamtumikia Yehova.
Inasikitisha kwamba, baadhi ya washiriki wa familia wamekufa kwa sababu hawakuacha jeuri.
Ikiwa ningeendelea kuishi kama wao, ningekuwa nimekufa.
Ninamshukuru sana Yehova kwa kunivuta kwake na waabudu wake, ambao wamenifundisha kwa subira na fadhili kutumia kanuni za Biblia maishani.






    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Machapisho ya Kiswahili (2000-2017)
			
		

		
			
				
				Mapendezi
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Masharti ya Utumiaji
			
			
				Sera ya Faragha
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



