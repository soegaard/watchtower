JE,
UNAMJUA ndugu au dada ambaye amehamia nchi nyingine ambayo ina uhitaji wa wahubiri wa Ufalme?
Je, umewahi kujiuliza hivi: ‘Ni nini kinachowachochea kutumikia katika nchi nyingine?
Wanajitayarishaje kwa ajili ya utumishi huo?
Je, ninaweza kushiriki katika utumishi huo?’ Ili tupate majibu ya maswali hayo ni vizuri tuwaulize ndugu na dada hao.
Acheni tufanye hivyo.



WANACHOCHEWA NA NINI?

Ni nini kilikufanya ufikirie kutumikia katika nchi yenye uhitaji mkubwa wa wahubiri? Amy ni dada kutoka Marekani mwenye umri wa miaka 35 hivi.
Anaeleza hivi: “Kwa miaka mingi nilifikiria kutumikia katika nchi nyingine lakini sikuwa na uhakika kwamba nitaweza.” Ni jambo gani lilibadili maoni yake? “Mwaka wa 2004 wenzi wa ndoa waliokuwa wakitumikia nchini Belize walinialika niwatembelee na kufanya upainia pamoja nao kwa mwezi mmoja.
Nilikubali mwaliko wao na nilifurahia sana utumishi huo!
Mwaka mmoja baadaye, nilihamia nchini Ghana ili nitumikie nikiwa painia.”





Aaron na Stephanie




Miaka michache iliyopita, Stephanie kutoka Marekani ambaye sasa anakaribia umri wa miaka 30, alichunguza kwa makini hali zake na kujiambia hivi: ‘Nina afya nzuri na sina majukumu ya familia.
Kwa kweli, ninaweza kumfanyia Yehova mambo mengi zaidi kuliko ninayofanya sasa.’ Kujichunguza hivyo kwa unyoofu kulimchochea ahamie nchini Ghana ili apanue huduma yake. Filip na Ida ni wenzi wa ndoa wenye umri wa makamo kutoka Denmark, ambao kwa muda mrefu walitamani kuhamia nchi yenye uhitaji mkubwa.
Walitafuta njia mbalimbali za kutimiza tamaa yao.
Filip anasema hivi: “Nafasi ilipojitokeza ni kana kwamba Yehova alituambia hivi: ‘Nendeni!’” Mwaka wa 2008, walihamia Ghana na kutumikia huko kwa miaka zaidi ya mitatu.





Brook na Hans




Hans na Brook ni wenzi wa ndoa mapainia wenye umri wa miaka 30 hivi, ambao sasa wanatumikia nchini Marekani. Mwaka wa 2005 walijitolea kutoa msaada baada ya Kimbunga cha Katrina.
Baadaye, walijaza fomu ya kusaidia miradi ya ujenzi ya kimataifa lakini hawakualikwa.
Hans anakumbuka hivi: “Kisha tulisikiliza hotuba ya kusanyiko iliyotaja kwamba Mfalme Daudi alikubali kwamba hangeruhusiwa kujenga hekalu, hivyo akabadili lengo lake.
Jambo hilo lilitusaidia kuona kwamba si vibaya kubadili malengo yetu ya kitheokrasi.” (1 Nya. 17:1-4, 11, 12; 22:5-11)
Brook anaongeza hivi: “Yehova alitaka tubishe mlango mwingine.”
Baada ya kusikiliza mambo yaliyoonwa yenye kusisimua kutoka kwa marafiki wao waliotumikia katika nchi nyingine,
Hans na Brook walichochewa kujaribu kufanya upainia kwenye eneo lenye uhitaji.
Mwaka wa 2012 walienda Ghana na kutumikia kwa miezi minne, wakisaidia kutaniko la lugha ya alama.
Ingawa walihitaji kurudi Marekani, kutumikia nchini Ghana kuliongeza tamaa yao ya kuendelea kutanguliza masilahi ya Ufalme maishani.
Baadaye walisaidia katika ujenzi wa ofisi ya tawi ya Micronesia.



HATUA WALIZOCHUKUA ILI KUFIKIA MALENGO YAO

Ulijitayarishaje ili uweze kutumikia katika eneo lenye uhitaji mkubwa wa wahubiri? Stephanie* anasema hivi: “Nilifanya utafiti kwenye magazeti ya Mnara wa Mlinzi yenye habari kuhusu jinsi ya kutumikia katika maeneo yenye uhitaji mkubwa.
Pia, nilizungumza na wazee wa kutaniko langu na mwangalizi wa mzunguko na mke wake kuhusu tamaa yangu ya kutumikia katika nchi nyingine.
Zaidi ya yote, nilisali kwa Yehova mara nyingi kuhusu malengo yangu.” Wakati huohuo,
Stephanie aliendelea kuishi maisha rahisi, jambo lililomwezesha kuhifadhi pesa ambazo zingemsaidia anapotumikia katika nchi nyingine.
Hans anaeleza hivi: “Tulimwomba Yehova atupe mwongozo kwa sababu tulitaka kwenda mahali atakapotuelekeza.
Pia, tuliposali tulitaja tarehe hususa tuliyopanga kufanya hivyo.” Wenzi hao walituma barua kwenye ofisi nne za tawi.
Baada ya ombi lao kukubaliwa na ofisi ya Ghana, walisafiri kwenda huko, wakiwa na lengo la kukaa kwa miezi miwili.
Hans anasema hivi: “Tulifurahia sana kushirikiana na kutaniko hivi kwamba tukaongeza muda wetu wa kukaa huko.”





Adria na George




George na Adria, wenzi wa ndoa kutoka Kanada, wenye umri wa miaka 40 hivi, walijua kwamba Yehova hubariki uamuzi mzuri, si nia nzuri pekee.
Hivyo, walichukua hatua ili kufikia malengo yao.
Waliwasiliana na dada aliyekuwa akitumikia nchini Ghana na kumwuliza maswali mengi.
Pia, walituma barua kwenye ofisi ya tawi ya Kanada na ya Ghana.
Adria anasema hivi: “Tulitafuta njia za kurahisisha maisha yetu zaidi.” Maamuzi hayo yaliwasaidia kuhamia nchi ya Ghana mwaka wa 2004.



KUKABILIANA NA CHANGAMOTO

Ulipata changamoto gani ulipohamia nchi nyingine, na ulikabilianaje nazo? Mwanzoni,
Amy aliwakosa sana watu wa familia yao. “Mambo yote yalikuwa tofauti na kila kitu nilichokizoea.” Ni nini kilichomsaidia? “Washiriki wa familia walinipigia simu na kunieleza jinsi walivyofurahishwa na utumishi wangu, jambo ambalo lilinisaidia kukazia fikira sababu iliyofanya nihamie eneo hilo.
Baada ya muda tulianza kuwasiliana kupitia video.
Kwa kuwa tungeweza kuonana, sikuhisi kwamba walikuwa mbali sana nami.” Amy anaeleza kwamba urafiki pamoja na dada katika kutaniko alilohamia ulimsaidia azielewe vizuri desturi mbalimbali za eneo hilo.
Anasema hivi: “Alikuwa rafiki mwenye kutegemeka ambaye alinisaidia kuelewa kwa nini watu walitenda kwa njia fulani.
Alinisaidia kujua mambo ninayopaswa kufanya na yale ninayohitaji kuepuka, jambo ambalo lilinisaidia nifurahie huduma yangu.”
George na Adria wanaeleza kwamba walipohamia Ghana walihisi ni kana kwamba waliishi katika enzi za zamani sana.
Adria anasema hivi: “Badala ya kufua kwa kutumia mashine tulitumia ndoo.
Kupika kulichukua muda mwingi kuliko tulivyozoea.
Hata hivyo, baada ya muda tulizoea hali hizo zilizoonekana kuwa ngumu mwanzoni.” Brook anasema hivi: “Tunafurahia maisha licha ya changamoto tunazokabili tukiwa mapainia.
Tunapofikiria mambo ambayo kila mmoja wetu amepitia, yanatukumbusha mambo mengi mazuri ambayo ni kama hazina kwetu.”



HUDUMA YENYE KURIDHISHA

Kwa nini unawatia moyo wengine wafanye utumishi huu? Stephanie anasema hivi: “Unapata shangwe kubwa unapohubiri kila siku katika eneo ambalo lina watu wanaopenda kweli na wanaotaka kujifunza Biblia.
Kuhamia eneo lenye uhitaji mkubwa ndio uamuzi bora zaidi niliowahi kufanya!” Mwaka wa 2014,
Stephanie alifunga ndoa na Aaron, na sasa wanatumikia katika ofisi ya tawi ya Ghana.
Christine, dada mwenye umri wa miaka 30 hivi kutoka Ujerumani, anasema hivi: “Utumishi huo unafurahisha sana.” Christine alitumikia nchini Bolivia kabla ya kuhamia Ghana.
Anaongeza hivi: “Kwa kuwa niliishi mbali na familia yetu, sikuzote nilimtegemea Yehova anisaidie.
Amekuwa halisi zaidi kwangu.
Pia, nimefurahia umoja ambao unapatikana kati ya watu wa Yehova.
Utumishi huu umeboresha maisha yangu.” Christine na Gideon walifunga ndoa hivi karibuni, nao wanaendelea kutumikia pamoja nchini Ghana.





Christine na Gideon




Filip na Ida wanaeleza mambo waliyofanya ili kuwasaidia wanafunzi wa Biblia wafanye maendeleo. “Mwanzoni tulikuwa na wanafunzi wa Biblia 15 au zaidi, lakini tuliamua kuwa na wanafunzi 10 tu ili tuweze kuwafundisha kikamili.” Je, wanafunzi hao walinufaika?
Filip anaeleza hivi: “Nilijifunza Biblia na kijana fulani anayeitwa Michael.
Alitayarisha vizuri na tulijifunza kila siku, hivyo tulimaliza kitabu Biblia Inafundisha kwa mwezi mmoja.
Kisha,
Michael akawa mhubiri ambaye hajabatizwa.
Siku ya kwanza aliposhiriki katika utumishi, aliniuliza hivi: ‘Unaweza kunisaidia kuwafundisha wanafunzi wangu wa Biblia?’ Nilimtazama kwa mshangao.
Michael alinieleza kwamba alikuwa na wanafunzi watatu wa Biblia na alihitaji msaada wa kuwafundisha.” Kwa kweli, huo ni uhitaji mkubwa sana wa walimu wa Biblia!





Ida na Filip




Amy anaeleza kwamba baada ya muda mfupi alitambua kuna uhitaji mkubwa wa wahubiri: “Muda mfupi baada ya kuwasili Ghana, tulihubiri katika kijiji kidogo na kutafuta viziwi.
Tulipata viziwi wanane katika kijiji kimoja pekee!” Wakati huohuo,
Amy alifunga ndoa na Eric, na wanatumikia wakiwa mapainia wa pekee.
Wanatumikia katika kutaniko la lugha ya alama wakiwasaidia wahubiri na watu wanaopendezwa zaidi ya 300 ambao ni viziwi.
Kutumikia nchini Ghana kumewasaidia George na Adria kujua jinsi umishonari ulivyo.
Wazia jinsi walivyofurahi kupokea mwaliko wa kuhudhuria darasa la 126 la Shule ya Gileadi!
Sasa wanatumikia wakiwa wamishonari nchini Msumbiji.



WANACHOCHEWA NA UPENDO

Inatia moyo sana kuona wahubiri wengi kutoka nchi nyingine wakifanya kazi kwa bidii pamoja na ndugu na dada wenyeji ili kukusanya mavuno. (Yoh. 4:35)
Kwa wastani, watu 120 wanabatizwa kila juma nchini Ghana.
Kama ilivyo katika kisa cha wahubiri 17 waliohamia nchini Ghana, wahubiri wengi ulimwenguni pote wamechochewa na upendo wao kwa Yehova ‘kujitoa wenyewe kwa hiari.’ Wanatumikia katika maeneo yenye uhitaji mkubwa wa wahubiri wa Ufalme.
Bila shaka wafanyakazi hao wanaojitolea kwa hiari wanaufanya moyo wa Yehova ushangilie!—Zab. 110:3; Met. 27:11.



Kwa mfano, tazama makala zenye kichwa “Je,
Unaweza Kutumika Mahali Penye Uhitaji Mkubwa Zaidi wa Wahubiri?” na “Je,
Unaweza Kuvuka na Kuingia Makedonia?” katika gazeti la Mnara wa Mlinzi la Aprili 15 na Desemba 15, 2009.

Kabla ya kuhamia nchi nyingine jiulize hivi . . .
  Je, ninaweza kujitegemeza au kuitegemeza familia yangu?—1 The. 2:9.

  Je, ninaweza kupata viza?

  Je, nimefikiria hali ya hewa na utamaduni wa nchi hiyo?

  Je, nimechukua hatua yoyote ya kutunza afya yangu?

  Je, niko tayari kujifunza lugha ya nchi hiyo?

  Je, nitakuwa ‘msaada wenye kutia nguvu’ katika kutaniko? —Kol. 4:11.







    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Machapisho ya Kiswahili (2000-2017)
			
		

		
			
				
				Mapendezi
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Masharti ya Utumiaji
			
			
				Sera ya Faragha
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



