ABELPA tiemponmantapacha kay tiempokama,
Diospa kamachisnenqa cheqa sonqos kananchejpaj uj maqanakupi jina kashanchej.
Apóstol Pabloqa hebreo cristianosman nerqa: “Ñakʼariypi kallpakuspa sinchʼita sayarqankichej”, nispa. ¿Imaraykú ajinata kallpachakorqanku?
Jehovaj ñaupaqenpi allinpaj qhawasqa kanankupaj, bendicionninta japʼinankupaj ima (Heb. 10:32-34).
Pabloqa chay cristianosta, atipanaku pujllaypi kajkunawan kikincharqa.
Greciapi ajina atipanakupi kajkunaqa, nisunman correjkuna, maqanakojkuna, tinkojkuna ima, mayta kallpachakoj kanku (Heb. 12:1, 4).
Kunanpis noqanchejqa wiñay kausayta japʼinapaj, correjkuna jina kanchej.
Jinapis enemigosniyoj kanchej.
Paykunataj Jehovaj bendicionesninta mana japʼinanchejpaj, pantachiyta, phutichiyta ima munawanchej.
2 Qallarinapajqa Satanaswan, sajra pachanwan ima maqanakushanchej (Efe. 6:12).
Chayrayku mayta kallpachakunanchej tiyan, kay mundoj yachachiynin, yuyaynin ima, ama umanchejta muyuchinawanchejpaj.
Kallpachakunallataj tiyan sajra ruwaykunasninpi mana urmanapaj, nisunman khuchichakuypi, pitaypi, machaypi, drogakuypi ima.
Chantapis llakiykunasninchejwan, juchasapa kayninchejwanpis maqanakuna tiyan (2 Cor. 10:3-6;
Col. 3:5-10).
 3. ¿Imaynatá Jehová wakichiwanchej enemigosninchejwan maqanakunapaj?
3 ¿Chay atiyniyoj enemigosta atipayta atisunmanchu?
Arí, mana atinapaj jina kajtinpis.
Pabloqa tinkupi maqanakoj runawan kikinchakorqa.
Pay nerqa: “Imaynatachus tinkupi maqanakojkuna mana wayrallatachu sajmanku, ajinallatataj noqapis mana qhasipajchu imatapis ruwani”, nispa (1 Cor. 9:26).
Noqanchejpis tinkupi jina enemigosninchejwan maqanakunanchej tiyan.
Jehovataj chayta ruwanapaj yanapawanchej, wakichiwanchejtaj. ¿Imaynatá wakichiwanchej?
Biblianejta, publicacionesnejta, tantakuykunanejta, jatun tantakuykunanejta ima. ¿Kallpachakushanchejchu chaykunapi yachakusqanchejman jina ruwanapaj?
Mana kallpachakushanchejchu chayqa, tinkupi wayrallata sajmaj runa jina kanchej.
 4. ¿Imatá ruwasunman sajra kajwan mana atipachikunapaj?
4 Enemigosninchejqa mana suyashajtillanchej chayri pisi kallpa kashajtinchej, urmachiyta munawasunman.
Chayrayku wakichisqapuni kananchej tiyan.
Bibliaqa niwanchej: “Ama sajra kajwan atipachikuychu, astawanqa sajra kajta allin kajwan atipay”, nispa (Rom. 12:21).
Chayqa rikuchiwanchej, sajra kajta atipanapaj kallpachakullasunpuni chayqa, atipayta atisqanchejta.
Jinapis mana wakichisqachu kajtinchej, nitaj kallpachakojtinchejqa,
Satanás, sajra pachan, juchasapa kayninchej ima atipawasunman.
Chayrayku maqanakunallapuni.
Ama phutiywan atipachikunachu, nitaj makisninchejtapis llauchhiyachinachu (1 Ped. 5:9).
 5. 1)
Jehová allinpaj qhawanawanchejpaj, ¿imatá yuyarikunanchej tiyan? 2) ¿Pikunamantataj yachakusun?
5 Atipayta munaspaqa, imaraykuchus maqanakushasqanchejta yuyarikunanchej tiyan.
Noqanchejqa,
Dios allinpaj qhawanawanchejpaj maqanakushanchej.
Hebreos 11:6 nin: “Diosman qayllaykojqa creenan tiyan Diosqa kasqantapuni.
Creenallantaj tiyan, pikunachus Diosta tukuy sonqo maskʼanku, chaykunaman sumaj tʼinkata qonanta”, nispa.
Rikunchej jina,
Jehová bendecinawanchejta munanchej chayqa, tukuy atisqanchejta kallpachakunanchej tiyan (Hech. 15:15-17).
Bibliaqa ajinata kallpachakoj ashkha runasmanta parlan.
Paykunamanta wakenqa karqanku:
Jacob,
Raquel,
José,
Pablo ima.
Paykunaqa imaymana llakiykunapi rikukorqanku.
Jinapis kallpachakusqankurayku,
Diospa bendicionninta japʼerqanku.
Kausayninkoqa rikuchiwasun, kallpachakojtinchej Jehová bendecinawanchejta.
Chayrayku paykunamanta yachakuna.



AGUANTAJTINCHEJ,
JEHOVÁ BENDECIWASUN

 6. 1) ¿Imaraykú Jacob tukuy atisqanta kallpachakorqa? 2) ¿Ima tʼinkatataj Jehová Jacobman qorqa? (8 paginapi kaj dibujota qhawariy).
6 Jacobqa cheqa sonqo runa karqa, tukuy atisqantataj kallpachakorqa. ¿Imaraykú?
Jehovata munakusqanrayku, paypa amigon kasqanta sumajpaj qhawasqanrayku ima.
Jehovaqa Jacobman nerqa, mirayninta bendecinanta.
Jacobtaj chaypi atienekorqa (Gén. 28:3, 4).
Chayrayku 100 watasniyoj jina kashajtin,
Jehovaj bendicionninta japʼinanpaj uj angelwan maqanakorqa (Génesis 32:24-28 leey). Chay angelwan maqanakunanpaj, ¿atiyniyojchu karqa? Mana.
Jinapis Jehovaj bendicionninta japʼiytapuni munarqa, japʼerqataj.
Jehovaqa Jacobpa kallpachakusqanta bendicerqa.
Chayrayku Israel sutita churarqa.
Chay suteqa, “Dioswan maqanakoj” niyta munan.
Jehovaqa Jacobta tʼinkarqa, payta allinpaj qhawaspa, bendecispa ima.
Noqanchejpis chay tʼinkallatataj japʼiyta munanchej.
 7. 1) ¿Ima llakiyniyojtaj Raquel karqa? 2) ¿Imatá Raquel ruwarqa, chantá ima bendiciontataj japʼerqa?
7 Raquelqa Jacobpa munasqa warmin karqa.
Raquelqa imatachus Jehová qosanman nisqanta juntʼakojta rikuyta munarqa.
Jinapis Raquelqa jatun llakiyniyoj karqa.
Payqa mana wawasniyoj kayta aterqachu.
Chay tiempopitaj, mana wawasniyoj kayqa uj warmipaj may llakiy karqa. ¿Imatá Raquel ruwarqa?
Payqa yacharqa chay llakiynin mana wiñaypajchu kananta.
Chayrayku ñaupajman rinanpaj, kutin kutita Jehovamanta mañakorqa.
Jehovataj mañakusqanta uyarerqa, bendicerqataj wawasniyoj kananpaj.
Chayrayku Raquel nerqa: “Maqanakuni, atipanitaj”, nispa (Gén. 30:8, 20-24).
 8. 1) ¿Ima chʼampaykunapitaj José rikukorqa? 2) ¿Imatá Josemanta yachakusunman?
8 Jacobpa,
Raquelpa kausayninkoqa, wawanku Joseta maytachá yanaparqa chʼampaykunata atipananpaj.
Imaraykuchus José 17 watasniyoj kashajtin, kausaynenqa ujllata mana allinman tukorqa.
Hermanosnenqa envidiakusqankurayku, esclavo kananpaj vendeykorqanku.
Egiptopiña kashajtintaj, mana imallamanta carcelman wisqʼaykorqanku (Gén. 37:23-28; 39:7-9, 20, 21).
Jinapis Joseqa, mana llakiywan atipachikorqachu, nitaj phiñakorqachu, nillataj vengakorqachu. ¿Imaraykú?
Imaraykuchus Jehovawan amigos kasqanta aswan sumajpaj qhawarqa (Lev. 19:18;
Rom. 12:17-21). ¿Imatá Josemanta yachakusunman?
Wawa kashaspa llakiyta kausakuspapis, chayri chʼampaykunasninchej may jatuchaj kajtinpis, ñaupajllamanpuni rinapaj kallpachakunanchej tiyan.
Chayta ruwasun chayqa,
Jehová bendeciwasunpuni (Génesis 39:21-23 leey).
 9. ¿Imatá yachakunchej Jacobmanta,
Raquelmanta,
Josemanta ima?
9 Kunanpis, noqanchejqa imaymana chʼampaykunasniyoj kanchej.
Ichapis qhasimanta ñakʼarichisqas, chejnisqas, asipayasqas ima kanchej, chayri enviadiarayku pillapis llakichiwanchej.
Jina kajtin ama phutikunachu.
Astawanpis Jacobmanta,
Raquelmanta,
Josemanta ima yuyarikuna.
Tʼukurinataj imachus Jehovata sirvinallankupajpuni yanapasqanpi.
Diosqa paykunata kallpacharqa, bendicerqataj. ¿Imaraykú?
Paywan amigos kasqankuta sumajpaj qhawasqankurayku.
Paykunaqa ñaupajman rinankupaj mayta kallpachakorqanku, mañakusqankuman jinataj ruwarqanku.
Tukukuy pʼunchaykunapi kausashasqanchejrayku, ñaupaqenchejpi churasqa kashan chay suyakuymanta chʼipakuna tiyan.
Chayrayku Jehovaj bendicionninta japʼinanchejpaj, tukuy atisqanchejta kallpachakunallapuni.



BENDICIONESTA JAPʼINAPAJ KALLPACHAKUNA

10, 11. 1) ¿Imaraykutaj Diospa bendicionninta japʼinapaj sinchʼita kallpachakunanchej tiyan? 2) ¿Imataj allin kajta ruwanapaj yanapawasun?
10 ¿Imaraykutaj Diospa bendicionninta japʼinapaj sinchʼita kallpachakunanchej tiyan?
Pantaj runas kasqanchejrayku.
Chayrayku wakenqa sajra munayninkuwan maqanakushanku.
Wakintaj Diosmanta kusiywan willamunankupaj kallpachakunanku tiyan.
Wajkunataj onqoyrayku chayri sapallanku kasqankurayku llakikusqankuta atipananku tiyan.
Wakinrí, sonqonkuta nanachejkunata perdonanankupaj kallpachakunanku tiyan.
Jehovata ashkha watastaña chayri niraj unaytachu sirvispapis, tukuyninchej Diosta sirvinallapajpuni imatapis atipananchej tiyan.
Yuyarikunataj Jehová cheqa sonqo kajkunata bendecinanta.





¿Jehovaj bendicionninta japʼinapaj kallpachakushanchejchu? (10, 11 parrafosta qhawariy).




11 Cristiano kay, allin kajta ruwayta ajllaypis mana atikullanchu, astawanraj sajra munayninchejwan maqanakushanchej chayqa (Jer. 17:9).
Ajinapi rikukunchej chayqa,
Jehovamanta atiyninta mañakuna.
Chaytaj allin kajta ruwanapaj kallpata qowasun,
Diospa bendicionesnintataj japʼisunchej.
Chayrayku Diosmanta mañakusqanchejman jina kausana.
Kallpachakunataj Bibliata sapa día leenapaj, sapallanchejpi estudianapaj, familiapi Diosta yupaychanapaj ima (Salmo 119:32 leey).
12, 13. ¿Imataj iskay cristianosta yanaparqa sajra munayninkuta atipanankupaj?
12 Diospa Palabran, atiynin, publicacionesninchej ima, ashkha hermanosta yanapan sajra munayninkuta atipanankupaj.
Uj jovencitoqa, 2003 watamanta 8 de diciembre ¡Despertad! revistapi, “¿Cómo puede usted resistirse a los malos deseos?” nisqa yachaqanata leesqa.
Chantá pay nin: “Sajra munayniyta atipanaypaj mayta kallpachakushani.
Jinapis chay revistapi nin: ‘Ashkha runas sajra munayninkuta atipanankupaj mayta kallpachakushanku’, nispa.
Chayta leespa, ashkha hermanos kikin chʼampayniyojllataj kasqankuta repararqani, nitaj sapaychu kashasqayta”, nispa.
Paytaqa “¿Aprueba Dios los estilos de vida alternativos?” nisqa yachaqana, mayta yanapallarqataj.
Chay yachaqanaqa 2003 watapi llojserqa, 8 de octubre ¡Despertad! revistapi.
Chay hermanitoqa repararqa, chay sajra munaywan maqanakoyqa ‘uj ayjon jina tʼujsisqanta’ (2 Cor. 12:7).
Jinapis pay nin: “Chay hermanosqa llimphu kausayniyoj kanankupaj kallpachakushallankupuni, atipanankutataj yachanku.
Chayrayku yachani, noqapis Jehovaman cheqa sonqo kayta atisqayta.
Jehovaman may agradecesqa kashani, kay sajra pachaman mana chhapukunapaj llajtannejta yanapawasqanchejmanta”, nispa.
13 Estados Unidosmanta uj hermana nillantaj: “Jehovaman mayta agradecekuni, yachachiykunata mikhunata jina horanpi qowasqanchejmanta.
Publicacionesta leespa, ashkha kutispi nini: ‘kayqa noqapaj jinapuni’, nispa.
May unayña sajra munayniywan maqanakushani.
Wakin kutisqa mana atipanayta yuyaspa, niña kallpachakuyta munanichu.
Jehovaqa khuyakuyniyoj, perdonaj Diostaj.
Jinapis chay sajra munayniy sonqollaypipuni kasqanrayku, yuyarqani Jehová ni jaykʼaj yanapanawanta.
Chayrayku llakisqalla kani.
Chaywanpis 15 de marzo de 2013 Torremanta Qhawaj revistapi, “Jehovata rejsinaykipaj ¿sonqoyki aysakunchu?” nisqa yachaqanata leeytawan,
Jehová yanapayta munawasqanta repararqani”, nispa.
14. 1) ¿Imatataj Pablo kausayninpi rikorqa? 2) ¿Imaynatataj sajra munayninchejta atipasunman?
14 (Romanos 7:21-25 leey). Pabloqa kikin kausayninpi rikorqa, sajra kajta munaywan, juchasapa kaywan ima maqanakoyqa, mana atinapaj jinallachu kasqanta.
Chaywanpis yachallarqataj,
Jehovamanta mañakojtin, paypa yanapayninpi atienekojtin,
Jesuspa wañuyninpi creejtin ima, atipayta atisqanta. ¿Noqanchejrí? ¿Sajra munayninchejta atipayta atisunmanchu?
Arí.
Chaypajqa Pablo jina ruwana,
Jesuspa wañuyninpi creena, kallpanchejpi atienekunamantataj Jehovapi tukuy sonqo atienekuna.
15. Cheqa sonqos kanapaj, chʼampaykunata aguantanapaj ima, ¿imaynatá Jehová yanapawasunman?
15 Wakin kutis,
Jehovaqa uj chʼampaymanta may jinatachus llakikusqanchejta rikuyta munan.
Sutʼincharinapaj, ujnin familiarninchej sinchʼi onqosqa kajtin chayri pillapis payta qhasimanta ñakʼarichejtin, ¿imatá ruwasun? ¿Noqanchej ajinapi rikukojtinchejrí?
Jehovapi tukuy sonqo atienekojtinchejqa, cheqa sonqos kanapaj paymanta kallpata mañakusun, jinallataj kusisqallapuni, amigosnillanpuni kanapajpis (Fili. 4:13, NM). Ñaupa tiempomanta, jinallataj kay tiempomanta cristianospa kausayninkupis, kayta yachachiwanchej:
Jehovamanta mañakojtinchejqa, pay kallpachawasun ñakʼariykunata aguantanapaj.



JEHOVAJ BENDICIONNINTA JAPʼINAPAJ KALLPACHAKUNALLAPUNI

16, 17. ¿Imatataj ruwanallanchejpuni tiyan?
16 Llakiywan atipachikojtinchej, manaña kallpachakojtinchej, chayri makisninchejta llauchhiyachejtinchej,
Satanasqa mayta kusikunman.
Chayrayku ‘allin kajmanta japʼikunallapuni’ (1 Tes. 5:21).
Tukuyninchej Satanasta, sajra pachanta, sajra munayninchejta ima atipayta atinchej.
Chaypajqa, tukuy sonqo atienekuna Jehová kallpachanawanchejpi, yanapanawanchejpi ima (2 Cor. 4:7-9;
Gál. 6:9).
17 Chayrayku mana saykʼuspa maqanakunallapuni, kallpachakunallapuni ima.
Chayta ruwajtinchej,
Jehová Diosqa “may chhika bendicioneswan juntʼachi[wasun]” (Mal. 3:10).






    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Quechuapi publicaciones (2004-2017)
			
		

		
			
				
				Allinchanapaj
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Kay kamachiykunata kasukuy
			
			
				Datosta apaykachakusqanmanta
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



