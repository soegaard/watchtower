CRISTINAQA 18 watasniyoj, pay nin: “Tatasneyqa casi ni jaykʼaj kallpachawankuchu, tukuy imamantataj rimawanku.
Parlasqankuwan sonqoyta nanachiwanku.
Paykunaqa wawa jina kasqayta niwanku, ni jaykʼaj imatapis allinta ruwanayta.
Wira kasqaytataj niwanku.
Chayrayku sapa kuti waqani, niñataj paykunawan parlayta munanichu.
Ni imapaj valesqayta yuyani”, nispa.[1] Kausayqa may llakiy, mana pipis kallpachariwajtinchejqa.
2 Wajkunata kallpacharejtinchejrí, paykunata mayta yanapasunman.
Rubén nin: “Unaytaña kallpachakushani mana pisipaj qhawakunaypaj.
Uj kuti ancianowan predicasharqani, paytaj repararqa mana allinchu kashasqayta.
Llakiyniyta willakojtiy, payqa sumajta uyariwarqa.
Chantá niwarqa imastachus allinta ruwashasqayta.
Yuyarichiwarqataj Jesuspa kay nisqanta, ‘qankunaqa pʼisqetusmanta nisqaqa aswan valorniyoj kankichej’.
Chay versiculota yuyarikullanipuni, sapa kutitaj sonqochawan.
Chay hermanoj niwasqanqa mayta yanapawarqa”, nispa (Mat. 10:31).
 3. 1) ¿Imatataj apóstol Pablo nerqa wajkunata kallpachariymanta? 2) ¿Imatá kay yachaqanapi yachakusun?
3 Bibliaqa nin, purajmanta kallpachanakuna kasqanta.
Apóstol Pabloqa hebreo cristianosman nerqa: “Hermanos, sumajta qhawakuychej; ama pipis qankunamanta sajra mana creej sonqoyoj kachunchu,
Kawsaj Diosmanta karunchakunampaj.
Astawanqa, sapa pʼunchay purajmanta kallpachanakuychej [...].
Ajinamanta mana pipis juchaj chʼawkiyasqanwan sonqompi rumiyachisqachu kanqa”, nispa (Heb. 3:12, 13).
Pipis allinta ruwanki niwajtinchejqa, mayta kallpachawanchej.
Chayrayku kay yachaqanapi kay tapuykunata kutichisun: ¿Imaraykutaj hermanosninchejta kallpacharina tiyan?
Jehová,
Jesús,
Pablo ima wajkunata kallpachasqankumanta, ¿imatá yachakusunman? ¿Imaynasmantá wajkunata kallpacharisunman?



KALLPACHASQA KAYTA NECESITANCHEJ

 4. 1) ¿Pikunataj kallpacharinata necesitanku? 2) ¿Imaraykú runas niña wajkunata kallpacharinkuchu?
4 Tukuypis kallpacharinata necesitanchej.
Astawanqa tatas wawasninkuta kallpacharinanku tiyan.
Profesor Timothy Evans nin: “Imaynatachus uj plantita yakuta necesitan, ajinallatataj wawasqa kallpachasqa kayta necesitanku. [...]
Tatamama wawankuta kallpacharejtinku, wawaqa munasqa kasqanta yachan, nitaj pisipajchu qhawakun”, nispa.
Sajra pʼunchaykunapi kashasqanchejrayku, runasqa paykunallapaj imatapis munanku, nitaj wajkunata munakunkuchu (2 Tim. 3:1-5).
Wakin tatasqa wawasninkuta mana ninkuchu “allinta ruwanki” nispa, tatasninkumanta mana chayta uyarisqankurayku.
Kurajkunapis kallpachanata necesitallankutaj, jinapis chayqa mana rikukunchu.
Ashkha llankʼajkunaqa, quejakunku ni pipis “allinta ruwanki” nisqanmanta.
 5. ¿Imaynatá wajkunata kallpacharisunman?
5 Wajkunata kallpacharisunman: 1) imatapis allinta ruwasqankumanta “sumajta ruwanki” nispa, 2) paykunata manchay kʼachas kasqankuta nispa, 3) llakisqa chayri pisi kallpa kashajtinku sonqocharispa ima (1 Tes. 5:14).
Hermanosninchejwan khuska kashaspaqa, imallatapis kallpacharinapaj nisunman (Eclesiastés 4:9, 10 leey). Ichapis tapurikusunman: “¿Wajkunata niyta yachanichu paykunata munakusqayta, jatunpajtaj qhawasqayta? ¿Sapa kutichu chayta ruwani?”, nispa.
Bibliaqa nin, pipis tiemponpi parlarejtin, ancha sumajpuni kasqanta (Pro. 15:23).
 6. ¿Imaraykú Diospa kamachisninta Satanás llauchhiyachiyta munan?
Sutʼinchariy.
6 Proverbios 24:10 nin: “Sichus ima llakiypi rikhurispa llawchʼiyawaj chayqa, ajinamanta rikuchinki kallpayki pisi kasqanta”, nispa.
Satanasqa yachan, llauchhiyachiwajtinchejqa Jehovamanta karunchakunanchejta.
Satanasqa Jobta sinchʼita ñakʼarichispa llauchhiyachiyta munarqa, nitaj aterqachu.
Jobqa Diosman cheqa sonqollapuni karqa (Job 2:3; 22:3; 27:5).
Noqanchejpis Satanasta atipayta atisunman. ¿Imaynamantá?
Familianchejta, congregacionmanta hermanosninchejtapis kallpachaspa.
Chayta ruwaspaqa purajmanta yanapanakusun kusisqa kanapaj,
Diospa qayllallanpipuni kanapaj ima.



¿PIKUNAMANTÁ YACHAKUSUMAN?

7, 8. 1) ¿Imaynatá Jehová kamachisninta kallpachan? 2) ¿Imaynatá tatas Jehová jina ruwankuman? (4 paginapi dibujota qhawariy).
7 Jehovaqa kamachisninta kallpachan.
Salmista qhelqarqa: “Tata Diosqa qayllallapi kashan salvanampaj juchallisqankumanta sonqonkuta nanachikojkunata, pikunachus manaña salvasqa kayta suyankuchu, chaykunata”, nispa (Sal. 34:18).
Profeta Jeremías mancharisqa, llakisqataj kashajtin,
Jehová yanapananta nerqa (Jer. 1:6-10). Machuyasqa profeta Danielta kallpachananpajtaj uj angelta kacharqa.
Angeltaj nerqa Diospa “may munasqa kamachin” kasqanta (Dan. 10:8, 11, 18, 19).
Qanrí, ¿wajkunata kallpachariwajchu?
Precursoresta, kuraj hermanitostapis kallpachariwaj.
Paykunaqa niña ñaupajta jinachu ruwayta atinku.
8 Jehovawan Jesuswanqa may chhika unayta khuska karqanku.
Jinapis Jesús Jallpʼaman jamojtin,
Jehovaqa Wawanta kallpachallarqapuni.
Jesusqa Tatanta iskay kutita cielomanta parlamojta uyarerqa: ujtaqa predicayta qallarishajtin, ujtataj wañupunan watapi.
Chay kutispi Jehová nerqa: “Kaymin ancha munasqa Chureyqa.
Paywan sonqo juntʼasqa kani”, nispa (Mat. 3:17; 17:5).
Chayta uyarispa Jesusqa maytachá kusikorqa.
Aswan qhepaman payqa wañupunan qayllata llakisqa kasharqa.
Jehovataj uj angelta kacharqa Jesusta kallpachananpaj, sonqochananpajtaj (Luc. 22:43).
Tatas, qankunapis Jehová jina wawasniykichejta kallpachaychej.
Imatapis allinta ruwajtinku, “sumajta ruwanki” niychej.
Escuelapi chayri colegiopi chʼampaypi kajtinkutaj, astawan yanapaychej, kallpachariychejtaj.
 9. ¿Imatá yachakusunman Jesús imaynatachus apostolesninta kallpachasqanmanta?
9 Jesusmantapis yachakullasunmantaj.
Wañuchinankupaj japʼerqanku chay chʼisi,
Jesusqa apostolesninpa chakisninta mayllarqa, llampʼu sonqos kanankuta yachachinanpaj.
Paykunarí mana llampʼu sonqoschu karqanku, astawanpis siminakorqanku mayqenninkuchus aswan kurajpaj qhawasqa kanankumanta.
Pedropis Jesusta ni jaykʼaj saqerparinanta nispa jatunchakorqa (Luc. 22:24, 33, 34).
Jinapis Jesusqa mana apostolesninpa pantasqasninkutachu qhawarqa.
Astawanqa payllawanpuni kasqankumanta, “allinta ruwasqankuta” nerqa.
Chantá nillarqataj paymanta nisqa “astawanraj ruwa[nankuta]”, paykunata Jehová munakusqanta ima (Luc. 22:28;
Juan 14:12; 16:27).
Noqanchejrí wajkunaj pantasqankuta qhawaranamanta, ¿Jesús jinachu allin imasta ruwasqankumanta “sumajta ruwanki” ninchej?
10, 11. 1) ¿Imaynatá Pablo hermanosta kallpacharqa? 2) Hermanosta kallpachananpaj, ¿imastá ruwarqa?
10 Apóstol Pabloqa allintapuni hermanosmanta parlarqa.
Wakin hermanostaqa sumajta rejserqa, paykunawan unayta viajasqanrayku.
Jinapis mana pantasqasninkumantachu qhelqarqa.
Astawanpis allin imasta ruwasqankumanta parlarqa.
Payqa Timoteomanta nerqa, “kasukoj waway jina” nispa.
Chantapis nerqa hermanospa allinninkutapuni maskʼasqanta (1 Cor. 4:17;
Fili. 2:19, 20).
Titomantapis Corinto congregacionman nerqa:
Payqa kausaqey, “yanapawajniytaj, qankunata yanapanaykupaj” (2 Cor. 8:23).
Chayta yachaspa,
Timoteowan Titowanqa maytachá kusikorqanku.
11 Pablowan Bernabewanqa hermanosta kallpachanankurayku, kausayninkutapis wañuy pataman churarqanku.
Paykunataqa Listramanta runas wañuchiyta munarqanku.
Jinapis chayman kutiykuspa, mosoj cristianosta kallpacharqanku creesqankupi sinchʼita sayanankupaj (Hech. 14:19-22).
Aswan qhepaman Pablo Éfeso llajtapi kashajtin, runasqa “chʼajwata jataricherqanku”.
Jinapis payqa chaypi qhepakorqa hermanosta kallpachananpaj.
Hechos 20:1, 2 nin: “Chʼajwa pasayta Pablo wajyacherqa creejkunata.
Paykunata sonqochaykuspa, ojllaykuytawantaj Macedoniaman kachaykukorqa.
Llajtamanta llajta rispataj, hermanosta watukorqa sonqochaspa Diospa Palabranwan,
Greciaman chayanankama”, nispa.



PURAJMANTA KALLPACHANAKUNA

12. ¿Imaraykutaj tantakuykunaman rinanchej tiyan?
12 Jehovaqa allinninchejta munan.
Chayrayku hermanosninchejwan tantakunanchejta kamachiwanchej.
Tantakuykunapeqa Jehovamanta yachakunchej, purajmantataj kallpachanakunchej (1 Cor. 14:31; Hebreos 10:24, 25 leey). Ñaupaj parrafopi Cristinamanta parlarerqanchej.
Pay nin: “Tantakuykunapi hermanos munakuyninkuta rikuchiwasqanku, kallpachariwasqanku ima mayta kusichiwan.
Wakin kutisqa tantakuyman llakisqa chayani.
Jinapis hermanasqa abrazaykuwanku, kʼachita kashasqaytataj niwanku.
Chantá munakuwasqankuta niwanku.
Diospa llajtanpi ñauparishasqaymantapis kusikunku.
Ajinata kallpachariwasqankoqa, mayta sonqochawan”, nispa.
Purajmanta kallpachanakoyqa, ¡may sumajpuni! (Rom. 1:11, 12).
13. ¿Imaraykutaj unaytaña Jehovata sirvejkunapis kallpacharinata necesitanku?
13 Jehovata unaytaña sirvejkunapis kallpachanata necesitanku.
Josuemanta parlarina.
Israelitas Sumaj Jallpʼaman yaykunankupajña kashajtinku,
Jehovaqa Josueta ajllarqa israelitasta chayman pusaykunanpaj.
Josué unaytaña Jehovata yupaychajtinpis,
Jehovaqa Moisesta nerqa Josueta kallpachananta.
Pay nerqa: “Josueman sumajta yachachiy, sonqochay, kallpachaytaj, imaraykuchus paymin israelitasta pusanqa.
Payqa jallpʼasta rakʼenqa paykunaman, ima jallpʼatachus kunan rikumunki, chayta”, nispa (Deu. 3:27, 28).
Josueqa kallpachasqa kanan karqa, imaraykuchus aswan qhepaman israelitasqa waj nacioneswan maqanakorqanku, uj kutitaj atipachikorqanku (Jos. 7:1-9).
Kunanpis noqanchejqa ancianosta, congregacionesta waturikoj hermanostapis kallpacharisunman.
Paykunaqa Diospa llajtanpaj sinchʼita llankʼashanku (1 Tesalonicenses 5:12, 13 leey). Congregacionesta waturej hermano nerqa: “Wakin kutis hermanosqa cartasta jaywawayku waturikusqaykuta agradecekuspa.
Chay cartasta jallchʼakuyta yachayku, llakisqa kashaspataj leeriyku.
Chayqa sumajta kallpachawayku”, nispa.





Wawasta “allinta ruwanki” nispaqa,
Jehovaman qayllaykunankupaj yanapanchej. (14 parrafota qhawariy).




14. Pitapis yuyaycharishaspa, ¿imaraykutaj kallpacharina tiyan?
14 Uj kuti, apóstol Pabloqa Corintomanta hermanosta yuyaycharqa.
Kasukojtinkutaj sumajta ruwasqankuta nerqa (2 Cor. 7:8-11).
Chay nisqanqa cristianosta kallpacharqa allin kajta ruwanallankupajpuni.
Ancianospis tataspis,
Pablo jina ruwallankumantaj.
Andreasqa iskay wawasniyoj.
Pay nin: “Wawasninchejta ‘allinta ruwanki’ nispaqa, yanapanchej Diosman astawan qayllaykunankupaj, wiñay tukusqataj kanankupaj.
Paykunaqa kallpacharejtinchej yuyaychasqata astawan japʼikunku.
Allin kajta yachajtinkupis, kallpachasqa kaspalla chayta ruwayman yachakunku”, nispa.



¿IMATAJ YANAPAWASUNMAN WAJKUNATA KALLPACHANAPAJ?

15. ¿Imaynatá wajkunata kallpacharisunman?
15 Hermanos kallpachakusqankuta, kʼacha kasqankutapis sumajpaj qhawasqanchejta rikuchina (2 Cró. 16:9;
Job 1:8).
Chayta ruwaspaqa Jehová jina,
Jesús jinataj ruwashanchej.
Paykunaqa imastachus paykunapaj ruwasqanchejta jatunpaj qhawanku, mana munasqanchejman jinapunichu ruwajtinchejpis (Lucas 21:1-4; 2 Corintios 8:12 leey). Sutʼincharinapaj, kuraj hermanosninchejqa mayta kallpachakunku predicamunankupaj, tantakuykunaman rinankupajpis. ¿Paykunata kallpacharinchejchu “allinta ruwanki” nispa?
16. ¿Maykʼajtaj wajkunata kallpacharisunman?
16 Wajkunata sapa kuti kallpacharina. Pillatapis sumajta ruwasqanta rikuspaqa, “allinta ruwanki” ninapuni.
Pablowan Bernabewan Pisidiapi kaj Antioquiaman rejtinku, sinagogapi kamachejkunaqa paykunata nerqanku: “Wawqekuna, sichus imallatapis niyta munankichej kay runasta kallpacharinaykichejpaj chayqa, kunan parlayta atinkichej”, nispa.
Pablotaj runasta kallpacharerqa (Hech. 13:13-16, 42-44).
Wajkunata kallpachajtinchejqa, ichá paykunapis kallpachallawasuntaj (Luc. 6:38).
17. ¿Imaynatá wajkunata aswan sumajta kallpachasunman?
17 Imatachus allinta ruwasqanta sutʼita nina. Jesusqa Tiatira congregacionta kallpacharishaspa, imatachus allinta ruwasqankuta sutʼita nerqa (Apocalipsis 2:18, 19 leey). ¿Imaynatá Jesús jina ruwasunman?
Uj warmisapa wawitasninta uywananpaj kallpachakushasqanmanta, “allinta ruwanki” nisunman.
Tata chayri mama kanki chayqa, wawasniyki Jehovata sirvinankupaj kallpachakusqankuta sumajpaj qhawasqaykita niy.
Wawasniykiman sutʼita niy imastachus allinta ruwashasqankuta.
Wajkunaman imatachus allinta ruwasqankuta sutʼita nejtinchejqa, kallpachasqa kanqanku.
18, 19. ¿Imaynatá yanapanakusunman Jehovaj qayllallanpipuni kanapaj?
18 Jehovaqa Moisesta kamacherqa Josueta kallpachananta.
Kay tiempopi,
Jehovaqa mana Moisesta jinachu wajkunata kallpachananchejta kamachiwasun.
Chaywanpis Jehovaqa mayta kusikun, wajkunata “allinta ruwanki” ninapaj kallpachakojtinchej (Pro. 19:17;
Heb. 12:12).
Uj hermano congregacionninchejpi umallirichejtenqa, sumajta parlarimusqanta nisunman.
Ichapis parlarimusqanqa, llakiyninchejta atipanapaj chayri Bibliamanta uj versiculota entiendenapaj yanapawarqanchej.
Uj hermanaqa, uj hermanoman ajinata qhelqarqa: “Pisillata parlarejtinchejpis, qanqa llakiyniytapis yachawaj jina sonqochawarqanki, kallpachawarqankitaj.
Umallirichishaspa, noqawan parlarishaspapis imastachus nisqaykeqa,
Jehovamanta uj regalo jina karqa”, nispa.
19 ¿Imaynatá yanapanakusunman Jehovaj qayllallanpipuni kanapaj?
Pabloj yuyaychasqanta kasukuna.
Pay nerqa: “Qankunapura sonqochanakuychej, jinallataj kallpachanakuychej, imaynatachus ruwashankichej, ajinata” (1 Tes. 5:11). “Sapa pʼunchay purajmanta kallpachanaku[sun]” chayqa,
Jehovata mayta kusichisunchej.



1 párrafo: [1] Wakin sutisqa mana chaychu.




    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Quechuapi publicaciones (2004-2017)
			
		

		
			
				
				Allinchanapaj
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Kay kamachiykunata kasukuy
			
			
				Datosta apaykachakusqanmanta
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



