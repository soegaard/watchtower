¿Wiñaypaj kausakuyta munawajchu?
Chayta uyarispa ashkha runasqa mana wañuyta munaspapis, wañunapuni kasqanta ninkuman.
Chantapis runasqa ninku wañunapajpuni ruwasqa kasqanchejta.
¿Wañunapaj wakichisqachu kashanki?
Chayta uyarispataj ashkha runasqa ninkuman ni imarayku.
Imajtinchus runasqa kausaytapuni munanchej, ima chʼampay, llakiy kajtinpis.
Bibliaqa yachachin,
Dios kausayta munanankupaj jina runasta ruwasqanta.
Astawanpis nin “runaj sonqonman wiñay kasqanta” churasqanta (Eclesiastés 3:11).
Chaywanpis runasqa mana wiñaypajchu kausanchej. ¿Imataj mana allinchu karqa? ¿Imatá Dios ruwarqa chayta allinchananpaj?
Bibliaqa chayta sutʼinchan, nillantaj imaraykuchus Jesús ñakʼarisqanta, wañusqanta ima.



¿IMATAJ MANA ALLINCHU KARQA?

Bibliapi Génesis libroj 1-3 capitulosninpi nin Diosqa Adanman Evamanwan wiñaypaj kausanankuta, chaypajtaj imatachus ruwanankuta nisqanta.
Chantapis nin paykuna Diosta mana kasukusqankuta, jinamanta wiñay kausayta chinkachisqankuta.
Chaytataj wakenqa cuentolla kasqanta ninku.
Chaywanpis Génesis libropi nisqan,
Mateo libromanta Juan librokama nisqanpis cheqapuni.*
¿Imataj Adán mana kasukusqanrayku karqa?
Biblia jinata nin: “Uj runanejta jucha kay pachaman yaykumorqa, jucharaykutaj wañuypis tiyan.
Ajinamanta wañoyqa tukuy runasman chayarqa, tukuyninku juchallikusqankurayku”, nispa (Romanos 5:12).
Adanqa Diosta mana kasukusqanrayku juchallikorqa.
Chayrayku manaña wiñaypaj kausayta aterqachu, tiempo pasasqanman jina machuyarqa, wañorqataj.
Mirayninmanta kasqanchejraykutaj juchasapas kanchej, onqonchej, machuyanchej, wañunchejtaj.
Ajinamanta payman rijchʼakunchej, imaynatachus wawas tatasninkuman rijchʼakunku ajinata.
Chayrayku chayta allinchananpaj, ¿imatá Dios ruwarqa?



¿IMATÁ DIOS RUWARQA?

¿Imatá Adanpa miraynin wiñaypaj kausakunankupaj Dios ruwarqa?
Romanos 6:23 nin: “Juchaj pagonqa wañuymin”, nispa.
Kaytaj niyta munan jucharayku runas wañusqankuta.
Adanqa juchallikusqanrayku wañorqa.
Noqanchejpis juchasapas kanchej chayraykutaj wañunchej.
Chaywanpis noqanchejqa juchayojña nacekorqanchej.
Diostaj khuyakuwasqanchejrayku Wawan Jesucristota kachamorqa juchanchejmanta pagananpaj. ¿Imaraykú?





Jesús wañusqanrayku tukuypis uj sumaj kausayta suyakushanchej




Adanqa mana juchayoj karqa, jinapis mana kasukusqanrayku tukuy runas juchasapas kanchej, wañunchejtaj.
Chayrayku juchamanta kacharichinawanchejpajqa mana juchayoj, kasukoj runallataj wañunan karqa.
Bibliaqa jinata sutʼinchan: “Uj runa mana kasukusqanrayku tukuy runas juchayoj rikhurerqanku.
Ajinallatataj uj runa kasukusqanrayku ashkhas cheqan runapaj qhawasqa kanqanku”, nispa (Romanos 5:19).
Chay runaqa Jesús karqa.
Cielomanta uraykamorqa, mana juchayojtaj karqa*, noqanchejraykutaj wañorqa.
Chayrayku Diospa ñaupaqenpi cheqan runaspaj qhawasqa kayta atinchej, wiñay kausayta suyakuytataj.



¿IMARAYKUTAJ JESÚS ÑAKʼARISPA WAÑORQA?

Jesusqa wañunanpuni karqa.
Diosqa niyta atinman karqa Adanpa miraynin wiñaypaj kausayta atisqanta.
Chayta ninanpajtaj tukuy atiyniyoj.
Chaywanpis chayta nispaqa, mana nisqanta juntʼashanmanchu karqa.
Imatachus nisqantaqa juntʼananpuni karqa.
Imajtinchus Diosqa cheqan kajta munakun (Salmo 37:28).
Dios chay kutipi mana cheqan kajman jinachu ruwanman karqa chayqa, runasqa manaña creenkumanchu karqa cheqan kajta ruwananpi.
Chayrayku Adanpa mirayninmanta pikunachus wiñay kausayta japʼiyta atisqankuta nispa, ¿cheqan kajman jinachu ruwanman karqa? ¿Nisqasninta juntʼanmanchu?
Diosqa salvanawanchejpaj cheqan kajman jina ruwaspa, chayman jinapuni ruwananta rikuchiwanchej.
Diosqa,
Jesuspa wañuyninrayku runasman Jallpʼapi wiñay kausayta qoyta atin.
Jesucristo nerqa: “Diosqa chay jinatapuni kay pachapi kaj chhikata munakusqanrayku, uj kʼata Churinta qorqa.
Ajinamanta pillapis paypi creejqa mana chinkayman renqachu, astawanqa wiñay kawsayniyoj kanqa”, nispa (Juan 3:16).
Jesuspa wañuynenqa rikuchin Dios cheqan kajman jina ruwasqanta, tukuy runastataj mayta munakusqanta.
Chaywanpis, ¿imaraykú Jesús mayta ñakʼarispa wañunan karqa?
Jesusqa chay jinapi rikukuspa, cheqa sonqollapunitaj kaspa,
Kuraj Supay llulla kasqanta rikucherqa.
Imajtinchus Kuraj Supayqa nerqa runa kausayninrayku imatapis qonanpaj jinalla kasqanta (Job 2:4, 5).
Adán juchallikusqanmantawan,
Satanaspa nisqan cheqapuni kasqanman rijchʼakorqa.
Chaywanpis Jesusqa mayta ñakʼarispapis kasukullarqapuni, payqa Adán jina mana juchayojllataj karqa (1 Corintios 15:45).
Ajinamanta Jesusqa rikucherqa,
Adanpis Diosta kasukuyta atisqanta.
Chantapis Jesuspa ñakʼarisqanmantaqa mayta yachakusunman (1 Pedro 2:21).
Jesusqa Diosta kasukusqanrayku cielopi wiñay kausayta japʼerqa.



¿IMAYNATÁ YANAPAWANCHEJ?

Jesusqa wañorqapuni.
Chayraykutaj tukuypis uj sumaj kausayta suyakushanchej.
Qanrí, ¿wiñaypaj kausakuyta munawajchu?
Jesusqa imatachus chaypaj ruwananchejta jinata nerqa: “Kaymin chay wiñay kawsayqa:
Qan kʼata cheqa Diosta rejsisunanku, kachamusqayki Jesucristotapis”, nispa (Juan 17:3).
Kay revistata orqhojkunaqa Jehovamanta,
Wawan Jesucristomanta ima astawan yachakunaykita munayku.
Maypichus tiyakunki chaypi kaj Jehovaj testigosnin tukuy sonqo yanaparisonqanku.
Chayri Internetpi www.jw.org nisqaman yaykuspa astawan yachakullawajtaj.



Kay Perspicacia para comprender las Escrituras, volumen 1, página 1017 nisqapi, “Carácter histórico de Génesis” nisqata leeriy.
Kaytaqa Jehovaj testigosnin orqhonku.
Diosqa, espíritu santonnejta Wawanpa kausayninta Mariaj wijsanman churarqa.
Chantapis wawita mana juchayoj kananpaj jarkʼarqa (Lucas 1:31, 35).






‘Kayta ruwallaychejpuni’
Jesusqa wañupunan pʼunchaymanta ñaupaj chʼisinpi, apostolesninman wañuyninta yuyarikunankuta kamacherqa.
Nerqataj: “Kayta ruwaychej noqamanta yuyarikuspa”, nispa (Lucas 22:19).
Jehovaj testigosnenqa Jesuspa wañuyninta yuyarinaykupaj, sapa wata uj tantakuyta ruwayku.
Qayna wata chay tantakuyman 19.862.783 runas rerqanku.
Kay wataqa,
Jesuspa wañuyninta miércoles 23 de marzo inti yaykusqantawan yuyarisqayku.
Chay sumaj tantakuyman qanta, familiaykita, amigosniykita ima wajyariyku, uj umallirichiyta uyarinaykichejpaj.
Chaypeqa Jesuspa wañuynin imaynatachus yanapawasqanchejta sutʼinchakonqa.
Yaykunapajqa mana paganachu.
Maypichus, ima horachus, chay tantakuy ruwakunanta yachayta munaspaqa,
Jehovaj testigosninta tapuriy chayri Internetpi kay www.jw.org nisqaman yaykuwaj.





    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Quechuapi publicaciones (2004-2017)
			
		

		
			
				
				Allinchanapaj
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Kay kamachiykunata kasukuy
			
			
				Datosta apaykachakusqanmanta
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



