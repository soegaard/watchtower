Würden Sie gerne ewig leben?
Den meisten gefällt dieser Gedanke vielleicht schon, aber er kommt ihnen völlig abwegig vor.
Für sie gehört der Tod einfach zum Leben dazu.
Sie sehen ihn als das natürliche Ende unseres Daseins.
Aber stellen wir die Frage doch einmal anders: „Wann würden Sie sterben wollen?“ Unter normalen Umständen würde wohl keiner einen Zeitpunkt festlegen wollen.
Der Punkt ist:
Wir haben den natürlichen Wunsch zu leben, und das trotz vieler Härten und Probleme.
Wie aus der Bibel hervorgeht, hat Gott den Menschen mit dem Wunsch und Willen zu leben erschaffen — „er hat sogar die Ewigkeit in die Herzen der Menschen gelegt“ (Prediger 3:11, Begegnung fürs Leben).
Die Realität sieht allerdings ganz anders aus:
Kein Mensch lebt ewig.
Was ist passiert?
Und hat Gott irgendetwas unternommen, um die Sache wieder in Ordnung zu bringen?
Die Antworten aus der Bibel lassen aufatmen.
Und sie haben direkt mit Jesu Leidensweg und Tod zu tun.



WAS PASSIERT IST

Die ersten drei Kapitel der Bibel geben Aufschluss:
In der Genesis, oder dem 1. Buch Mose, kann man lesen, wie Gott den beiden ersten Menschen Adam und Eva ewiges Leben in Aussicht stellte, und er erklärte ihnen auch die Voraussetzungen dafür.
Allerdings wurden sie Gott ungehorsam und verloren diese Aussicht.
Die Geschichte klingt ziemlich simpel — so simpel, dass sie oft als Märchen abgetan wird.
Doch wie die Evangelien weist auch das 1. Buch Mose alle Merkmale eines historischen Tatsachenberichts auf.*
Was war die Folge von Adams Ungehorsam?
Die Bibel sagt, dass „durch e i n e n Menschen [Adam] die Sünde in die Welt gekommen ist und durch die Sünde der Tod und sich so der Tod zu allen Menschen verbreitet hat, weil sie alle gesündigt hatten“ (Römer 5:12).
Durch seinen Ungehorsam gegenüber Gott sündigte Adam.
So verlor er die Aussicht, ewig leben zu können, und starb schließlich.
Als seine Nachkommen haben wir diesen sündigen Zustand geerbt.
Die Folge:
Wir werden krank, alt und sterben.
Diese Erklärung deckt sich mit dem, was man heute über die Vererbung weiß.
Aber hat Gott auch etwas getan, um das alles wieder in Ordnung zu bringen?



WAS GOTT GETAN HAT

Gott hat alles Nötige in die Wege geleitet, um das zurückzukaufen, was Adam für seine Nachkommen verspielt hatte: die Möglichkeit, ewig zu leben.
Wie ist er vorgegangen?
Die Bibel erklärt in Römer 6:23: „Der Lohn, den die Sünde zahlt, ist der Tod.“ Das heißt, dass der Tod die Folge von Sünde ist.
Adam sündigte, also starb er.
Wir sündigen und erhalten ebenfalls den „Lohn“ beziehungsweise die Strafe für Sünde — den Tod.
Dafür können wir allerdings nichts, wir wurden in diesen Zustand hineingeboren.
Deshalb sandte Gott aus Liebe seinen Sohn Jesus mit dem Auftrag auf die Erde, an unserer Stelle die Strafe für Sünde zu übernehmen.
Aber wie sollte das funktionieren?





Durch Jesu Tod steht einem glücklichen, ewigen Leben nichts mehr im Weg




Als Adam durch seinen Ungehorsam Sünde und Tod über uns brachte, war er ein vollkommener Mensch — fähig, keine Fehler zu machen.
Um die Sünden der Menschheit zu begleichen, bräuchte es also wieder einen vollkommenen Menschen, der aber bis in den Tod gehorsam wäre.
Die Bibel erklärt das so: „Weil ein Einziger ungehorsam war, sind alle zu Sündern geworden.
Ebenso werden alle vor Gott zu Gerechten, weil der eine gehorsam war“ (Römer 5:19, Gute Nachricht Bibel). „Der eine“ war Jesus.
Er verließ den Himmel, wurde ein vollkommener Mensch* und starb für uns.
Dadurch wurde für uns ein gutes Verhältnis zu Gott und ewiges Leben möglich.



WARUM SO UND NICHT ANDERS?

Aber warum musste Jesus unbedingt sterben?
Gott ist doch allmächtig.
Hätte er nicht einfach bestimmen können, dass Adams Nachkommen ewig leben dürfen?
Die Macht und das Recht dazu hatte er auf alle Fälle.
Aber dann hätte er gegen sein eigenes, klar formuliertes Gesetz verstoßen, dass Sünde den Tod zur Folge hat.
Dieses Gesetz ist nicht irgendeine unbedeutende Regel, die man nach Belieben ändern oder ignorieren kann, sondern grundlegend für wahre Gerechtigkeit (Psalm 37:28).
Denn hätte Gott in dieser Situation sein Gesetz beiseitegeschoben, hätte man schlussfolgern können, dass er es wieder tun würde.
Würde er zum Beispiel gerecht entscheiden, wenn es darum geht, wer von Adams Nachkommen das ewige Leben verdient?
Könnte man Gottes Versprechen wirklich vertrauen?
Als er für unsere Rettung sorgte, ist er absolut gerecht vorgegangen.
Das gibt uns die Garantie, dass er immer das Richtige tun wird.
Durch den Tod Jesu ermöglicht Gott uns, einmal ewig im Paradies auf der Erde zu leben.
In Johannes 3:16 sagte Jesus: „So sehr hat Gott die Welt geliebt, dass er seinen einziggezeugten Sohn gab, damit jeder, der Glauben an ihn ausübt, nicht vernichtet werde, sondern ewiges Leben habe.“ Jesu Tod beweist also nicht nur, wie vollkommen gerecht Gott ist, sondern vor allem auch, wie sehr er uns Menschen liebt.
Aber warum musste Jesus auf so eine grausame Art sterben?
Der Grund:
Der Teufel hatte behauptet, dass kein Mensch treu zu Gott stehen würde, wenn er Schweres durchmachen müsste (Hiob 2:4, 5).
Nachdem er den vollkommenen Adam dazu gebracht hatte zu sündigen, schien er damit Recht zu haben.
Aber Jesus — genauso vollkommen wie Adam — blieb treu, obwohl er unvorstellbare Qualen durchmachte (1. Korinther 15:45).
Weil er bereit war, diese extreme Prüfung auf sich zu nehmen, und sie auch bestand, widerlegte er die Behauptung des Teufels ein für alle Mal.
Wenn Adam gewollt hätte, wäre also auch er in der Lage gewesen,
Gott treu zu sein.
Und uns hat Jesus dadurch gezeigt, wie auch wir in schweren Zeiten durchhalten können (1. Petrus 2:21).
Gott belohnte seinen Sohn für seinen absoluten Gehorsam:
Er schenkte ihm unvergängliches Leben im Himmel.



WAS DAS FÜR UNS BEDEUTEN KANN

Jesus ist wirklich gestorben.
Dem ewigen Leben steht nichts mehr im Weg.
Würden Sie gerne ewig leben?
Jesus erklärte, was man dafür tun muss:
Den allein wahren Gott kennenlernen und auch den, den er zur Erde geschickt hat,
Jesus Christus (Johannes 17:3).
Die Herausgeber dieser Zeitschrift laden Sie ein, den wahren Gott Jehova und seinen Sohn Jesus Christus besser kennenzulernen.
Jehovas Zeugen bieten dabei gern ihre Hilfe an.
Weitere Informationen finden Sie auch auf unserer Website www.jw.org.



Siehe „Der historische Charakter der Genesis“ in Einsichten über die Heilige Schrift, Band 1,
Seite 865 (herausgegeben von Jehovas Zeugen).
Jesus konnte als Mensch geboren werden, weil Gott das Leben seines Sohnes in den Mutterleib Marias übertrug.
Durch seinen heiligen Geist schützte er Jesus davor, von Maria Sünde und Unvollkommenheit zu erben (Lukas 1:31, 35).






„Tut dies immer wieder“
In der Nacht bevor Jesus starb, kam er mit seinen treuen Aposteln zusammen und führte eine Feier zum Gedenken an seinen Tod ein.
Er sagte zu ihnen: „Tut dies immer wieder zur Erinnerung an mich“ (Lukas 22:19).
Jehovas Zeugen halten sich auch heute noch daran und begehen diesen Jahrestag auf der ganzen Welt.
Letztes Jahr waren 19 862 783 anwesend.
Dieses Jahr findet das Abendmahl am Mittwoch, den 23. März, nach Sonnenuntergang statt.
Sie sind herzlich dazu eingeladen und können auch gern noch andere mitbringen.
Bei der Feier wird anhand der Bibel erklärt, warum Jesu Tod so bedeutend war und was das für uns persönlich heißt.
Der Eintritt ist frei und es wird kein Geld gesammelt.
Erkundigen Sie sich doch bei Jehovas Zeugen in Ihrer Nähe nach Ort und Zeit oder sehen Sie auf unserer Website www.jw.org nach.





    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Deutsche Publikationen (2000-2017)
			
		

		
			
				
				Einstellungen
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Nutzungsbedingungen
			
			
				Datenschutzerklärung
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



