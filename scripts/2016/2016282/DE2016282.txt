DIE israelitischen Soldaten unter dem Richter Gideon hatten das midianitische Heer und dessen Verbündete die ganze Nacht hindurch verfolgt — und das über 30 Kilometer weit!
Wie die Bibel berichtet, kam Gideon schließlich „an den Jordan und ging hinüber, er und die dreihundert Mann“.
Von der Schlacht waren sie alle müde und ausgelaugt.
Dennoch hatten sie den Krieg noch nicht gewonnen, denn es waren noch etwa 15 000 feindliche Soldaten übrig.
Nachdem die Israeliten jahrelang von den Midianitern unterdrückt worden waren, wussten sie, dass jetzt nicht die Zeit war aufzugeben.
Um ihre Feinde zu besiegen, setzten sie die Verfolgung fort, und so konnte Midian unterworfen werden (Ri. 7:22; 8:4, 10, 28).
2 Auch wir führen einen ständigen Kampf.
Zu unseren Feinden gehören der Teufel, seine Welt und unsere eigene Unvollkommenheit.
Etliche von uns kämpfen schon seit Jahrzehnten und mit Jehovas Hilfe haben sie viele Schlachten gewonnen.
Aber ständig gegen unsere Feinde ankämpfen und auf das Ende des gegenwärtigen Systems warten zu müssen, ermüdet uns mitunter.
Der Kampf ist aber noch nicht vorbei.
Jesus sagte ja voraus, dass alle, die in den letzten Tagen leben, schwere Prüfungen durchmachen und grausam behandelt werden würden.
Wenn wir aber ausharren, können wir siegen. (Lies Lukas 21:19.) Was ist mit Ausharren gemeint?
Was kann uns helfen auszuharren?
Was können wir von denen lernen, die ausgeharrt haben?
Und wie können wir „das Ausharren sein Werk“ vollenden lassen? (Jak. 1:4).



WAS IST MIT AUSHARREN GEMEINT?

 3. Was bedeutet Ausharren?
3 In der Bibel bedeutet Ausharren mehr, als Prüfungen oder Schwierigkeiten einfach hinzunehmen.
Es schließt auch unser Herz und unseren Sinn ein, also die Art, wie wir auf Belastungen reagieren.
Wer ausharrt, ist mutig, standhaft und geduldig.
Gemäß einem Nachschlagewerk ist Ausharren „der Geist, der Belastungen tragen kann, nicht in Resignation, sondern in strahlender Hoffnung. . . . [Ausharren] lässt einen Mann aufrecht stehen und dem Sturm trotzen.
Es ist die Tugend, die die härteste Prüfung in Sieg verwandeln kann, weil sie hinter der Pein das Ziel sieht.“
 4. Warum können wir sagen, dass Liebe der Motor für Ausharren ist?
4 Liebe ist der Motor für unser Ausharren. (Lies 1. Korinther 13:4, 7.) Aus Liebe zu Jehova stellen wir uns allem, was er zulässt (Luk. 22:41, 42).
Aus Liebe zu unseren Brüdern ertragen wir ihre Unvollkommenheiten (1. Pet. 4:8).
Und aus Liebe zu unserem Ehepartner stärken wir die gemeinsame Bindung und stehen „Drangsale“ durch, mit denen sogar glücklich Verheiratete zu kämpfen haben (1. Kor. 7:28).



WAS KANN UNS HELFEN AUSZUHARREN?

 5. Warum kann Jehova uns am besten helfen durchzuhalten?
5 Bitte Jehova um Kraft. Jehova ist „der Gott, der Ausharren und Trost verleiht“ (Röm. 15:5).
Keiner versteht unsere Probleme so gut wie er.
Und er versteht auch, wie uns unser Umfeld, unsere Gefühle oder sogar unsere Gene beeinflussen.
Niemand kann uns besser helfen durchzuhalten.
Die Bibel versichert uns:
Die Bitten „derer, die ihn fürchten, wird er ausführen, und ihren Hilferuf wird er hören, und er wird sie retten“ (Ps. 145:19).
Aber wie erhört Gott unsere Gebete um die nötige Kraft?
 6. Wie schafft Jehova für uns den Ausweg aus Prüfungen?
6 Lies 1. Korinther 10:13. Wenn wir Jehova darum bitten, mit Prüfungen zurechtzukommen, „wird er auch den Ausweg schaffen“.
Heißt das,
Jehova würde Prüfungen beseitigen?
Schon möglich.
Meistens sorgt er jedoch für den Ausweg, damit wir sie ertragen können.
Ja,
Jehova gibt uns die Kraft, „völlig auszuharren und mit Freuden langmütig zu sein“ (Kol. 1:11).
Und weil Jehova unsere körperlichen, mentalen und emotionalen Grenzen ganz genau kennt, lässt er keine Situation so schlimm werden, dass wir nicht treu bleiben könnten.
 7. Warum benötigen wir geistige Speise?
Veranschauliche es.
7 Stärke deinen Glauben mit geistiger Speise. Auf dem höchsten Berg der Erde, dem Mount Everest, verbraucht ein Bergsteiger etwa 6 000 Kalorien pro Tag.
Das ist erheblich mehr, als ein Durchschnittsmensch benötigt.
Ein Bergsteiger muss so viele Kalorien wie möglich aufnehmen, damit er den Aufstieg schafft und das Ziel erreicht.
Damit auch wir durchhalten und unser Ziel erreichen können, müssen wir ebenfalls regelmäßig so viel geistige Speise wie möglich zu uns nehmen.
Um Zeit für das Lesen,
Studieren und den Besuch der Zusammenkünfte zu haben, brauchen wir Selbstdisziplin.
So wird unser Glaube durch die „Speise, die für das ewige Leben bleibt“, gestärkt (Joh. 6:27).
 8, 9. (a)
Worum geht es gemäß Hiob 2:4, 5, wenn wir mit widrigen Umständen zu kämpfen haben? (b) Welche Szene könntest du dir vorstellen, wenn du mit Problemen zu kämpfen hast?
8 Denke an den Streitpunkt der unversehrten Lauterkeit. In einer Prüfung geht es für einen Diener Jehovas um viel mehr, als nur um das Leid, das er durchmacht.
Wie wir auf Prüfungen reagieren, offenbart nämlich, ob wir Jehova wirklich als universellen Souverän anerkennen.
Satan, der Jehovas Souveränität bekämpft, hat ihn mit den Worten verspottet: „Alles, was ein Mensch hat, wird er für seine Seele geben.
Strecke zur Abwechslung doch deine Hand aus, und rühre an sein Gebein und sein Fleisch, und sieh, ob er dir nicht direkt ins Angesicht fluchen wird“ (Hiob 2:4, 5).
Satan hat behauptet, keiner würde Jehova aus selbstloser Liebe dienen.
Hat der Teufel seitdem seine Meinung geändert?
Ganz sicher nicht!
Als er Jahrtausende später aus dem Himmel geworfen wurde, war er immer noch „der Ankläger unserer Brüder . . . , der sie Tag und Nacht vor unserem Gott verklagt!“ (Offb. 12:10).
Satan hat den Streitpunkt der unversehrten Lauterkeit nicht vergessen.
Er will unbedingt erleben, dass wir in Prüfungen aufgeben und nicht mehr für Gottes Souveränität eintreten.
9 Solltest du also gegen widrige Umstände ankämpfen müssen, dann stell dir folgende Szene im unsichtbaren Bereich vor:
Auf der einen Seite stehen Satan und die Dämonen.
Sie zeigen darauf, wie du dich mit deinen Schwierigkeiten abkämpfst, und behaupten, du würdest dem Druck nachgeben und resignieren.
Auf der anderen Seite stehen Jehova, unser König Jesus Christus, die auferweckten Gesalbten und unzählige Engel.
Sie feuern dich an und sind begeistert, dass du Tag für Tag ausharrst und für Jehovas Souveränität eintrittst.
Jehova richtet an dich persönlich die Bitte: „Sei weise, mein Sohn, und erfreue mein Herz, damit ich dem, der mich höhnt, eine Antwort geben kann“ (Spr. 27:11).
10. Wie können wir Jesus in schwierigen Situationen nachahmen?
10 Konzentriere dich auf den Lohn für dein Ausharren. Angenommen, du befindest dich auf einer Reise.
In einem Tunnel musst du plötzlich anhalten.
Um dich herum ist alles dunkel.
Trotzdem bist du davon überzeugt, dass es am Ende des Tunnels wieder hell wird. Ähnlich verhält es sich auch mit unseren Problemen.
Manchmal fühlen wir uns von ihnen regelrecht erdrückt.
Sogar Jesus könnte solche Gefühle gehabt haben.
Er wurde von sündigen Menschen angefeindet, gedemütigt und an einem Marterpfahl grausam hingerichtet — zweifellos die dunkelste Zeit seines Lebens auf der Erde!
Dennoch ertrug er alles „für die vor ihm liegende Freude“ (Heb. 12:2, 3).
Er konzentrierte sich auf den Lohn für sein Ausharren, vor allem auf seinen Beitrag zur Heiligung des Namens Gottes und zur Rechtfertigung der Souveränität Jehovas.
Jesu Prüfungen waren für ihn zwar schwer, doch sie gingen vorüber.
Der Glanz seines Lohnes im Himmel hingegen würde ewig strahlen.
Was wir heute durchmachen, kann schmerzhaft und sogar erdrückend sein.
Aber vergessen wir nie:
Unsere Probleme auf dem Weg zum ewigen Leben sind nur vorübergehend!



SIE HARRTEN AUS

11. Warum sollten wir uns mit denen beschäftigen, „die ausgeharrt haben“?
11 Der Teufel legte den ersten Christen viele Steine in den Weg.
Deshalb machte der Apostel Petrus ihnen mit den Worten Mut: „Widersteht ihm, fest im Glauben, wissend, dass die gleichen Dinge in Bezug auf Leiden sich an eurer ganzen Bruderschaft in der Welt vollziehen“ (1. Pet. 5:9).
Wir sind also nicht allein, wenn es um das Ausharren geht.
Von denen, „die ausgeharrt haben“, lernen wir, wie wir standhaft bleiben können.
Außerdem wird durch sie deutlich, dass auch wir ausharren können und dass unsere Treue belohnt wird (Jak. 5:11).
Dazu einige Beispiele.[1]
12. Was lernen wir von den Cheruben, die den Zugang zum Garten Eden bewachen sollten?
12 Die Cherube. Aus ihren Reihen stammten die ersten Geistwesen, die für Menschen sichtbar wurden.
Von ihnen können wir lernen, trotz schwieriger Aufgaben auszuharren.
Jehova Gott „stellte im Osten des Gartens Eden die Cherube auf und die flammende Klinge eines sich fortwährend drehenden Schwertes, um den Weg zum Baum des Lebens zu bewachen“ (1. Mo. 3:24).[2] Natürlich wurden die Cherube ursprünglich nicht für diese Aufgabe erschaffen.
Schließlich gehörten die Sünde und die Rebellion nicht zu Jehovas Vorhaben für die Menschheit.
Dennoch lesen wir in der Bibel nicht, diese hochrangigen Geistwesen hätten sich darüber beschwert, für diese Aufgabe überqualifiziert zu sein.
Ihnen wurde weder langweilig noch gaben sie auf.
Sie erfüllten gehorsam ihre Aufgabe und hielten durch, bis ihr Auftrag erfüllt war.
Das war vielleicht erst über 1 600 Jahre später der Fall, als die Sintflut kam.
13. Was half Hiob, seine Prüfungen durchzustehen?
13 Hiob. Hat dich ein Freund oder Familienmitglied mit Worten verletzt?
Bist du schwer krank?
Oder trauerst du um einen geliebten Menschen, der gestorben ist?
Dann kann dich das Beispiel von Hiob trösten (Hiob 1:18, 19; 2:7, 9; 19:1-3).
Obwohl Hiob nicht verstand, warum er leiden musste, gab er nicht auf.
Aber warum verzweifelte er nicht?
Unter anderem, weil er gottesfürchtig war (Hiob 1:1).
Hiob wollte Jehova unter allen Umständen gefallen.
Gott regte Hiob dazu an, über die erstaunlichen Dinge nachzudenken, die Jehova durch seinen heiligen Geist bereits vollbracht hatte.
Dadurch war sich Hiob noch sicherer:
Jehova würde die Prüfung zum richtigen Zeitpunkt beenden (Hiob 42:1, 2).
Und genau das geschah auch.
Jehova wandte „für ihn alles zum Guten.
Er gab ihm doppelt so viel, wie er früher besessen hatte“.
Hiob starb schließlich „in hohem Alter nach einem . . . erfüllten Leben“ (Hiob 42:10, 17, Hoffnung für alle).
14. Was bewirkte das Ausharren von Paulus gemäß 2. Korinther 1:6?
14 Der Apostel Paulus. Erlebst du erbitterten Widerstand oder sogar Verfolgung durch Feinde der wahren Anbetung?
Bist du ein Ältester oder ein Kreisaufseher, dem die schwere Last der Verantwortung zu schaffen macht?
Dann denk über Paulus nach.
Er erlitt brutale Verfolgung und war durch die ständige Sorge um die Versammlungen stark belastet (2. Kor. 11:23-29). Dennoch kam es für ihn nicht infrage aufzugeben, wodurch er andere stärkte. (Lies 2. Korinther 1:6.) Wenn du also Schlimmes durchmachst, dann vergiss nicht, dass dein Beispiel andere wahrscheinlich ermuntert nicht aufzugeben.



VOLLENDET AUSHARREN SEIN WERK IN DIR?

15, 16. (a)
Welches „Werk“ muss das Ausharren vollenden? (b) Was kann durch Ausharren bewirkt werden?
Nenne Beispiele.
15 Jakobus schrieb unter Inspiration: „Lasst das Ausharren sein Werk vollständig haben.“ Was genau soll Ausharren in uns bewirken?
Dass wir „vollständig und in jeder Hinsicht gesund“ sind und es uns „an nichts fehlt“ (Jak. 1:4).
Prüfungen bringen oft Charakterschwächen zum Vorschein.
Wenn wir dann aber daran arbeiten und in den Prüfungen durchhalten, wird unsere christliche Persönlichkeit vollständiger oder „gesund“.
Dadurch können wir geduldiger, dankbarer und mitfühlender werden.





Wenn wir in Prüfungen durchhalten, wird unsere christliche Persönlichkeit vervollständigt (Siehe Absatz 15, 16)




16 Wenn wir ausharren, werden wir als Diener Jehovas weiter geformt, und das ist lebenswichtig.
Machen wir deshalb bei biblischen Grundsätzen keine Zugeständnisse, nur um eine Prüfung zu beenden.
Haben wir beispielsweise mit unreinen Gedanken zu kämpfen?
Dann geben wir der Versuchung nicht nach, sondern bitten Jehova um Hilfe, damit wir solche Gedanken zurückweisen können.
Auf diese Weise stärken wir unsere Selbstbeherrschung.
Oder müssen wir von jemandem aus der Familie, der kein Zeuge Jehovas ist,
Widerstand ertragen?
Dann geben wir dem Druck nicht nach, sondern sind entschlossen Jehova weiterhin zu dienen.
So stärken wir unser Vertrauen in Jehova.
Vergessen wir nicht:
Damit sich Gott über uns freuen kann, müssen wir ausharren (Röm. 5:3-5; Jak. 1:12).
17, 18. (a)
Warum ist es so wichtig, bis zum Ende auszuharren?
Veranschauliche es. (b) Wovon können wir überzeugt sein, je näher das Ende kommt?
17 Wir müssen nicht nur eine Zeit lang ausharren, sondern bis zum Ende. Hierzu ein Vergleich:
Angenommen, ein Schiff gerät in Seenot.
Um zu überleben, müssen die Passagiere ans Ufer schwimmen.
Wer aber kurz vor dem Ufer aufgibt, stirbt genauso wie derjenige, der schon viel früher aufgegeben hat.
So ähnlich hängt auch unser Überleben davon ab, dass wir entschlossen durchhalten, um die neue Welt zu erreichen.
Wir sind wie der Apostel Paulus eingestellt, der zweimal sagte:
Wir „lassen . . . nicht nach“ (2. Kor. 4:1, 16).
18 Wir können uns absolut sicher sein, dass Jehova uns hilft, damit wir bis zum Ende ausharren können.
Wie Paulus sind wir überzeugt: „Aus allen diesen Dingen gehen wir vollständig siegreich hervor durch ihn, der uns geliebt hat.
Denn ich bin überzeugt, dass weder Tod noch Leben noch Engel noch Regierungen noch Gegenwärtiges noch Zukünftiges noch Mächte noch Höhe noch Tiefe noch irgendeine andere Schöpfung imstande sein wird, uns von Gottes Liebe zu trennen, die in Christus Jesus ist, unserem Herrn“ (Röm. 8:37-39).
Ja, manchmal können wir wirklich ermüden.
Aber wenn wir bis zum Ende ausharren, kann über uns das Gleiche gesagt werden wie über Gideon und seine Männer:
Sie „setzten . . . die Verfolgung fort“ (Ri. 8:4).



[1] (Absatz 11)
Auch die Beispiele von Dienern Gottes, die in der heutigen Zeit treu geblieben sind, machen uns Mut.
Die Jahrbücher von 1992, 1999 und 2008 enthalten beispielsweise glaubensstärkende Berichte aus Äthiopien,
Malawi und Russland.
[2] (Absatz 12)
Die Bibel sagt nichts darüber, wie viele Cherube diese Aufgabe hatten.




    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Deutsche Publikationen (2000-2017)
			
		

		
			
				
				Einstellungen
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Nutzungsbedingungen
			
			
				Datenschutzerklärung
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



