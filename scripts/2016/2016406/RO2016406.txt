M-am născut în 1960.
Mama avea doar 17 ani, iar tata ne-a părăsit înainte ca eu să mă fi născut.
Eu și cu mama am locuit la bunici, în Burg, un orășel mic din fosta Republică Democrată Germană, sau Germania de Est.
La fel ca majoritatea oamenilor de aici, membrii familiei mele erau atei.
Pentru noi,
Dumnezeu nu însemna nimic.
Bunicul s-a ocupat mult de mine când eram mic.
El m-a implicat în diverse activități, de exemplu mă punea să mă urc în copaci ca să tai crengi cu ferăstrăul.
Copil fiind, îmi plăceau astfel de aventuri.
N-aveam nicio grijă și eram fericit.



UN ACCIDENT ÎMI SCHIMBĂ VIAȚA

Când aveam șapte ani mi s-a întâmplat ceva îngrozitor.
De-abia începusem clasa a doua. În drum spre casă m-am urcat pe un stâlp de înaltă tensiune.
Când am ajuns la 8 metri înălțime am fost electrocutat și mi-am pierdut cunoștința.
M-am trezit la spital, însă nu mai îmi simțeam brațele.
Suferisem arsuri severe, iar rănile erau atât de grave, încât a trebuit să-mi fie amputate ambele brațe pentru a nu face septicemie.
Așa cum vă puteți imagina, mama și bunicii au fost distruși. Însă eu eram copil și nu înțelegeam pe deplin cum avea să-mi fie afectată viața de pierderea brațelor.
După ce am fost externat m-am întors la școală.
Copiii râdeau de mine, mă împingeau și, fiindcă nu mă puteam apăra, aruncau cu lucruri în mine.
Cuvintele lor crude și jignitoare m-au rănit profund. În cele din urmă, am fost trimis la Școala Birkenwerder pentru Copii cu Dizabilități, o școală cu internat pentru persoane cu handicap fizic. Întrucât era departe de casă, mama și bunicii nu-și permiteau să mă viziteze, așa că îi vedeam doar în vacanțe. În următorii zece ani am crescut departe de familie.



CRESC FĂRĂ MÂINI

Am învățat să fac multe lucruri folosindu-mă de picioare.
Vă puteți imagina cum este să mănânci ținând lingura sau furculița cu degetele de la picioare?
Am reușit cumva să dezvolt această aptitudine.
De asemenea, am învățat să mă spăl pe dinți și să mă pieptăn folosindu-mi picioarele.
Am început chiar să gesticulez cu picioarele în timp ce vorbeam cu oamenii.
Da, picioarele mele au devenit mâinile mele.
În adolescență îmi plăcea literatura SF.
Uneori, îmi imaginam că am brațe robotizate care îmi permiteau să fac orice.
La 14 ani am început să fumez.
Simțeam că am mai multă încredere în mine și că sunt la fel ca ceilalți.
Era ca și cum spuneam: „Da, pot s-o fac și pe asta.
Fumătorii sunt adulți, cu sau fără brațe”.
Mi-am ocupat tot timpul implicându-mă în multe activități sociale.
Am devenit membru al Tineretului German Liber, o organizație socialistă de tineret, având funcția de secretar, o poziție de mare răspundere printre membrii din comunitate.
De asemenea, m-am înscris într-un club de muzică, am recitat poezii pe scenă și am participat la activități sportive pentru persoane cu dizabilități.
După ce am învățat o meserie m-am angajat la o întreprindere din oraș.
Am început să port proteză din ce în ce mai des, deoarece îmi doream să fiu un om întreg.



ÎMBRĂȚIȘEZ ADEVĂRUL BIBLIC

Într-o zi, în timp ce așteptam trenul ca să merg la muncă, un bărbat m-a întrebat dacă îmi puteam imagina cum ar fi ca Dumnezeu să-mi dea brațele înapoi.
Eram nedumerit.
Bineînțeles că îmi doream să am din nou brațe, dar acest lucru mi se părea imposibil!
Fiind ateu, eram convins că Dumnezeu nu există.
După acea conversație am început să-l evit pe bărbatul respectiv.
Mai târziu, o colegă de muncă m-a invitat la o cafea la ea acasă.
Părinții ei mi-au vorbit despre Iehova Dumnezeu.
Era prima dată când auzeam că Dumnezeu are un nume (Psalmul 83:18).
Totuși, am zis în sinea mea: „Dumnezeu nu există, indiferent ce nume ar avea.
Le voi dovedi acestor oameni că se înșală”.
Simțindu-mă stăpân pe convingerile mele, am fost de acord să discutăm pe baza Bibliei.
Spre surprinderea mea însă, nu am putut să demonstrez că Dumnezeu nu există.
Pe măsură ce analizam profețiile biblice, mi-am schimbat încetul cu încetul părerea cu privire la existența lui Dumnezeu.
Multe profeții divine s-au împlinit chiar dacă au fost scrise cu sute sau chiar cu mii de ani înainte. În timpul unei discuții am comparat condițiile mondiale actuale cu profețiile din Matei, capitolul 24, Luca, capitolul 21, și 2 Timotei, capitolul 3.
Așa cum mai multe simptome îl pot ajuta pe un medic să stabilească diagnosticul corect, tot așa diversele evenimente menționate în acele profeții m-au ajutat să înțeleg că trăim în perioada pe care Biblia o numește „zilele din urmă”.* Am fost foarte impresionat.
Acele profeții se împlineau chiar sub ochii mei!
Eram convins că învățam adevărul.
Am început să mă rog lui Iehova Dumnezeu și m-am lăsat de fumat, chiar dacă fumasem mai mult de zece ani.
Am continuat să studiez Biblia timp de aproape un an.
La 27 aprilie 1986 am fost botezat în secret într-o cadă de baie, deoarece activitatea Martorilor era interzisă în Germania de Est.



ÎI AJUT PE ALȚII

Din cauza interdicției ne întruneam în grupuri mici în locuințe particulare și cunoșteam puțini frați de credință. În mod surprinzător, autoritățile mi-au permis să călătoresc în Germania de Vest, unde Martorii nu erau interziși.
Pentru prima dată în viață puteam să asist la un congres și să văd mii de frați și de surori.
Aceasta a fost o experiență unică!
După căderea Zidului Berlinului, interdicția împotriva Martorilor a fost ridicată. În sfârșit, puteam să-i aducem închinare lui Iehova Dumnezeu în libertate.
Doream să-mi intensific activitatea de predicare.
Totuși, mă îngrozea ideea de a începe conversații cu persoane necunoscute.
Mă simțeam inferior din cauza handicapului meu și din cauză că îmi petrecusem copilăria într-o instituție pentru persoane cu dizabilități. Însă în 1992 am încercat să predic 60 de ore într-o lună.
Am reușit și am fost foarte fericit.
Așa că am hotărât să fac acest lucru în fiecare lună și am reușit să continui timp de aproximativ trei ani.
Mă gândesc mereu la următoarele cuvinte din Biblie: „Cine este slab, și eu să nu fiu slab?” (2 Corinteni 11:29). Încă am cap și gură, așa că pot să-i ajut pe alții în pofida dizabilității mele.
Deoarece nu am brațe îi înțeleg pe deplin pe cei care au anumite limite. Știu ce înseamnă să-ți dorești cu disperare ceva ce nu poți să faci.
Mă străduiesc să-i încurajez pe cei care sunt în această situație, iar faptul de a-i ajuta pe alții mă face fericit.





Când le împărtășesc altor oameni vestea bună sunt fericit







IEHOVA MĂ AJUTĂ ÎN FIECARE ZI






Trebuie să recunosc că uneori mă simt puțin descurajat.
Pur și simplu, mi-aș dori să fiu un om întreg.
Pot să fac singur multe lucruri, însă îmi ia mult mai mult timp, efort și energie decât îi ia în mod normal unei persoane obișnuite.
Deviza mea în fiecare zi este: „Pentru toate lucrurile am tărie datorită celui care îmi dă putere” (Filipeni 4:13).
Iehova îmi dă putere în fiecare zi ca să pot face lucruri „normale”.
Am simțit că Iehova nu a renunțat la mine.
De aceea nici eu nu vreau să renunț vreodată la el.
Iehova m-a binecuvântat cu o familie, exact lucrul care mi-a lipsit în copilărie și în tinerețe.
Elke este o soție minunată, iubitoare și plină de compasiune. În plus, milioane de Martori ai lui Iehova mi-au devenit frați și surori spirituale, o adevărată familie mondială.





Alături de iubita mea soție,
Elke




Promisiunea lui Dumnezeu cu privire la Paradis îmi aduce multă mângâiere. Știu că atunci Dumnezeu va face „toate lucrurile noi”, inclusiv brațele mele (Revelația 21:5).
Această promisiune devine mai reală pentru mine când meditez la ceea ce a făcut Isus în timpul vieții sale pământești.
El a vindecat în mod miraculos membre bolnave și chiar a vindecat un om a cărui ureche fusese tăiată (Matei 12:13; Luca 22:50, 51).
Promisiunile lui Iehova și miracolele înfăptuite de Isus îmi dau convingerea că, în curând, voi avea din nou brațe.
Însă cea mai mare binecuvântare este faptul că am ajuns să-l cunosc pe Iehova Dumnezeu.
El a devenit tatăl meu și prietenul meu, cel care mă mângâie și îmi dă putere.
Mă simt la fel ca regele David, care a scris: „Iehova este puterea [mea]. . . . [El m-a] ajutat, de aceea inima mea exultă” (Psalmul 28:7).
Vreau să-mi amintesc mereu acest adevăr minunat.
Pot spune pe bună dreptate că am „îmbrățișat” adevărul biblic chiar și fără mâini!



Pentru o analiză detaliată a semnului zilelor din urmă, vezi capitolul 9, „Trăim în «zilele din urmă»?”, din cartea Ce ne învață în realitate Biblia?, publicată de Martorii lui Iehova și disponibilă online pe www.jw.org.




    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Publicații în limba română (2000-2017)
			
		

		
			
				
				Preferințe
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Termeni de utilizare
			
			
				Politică de confidențialitate
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



