DUNIANI pote, kuna tengenezo linalofanyizwa na watu wanaompenda na kumtumikia Yehova.
Watu hao ni Mashahidi wa Yehova.
Ni kweli kwamba watumishi hao si wakamilifu na hukosea.
Hata hivyo, roho takatifu ya Mungu imesaidia kutaniko la ulimwenguni pote likue na kunawiri.
Fikiria baadhi ya mambo mazuri ambayo Yehova amekuwa akitimiza kupitia watu wake wanaojitoa kwa hiari ingawa si wakamilifu.
2 Siku za mwisho za mfumo huu wa mambo zilipoanza mwaka wa 1914, watumishi wa Mungu duniani walikuwa wachache.
Lakini Yehova alibariki kazi yao ya kuhubiri.
Katika miaka iliyofuata, mamilioni ya watu wamejifunza kweli za Biblia na kuwa Mashahidi wa Yehova.
Yehova alitabiri kwamba kungekuwa na ukuzi huo wa ajabu aliposema: “Mdogo atakuwa elfu, na mnyonge atakuwa taifa lenye nguvu.
Mimi mwenyewe,
Yehova, nitaharakisha jambo hilo kwa wakati wake.” (Isa. 60:22)
Bila shaka, unabii huo umetimizwa katika siku hizi za mwisho.
Kwa hiyo, idadi ya watu wa Mungu duniani ni kubwa kuliko idadi ya watu katika nchi nyingi.
 3. Watumishi wa Mungu wameonyeshaje upendo?
3 Leo,
Yehova pia amesaidia watu wake kusitawisha kwa kiwango kikubwa zaidi sifa yake kuu ya upendo. (1 Yoh. 4:8)
Yesu, ambaye aliiga upendo wa Mungu, aliwaambia hivi wafuasi wake: “Ninawapa ninyi amri mpya, kwamba mpendane . . .
Kwa jambo hili wote watajua kwamba ninyi ni wanafunzi wangu, mkiwa na upendo kati yenu wenyewe.” (Yoh. 13:34, 35)
Amri hiyo ya kupendana iliwasaidia sana katika miaka ya karibuni mataifa yalipopigana vita vikali ulimwenguni pote.
Kwa mfano, katika Vita vya Pili vya Ulimwengu, watu milioni 55 hivi waliuawa.
Hata hivyo,
Mashahidi wa Yehova hawakushiriki katika vita hivyo vya duniani pote. (Soma Mika 4:1, 3.) Hilo limewasaidia kubaki “safi kutokana na damu ya watu wote.”—Mdo. 20:26.
 4. Kwa nini inashangaza kwamba watu wa Yehova wanaongezeka?
4 Watu wa Mungu wanazidi kuongezeka licha ya kuwa na adui mwenye nguvu Shetani, “mungu wa mfumo huu wa mambo.” (2 Kor. 4:4)
Anaongoza mashirika ya kisiasa ya ulimwengu huu na pia vyombo vya habari.
Lakini hawezi kukomesha kazi ya kuhubiri habari njema.
Hata hivyo, kwa sababu anajua ana muda mfupi,
Shetani anajaribu kuwageuza watu waiache ibada ya kweli na anatumia njia mbalimbali kufanya hivyo.—Ufu. 12:12.



JARIBU LA USHIKAMANIFU

 5. Kwa nini huenda nyakati fulani wengine wakaumiza hisia zetu? (Tazama picha mwanzoni mwa makala hii.)
5 Kutaniko la Kikristo linasisitiza umuhimu wa kumpenda Mungu na wanadamu wenzetu.
Yesu alionyesha kwamba wafuasi wake wangefanya hivyo.
Alijibu hivi swali kuhusu amri kuu zaidi: “‘Lazima umpende Yehova Mungu wako kwa moyo wako wote na kwa nafsi yako yote na kwa akili yako yote.’ Hii ndiyo amri iliyo kuu na ya kwanza.
Ya pili, kama hiyo, ni hii, ‘Mpende jirani yako kama wewe mwenyewe.’” (Mt. 22:35-39)
Hata hivyo,
Biblia inaonyesha wazi kwamba sisi si wakamilifu kwa sababu tumerithi dhambi ya Adamu. (Soma Waroma 5:12, 19.) Kwa hiyo, huenda nyakati fulani watu kutanikoni wakatuumiza kwa maneno au matendo yao.
Hali hiyo inaweza kujaribu upendo wetu kwa Yehova na kwa watu wake.
Tutafanya nini nyakati kama hizo?
Hata watumishi waaminifu wa Mungu wa nyakati zilizopita, walisema au kufanya mambo yaliyowaumiza wengine na tunaweza kujifunza kutokana na mambo yaliyoandikwa katika Biblia kuwahusu.





Ikiwa ungeishi Israeli wakati huo, ungemwonaje Eli ambaye aliachilia dhambi za wanawe? (Tazama fungu la 6)




 6. Eli alikosa kuwaadhibu wana wake katika njia gani?
6 Kwa mfano,
Kuhani Mkuu Eli alikuwa na wana wawili ambao hawakuheshimu sheria za Yehova. “Wana wa Eli walikuwa watu wasiofaa kitu; hawakumtambua Yehova.” (1 Sam. 2:12)
Ingawa baba yao alifanya kazi muhimu ili kuendeleza ibada ya kweli, wana wake wawili walitenda dhambi nzito sana.
Eli alijua mambo waliyofanya na alipaswa kuwaadhibu, lakini alilegeza msimamo.
Kwa sababu hiyo,
Mungu aliihukumu nyumba ya Eli. (1 Sam. 3:10-14)
Baadaye, wazao wake hawakuruhusiwa kuwa makuhani wakuu.
Ikiwa ungeishi wakati huo, ungemwonaje Eli ambaye aliachilia dhambi za wanawe?
Je, ungeruhusu matendo yake yakukwaze mpaka uache kumtumikia Mungu?
 7. Daudi alifanya dhambi gani nzito, na Mungu alifanya nini?
7 Yehova alimpenda Daudi na alimwona kuwa “anayekubalika kwa moyo wake.” (1 Sam. 13:13, 14; Mdo. 13:22)
Lakini baadaye Daudi alifanya uzinzi na Bath-sheba, naye akapata mimba.
Daudi alifanya hivyo Uria mume wa Bath-sheba alipokuwa vitani.
Aliporudi nyumbani kwa muda mfupi,
Daudi alijaribu kumfanya alale na Bath-sheba ili ionekane kana kwamba Uria ndiye aliyekuwa baba ya mtoto huyo.
Uria alikataa kufanya jambo ambalo mfalme alitaka, kwa hiyo Daudi akapanga njama auawe vitani.
Daudi alipatwa na madhara makubwa kwa sababu ya dhambi zake, yeye na familia yake walipatwa na misiba. (2 Sam. 12:9-12)
Hata hivyo,
Mungu alimwonyesha rehema mwanamume huyo ambaye kwa ujumla alitembea mbele za Yehova “kwa utimilifu wa moyo.” (1 Fal. 9:4)
Ikiwa ungeishi kati ya watu wa Mungu siku hizo ungefanyaje?
Je, ungekwazwa na mwenendo mbaya wa Daudi?
 8. (a)
Mtume Petro alishindwa kutimiza ahadi yake jinsi gani? (b) Kwa nini Yehova aliendelea kumtumia Petro hata baada ya kukosea?
8 Mfano mwingine katika Biblia ni ule wa mtume Petro.
Yesu alikuwa amemchagua awe mmoja wa mitume wake; lakini nyakati nyingine Petro alisema au kufanya mambo ambayo alijutia baadaye.
Kwa mfano, mitume walimwacha Yesu katika pindi muhimu.
Petro alikuwa amesema kwamba hata wengine wakimwacha, yeye hangefanya hivyo. (Marko 14:27-31, 50)
Hata hivyo,
Yesu alipokamatwa, mitume wote, kutia ndani Petro, walimwacha.
Hata Petro alimkana Yesu mara kadhaa. (Marko 14:53, 54, 66-72)
Lakini Petro alitubu, na Yehova akaendelea kumtumia.
Ikiwa ungekuwa mfuasi wa Yesu wakati huo, je, matendo ya Petro yangeathiri ushikamanifu wako kwa Yehova?
 9. Kwa nini unaamini kwamba Mungu ni mwenye haki sikuzote?
9 Hiyo ni mifano michache tu ya watu waliofanya mambo yaliyowaumiza wengine.
Kuna mifano mingine mingi ya watu waliomtumikia Yehova katika karne zilizopita na nyakati za hivi karibuni ambao wamefanya mambo mabaya na kuwaumiza wengine.
Swali ni, utafanyaje?
Je, utakwazwa na makosa yao, na kumwacha Yehova na watu wake kutia ndani wale walio kwenye kutaniko lenu?
Au je, utatambua kwamba huenda Yehova akaruhusu muda upite ili watenda dhambi watubu na kwamba mwishowe atasahihisha makosa yoyote na kutenda kwa haki?
Kwa upande mwingine, nyakati fulani wale waliotenda dhambi nzito wanakataa rehema za Yehova na hawatubu.
Katika hali kama hizo, je, utakuwa na hakika kwamba Yehova atawahukumu watenda dhambi hao, huenda kwa kuwaondoa kutanikoni?



DUMISHA USHIKAMANIFU

10. Yesu alifahamu nini kuhusu makosa ya Yuda Iskariote na Petro?
10 Biblia ina masimulizi ya watumishi wa Yehova waliodumisha ushikamanifu kwake na kwa watu wake licha ya makosa mazito ya wengine walioishi karibu nao.
Kwa mfano, baada ya kusali kwa Baba yake usiku mzima,
Yesu aliwachagua mitume 12.
Yuda Iskariote alikuwa mmoja wao.
Baadaye Yuda alipomsaliti,
Kristo hakuruhusu jambo hilo liharibu uhusiano wake na Baba yake Yehova.
Pia,
Petro alipomkana,
Yesu hakuruhusu hilo litokee. (Luka 6:12-16; 22:2-6, 31, 32)
Yesu alijua kwamba matendo hayo hayakuwa makosa ya Yehova au watu wake kwa ujumla.
Yesu aliendelea na kazi yake nzuri licha ya kuvunjwa moyo na baadhi ya wafuasi wake.
Yehova alimthawabisha kwa kumfufua kutoka kwa wafu, na hivyo kumfungulia njia awe Mfalme wa Ufalme wa mbinguni.—Mt. 28:7, 18-20.
11. Biblia ilitabiri nini kuhusu watumishi wa Yehova leo?
11 Yesu alikuwa na msingi thabiti wa kumtumaini Yehova na watu wake na bado anawatumaini.
Kwa kweli, inashangaza tunapofikiria mambo ambayo Yehova anatimiza kupitia watumishi wake katika siku hizi za mwisho.
Anawasaidia kuhubiri kweli ulimwenguni pote, na ndio watu pekee wanaofanya kazi hiyo.
Pia wana umoja wa kweli na furaha kwa sababu ya kufundishwa na Yehova.
Andiko la Isaya 65:14 linafafanua hivi hali ya kiroho ya watu wa Mungu leo: “Tazama!
Watumishi wangu watapiga vigelegele kwa shangwe kwa sababu ya hali nzuri ya moyo.”
12. Tuyaoneje makosa ya wengine?
12 Watumishi wa Yehova wanashangilia wanapoona mambo mazuri wanayoweza kufanya kwa sababu wanaongozwa na Yehova.
Tofauti na hilo, ni kana kwamba ulimwengu unaoongozwa na Shetani unalia, kadiri hali zinavyozorota.
Bila shaka, si jambo la hekima kumlaumu Yehova au kutaniko lake kwa sababu ya makosa ya watumishi wachache wa Mungu.
Tunapaswa kudumisha ushikamanifu wetu kwa Yehova na mipango yake na kujua jinsi tunavyopaswa kuyaona au kushughulikia makosa ya wengine.



KUSHUGHULIKIA MAKOSA

13, 14. (a)
Kwa nini tunapaswa kuonyeshana subira? (b) Tungependa kukumbuka ahadi gani?
13 Hivyo basi, tunaweza kufanya nini mtumishi wa Mungu anaposema au kufanya jambo linalotuumiza hisia?
Kanuni nzuri ya Biblia ya kufuata ni hii: “Usiwe na haraka kuudhika katika roho yako, kwa maana kuudhika hukaa katika kifua cha wajinga.” (Mhu. 7:9)
Tunapaswa kukumbuka kwamba miaka 6,000 hivi imepita tangu wanadamu wapoteze ukamilifu katika bustani ya Edeni.
Watu wasio wakamilifu wana mwelekeo wa kukosea.
Kwa hiyo, haingefaa kutarajia waabudu wenzetu wafanye mambo yanayopita uwezo wao na kuruhusu makosa yao yafanye tupoteze shangwe tunayopata kwa kumtumikia Mungu katika siku hizi za mwisho.
Tutakuwa tumekosea sana tukiruhusu makosa ya wengine yatukwaze na kutufanya tuliache tengenezo la Yehova.
Tukifanya hivyo, tutapoteza pendeleo la kufanya mapenzi ya Mungu na pia tumaini la kuishi katika ulimwengu mpya wa Mungu.
14 Ili tudumishe shangwe yetu na tumaini thabiti, tungependa kukumbuka ahadi hii ya Yehova inayotufariji: “Tazama, mimi ninaumba mbingu mpya na dunia mpya; na mambo ya zamani hayatakumbukwa akilini, wala hayataingia moyoni.” (Isa. 65:17; 2 Pet. 3:13)
Usiruhusu makosa ya wengine yakuzuie kupata baraka hizo.
15. Yesu alisema tunapaswa kufanya nini wengine wanapokosea?
15 Hata hivyo, kwa kuwa hatujaingia kwenye ulimwengu mpya, tunapaswa kufikiria mawazo ya Mungu kuhusu jinsi tunavyoweza kutenda watu wanaposema au kufanya mambo yanayotuumiza hisia.
Kwa mfano, kanuni moja tunayopaswa kukumbuka ni ile ambayo Yesu alitupatia aliposema: “Ikiwa mnawasamehe watu makosa yao,
Baba yenu wa mbinguni atawasamehe ninyi pia; lakini ikiwa hamtawasamehe watu makosa yao,
Baba yenu naye hatawasamehe makosa yenu.” Pia kumbuka kwamba Petro alipouliza ikiwa anapaswa kusamehe “mpaka mara 7,” Yesu alijibu: “Ninakuambia, si, mpaka mara 7, bali, mpaka mara 77.” Bila shaka,
Yesu alimaanisha kwamba tunapaswa kuwa tayari kusamehe; huo unapaswa kuwa mwelekeo wetu mkuu na wa kwanza.—Mt. 6:14, 15; 18:21, 22.
16. Yosefu aliweka mfano gani mzuri?
16 Yosefu anatuwekea mfano mzuri wa kushughulikia makosa. Yosefu na ndugu yake mdogo walikuwa wana pekee wa Yakobo na Raheli.
Ndugu kumi wa kambo wa Yosefu walimwonea wivu kwa sababu alipendwa na baba yake.
Kisha wakamwuza Yosefu utumwani.
Baada ya miaka mingi, kazi nzuri ambayo Yosefu alifanya akiwa Misri ilimfanya apate cheo cha pili katika nchi hiyo.
Nchi yao ilipopatwa na njaa, ndugu za Yosefu walienda Misri kununua chakula lakini hawakumtambua.
Yosefu angeweza kutumia mamlaka yake kulipiza kisasi kwa jinsi walivyomtendea vibaya.
Badala yake aliwajaribu ndugu zake ili kuona ikiwa walikuwa wamebadili mtazamo wao.
Alipoona kwamba ndugu zake walikuwa wamebadilika,
Yosefu alijitambulisha kwao.
Baadaye alisema: “Msiogope.
Mimi nitaendelea kuwapa chakula ninyi na watoto wenu wadogo.” Simulizi hilo la Biblia linaendelea kusema: “Basi akawafariji na kusema nao kwa kuwatuliza.”—Mwa. 50:21.
17. Ungependa kufanya nini wengine wanapokukosea?
17 Pia ni jambo la hekima kukumbuka kwamba sisi sote hukosea na tunaweza kuwaudhi wengine.
Tukitambua kwamba tumekosea,
Biblia inatuhimiza tumwendee mtu tuliyemkosea na kujaribu kusuluhisha mambo. (Soma Mathayo 5:23, 24.) Sisi huthamini wengine wanapotusamehe, kwa hiyo tunapaswa kuwasamehe pia.
Andiko la Wakolosai 3:13 linatuhimiza hivi: “Endeleeni kuvumiliana na kusameheana kwa hiari ikiwa yeyote ana sababu ya kulalamika juu ya mwingine.
Kama vile Yehova alivyowasamehe ninyi kwa hiari, nanyi fanyeni vivyo hivyo pia.” Upendo wa Kikristo “hauweki hesabu ya ubaya” linasema andiko la 1 Wakorintho 13:5.
Tukizoea kuwasamehe wengine,
Yehova atatusamehe.
Kwa hiyo, wengine wanapotukosea, na tuonyeshe rehema kama Baba yetu Yehova anavyotuonyesha rehema.—Soma Zaburi 103:12-14.






    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Machapisho ya Kiswahili (2000-2017)
			
		

		
			
				
				Mapendezi
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Masharti ya Utumiaji
			
			
				Sera ya Faragha
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



