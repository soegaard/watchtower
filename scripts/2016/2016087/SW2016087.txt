Je, umewahi kuhisi hujui la kufanya ili kumsaidia mtu anayeomboleza baada ya kufiwa na mtu anayempenda?
Wakati mwingine huenda tunahisi kwamba hatujui la kusema au kufanya hivyo tunaamua kukaa kimya na kutofanya lolote.
Hata hivyo, kuna mambo tunayoweza kufanya ili kumsaidia aliyefiwa.
Mara nyingi unachohitaji kufanya ni kuwa pamoja na yule anayeomboleza na kumwambia maneno kama vile “pole sana.” Katika tamaduni nyingi, kumkumbatia mtu au kumshika mkono ni njia moja ya kumwonyesha kwamba unamjali.
Mtu aliyefiwa na mpendwa wake anapoongea, msikilize kwa huruma.
Zaidi ya yote, wasaidie waliofiwa kwa kufanya kazi za nyumbani kama vile kupika chakula, kutunza watoto, au kufanya mipango ya mazishi.
Kumbuka kwamba maneno matupu hayavunji mfupa.
Baada ya muda, unaweza kuzungumzia sifa nzuri au mambo mazuri ambayo yule aliyekufa alifanya.
Mazungumzo hayo yanaweza kumfariji mfiwa.
Kwa mfano,
Pam aliyefiwa na mume wake,
Ian, miaka sita iliyopita, anasema hivi: “Mara nyingine watu hunieleza mambo mazuri aliyofanya Ian ambayo sikuyajua, na jambo hilo hunifanya nihisi vizuri.”
Kulingana na utafiti, wafiwa wengi hupewa misaada mingi wakati wa msiba na kisha kusahauliwa baada ya hapo kwa kuwa kila mtu anaendelea na maisha yake.
Hivyo, jitahidi kuwasiliana na rafiki yako aliyefiwa kwa ukawaida.* Wafiwa wengi huthamini sana jambo hilo kwa kuwa wanapata nafasi ya kuzungumzia huzuni yao ya muda mrefu.
Mfikirie Kaori, mwanamke Mjapani aliyehuzunika sana baada ya kufiwa na mama yake na kisha miezi 15 baadaye, akafiwa na dada yake.
Jambo la kupendeza ni kwamba rafiki zake wa karibu waliendelea kumsaidia.
Ritsuko ambaye ana umri mkubwa kuliko Kaori aliamua kuwa rafiki yake wa karibu.
Kaori anasema hivi: “Kusema kweli, sikufurahia jambo hilo.
Sikutaka mtu mwingine achukue nafasi ya mama yangu, na sikufikiria kwamba kuna mtu angeweza kuwa kama mama.
Hata hivyo, kwa sababu ya jinsi Mama Ritsuko alivyonitendea, nilianza kumpenda.
Kila juma tulihubiri na kuhudhuria mikutano ya Kikristo pamoja.
Alinialika tunywe chai pamoja, aliniletea vyakula, na mara nyingi aliniandikia barua na kadi.
Mtazamo mzuri wa Mama Ritsuko ulinisaidia sana.”
Miaka kumi na mbili imepita tangu mama yake Kaori alipokufa na sasa Kaori na mume wake ni waeneza injili wa wakati wote.
Kaori anasema: “Mama Ritsuko anaendelea kunihangaikia.
Ninaporudi kwetu, mara zote ninamtembelea na ninafurahia kuwa pamoja naye.”
Mtu mwingine aliyenufaika kwa kuendelea kusaidiwa na watu wake wa karibu ni Poli ambaye ni Shahidi wa Yehova kutoka Cyprus.
Sozos, mume wa Poli, alikuwa mwenye fadhili na aliweka mfano mzuri akiwa mchungaji Mkristo kwa kuwakaribisha mayatima na wajane nyumbani kwao ili kushirikiana na kula pamoja nao. (Yakobo 1:27)
Inasikitisha kwamba Sozos alikufa akiwa na umri wa miaka 53, baada ya kuugua kansa ya ubongo.
Poli anasema: “Nilifiwa na mume wangu mshikamanifu niliyekuwa nimeishi naye kwa miaka 33.”





Tafuta njia nzuri za kumsaidia mfiwa




Baada ya mazishi,
Poli na mtoto wake mwenye umri wa miaka 15 anayeitwa Daniel, walihamia nchini Kanada.
Wakiwa huko walianza kushirikiana na kutaniko la Mashahidi wa Yehova.
Poli anasema: “Rafiki zangu katika kutaniko jipya hawakujua lolote kuhusu maisha yetu ya awali na hali ngumu tulizokabili.
Hata hivyo, hilo halikuwazuia kututia moyo kwa maneno yanayofariji na kutusaidia.
Msaada huo ulikuwa muhimu kwa mwana wangu hasa katika kipindi hicho ambacho alihitaji kuwa karibu sana na baba yake!
Wale wanaosimamia mambo katika kutaniko walimjali sana Daniel.
Mmoja wao alihakikisha kwamba lazima Daniel awepo kila mara alipoalika marafiki au walipoenda kucheza mpira.” Kwa sasa Poli na Daniel wanaendelea vizuri.
Ni kweli kwamba kuna njia nyingi za kutoa msaada na kuwafariji wanaoomboleza.
Biblia hutufariji kwa kutoa tumaini zuri la wakati ujao.



Wengine wametia alama tarehe ya kifo cha mpendwa wa rafiki au mshiriki wa familia katika kalenda ili wakumbuke kutoa faraja inapohitajika zaidi, yaani, katika tarehe hiyo au karibu wakati huo.




    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Machapisho ya Kiswahili (2000-2017)
			
		

		
			
				
				Mapendezi
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Masharti ya Utumiaji
			
			
				Sera ya Faragha
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



