Ai fost vreodată în situația de a nu ști ce să-i spui unei persoane apropiate, căreia i-a murit cineva drag?
Pentru că nu știm ce să facem sau ce să spunem, uneori alegem să nu facem și să nu spunem nimic.
Totuși există multe lucruri practice pe care le putem face.
În general, simpla prezență și câteva cuvinte, cum ar fi: „Îmi pare rău”, sunt suficiente. În multe culturi, o îmbrățișare sau o strângere caldă de mână constituie o modalitate de a arăta că ești alături de persoana îndurerată.
Dacă aceasta vrea să vorbească, ascult-o cu atenție și, lucrul cel mai important, fă ceva practic pentru ea.
De exemplu, te-ai putea oferi să faci unele treburi casnice pe care ea nu reușește să le facă, să gătești, să ai grijă de copii sau să ajuți la pregătirile de înmormântare, dacă ți se permite.
Aceste acțiuni ar putea conta mai mult decât cele mai sensibile cuvinte.
După un timp, i-ai putea vorbi despre calitățile persoanei decedate sau i-ai putea relata unele experiențe frumoase avute împreună.
Asemenea discuții ar putea chiar să aducă un zâmbet pe fața persoanei îndurerate.
De exemplu,
Pam, care în urmă cu șase ani și-a pierdut soțul, spune: „Mă bucur mult când mi se povestesc lucruri frumoase pe care le-a făcut Ian, soțul meu, lucruri despre care nu știam nimic până atunci”.
Cercetătorii spun că, în general, persoanele îndoliate primesc mult ajutor la început, dar că necesitățile lor sunt neglijate după aceea, când prietenii se întorc la viața cotidiană.
Prin urmare, fă-ți obiceiul să vorbești cu regularitate cu un prieten căruia i-a murit cineva apropiat.* Aceste ocazii sunt apreciate de persoanele îndoliate deoarece le ajută să se elibereze de sentimentele persistente de durere.
De exemplu,
Kaori, o tânără din Japonia, și-a pierdut mama, iar, după un an și trei luni, sora mai mare.
Din fericire, prietenele ei i-au oferit sprijin permanent.
Una dintre ele, pe nume Ritsuko, i-a propus să-i devină prietenă apropiată, deși este mult mai în vârstă decât ea.
Kaori spune: „Sinceră să fiu, la început nu mi-a surâs ideea, pentru că nu voiam ca cineva să ia locul mamei mele și nici nu credeam că era posibil lucrul acesta.
Totuși, datorită modului în care mama Ritsuko s-a purtat cu mine, am reușit să mă apropii de ea. În fiecare săptămână mergeam împreună în lucrarea de predicare și la întrunirile creștine.
Mă invita la ceai, îmi aducea mâncare și îmi scria scrisori și cărți poștale.
Atitudinea mamei Ritsuko a avut o influență benefică asupra mea”.
Au trecut doisprezece ani de la moartea mamei lui Kaori, iar, în prezent, ea și soțul ei sunt evanghelizatori cu timp integral.
Kaori spune: „Mama Ritsuko are în continuare grijă de mine.
Ori de câte ori merg acasă, o vizitez și mă bucur de compania ei încurajatoare”.
Și Poli, o Martoră a lui Iehova din Cipru, a beneficiat permanent de sprijin.
Soțul ei,
Sozos, a fost un păstor creștin exemplar, care deseori îi invita pe orfani și pe văduve la ei acasă pentru a petrece timp sau pentru a lua masa împreună (Iacov 1:27).
Din nefericire, la vârsta de 53 de ani,
Sozos a murit din cauza unei tumori cerebrale.
Poli spune: „Mi-am pierdut soțul mult iubit, alături de care am petrecut 33 de ani”.





Caută modalități practice de a le oferi ajutor celor îndurerați




După înmormântare,
Poli s-a mutat în Canada împreună cu fiul lor cel mai mic,
Daniel, de 15 ani, și s-a alăturat congregației Martorilor lui Iehova de acolo.
Poli își amintește: „Prietenii din noua congregație n-au știut nimic despre problemele noastre.
Cu toate acestea, ne-au întâmpinat cu căldură, ne-au adresat cuvinte amabile și ne-au oferit ajutor practic.
Sprijinul lor a contat foarte mult, îndeosebi în acele momente, când fiul meu avea cel mai mult nevoie de tatăl său!
Frații cu răspundere din congregație s-au îngrijit mult de Daniel.
Unul dintre ei îl lua cu el la reuniuni sau la fotbal”. În prezent, atât Poli, cât și Daniel sunt fericiți.
Cu siguranță, există numeroase modalități prin care le putem oferi ajutor practic și alinare celor ce suferă în urma pierderii cuiva drag.
De asemenea,
Biblia ne mângâie dându-ne o speranță emoționantă.



Unii și-au notat în calendar data decesului pentru a oferi mângâiere când este nevoie cel mai mult de lucrul acesta – în ziua sau în jurul zilei în care a avut loc decesul.




    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Publicații în limba română (2000-2017)
			
		

		
			
				
				Preferințe
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Termeni de utilizare
			
			
				Politică de confidențialitate
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



