„Die Toten wissen gar nichts“ (Prediger 9:5, Schlachter).
Wenn der Mensch stirbt, hört er auf zu existieren.
DIE BIBEL SAGT NOCH MEHR DAZU
  Als Adam, der erste Mensch, starb, wurde er wieder zu Staub (1. Mose 2:7; 3:19).
Genau das passiert mit jedem Menschen, der stirbt (Prediger 3:19, 20).

  Wenn jemand gestorben ist, ist er von allem, was er im Leben falsch gemacht hat, freigesprochen (Römer 6:7).
Es gibt nach dem Tod keine weitere Strafe.





Können die Toten wieder leben?

WAS MEINEN SIE?
  Ja

  Nein

  Das weiß keiner


WAS IN DER BIBEL STEHT
„Es [wird] eine künftige Auferstehung der Toten geben“ (Apostelgeschichte 24:15, Schlachter).
DIE BIBEL SAGT NOCH MEHR DAZU
  Tot sein wird in der Bibel oft mit Schlafen verglichen (Johannes 11:11-14).
Genauso wie wir jemand wecken können, der schläft, kann Gott tote Menschen wieder zum Leben bringen (Hiob 14:13-15).

  Die Bibel beschreibt verschiedene Auferstehungen, die unseren Glauben daran stärken können, dass Tote wirklich wieder leben werden (1. Könige 17:17-24; Lukas 7:11-17; Johannes 11:39-44).





Mehr dazu in Kapitel 6 des Buches Was lehrt die Bibel wirklich? (herausgegeben von Jehovas Zeugen)
Auch online auf www.jw.org




    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Deutsche Publikationen (2000-2017)
			
		

		
			
				
				Einstellungen
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Nutzungsbedingungen
			
			
				Datenschutzerklärung
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



