FÜNF, vier, drei, zwei, eins!
Wenn auf dem Kongress die Uhr auf der Videowand rückwärts zählt, ist das für uns die Zeit, auf unserem Platz zu sein und den musikalischen Auftakt zu genießen.
Es ist Zeit, die schöne Musik des Wachtturm-Orchesters auf uns wirken zu lassen und uns auf das Programm einzustimmen.
Aber was ist, wenn einige noch umherlaufen oder sich unterhalten und gar nicht mitbekommen, dass das Programm beginnt?
Offensichtlich haben sie sich ablenken lassen und haben nicht gemerkt, wie spät es ist und was um sie herum geschieht — der Vorsitzende ist auf der Bühne, die Musik spielt und die Anwesenden sitzen schon.
Diese Szene kann uns helfen, den „Countdown“ eines größeren Ereignisses bewusst wahrzunehmen, das kurz bevorsteht und höchste Wachsamkeit erfordert.
Um welches Ereignis handelt es sich?
 2. Warum forderte Jesus seine Jünger auf, wachsam zu bleiben?
2 Als Jesus Christus vom „Abschluss des Systems der Dinge“ sprach, riet er seinen Jüngern dringend: „Haltet ständig Ausschau, bleibt wach, denn ihr wisst nicht, wann die bestimmte Zeit da ist.“ Danach forderte Jesus sie wiederholt auf: „Wacht beständig.“ (Mat. 24:3; lies Markus 13:32-37)
Auch im Parallelbericht von Matthäus forderte Jesus seine Nachfolger auf, wachsam zu bleiben: „Wacht deshalb beharrlich, weil ihr nicht wisst, an welchem Tag euer Herr kommt.  . . . erweist . . . euch als solche, die bereit sind, denn zu einer Stunde, da ihr es nicht denkt, kommt der Menschensohn.“ Und wieder sagte er: „Wacht daher beharrlich, denn ihr kennt weder den Tag noch die Stunde“ (Mat. 24:42-44; 25:13).
 3. Warum nehmen wir Jesu Warnung ernst?
3 Als Zeugen Jehovas nehmen wir Jesu Warnung ernst.
Wieso?
Wir wissen, dass wir in einer späten Phase der „Zeit des Endes“ leben und es bis zum Beginn der „großen Drangsal“ nicht mehr lange dauern kann (Dan. 12:4; Mat. 24:21).
Wir beobachten weltweit schreckliche Kriege, zunehmende Unmoral und Gesetzlosigkeit, religiöse Orientierungslosigkeit,
Hungersnöte,
Seuchen und Erdbeben.
Und wir sehen ein gewaltiges Königreichspredigtwerk, das Gottes Volk auf der ganzen Erde durchführt (Mat. 24:7, 11, 12, 14; Luk. 21:11).
Gespannt erwarten wir, was das Kommen des Herrn für uns und für die Verwirklichung des Vorsatzes Gottes bedeuten wird (Mar. 13:26, 27).



DIE ZEIT LÄUFT AB

 4. (a)
Warum dürfte Jesus jetzt wohl den Zeitpunkt für Harmagedon kennen? (b) Was ist sicher, obwohl wir nicht wissen, wann die große Drangsal ausbricht?
4 Bei einem Kongress wissen wir genau, wann jeder Programmteil beginnt.
Doch auch wenn wir uns noch so anstrengen, können wir den Beginn der großen Drangsal nicht genau bestimmen — nicht das Jahr und schon gar nicht Tag und Stunde.
Als Jesus auf der Erde war, erklärte er: „Von jenem Tag und jener Stunde hat niemand Kenntnis, weder die Engel der Himmel noch der Sohn, sondern nur der Vater“ (Mat. 24:36).
Später wurde Christus im Himmel die Macht übertragen, gegen Satans Welt zu kämpfen (Offb. 19:11-16).
Deshalb wird Jesus jetzt wohl wissen, wann Harmagedon ausbrechen wird.
Wir wissen es nicht.
Daher ist es für uns unerlässlich, bis zum Beginn der Drangsal wachsam zu bleiben.
Jehova war sich über diesen Zeitpunkt nie im Unklaren.
Schließlich hat er genau festgelegt, wann das Ende kommt.
Der „Countdown“ bis zum Start der großen Drangsal läuft, und ihr Start „wird sich nicht verspäten“. (Lies Habakuk 2:1-3.) Warum können wir uns dessen sicher sein?
 5. Nenne ein Beispiel dafür, dass sich Jehovas Prophezeiungen immer zur vorgesehenen Zeit erfüllen.
5 Die Prophezeiungen Jehovas haben sich immer genau zur vorgesehenen Zeit erfüllt.
Ein Beispiel dafür ist die Befreiung der Israeliten aus Ägypten.
Moses sagte in Bezug auf den 14. Nisan 1513 v. u. Z.: „Es geschah am Ende der vierhundertdreißig Jahre, . . . an ebendiesem Tag, dass alle Heere Jehovas aus dem Land Ägypten auszogen“ (2. Mo. 12:40-42).
Diese „430 Jahre“ begannen, als Jehovas Bund mit Abraham 1943 v. u. Z. in Kraft trat (Gal. 3:17, 18).
Etwas später kündigte Jehova Abraham an: „Wisse bestimmt, dass deine Nachkommen ansässige Fremdlinge werden in einem Land, das nicht das ihre ist, und sie werden ihnen dienen müssen, und diese werden sie bestimmt vierhundert Jahre lang niederdrücken“ (1. Mo. 15:13; Apg. 7:6).
Diese „400 Jahre“ zählten offensichtlich ab 1913 v. u. Z., als Ismael den gerade entwöhnten Isaak verspottete, und endeten, als die Israeliten 1513 v. u. Z. aus Ägypten auszogen (1. Mo. 21:8-10; Gal. 4:22-29).
Jehova legte also 400 Jahre im Voraus den genauen Zeitpunkt für die Befreiung seines Volkes fest.
 6. Warum können wir sicher sein, dass Jehova sein Volk retten wird?
6 Josua gehörte zu denen, die aus Ägypten befreit wurden.
Er erinnerte ganz Israel: „Ihr wisst wohl mit eurem ganzen Herzen und mit eurer ganzen Seele, dass kein einziges Wort von allen guten Worten, die Jehova, euer Gott, zu euch geredet hat, dahingefallen ist.
Sie alle sind für euch eingetroffen.
Kein einziges Wort von ihnen ist dahingefallen“ (Jos. 23:2, 14).
Jehovas Versprechen, uns durch die große Drangsal zu retten, wird sich genauso sicher erfüllen.
Möchten wir die Vernichtung des gegenwärtigen Systems überleben, müssen wir allerdings wachsam bleiben.



WACH BLEIBEN,
UM ZU ÜBERLEBEN

 7, 8. (a)
Welche Aufgabe hatte ein Wächter, und was lässt sich daraus für uns ableiten? (b) Nenne ein Beispiel, was geschehen konnte, wenn ein Wächter einschlief.
7 Ein Blick in die Vergangenheit macht deutlich, wie wichtig es ist, wachsam zu sein.
Früher waren größere Städte, wie zum Beispiel Jerusalem, von hohen Mauern umgeben.
Sie boten Schutz vor Eindringlingen und hatten erhöhte Beobachtungsposten.
Tag und Nacht waren Wächter auf den Mauern und an den Toren postiert.
Es war ihre Aufgabe, die Umgebung im Auge zu behalten und die Stadtbewohner vor jeder herannahenden Gefahr zu warnen (Jes. 62:6).
Da Leben und Tod in der Hand der Wächter lag, mussten sie wach und aufmerksam auf ihrem Posten bleiben (Hes. 33:6).
8 Wie der jüdische Historiker Josephus berichtet, konnten die römischen Streitkräfte 70 u. Z. die an die Stadtmauer von Jerusalem angrenzende Burg Antonia einnehmen, weil die Wächter an den Toren eingeschlafen waren.
Die Römer stürmten das Tempelgebiet und legten Feuer.
Das führte zur größten Drangsal, die Jerusalem und die jüdische Nation je erlebt hatten.
 9. Was nehmen die meisten Menschen heute nicht wahr?
9 Heute haben die meisten Länder „Wächter“ in Form von Grenzkontrollen und modernen Überwachungssystemen.
Sie halten nach Personen Ausschau, die in das Gebiet eindringen oder eine Gefahr für die nationale Sicherheit darstellen.
Doch diese „Wächter“ können nur Gefahren erkennen, die von Menschen oder ihren Regierungen ausgehen.
Sie nehmen aber nicht wahr, dass Gottes Königreichsregierung in den Händen Christi bereits im Himmel tätig ist und welche Rolle diese dabei spielen wird, in Kürze an allen Nationen Gericht zu üben (Jes. 9:6, 7; 56:10; Dan. 2:44).
Bleiben wir hingegen geistig wach, werden wir für diesen Tag des Gerichts bereit sein, wann immer er kommt (Ps. 130:6).



LASS DICH NICHT ABLENKEN!

10, 11. (a)
In welcher Gefahr stehen wir, und warum? (b) Was überzeugt dich davon, dass der Teufel Menschen dazu gebracht hat, biblische Prophezeiungen nicht zu beachten?
10 Stellen wir uns einen Wächter auf seinem Posten vor, der schon die ganze Nacht wach geblieben ist.
Gegen Ende seiner Nachtwache wird er extrem müde und könnte auf der Stelle einschlafen.
Wie ist es mit uns?
Je näher wir dem Ende dieses Systems kommen, umso schwieriger ist es, wach zu bleiben.
Wie tragisch wäre es, jetzt einzuschlafen!
Welche drei negativen Einflüsse könnten unsere Wachsamkeit schwächen?
11 Der Teufel lullt Menschen ein, sodass sie in geistiger Hinsicht gleichgültig sind. Kurz vor seinem Tod warnte Jesus seine Jünger drei Mal vor dem „Herrscher dieser Welt“ (Joh. 12:31; 14:30; 16:11).
Jesus wusste, dass der Teufel die Menschen in geistiger Finsternis halten möchte, um das Dringlichkeitsbewusstsein zu untergraben, das in den Prophezeiungen Gottes über die Zukunft immer wieder betont wird (Zeph. 1:14).
Der Teufel verblendet die Menschen mithilfe des Weltimperiums der falschen Religion.
Was fällt uns in Gesprächen mit anderen auf?
Hat der Teufel nicht bereits „den Sinn der Ungläubigen verblendet“, sie also unfähig gemacht, das bevorstehende Ende des gegenwärtigen Systems zu erkennen sowie die Tatsache, dass Christus bereits im Himmel als König regiert? (2. Kor. 4:3-6).
Wie oft sagen die Menschen „Ich habe kein Interesse“!
Die meisten reagieren gleichgültig, wenn wir ihnen erklären möchten, wohin die Welt steuert.
12. Warum dürfen wir uns von Satan nicht täuschen lassen?
12 Lass dich durch diese Gleichgültigkeit nicht entmutigen oder von deiner Wachsamkeit abbringen.
Du weißt es besser.
Paulus schrieb an seine Glaubensbrüder: „Ihr selbst wisst sehr wohl, dass Jehovas Tag genauso kommt wie ein Dieb in der Nacht.“ (Lies 1. Thessalonicher 5:1-6.) Und Jesus warnte uns: „Haltet . . . euch bereit, denn zu einer Stunde, da ihr es nicht für wahrscheinlich haltet, kommt der Menschensohn“ (Luk. 12:39, 40).
Bald wird Satan die breite Masse täuschen, sodass sie sich in einem trügerischen Gefühl von „Frieden und Sicherheit“ wiegt.
Er wird sie zu dem Denken verleiten, auf der Weltbühne sei alles in Ordnung.
Damit es uns nicht auch so ergeht, müssen wir „wach und besonnen“ bleiben.
Der Tag des Gerichts wird uns dann nicht so „überfallen, wie er Diebe überfallen würde“.
Lesen wir deshalb täglich in Gottes Wort und denken wir darüber nach, was Jehova uns sagen möchte.
13. Was bewirkt der Geist der Welt, und wie können wir diesem gefährlichen Einfluss widerstehen?
13 Der Geist der Welt macht schläfrig. Viele sind so mit ihrem Alltag beschäftigt, dass sie sich „ihrer geistigen Bedürfnisse“ nicht bewusst sind (Mat. 5:3).
Sie lassen sich von verlockenden Angeboten der Welt, die die „Begierde des Fleisches und die Begierde der Augen“ fördern, völlig vereinnahmen (1. Joh. 2:16).
Zudem infiziert die Unterhaltungsindustrie Menschen mit der Liebe zu Vergnügungen — und die Verlockungen nehmen von Jahr zu Jahr zu (2. Tim. 3:4).
Daher forderte Paulus Christen auf, „nicht im Voraus für die Begierden des Fleisches“ zu planen, die geistig schläfrig machen (Röm. 13:11-14).
14. Welche Warnung enthält Lukas 21:34, 35?
14 Wir öffnen uns nicht dem Geist der Welt, sondern dem Geist Gottes, durch den uns Jehova klar verstehen lässt, welche Ereignisse uns erwarten (1. Kor. 2:12).[1] Uns ist aber auch bewusst, wie schnell man geistig schläfrig werden kann, wenn man sich von alltäglichen Dingen so in Beschlag nehmen lässt, dass der Dienst für Jehova verdrängt wird (Lies Lukas 21:34, 35.) Manche belächeln uns vielleicht, weil wir wachsam bleiben.
Aber das darf nicht dazu führen, dass wir unser Empfinden für die Dringlichkeit verlieren (2. Pet. 3:3-7).
Stattdessen müssen wir uns regelmäßig mit unseren Glaubensbrüdern versammeln, weil in den Zusammenkünften der Geist Gottes wirkt.





Was tust du, um geistig wach zu bleiben? (Siehe Absatz 11—16)




15. Was ist Petrus,
Jakobus und Johannes passiert, und wie könnte uns das Gleiche passieren?
15 Unsere Unvollkommenheit kann unsere Entschlossenheit schwächen, wach zu bleiben. Jesus wusste, wie schnell unvollkommene Menschen ihren Schwächen nachgeben.
Das wurde in der Nacht vor seiner Hinrichtung deutlich.
Er musste seinen Vater im Himmel um Kraft bitten, damit er treu bleiben konnte.
Jesus bat Petrus,
Jakobus und Johannes zu wachen, während er betete.
Doch sie erkannten nicht den Ernst der Lage.
Statt nach ihrem Herrn Ausschau zu halten, wurden sie schwach und ließen sich vom Schlaf übermannen.
Jesus war körperlich zwar auch müde, aber im flehentlichen Gebet zu seinem Vater hellwach.
Genau das hätten seine Begleiter auch tun sollen (Mar. 14:32-41).
16. Wie kann es uns gemäß Lukas 21:36 gelingen, wach zu bleiben?
16 Um geistig wach zu bleiben, genügt es nicht, nur gute Vorsätze zu haben.
Einige Tage vor dem Vorfall im Garten Gethsemane hatte Jesus die Jünger bereits aufgefordert, zu Jehova zu flehen (Lies Lukas 21:36.) Damit wir geistig nicht schläfrig werden, müssen auch wir „wachsam im Hinblick auf Gebete“ bleiben (1. Pet. 4:7).



BLEIBE WACHSAM!

17. Was müssen wir tun, um für Zukünftiges bereit zu sein?
17 Jesus sagte, dass das Ende zu einer Stunde kommt, da man es nicht denkt. Deshalb ist jetzt nicht die Zeit, geistig schläfrig zu werden.
Es ist nicht die Zeit, selbstsüchtigen Wünschen nachzugeben und Verlockungen und Fantasien zu folgen, die uns Satan und seine Welt präsentieren (Mat. 24:44).
Durch die Bibel lassen uns Gott und Christus wissen, was sie in naher Zukunft für uns tun werden und wie wir wach bleiben können.
Wir müssen auf unsere geistige Gesinnung und auf unser Verhältnis zu Jehova achten und dem Königreich Vorrang einräumen.
Nehmen wir bewusst wahr, wie spät es ist und was um uns herum geschieht, damit wir für Zukünftiges bereit sind (Offb. 22:20).
Unser Leben hängt davon ab!



[1] (Absatz 14)
Siehe das Buch Gottes Königreich regiert!, Kapitel 21.




    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Deutsche Publikationen (2000-2017)
			
		

		
			
				
				Einstellungen
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Nutzungsbedingungen
			
			
				Datenschutzerklärung
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



