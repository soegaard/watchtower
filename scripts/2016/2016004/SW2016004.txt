Katika Biblia, neno la awali la Kigiriki ambalo nyakati nyingine hutafsiriwa kuwa “unyoofu” linamaanisha “kitu ambacho kiasili ni kizuri.” Linaweza pia kumaanisha kitu kizuri kinachofaa kulingana na maadili.
Wakristo wanachukua kwa uzito maneno ya mtume Paulo yaliyoongozwa na roho takatifu: “Tunataka kujiendesha kwa unyoofu katika mambo yote.” Hilo linahusisha nini?



KUPAMBANA NA MWELEKEO WETU

Watu wengi hujitazama katika kioo kila asubuhi kabla ya kutoka nyumbani.
Kwa nini?
Kwa sababu wanataka kuonekana vizuri.
Hata hivyo kuna jambo muhimu zaidi kuliko mtindo wetu wa kunyoa au mavazi ya kisasa.
Utu wetu unaweza kuboresha au kuharibu mwonekano wetu.
Neno la Mungu linaeleza waziwazi kwamba tuna mwelekeo wa kufanya mambo mabaya. “Mwelekeo wa moyo wa mwanadamu ni mbaya tangu ujana wake na kuendelea,” linasema andiko la Mwanzo 8:21.
Hivyo, ili tuwe wanyoofu, tunapaswa kujitahidi kupambana na mwelekeo wa dhambi tuliorithi.
Mtume Paulo alitoa maelezo ya kina kuhusu pambano lake dhidi ya dhambi kwa kusema hivi: “Kwa kweli naipenda sheria ya Mungu kulingana na mtu niliye kwa ndani, lakini mimi naona ndani ya viungo vyangu sheria nyingine ikipigana na sheria ya akili yangu na kunichukua mateka kwa sheria ya dhambi iliyo ndani ya viungo vyangu.”—Waroma 7:22, 23.
Kwa mfano, si lazima tufanye mambo mabaya hata kama moyo wetu unatuchochea kufanya mambo hayo au tuna mwelekeo wenye nguvu wa kutokuwa wanyoofu.
Tuna uhuru wa kuamua cha kufanya.
Tunapoamua kukataa mawazo mabaya, tunaweza kubaki wanyoofu licha ya kuzungukwa na ukosefu wa unyoofu.



KUSHINDA PAMBANO

Ili tuwe wanyoofu, tunahitaji viwango vya juu vya maadili.
Kwa kusikitisha, watu wengi wanatumia wakati mwingi kuamua aina ya mavazi watakayovaa badala kufikiria maadili yao.
Matokeo ni kwamba wanatetea ukosefu wao wa unyoofu ili wasiwajibike.
Kitabu The (Honest) Truth About Dishonesty kinaelezea jambo hilo hivi: “Kwa kweli, tunadanganya ili tuendelee kuonekana kuwa watu wanyoofu.” Je, kuna viwango vyovyote vinavyoweza kutusaidia kuamua ikiwa kuna aina yoyote ya ukosefu wa unyoofu ambayo ni sawa?
Bila shaka, vipo.
Mamilioni ya watu duniani pote wamegundua kwamba Biblia inaweza kutimiza kwa ukamili jambo hilo.
Biblia ina viwango vya juu vya maadili kuliko vyote. (Zaburi 19:7)
Inatupatia mwongozo wenye kutegemeka kuhusu mambo kama vile maisha ya familia, kazi, maadili, na hali ya kiroho.
Mwongozo huo umewafaa watu kwa muda mrefu sana.
Sheria na kanuni za Biblia zinatumiwa na watu wa mataifa, jamii, makabila na vikundi vya watu wote.
Kwa kuisoma Biblia, kutafakari, na kutumia ushauri wake, tunaweza kuuzoeza moyo wetu uwe mnyoofu na mwadilifu.
Pamoja na kupata ujuzi sahihi wa Biblia, kuna mambo mengine yanayohitajika ili kupambana na ukosefu wa unyoofu.
Tunaishi katika ulimwengu uliopotoka kiadili ambao unatushinikiza tupende mambo mabaya.
Ndiyo sababu tunahitaji kusali ili kuomba msaada na utegemezo wa Mungu. (Wafilipi 4:6, 7, 13)
Kwa kufanya hivyo, tunaweza kupata ujasiri wa kusimama imara ili kutetea mambo mema na kuwa wanyoofu katika mambo yote.



BARAKA ZA UNYOOFU

Hitoshi, aliyenukuliwa katika makala ya kwanza, alinufaika kwa sababu ya kujulikana kuwa mfanyakazi mnyoofu.
Sasa anafanya kazi kwa mwajiri ambaye anathamini unyoofu wake.
Hitoshi anaeleza hivi: “Ninashukuru kwamba niliweza kupata kazi inayoniruhusu niendelee kuwa na dhamiri safi.”
Wengine pia wamenufaika kwa kuwa wanyoofu.
Fikiria mifano ya wale ambao wamenufaika kwa kutumia kanuni ya Biblia ya “kujiendesha kwa unyoofu katika mambo yote.”
Dhamiri Safi





“Niliacha shule nikiwa na umri wa miaka 13 ili kufanya kazi na wezi.
Matokeo ni kwamba, asilimia 95 ya mapato yangu yalitokana na wizi.
Niliolewa na hatimaye mimi na mume wangu tukaanza kujifunza Biblia na Mashahidi wa Yehova.
Tulijifunza kwamba Yehova* Mungu anachukia ukosefu wa unyoofu, hivyo tuliamua kubadili maisha yetu.
Mwaka wa 1990 tukawa Mashahidi wa Yehova kwa kujiweka wakfu kwa Yehova na kubatizwa.”—Methali 6:16-19
“Zamani, nyumba yangu ilijaa vitu vya wizi lakini sasa sina tena vitu hivyo na hilo linafanya niwe na dhamiri safi.
Ninapofikiria miaka mingi niliyoishi bila kuwa mnyoofu, ninamshukuru Yehova kwa rehema yake kubwa.
Ndiyo sababu ninaridhika ninapoenda kulala kila siku huku nikijua kwamba Yehova anapendezwa nami.”—Cheryl,
Ireland.






“Bosi wangu alipojua kwamba nilikataa kupokea rushwa kutoka kwa mteja, aliniambia hivi: ‘Mungu wako amekufanya kuwa mtu anayeaminika!
Ni baraka kubwa kuwa nawe katika kampuni yetu.’ Kuwa mnyoofu katika mambo yote kumenifanya niwe na dhamiri safi mbele za Yehova Mungu.
Pia kunanifanya niwe huru kuwahimiza watu wa familia yangu na watu wengine wafanye hivyo.”—Sonny,
Hong Kong.


Amani ya Akili





“Mimi ni meneja katika benki ya kimataifa.
Katika kazi hii mara nyingi unyoofu huwekwa pembeni ili kupata mali.
Maoni ya watu wengi ni, ‘hakuna shida yoyote ukikosa unyoofu kidogo ili kupata mali na kuboresha uchumi.’ Lakini nina amani ya akili kwa sababu ya kuwa mnyoofu.
Nimeazimia kuendelea kuwa mnyoofu na kukabili hali yoyote itakayotokea kwa sababu ya kufanya hivyo.
Waajiri wangu wanajua kwamba sitawadanganya na wala sitadanganya kwa niaba yao.”—Tom,
Marekani.


Kuheshimika





“Msimamizi wangu alinichochea nidanganye kuhusu kupotea kwa vitu fulani kazini lakini nilikataa.
Wezi walipogunduliwa, mwajiri wangu alinishukuru kwa sababu ya unyoofu wangu.
Inahitaji ujasiri kuwa mnyoofu katika ulimwengu huu wenye ukosefu wa unyoofu.
Lakini mwishowe tunaweza kuaminiwa na kuheshimiwa.”—Kaori,
Japani.



Dhamiri safi, amani ya akili, na kuheshimika ni baraka zinazoonyesha kwamba unyoofu una faida.
Bila shaka unakubaliana na jambo hilo.



Yehova ni jina la Mungu linalotajwa katika Biblia.




    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Machapisho ya Kiswahili (2000-2017)
			
		

		
			
				
				Mapendezi
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Masharti ya Utumiaji
			
			
				Sera ya Faragha
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



