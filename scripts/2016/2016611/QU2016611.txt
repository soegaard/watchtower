KAY 1 de septiembre de 1915 watamanta The Watch Tower revista, Ñaupaj kaj Guerra Mundialmanta nerqa: “Europapi guerra kasqanqa may jatunpuni karqa, ñaupaj guerrasmanta nisqaqa”, nispa.
Chay guerrapeqa 30 suyuspuni karqanku.
Chay jina chʼampaykuna kasqanrayku, chay revista nerqa: “Diosmanta willayqa mana atikullanchu, astawanqa Alemaniapi,
Franciapi ima”, nispa.
Jallpʼantinpi chay guerra kashajtin,
Bibliamanta Yachaqajkunaqa guerraman mana rinankuchu kasqanta niraj sutʼita entienderqankuchu.
Jinapis Diosmanta willallaytapuni munarqanku.
Wilhelm Hildebrandt hermanoqa Diosmanta willayta munaspa, The Bible Students Monthly (Mensuario de los Estudiantes de la Biblia) nisqa tratadota francespi mañakorqa.
Chantá Franciaman rerqa Alemaniamanta soldado jina, manataj precursor jinachu.
Soldado pʼachawan pʼachallisqa kaspapis, allin kausay kananmanta callespi willarqa.
The Watch Tower revistapi cartas imprimikorqa.
Chay cartas nisqanman jina,
Alemaniamanta wakin hermanos, soldados kaspapis Diosmanta willayta munarqanku.
Lemke sutiyoj hermanoqa barcopi llankʼarqa, nerqataj llankʼaj masisninmanta phishqa runas Diosmanta yachakuyta munasqankuta.
Pay qhelqarqa: “Barcopi llankʼaspapis, ashkha runasman Diosmanta yachachispa Jehovata jatunchani”, nispa.
Georg Kayser guerraman rerqa, wasinmantaj Jehovaj testigon jina kutiporqa. ¿Imaynataj chay karqa?
Payqa publicacionesninchejta leesqa, tukuy sonqotaj Diosmanta yachakusqa, nitaj maqanakorqañachu.
Guerrapi kaspapis waj llankʼayta ruwarqa.
Guerra pasaytataj ashkha watasta precursor jina sonqo kʼajaywan llankʼarqa.
Bibliamanta Yachaqajkuna guerraspi mana chhapukunankuchu kasqanta niraj sutʼita entiendejtinkupis, mana kay pachamanta runas jinachu guerrata qhawarqanku.
Iglesiasta kamachejkuna, gobiernos imaqa guerraman riyta jatunpaj qhawaj kanku, hermanostaj “Allin Kawsayta Apamoj Rey[pa]” kamachiyninpi kayta munarqanku (Isa. 9:6).
Wakin hermanosqa mana tukuy imapipunichu cheqa sonqo karqanku, jinapis hermano Konrad Mörtter jina yuyarqanku.
Chay hermanoqa nerqa: “Diospa Palabranpi sutʼita yachakuni uj cristiano runa masinta mana wañuchinanchu kasqanta”, nispa (Éxo. 20:13).*





Hans Hölterhoff hermano The Golden Age revistawan willashan, (chay revistaqa kunan ¡Despertad! sutikun).




Alemaniapeqa mana uj ley kajchu, uj runa sonqo yuyayninman jina ruwananpaj derechonta valechinanpaj.
Jinapis 20 hermanosmanta astawan guerraman mana riyta munarqankuchu.
Wakin hermanostataj locos kashasqankuta nerqanku,
Gustav Kujath hermanota jina.
Chay hermanotaqa locos kanku chayman wisqʼaykorqanku, drogaykojtaj kanku.
Hans Hölterhoff hermanotapis cuartelman mana riyta munasqanrayku carcelman wisqʼaykorqanku.
Payqa guerrapaj ni ima llankʼayta ruwayta munarqachu.
Chayraykutaj soldadosqa cuerpon nananankama uj ropata churaykuspa mayta matʼiykorqanku.
Creeyninta mana saqechiyta atispataj, mancharichinankupaj wañuchishankumanpis jina ruwarqanku.
Chayta ruwaspataj mayta asikorqanku.
Jinapis hermano Hansqa Jehovaman cheqa sonqollapuni karqa.
Waj hermanospis cuartelman rinankupaj wajyasqa kajtinku, mana guerraman riyta munarqankuchu, waj llankʼaytataj mañakorqanku.* Johannes Rauthe hermanopis waj llankʼayta mañakorqa, tren purinapitaj llankʼacherqanku.
Konrad Mörtterpis doctorta yanapaj jina llankʼarqa,
Reinhold Webertaj doctor jina llankʼarqa.
August Krafzig mayta agradecekorqa, llankʼaynin guerraman mana rinanpaj yanapasqanrayku.
Kay hermanos, paykuna jina yuyajkuna ima,
Jehovata sirviyta munarqanku, entiendesqankuman jina cheqa sonqo kaspa, munakuyta rikuchispa ima.
Autoridadesqa hermanospa qhepallankupi kasharqanku guerraman mana risqankurayku.
Chayrayku Alemaniamanta hermanosqa,
Diosmanta willasqankurayku ashkha kutista qhepan wataspi tribunalesman apasqas karqanku.
Alemania Betelpitaj Departamento legal nisqa kicharikorqa hermanosta yanapananpaj.
Chaytaj Magdeburgo llajtapi kashan.
Jehovaj testigosnenqa pisimanta pisi aswan sutʼita entienderqanku guerrasman mana rinankuchu kasqanta.
Iskay kaj Guerra Mundial qallarejtintaj, niña guerraman rerqankuchu.
Chayrayku Alemaniamanta gobiernoqa, enemigosninta jina qhawarqa, mana khuyarikuspataj paykunata qhatiykacharqa.
Chaytataj aswan qhepaman yachakusunchej. (Europa centralpi waqaychakusqanmanta).



Ñaupaj kaj Guerra Mundial kashajtin,
Bibliamanta Yachaqajkuna guerraspi mana chhapukuyta munasqankumanta, 15 de mayo de 2013 Torremanta Qhawaj revistaj “Waqaychakusqanmanta: ‘Ñakʼariy pʼunchaypi’ sinchʼita sayarqanku”, nisqa yachaqananta leey.
Ajinata ruwanankuta nikorqa, kay volumen VI de la obra Millennial Dawn (La Aurora del Milenio) de 1904 nisqapi, jinallataj agosto de 1906 watamanta alemanpi Zionʼs Watch Tower revistapi.
Chantá The Watch Tower revistaqa 1915 septiembre killapi sutʼincharqa, uj cristiano guerraman mana rinanchu kasqanta.
Jinapis chay yachaqanaqa mana alemanpi llojserqachu.




    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Quechuapi publicaciones (2004-2017)
			
		

		
			
				
				Allinchanapaj
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Kay kamachiykunata kasukuy
			
			
				Datosta apaykachakusqanmanta
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



