GEBURTSJAHR: 1953

GEBURTSLAND: AUSTRALIEN

VORGESCHICHTE: PORNOGRAFIESÜCHTIG










MEINE VERGANGENHEIT:
 Mein Vater wanderte 1949 von Deutschland nach Australien aus.
Er suchte in der Bergbau- und Kraftwerksindustrie Arbeit und lebte schließlich in einer ländlichen Gegend im Bundesstaat Victoria.
Dort heiratete er und 1953 wurde ich geboren.
Ein paar Jahre später fing meine Mutter an, mit Jehovas Zeugen die Bibel zu studieren.
Die Lehren der Bibel gehören deshalb auch zu meinen frühen Kindheitserinnerungen.
Mein Vater war jedoch ein absoluter Religionsgegner.
Er schüchterte uns ein und wurde auch handgreiflich; meine Mutter hatte richtig Angst vor ihm.
Sie beschäftigte sich aber heimlich weiter mit der Bibel und schätzte sie mit der Zeit immer mehr.
Wenn mein Vater nicht zu Hause war, erzählte sie mir und meiner Schwester, was sie gelernt hatte:
Dass die Erde bald ein Paradies wird und wie glücklich es macht, nach der Bibel zu leben (Psalm 37:10, 29; Jesaja 48:17).
Als ich 18 war, wurde mein Vater so gewalttätig, dass ich ausziehen musste.
Ich glaubte zwar, was mir meine Mutter aus der Bibel erzählt hatte, aber ich erkannte nicht den wahren Wert.
Also lebte ich auch nicht danach.
Ich arbeitete als Elektriker in den Kohlebergwerken und mit 20 heiratete ich.
Als drei Jahre später unsere erste Tochter zur Welt kam, fing ich an zu überdenken, was mir im Leben wirklich wichtig war.
Weil ich wusste, dass die Bibel uns als Familie helfen kann, begann ich einen Bibelkurs mit Jehovas Zeugen.
Aber meine Frau war absolut dagegen.
Nachdem ich eine Zusammenkunft von Jehovas Zeugen besucht hatte, stellte sie mir ein Ultimatum:
Entweder ich beende das Bibelstudium oder ich kann ausziehen.
Ich fühlte mich ohnmächtig und gab nach.
Ich brach die Verbindung zu Jehovas Zeugen ab, obwohl ich wusste, dass es das Richtige gewesen wäre.
Diesen Fehler habe ich später sehr bereut.
Durch meine Arbeitskollegen kam ich eines Tages mit Pornografie in Kontakt.
Obwohl es irgendwie faszinierend war, fand ich es doch auch widerlich und bekam furchtbare Schuldgefühle.
Ich wusste ja, dass die Bibel so etwas verurteilt, und glaubte deshalb,
Gott würde mich dafür bestrafen.
Doch ich beschäftigte mich immer öfter damit.
Pornografie wurde für mich zu etwas Normalem und irgendwann war ich süchtig danach.
In den nächsten 20 Jahren entfernte ich mich immer weiter von den Prinzipien, die mir meine Mutter hatte beibringen wollen.
An meinem Verhalten sah man, womit ich mich beschäftigte.
Ich war vulgär, hatte einen anzüglichen Humor und entwickelte eine verzerrte Ansicht über Sex.
Ich lebte zwar noch mit meiner Frau zusammen, hatte aber auch etwas mit anderen Frauen.
Irgendwann sah ich mich im Spiegel an und dachte: „Du widerst mich an!“ Aus meiner Selbstachtung war Verachtung geworden.
Meine Ehe ging in die Brüche und mein ganzes Leben war ein einziger Scherbenhaufen.
Ich betete zu Jehova und schüttete ihm mein Herz aus.
Ich setzte meinen Bibelkurs wieder fort — nach 20 Jahren Pause.
Zu der Zeit war mein Vater schon gestorben und meine Mutter war eine Zeugin Jehovas geworden.



WIE DIE BIBEL MEIN LEBEN VERÄNDERT HAT:
 Zwischen meiner Lebensweise und den Prinzipien der Bibel lag eine gewaltige Kluft.
Aber dieses Mal war ich fest entschlossen, mich zu ändern.
Ich wollte unbedingt den inneren Frieden verspüren, den die Bibel verspricht.
Also arbeitete ich daran, nicht mehr vulgär zu sein und mein aggressives Temperament in den Griff zu bekommen.
Ich beschloss, kein unmoralisches Leben mehr zu führen, mit Glücksspiel aufzuhören, mich nicht mehr zu betrinken und meinen Arbeitgeber nicht mehr zu bestehlen.
Meine Arbeitskollegen konnten nicht verstehen, warum ich mich so radikal ändern wollte.
Sie wollten, dass ich so bleibe, wie ich war, und zogen mich drei Jahre lang deswegen auf.
Sobald ich mal einen kleinen Rückfall hatte, vielleicht mal wieder laut wurde oder ein Schimpfwort sagte, riefen sie sofort triumphierend: „Ha!
Da ist er wieder, der alte Joe.“ Das tat weh!
Ich fühlte mich oft wie ein Versager.
Auf der Arbeit wurde ich überall mit Pornografie konfrontiert, ob gedruckt oder elektronisch.
Meine Kollegen verschickten über den Computer ständig anzügliche Bilder, so wie ich früher auch.
Ich wollte von meiner Sucht loskommen, aber irgendwie lagen mir meine Kollegen ständig auf der Lauer.
Ich wusste nicht mehr weiter und fragte meinen Bibellehrer, ob er mir helfen kann.
Er hörte geduldig zu, als ich ihm alles erzählte.
Mit einigen Bibelversen zeigte er mir, was mir bei meinem Problem helfen kann.
Er legte mir auch ans Herz, weiter intensiv zu Jehova zu beten (Psalm 119:37).
Eines Tages rief ich mein Team zusammen, von denen zwei als Alkoholiker bekannt waren.
Ich sagte zu den anderen, sie sollen den beiden ein Bier geben.
Alle protestierten lautstark: „Das kannst du nicht machen!
Die waren süchtig!“ Ich entgegnete: „Richtig.
Und ich auch.“ Von diesem Tag an verstanden sie, wie sehr ich mit der Pornografiesucht zu kämpfen hatte, und setzten mich nicht mehr unter Druck.
Nach einiger Zeit konnte ich dank der Hilfe Jehovas mit der Pornografiesucht brechen.
Ich war so dankbar, dass ich eine zweite Chance auf ein anständiges, glückliches Leben bekam. 1999 wurde ich ein Zeuge Jehovas.
Heute verstehe ich, warum Jehova die Dinge hasst, an denen ich so lange hing.
Wie ein liebevoller Vater wollte er mir die Folgen ersparen, die Pornografie nach sich zieht.
Die Worte aus Sprüche 3:5, 6 könnten es nicht treffender sagen: „Vertraue auf Jehova mit deinem ganzen Herzen, und stütze dich nicht auf deinen eigenen Verstand.
Beachte ihn auf all deinen Wegen, und er selbst wird deine Pfade gerade machen.“ Die Prinzipien der Bibel sind für mich nicht nur ein Schutz, sondern waren letzten Endes auch der Schlüssel zum Erfolg (Psalm 1:1-3).








WIE DIE BIBEL MEIN LEBEN BEREICHERT HAT:
 Früher hat mich mein Leben einfach nur angewidert.
Aber mittlerweile habe ich eine gesunde Selbstachtung und inneren Frieden.
Mein Leben fühlt sich sauber an und ich spüre, dass Jehova mir vergeben hat und weiter hilft.
Im Jahr 2000 heiratete ich meine Karolin, eine Glaubensschwester, die Jehova genauso liebt wie ich.
Unser Zuhause ist eine echte Oase des Friedens.
Es ist für uns etwas ganz Besonderes, zu einer weltweiten Familie zu gehören, die eine hohe Moral hat und in christlicher Liebe verbunden ist.






    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Deutsche Publikationen (2000-2017)
			
		

		
			
				
				Einstellungen
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Nutzungsbedingungen
			
			
				Datenschutzerklärung
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



