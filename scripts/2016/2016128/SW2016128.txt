KAZI ya Ufalme nchini Brazili ilifanywa kwa kasi ndogo mwanzoni mwa miaka ya 1930.
Lakini katika mwaka wa 1935, mapainia Nathaniel na Maud Yuille walimwandikia barua Joseph F. Rutherford, ambaye alikuwa akiongoza kazi ya kuhubiri wakati huo.
Walisema kwamba wamejitolea kufanya utumishi na ‘wangefurahia kwenda mahali popote.’
Nathaniel, mhandisi wa ujenzi mstaafu, alikuwa na umri wa miaka 62 wakati huo.
Alikuwa mkurugenzi wa utumishi katika kutaniko la Mashahidi wa Yehova huko San Francisco,
California,
Marekani.
Aliratibu kazi ya kuhubiri na alitumia vifaa vya sauti ili kueneza habari njema.
Uzoefu na utayari wake ulikuwa baraka katika mgawo wake mpya akiwa mtumishi wa tawi kwenye eneo kubwa sana na lenye lugha nyingi—Brazili.
Nathaniel na Maud waliwasili nchini Brazili mwaka wa 1936, wakiwa na mkalimani na painia mwenzao Antonio P. Andrade.
Walikuwa wamebeba vitu vyenye thamani sana, yaani gramafoni 35 na gari lenye kipaza sauti.
Wakati huo,
Brazili ambayo ni nchi ya tano kwa ukubwa duniani, ilikuwa na wahubiri wa Ufalme 60 tu!
Hata hivyo, vifaa hivyo vipya vya sauti vingewasaidia kuwahubiria mamilioni ya watu ndani ya miaka michache tu.
Mwezi mmoja baada ya Nathaniel na Maud kuwasili, ofisi ya tawi ya Brazili ilipanga kwa mara ya kwanza kusanyiko ambalo ratiba yake ilihusisha wakati wa kwenda kuhubiri; kusanyiko hilo lilifanywa jijini São Paulo.
Inaelekea Maud ndiye aliyekuwa akiendesha gari lenye kipaza sauti lilipokuwa likifanya kampeni ya kuwaalika watu kwenye hotuba ya watu wote, ambapo watu 110 walihudhuria!
Kusanyiko hilo liliwahamasisha zaidi wahubiri, nao walichochewa kuongeza bidii yao katika huduma ya shambani.
Walijifunza jinsi ya kuhubiri kwa kutumia machapisho na kadi za ushuhuda, na vilevile kutumia gramafoni kucheza sauti zilizorekodiwa katika Kihispania,
Kihungaria,
Kiingereza,
Kijerumani,
Kipolandi, na baadaye Kireno.





Gari hili lenye kipaza sauti lilitumika kuwahubiria habari njema mamilioni ya watu nchini Brazili




Makusanyiko matatu kama hayo, ambayo yalifanywa São Paulo,
Rio de Janeiro, na Curitiba mwaka wa 1937, yaliongeza kasi ya kazi ya kuhubiri.
Wahudhuriaji waliambatana na gari lenye kipaza sauti katika mahubiri ya nyumba kwa nyumba.
José Maglovsky, ambaye alikuwa kijana mdogo wakati huo, baadaye aliandika hivi: “Tungeweka machapisho yetu ya Biblia jukwaani, na gari lenye kikuza sauti lilipokuwa likitangaza ujumbe uliorekodiwa, sisi tungezungumza na watu waliotoka nyumbani mwao ili kuja kuona ni nini kilichokuwa kikitendeka.”
Ubatizo ulifanywa kwenye mito huku watu waliokuja kuoga wakiota jua pembezoni.
Kwa kweli, ilikuwa fursa ya pekee sana kuhubiri habari njema kwa kutumia gari lenye kipaza sauti!
Sauti nzito ya Ndugu Rutherford ilipokuwa ikisikika kwenye vikuza sauti alipokuwa akitoa hotuba ya ubatizo, watu wadadisi walilizunguka gari na kusikiliza hotuba hiyo iliyokuwa ikitafsiriwa katika Kireno.
Baada ya hapo, watu walibatizwa huku nyimbo za Ufalme zilizorekodiwa katika Kipolandi zikichezwa.
Ndugu na dada waliimba kwa lugha tofauti tofauti wakifuatisha rekodi hizo. Kitabu cha Mwaka wa 1938, kilieleza hivi: “Hilo liliwakumbusha jinsi ambavyo kila mtu katika siku ya Pentekoste alielewa lugha yake mwenyewe.”
Baada ya makusanyiko, kila Jumapili, iwe kulikuwa na mvua au jua, hotuba za Biblia zilizorekodiwa zilichezwa katika gari lenye kipaza sauti na kuwafikia watu walio kwenye bustani, maeneo ya makazi, na viwanda vilivyokuwa katikati ya São Paulo na katika miji ya karibu.
Kila mwezi, gari lenye kipaza sauti lilitumiwa kuwafikia watu 3,000 walioishi kwenye makazi ya watu wenye ukoma, yaliyokuwa umbali wa kilometa 97 kaskazini magharibi mwa São Paulo.
Baada ya muda, kutaniko imara lilianzishwa.
Licha ya ugonjwa wao wenye kuhuzunisha, wahubiri hao wa Ufalme walipata kibali cha kutembelea makazi mengine ya watu wenye ukoma ili kuwapa ujumbe wa Biblia wenye kufariji.
Hatimaye, rekodi za ujumbe wa Ufalme katika Kireno zilianza kutumiwa mwishoni mwa mwaka wa 1938.
Katika Siku ya Wafu (siku ya kuwakumbuka wafu), gari hilo lenye kipaza sauti lilienda kutoka eneo moja la makaburi hadi lingine huku likicheza rekodi za sauti zenye vichwa “Wafu Wako Wapi?,” “Yehova,” na “Utajiri,” na ujumbe huo uliwafikia zaidi ya waombolezaji 40,000!
Viongozi wa dini wenye hasira walichukizwa sana na tangazo hilo la hadharani la kweli ya Biblia, na mara nyingi waliwashinikiza wenye mamlaka wapige marufuku gari hilo lenye kipaza sauti.
Dada Yuille anakumbuka wakati fulani ambapo kasisi mmoja aliuchochea umati wa watu ulizingire gari hilo lenye kipaza sauti.
Hata hivyo meya na maofisa wa polisi walifika na walisikiliza programu nzima.
Meya alikubali kupokea machapisho ya Biblia.
Hakukuwa na vurugu siku hiyo.
Licha ya upinzani huo, ripoti ya Kitabu cha mwaka wa 1940 kuhusu Brazili ilieleza kwamba mwaka wa 1939 ulikuwa “wakati bora zaidi wa kumtumikia Theokrati Mkuu na kutangaza jina lake.”
Kwa kweli, kuwasili kwa “gari lenye kipaza sauti la Watch Tower” kulibadili kabisa kazi ya kuhubiri nchini Brazili.
Lilikuwa kifaa muhimu sana cha kuwafikishia mamilioni ya watu ujumbe wa Ufalme.
Ingawa gari hilo lililojulikana sana liliuzwa mwaka wa 1941, maelfu ya Mashahidi wa Yehova wameendelea kuwatangazia habari njema watu wanyofu katika eneo kubwa sana la Brazili.—Kutoka katika hifadhi ya vitu vyetu vya kale nchini Brazili.






    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Machapisho ya Kiswahili (2000-2017)
			
		

		
			
				
				Mapendezi
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Masharti ya Utumiaji
			
			
				Sera ya Faragha
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



