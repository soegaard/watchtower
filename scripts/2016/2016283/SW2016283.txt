CORINNA alipokuwa na umri wa miaka 17 tu, mama yake alikamatwa na kupelekwa kwenye kambi ya mateso ya Sovieti.
Baadaye,
Corinna mwenyewe alihamishiwa nchini Siberia, maelfu ya kilomita mbali na nyumbani.
Alitendewa kama mtumwa, na wakati mwingine alilazimishwa kufanya kazi nje wakati wa baridi kali bila mavazi ya kutosha.
Licha ya hali hizo mbaya,
Corinna na dada mwingine waliazimia kuhudhuria mkutano wa kutaniko.
2 Anaeleza hivi: “Tuliondoka mahali petu pa kazi jioni, na tukatembea umbali wa kilomita 25 hadi kwenye kituo cha gari moshi.
Gari moshi liliondoka saa nane usiku, na tulisafiri kwa saa sita, kisha tukashuka na kutembea umbali wa kilomita 10 hadi mahali pa mikutano.” Safari hiyo ilikuwa na manufaa gani?
Corinna anaeleza hivi: “Katika mkutano, tulijifunza gazeti la Mnara wa Mlinzi na tukaimba nyimbo za Ufalme.
Imani yetu ilijengwa na tuliimarishwa sana.” Ingawa walirudi kazini siku tatu baadaye, msimamizi wao wa kazi hata hakutambua kwamba Corinna na mwenzake walikuwa wameondoka.
3 Siku zote watu wa Yehova wamethamini fursa za kukutanika pamoja.
Muda mfupi baada ya kutaniko la Kikristo kuanzishwa, wafuasi wa Yesu “walikuwa wakihudhuria daima kwenye hekalu.” (Mdo. 2:46)
Inaelekea wewe pia unapenda kuhudhuria mikutano kwa ukawaida kama Wakristo hao.
Hata hivyo,
Wakristo wote hukabili changamoto mbalimbali.
Kazi ya kuajiriwa, ratiba yenye mambo mengi, au uchovu kwa sababu ya shughuli za kila siku ni mambo yanayoweza kufanya iwe vigumu kuhudhuria mikutano.
Ni nini kitakachotusaidia tushinde changamoto hizo na kuwa na zoea la kuhudhuria mikutano kwa ukawaida?[1] Tunaweza kuwasaidiaje wanafunzi wetu wa Biblia na washiriki wengine wa kutaniko waone umuhimu wa kuhudhuria mikutano?
Katika makala hii, tutachunguza sababu nane za kukutanika pamoja kwa ajili ya ibada.
Sababu hizo zinaweza kuwekwa katika makundi matatu: jinsi unavyonufaika, jinsi wengine wanavyonufaika, na jinsi Yehova anavyohusika unapohudhuria mikutano.[2]



JINSI UNAVYONUFAIKA

4. Mikutano yetu hutusaidiaje kujifunza kumhusu Yehova?
4 Mikutano hutuelimisha. Kila mkutano wa kutaniko hutusaidia kujifunza kumhusu Mungu wetu,
Yehova.
Kwa mfano, kwa miaka miwili hivi iliyopita, makutaniko mengi yalijifunza kitabu Mkaribie Yehova katika Funzo la Biblia la Kutaniko.
Je, si kweli kwamba kujifunza sifa za Mungu, pamoja na maelezo ya kutoka moyoni ya ndugu na dada zako, kumefanya umpende zaidi Baba yako wa mbinguni?
Isitoshe, tunaongeza ujuzi wetu wa Neno la Mungu tunaposikiliza kwa makini hotuba, maonyesho, na usomaji wa Biblia. (Neh. 8:8)
Kwa mfano, fikiria hazina za kiroho ambazo unapata kila juma unapojitayarisha kwa ajili ya usomaji wa Biblia na unaposikiliza mambo makuu ya usomaji huo!
5. Mikutano imekusaidiaje kutumia mafundisho ya Biblia na kuboresha ustadi wako wa kuhubiri?
5 Mikutano hutufundisha jinsi ya kutumia kanuni za Biblia katika sehemu zote za maisha yetu. (1 The. 4:9, 10)
Kwa mfano, funzo la Mnara wa Mlinzi limeandaliwa kwa ajili ya mahitaji ya watu wa Mungu.
Je, funzo la Mnara wa Mlinzi limekuchochea uchunguze upya malengo yako, umsamehe Mkristo mwenzako, au uboreshe sala zako?
Mkutano wa katikati ya juma hutuelimisha kwa ajili ya huduma.
Tunajifunza jinsi ya kuhubiri habari njema na kufundisha kanuni za Biblia kwa ustadi.—Mt. 28:19, 20.
6. Ni kwa njia gani mikutano yetu inatutia moyo na kutuimarisha?
6 Mikutano hututia moyo. Mfumo huu unaweza kutudhoofisha kiakili, kihisia, na kiroho.
Tofauti na hilo, mikutano hututia moyo na kutuimarisha. (Soma Matendo 15:30-32.) Katika mikutano, mara nyingi tunachunguza kutimizwa kwa unabii wa Biblia.
Kufanya hivyo huimarisha uhakika wetu kwamba ahadi za Yehova za wakati ujao zitatimia pia.
Bila shaka, si walio na migawo tu wanaotutia moyo.
Waabudu wenzetu wanaotoa maelezo na kuimba kutoka moyoni hutujenga pia. (1 Kor. 14:26)
Na tunapozungumza na ndugu na dada zetu kabla na baada ya mikutano, tunahisi kwamba tunapendwa na hilo hutuburudisha sana.—1 Kor. 16:17, 18.
7. Kwa nini kuhudhuria mikutano ni muhimu sana?
7 Mikutano yetu hutusaidia kuongozwa na roho takatifu. Yesu Kristo alisema hivi baada ya kurudi mbinguni: “Yeye aliye na sikio na asikie yale ambayo roho inayaambia makutaniko.” (Ufu. 2:7)
Naam,
Yesu anaongoza kutaniko la Kikristo kupitia roho takatifu.
Tunahitaji roho takatifu ili tupinge vishawishi, tupate ujasiri na uwezo wa kutimiza huduma yetu, na vilevile tufanye maamuzi mazuri.
Je, hatupaswi kutumia vizuri fursa zote tulizonazo, ikiwemo mikutano yetu, ili tupokee roho hiyo?



JINSI WENGINE WANAVYONUFAIKA

8. Ndugu zetu wananufaikaje wanapotuona mikutanoni, na wanaposikia tukiimba na kutoa maelezo? (Tazama pia sanduku lenye kichwa “Sikuzote Yeye Huondoka Akiwa Mwenye Shangwe.”)
8 Mikutano hutupatia fursa ya kuwaonyesha ndugu zetu kwamba tunawapenda. Fikiria kwa makini changamoto ambazo baadhi ya ndugu na dada katika kutaniko lenu hupitia.
Haishangazi kwamba mtume Paulo aliandika hivi: “Acheni tufikiriane”!
Baadaye Paulo alieleza kwamba tunaweza kuwafikiria ndugu zetu kwa ‘kutoacha kukusanyika pamoja.’ (Ebr. 10:24, 25)
Kuwapo kwako mikutanoni kunaonyesha kwamba unawathamini waabudu wenzako kwa kutumia wakati wako kuwasikiliza na kuwafikiria.
Zaidi ya hilo, unapotoa maelezo ya kutoka moyoni na unapoimba, unawatia moyo Mashahidi wenzako.—Kol. 3:16.
9, 10. (a)
Eleza jinsi maneno ya Yesu kwenye Yohana 10:16 yanavyotusaidia kuelewa umuhimu wa kukutanika pamoja na ndugu zetu. (b) Tunapohudhuria mikutano kwa ukawaida tunawanufaishaje wale wanaopingwa na familia zao?
9 Mikutano hutuunganisha na waabudu wenzetu. (Soma Yohana 10:16.) Yesu alijilinganisha na mchungaji, na akawafananisha wafuasi wake na kundi la kondoo.
Fikiria jambo hili:
Ikiwa kondoo wawili wako mlimani, wengine wawili wako bondeni, na mwingine yuko malishoni katika eneo tofauti, je, tunaweza kusema kwamba kondoo hao watano ni kundi moja?
Kwa kawaida, kundi la kondoo hukaa pamoja chini ya utunzaji wa mchungaji.
Vivyo hivyo, hatuwezi kumfuata Mchungaji wetu ikiwa tunajitenga kimakusudi na wengine.
Tunahitaji kukutanika pamoja na Wakristo wenzetu ili tuwe sehemu ya “kundi moja” chini ya “mchungaji mmoja.”
10 Kila tunapohudhuria mkutano, tunachangia umoja wa undugu wetu. (Zab. 133:1)
Baadhi ya waabudu wenzetu wamekataliwa na wazazi na watu wao wa familia.
Hata hivyo,
Yesu aliahidi kwamba atawapa familia ya kiroho ambayo itawapenda na kuwatunza. (Marko 10:29, 30)
Unapohudhuria mikutano kwa ukawaida, unakuwa kama baba, mama, ndugu, au dada ya ndugu na dada hao wapendwa!
Je, hilo halituchochei kufanya yote tuwezayo kuhudhuria mikutano yote?
SIKUZOTE YEYE HUONDOKA AKIWA MWENYE SHANGWE
“KATIKA siku za hivi karibuni, imekuwa vigumu kwangu kufika mikutanoni kwa sababu ya matatizo ya afya.
Lakini ninapohudhuria, ninafurahia sana chakula kizuri cha kiroho ambacho Yehova ameandaa.
Ingawa mimi huja nikiwa na maumivu ya magoti, matatizo ya moyo na ugonjwa wa kisukari, sikuzote mimi huondoka nikiwa na shangwe zaidi kuliko nilivyokuja.
  “Niliposikia kwa mara ya kwanza wimbo namba 68, ‘Sala ya Mtu wa Hali ya Chini,’ ukiimbwa kutanikoni, nilitokwa na machozi.
Ulikuwa mtamu sana!
Vifaa vya kunisaidia kusikia vilinasa sauti ya kila mtu, nami nikaimba pamoja nao.
Ilisisimua sana kuwa hapo!”—George, miaka 58.






JINSI YEHOVA ANAVYOHUSIKA

11. Kuhudhuria mikutano hutusaidiaje kumpa Yehova kile anachostahili?
11 Tunapohudhuria mikutano tunampa Yehova kile anachostahili. Akiwa Muumba wetu,
Yehova anastahili sifa, utukufu, shukrani, na heshima. (Soma Ufunuo 7:12.) Tunaposali, kuimba, na kuzungumza kumhusu Yehova katika mikutano yetu, tunampa kile anachostahili kabisa, yaani ibada yetu.
Tunaonyesha kwamba tunathamini sana fursa ya pekee tuliyonayo ya kumheshimu kwa sababu ya mambo mengi ambayo ametufanyia.
12. Yehova anahisije tunapotii amri yake ya kuhudhuria mikutano?
12 Yehova anastahili pia utii wetu.
Ametuamuru tusiache kukusanyika pamoja, hasa katika nyakati hizi za mwisho.
Yehova anafurahi tunapotii kwa hiari amri hiyo. (1 Yoh. 3:22)
Anatambua na anathamini jitihada tunazofanya ili tuhudhurie kila mkutano.—Ebr. 6:10.
13, 14. Ni kwa njia gani tunamkaribia Yehova na Yesu tunapohudhuria mikutano?
13 Tunapohudhuria mikutano tunaonyesha kwamba tunataka kumkaribia Yehova na Mwana wake. Katika mikutano yetu,
Mfundishaji wetu Mkuu anatupatia maagizo kupitia Neno lake,
Biblia. (Isa. 30:20, 21)
Hata watu wasio Mashahidi wanapohudhuria mikutano yetu, wanaweza kukata shauri hivi: “Kwa kweli Mungu yupo katikati yenu.” (1 Kor. 14:23-25)
Yehova hubariki mikutano yetu kwa kutupatia roho yake takatifu na huongoza programu za ufundishaji wa Biblia.
Hivyo, katika mikutano yetu, tunasikiliza sauti ya Yehova na kujionea jinsi anavyotutunza kwa upendo.
Hilo hutufanya tumkaribie.
14 Yesu alisema hivi: “Palipo na wawili au watatu wakiwa wamekusanyika pamoja katika jina langu, hapo mimi nipo katikati yao.” (Mt. 18:20)
Kanuni inayopatikana katika maneno hayo ya Yesu inahusu mikutano yetu.
Akiwa kichwa cha kutaniko,
Kristo ‘anatembea katikati’ ya makutaniko ya watu wa Mungu. (Ufu. 1:20–2:1)
Fikiria jambo hilo!
Yehova na Yesu wako pamoja nasi, nao hutuimarisha tunapohudhuria mikutano ya Kikristo.
Unafikiri Yehova anahisije anapoona tamaa yetu ya kumkaribia yeye na Mwana wake?
15. Kuhudhuria mikutano kunaonyeshaje kwamba tunataka kumtii Mungu?
15 Tunapohudhuria mikutano tunaonyesha kwamba tunaunga mkono enzi kuu ya Mungu. Ingawa Yehova anatuamuru tuhudhurie mikutano, hatulazimishi kufanya hivyo. (Isa. 43:23)
Kwa hiyo, ni juu yetu kuonyesha jinsi tunavyompenda Yehova kutoka moyoni, na jinsi tunavyounga mkono kwa uthabiti utawala wake. (Rom. 6:17)
Kwa mfano, tunaweza kushinikizwa na mwajiri ambaye anasisitiza tusihudhurie mikutano kwa ukawaida ili tufanye kazi ya kimwili.
Tunaweza kupingwa na serikali inayotishia kututoza faini, kutufunga gerezani, au kutufanyia jambo lingine baya zaidi ikiwa tutakutanika pamoja kwa ajili ya ibada.
Au tunaweza kushawishiwa kujihusisha katika burudani badala ya kuhudhuria mkutano.
Katika visa vyote hivyo, tunahitaji kuamua tutamtumikia nani. (Mdo. 5:29)
Tunapochagua kuunga mkono enzi kuu ya Yehova, tunafanya moyo wake ushangilie.—Met. 27:11.



ENDELEA KUJITAHIDI KUHUDHURIA MIKUTANO

16, 17. (a)
Tunajuaje kwamba mikutano ilikuwa muhimu sana kwa Wakristo wa karne ya kwanza? (b) Ndugu George Gangas alihisije kuhusu mikutano ya Kikristo?
16 Baada ya muujiza wenye kustaajabisha katika Pentekoste ya mwaka wa 33 W.K.,
Wakristo wa karne ya kwanza hawakuacha kukutanika kwa ukawaida. ‘Nao wakaendelea kujitoa wenyewe kwa fundisho la mitume na kuhudhuria daima kwenye hekalu.’ (Mdo. 2:42, 46)
Neno la Kigiriki linalotafsiriwa “kujitoa wenyewe” linatoa wazo la kuendelea kwa uvumilivu katika mwendo fulani kwa jitihada nyingi.
Haikuwa rahisi kwa Wakristo hao kuhudhuria mikutano kwa sababu ya utawala wa Roma na upinzani kutoka kwa viongozi wa dini ya Kiyahudi.
Hata hivyo, waliendelea kufanya hivyo kwa uvumilivu.
17 Watumishi wengi wa Yehova leo wameonyesha kwamba wanathamini sana mikutano ya Kikristo.
George Gangas, aliyekuwa mshiriki wa Baraza Linaloongoza kwa zaidi ya miaka 22, alisema hivi: “Kwangu, kukutana na ndugu ni moja ya mambo yanayonifurahisha zaidi maishani na ni chanzo cha kitia moyo.
Napenda kuwa kati ya watu wa kwanza kufika Jumba la Ufalme, na ikiwezekana kuwa miongoni mwa watu wa mwisho kuondoka.
Ninapata shangwe ninapoongea na watu wa Mungu.
Ninapokuwa miongoni mwao nahisi niko nyumbani pamoja na familia, katika paradiso ya kiroho.” Aliongeza hivi: “Kama tu ambavyo sikuzote mshale wa dira unaonyesha upande wa kaskazini, ndivyo mawazo na tamaa yangu ya ndani kabisa inavyonichochea kuhudhuria mikutano.”
18. Unahisije kuhusu mikutano yetu, nawe umeazimia kufanya nini?
18 Je, unahisi hivyo kuhusu kukusanyika pamoja ili kumwabudu Yehova?
Basi azimia kuvumilia na kuendelea kujitahidi sana kuhudhuria mikutano kwa ukawaida.
Kwa njia hiyo, utaonyesha kwamba unakubaliana na Mfalme Daudi ambaye alisema hivi: “Yehova, nimependa makao ya nyumba yako.”—Zab. 26:8.



[1] (fungu la 3)
Baadhi ya waabudu wenzetu hawawezi kuhudhuria mikutano kwa ukawaida kwa sababu zilizo nje ya uwezo wao, kama vile ugonjwa mbaya.
Wanaweza kuwa na uhakika kwamba Yehova anaelewa hali yao na anathamini ibada yao ya nafsi yote.
Wazee wanaweza kuwasaidia watu wenye hali kama hizo wanufaike na programu ya kiroho, labda kwa kuwaunganisha kupitia simu wakati wa mikutano au kwa kurekodi mikutano hiyo kwa ajili yao.
[2] (fungu la 3)
Tazama sanduku lenye kichwa “Sababu za Kuhudhuria Mikutano.”






SABABU ZA KUHUDHURIA MIKUTANO
             Inatuelimisha.

             Inatutia moyo.

             Inatusaidia kuongozwa na roho takatifu.

             Inatupatia fursa ya kuwaonyesha ndugu zetu kwamba tunawapenda.

             Inatuunganisha pamoja na waabudu wenzetu.

             Tunapohudhuria tunampa Yehova kile anachostahili.

             Tunapohudhuria tunaonyesha kwamba tunataka kumkaribia Yehova na Mwana wake.

             Tunapohudhuria tunaonyesha kwamba tunaunga mkono enzi kuu ya Mungu.







    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Machapisho ya Kiswahili (2000-2017)
			
		

		
			
				
				Mapendezi
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Masharti ya Utumiaji
			
			
				Sera ya Faragha
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



