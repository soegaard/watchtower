ANLÄSSLICH der jährlichen Feier zum Gedenken an den Tod Jesu hat bestimmt schon jeder Römer 8:15-17 gelesen.
Dieser Schlüsseltext erklärt, woher ein Christ weiß, dass er gesalbt ist:
Der heilige Geist bezeugt es mit seinem Geist.
Gleich der erste Vers des Kapitels bezieht sich auf „die, welche mit Christus Jesus in Gemeinschaft sind“.
Richtet sich Römer,
Kapitel 8 aber nur an Gesalbte oder auch an Christen, die hoffen, ewig auf der Erde zu leben?
2 Das Kapitel betrifft vor allem gesalbte Christen.
Sie erhalten „den Geist“ und warten „auf die Annahme an Sohnes statt“, „die Befreiung . . . von [ihrem fleischlichen]
Leib“ (Röm. 8:23).
Es erwartet sie eine Zukunft als Söhne Gottes im Himmel.
Das ist möglich, weil sie getaufte Christen wurden und Gott das Lösegeld auf sie anwandte, ihnen ihre Sünden vergab und sie als seine geistigen Söhne gerechtsprach (Röm. 3:23-26; 4:25; 8:30).
 3. Warum sollten sich auch diejenigen für Römer,
Kapitel 8 interessieren, die hoffen, einmal ewig auf der Erde zu leben?
3 Römer,
Kapitel 8 ist aber auch für alle von Interesse, die hoffen, im Paradies zu leben, denn Gott betrachtet sie in gewissem Sinn als gerecht.
Das wird durch das angedeutet, was Paulus in Kapitel 4 über Abraham schrieb.
Dieser Glaubensmann lebte, bevor Jehova den Israeliten das Gesetz gab und lange bevor Jesus für unsere Sünden starb.
Dennoch bemerkte Jehova, welch außergewöhnlichen Glauben Abraham hatte, und bezeichnete ihn als gerecht. (Lies Römer 4:20-22.) Wie sieht es mit den glaubensvollen Christen heute aus, die hoffen, einmal für immer auf der Erde zu leben?
Auch sie kann Jehova in ähnlicher Weise als gerecht betrachten.
Somit kann ihnen der Rat aus Römer,
Kapitel 8 zugutekommen, der Gerechten gegeben wird.
 4. Worüber sollten wir in Verbindung mit Römer 8:21 nachdenken?
4 Römer 8:21 enthält die Garantie:
Die neue Welt wird ganz sicher kommen!
Dieser Vers verspricht, dass „die Schöpfung selbst auch von der Sklaverei des Verderbens frei gemacht werden wird zur herrlichen Freiheit der Kinder Gottes.“ Die Frage ist, ob wir dann dabei sind, ob wir den Lohn erhalten.
Bist du zuversichtlich, dass du dabei sein wirst?
Der Rat aus Römer,
Kapitel 8 wird dir dabei helfen.



„DAS SINNEN DES FLEISCHES“

 5. Über welche ernste Angelegenheit sprach Paulus in Römer 8:4-13?
5 Lies Römer 8:4-13. Römer,
Kapitel 8 stellt Menschen, die „in Übereinstimmung mit dem Fleisch“ leben, denen gegenüber, die „in Übereinstimmung mit dem Geist“ leben.
Einige denken vielleicht, hier würden die Christen den Nichtchristen gegenübergestellt beziehungsweise die, die in der Wahrheit sind, denen, die es nicht sind.
Paulus schrieb jedoch an die, „die als Geliebte Gottes, zu Heiligen Berufene in Rom“ waren (Röm. 1:7).
Er verglich hier also Christen, die in Übereinstimmung mit dem Fleisch lebten, mit Christen, die in Übereinstimmung mit dem Geist lebten.
Was unterschied sie voneinander?
 6, 7. (a)
Welche Bedeutungen hat das Wort „Fleisch“ in der Bibel? (b) Was meinte Paulus mit dem Wort „Fleisch“ in Römer 8:4-13?
6 Befassen wir uns zuerst mit dem Wort „Fleisch“.
Was meinte Paulus damit? „Fleisch“ bezeichnet in der Bibel unterschiedliche Dinge.
Manchmal bezieht es sich auf das buchstäbliche Fleisch unseres Körpers (Röm. 2:28; 1. Kor. 15:39, 50).
Es kann auch Blutsverwandtschaft bedeuten: „Dem Fleische nach“ ging Jesus „aus dem Samen Davids“ hervor.
Und Paulus betrachtete die Juden als „Verwandte nach dem Fleische“ (Röm. 1:3; 9:3).
7 Was meinte Paulus mit dem Wort „Fleisch“ in Römer 8:4-13? Kapitel 7 liefert uns einen Hinweis.
Paulus brachte dort den Gedanken, in Übereinstimmung mit dem „Fleisch“ zu leben, mit „sündigen Leidenschaften“ in Verbindung, die „in . . . [den]
Gliedern wirksam“ sind (Röm. 7:5).
Das wirft Licht darauf, um wen es sich bei denjenigen handelt, „die mit dem Fleisch in Übereinstimmung sind“.
Von ihnen sagte Paulus, sie würden „ihren Sinn auf die Dinge des Fleisches“ richten.
Er bezog sich auf Personen, die sich auf Wünsche und Neigungen, die die Unvollkommenheit mit sich bringt, konzentrieren oder sich davon beherrschen lassen.
Das sind vor allem Menschen, die ihren Impulsen,
Begierden und Leidenschaften folgen, seien sie sexueller oder anderer Art.
 8. Warum war es passend, sogar gesalbte Christen vor dem „Sinnen des Fleisches“ zu warnen?
8 Vielleicht fragst du dich, warum Paulus gerade Gesalbte davor warnte, „in Übereinstimmung mit dem Fleisch“ zu leben.
Besteht für Christen heute, die Gott als seine Freunde angenommen hat und die er als gerecht betrachtet, diese Gefahr ebenfalls?
Tatsächlich könnte jeder Christ beginnen, in Übereinstimmung mit dem sündigen Fleisch zu leben.
Von einigen Brüdern in Rom schrieb Paulus beispielsweise, dass sie Sklaven „ihres eigenen Bauches“ waren, also ihrer Begierden — ob sexueller Natur oder in Bezug auf Essen,
Trinken und anderes.
Manche dieser Brüder täuschten „das Herz der Arglosen“ (Röm. 16:17, 18; Phil. 3:18, 19; Jud. 4, 8, 12).
Denken wir auch an den Bruder in Korinth, der eine Zeit lang mit der „Frau seines Vaters“ zusammenlebte (1. Kor. 5:1).
Kein Wunder, dass Gott Christen durch Paulus vor dem „Sinnen des Fleisches“ warnte (Röm. 8:5, 6).
 9. Worauf bezieht sich die Warnung von Paulus in Römer 8:6 nicht?
9 Heute ist diese Warnung noch genauso gültig.
Auch wenn ein Christ Gott schon viele Jahre dient, könnte er beginnen, seinen Sinn auf Dinge des Fleisches zu richten.
Damit ist aber nicht jemand gemeint, der gelegentlich über Essen,
Arbeit,
Freizeit oder auch seine Beziehung nachdenkt.
All das gehört für Diener Jehovas gewöhnlich zum Leben dazu.
Auch Jesus aß gerne, versorgte andere mit Essen und erkannte, wie wichtig es ist, zwischendurch eine Pause zu machen.
Und Paulus schrieb, dass Intimität und Leidenschaft in der Ehe sehr wohl ihren Platz haben.





Ist unser Sinn auf das Fleisch oder den Geist gerichtet?
Was verraten unsere Gespräche? (Siehe Absatz 10, 11)




10. Was schließt der Ausdruck „Sinnen des Fleisches“ in Römer 8:5, 6 ein?
10 Was meinte Paulus also mit „Sinnen des Fleisches“?
Das griechische Wort, das Paulus verwendete, bedeutet, „Sinn oder Herz auf etwas zu richten, die Fähigkeit einzusetzen,
Gedanken im Geiste zu entwickeln, wobei die zugrunde liegende Einstellung oder Haltung ausschlaggebend ist“.
Wer in Übereinstimmung mit dem Fleisch lebt, lässt sein Leben hauptsächlich von seiner sündigen menschlichen Natur bestimmen.
Ein Gelehrter sagt über dieses Wort in Römer 8:5: „Sie richten ihren Sinn auf die Dinge des Fleisches — sind daran am meisten interessiert, sprechen ständig darüber, beschäftigen sich eingehend damit und gehen darin auf.“
11. Wie können wir herausfinden, worum sich unser Leben dreht?
11 Die Christen in Rom sollten darüber nachdenken, was in ihrem Leben wirklich das Wichtigste war.
Drehte sich ihr Leben vielleicht um „Dinge des Fleisches“ oder wurde es davon dominiert?
Auch für uns ist es gut, unser Leben einmal zu untersuchen.
Was begeistert uns am meisten und wohin tendieren unsere Gespräche?
Womit beschäftigen wir uns tagein, tagaus?
Einige stellen möglicherweise fest, dass sich ihr Leben darum dreht,
Weine zu probieren, sich chic einzurichten oder neu einzukleiden,
Geld anzulegen,
Urlaubsreisen zu planen und so weiter.
Das alles ist an sich nichts Schlechtes und kann zum Leben dazugehören.
Jesus sorgte zum Beispiel einmal für Wein und Paulus sagte Timotheus, er solle „ein wenig Wein“ trinken (1. Tim. 5:23; Joh. 2:3-11).
Doch sprachen Jesus und Paulus ständig über Wein, beschäftigten sie sich nur noch damit oder gingen sie darin auf?
War das ihre große Leidenschaft?
Nein.
Wie ist es mit uns?
Was steht in unserem Leben im Mittelpunkt?
12, 13. Warum sollten wir uns ernsthaft Gedanken darüber machen, worauf wir unseren Sinn richten?
12 Warum ist eine Selbstprüfung wichtig?
Paulus schrieb: „Das Sinnen des Fleisches bedeutet Tod“ — geistigen Tod heute und buchstäblichen Tod in der Zukunft (Röm. 8:6).
Nehmen wir das nicht auf die leichte Schulter!
Paulus wollte jedoch nicht sagen, dass jemand in jedem Fall stirbt, wenn er seinen Sinn auf das Fleisch richtet.
Man kann sich ändern.
Denken wir an den Mann in Korinth, der „dem Fleisch“ nachging und ausgeschlossen werden musste.
Er änderte sich.
Er richtete sich nicht länger nach dem Fleisch aus, sondern kehrte wieder auf den richtigen Weg zurück (2. Kor. 2:6-8).
13 Wenn er das konnte, dann kann das auch ein Christ heute — vor allem wenn er nicht so weit gegangen ist wie der Mann in Korinth.
Wer über die warnenden Worte von Paulus nachdenkt, wozu das Sinnen des Fleisches letztendlich führen könnte, fühlt sich bestimmt motiviert, nötige Änderungen vorzunehmen.



„DAS SINNEN DES GEISTES“

14, 15. (a)
Worauf sollten wir den Sinn richten? (b) Was ist mit dem „Sinnen des Geistes“ nicht gemeint?
14 Nachdem Paulus vor dem „Sinnen des Fleisches“ gewarnt hatte, gab er die Zusicherung: „Das Sinnen des Geistes . . . bedeutet Leben und Frieden.“ Welch eine Belohnung:
Leben und Frieden!
Wie können wir die Belohnung bekommen?
15 Mit dem „Sinnen des Geistes“ ist nicht gemeint, vergeistigt zu sein, als ob man nur noch über die Bibel, die Liebe zu Gott und die Zukunftshoffnung nachdenken und sprechen müsste.
Wie war es mit Paulus und anderen im 1. Jahrhundert, die Gott gefielen?
Im Großen und Ganzen führten sie ein recht normales Leben.
Dazu gehörte Essen und Trinken sowie zu arbeiten, um für sich zu sorgen; viele heirateten und führten ein schönes Familienleben (Mar. 6:3; 1. Thes. 2:9).
16. Was war für Paulus das Wichtigste?
16 Diese Diener Gottes ließen Alltägliches allerdings nicht zum Wichtigsten in ihrem Leben werden.
Nach dem Hinweis, dass Paulus als Zeltmacher arbeitete, zeigt der Bericht, worum sich sein Leben drehte:
Er war regelmäßig als Prediger und Lehrer unterwegs. (Lies Apostelgeschichte 18:2-4; 20:20, 21, 34, 35.) Der Dienst für Gott stand in seinem Leben immer im Mittelpunkt.
Und genau das empfahl er auch den Brüdern und Schwestern in Rom.
Sie sollten ihn nachahmen, und das sollten auch wir (Röm. 15:15, 16).
17. Wie wirkt sich das „Sinnen des Geistes“ aus?
17 Wozu führt es, wenn wir uns weiter auf den Dienst für Gott konzentrieren?
Die klare Antwort finden wir in Römer 8:6: „Das Sinnen des Geistes . . . bedeutet Leben und Frieden.“ Das schließt ein, unseren Sinn vom heiligen Geist beeinflussen und bestimmen zu lassen — also in Übereinstimmung mit Gott zu sein und so zu denken wie er.
Wir können darauf vertrauen:
Wenn wir den heiligen Geist zum Herzstück unseres Daseins machen, können wir heute schon ein zufriedenes und erfülltes Leben führen.
Und die bleibende Belohnung ist ewiges Leben, ob im Himmel oder auf der Erde.
18. Warum kann man sagen, dass das „Sinnen des Geistes“ zu Frieden führt?
18 Denken wir über die Zusicherung nach, dass „das Sinnen des Geistes . . . Frieden“ bedeutet.
Viele kämpfen um inneren Frieden und suchen verzweifelt danach.
Wir dürfen ihn schon verspüren — nicht zuletzt deshalb, weil wir uns bemühen, mit unserer Familie und unserer Versammlung Frieden zu halten.
Natürlich sind wir und unsere Glaubensbrüder unvollkommen und deshalb kommt es hin und wieder zu Problemen.
Dann folgen wir dem Rat Jesu: „Schließe . . . mit deinem Bruder Frieden“ (Mat. 5:24).
Das fällt uns leichter, wenn wir daran denken, dass unser Bruder oder unsere Schwester auch dem „Gott, der Frieden gibt“, dient (Röm. 15:33; 16:20).
19. Welchen besonderen Frieden können wir verspüren?
19 Und es gibt noch einen anderen Frieden, der von unschätzbarem Wert ist: den Frieden mit unserem Schöpfer.
Auch er ist das Ergebnis davon, unseren Sinn auf den Geist zu richten.
Der Prophet Jesaja zeichnete Worte auf, die auf seine Zeit zutrafen und heute eine noch größere Erfüllung haben: „Du [Jehova] gibst Frieden dem, der sich fest an dich hält und dir allein vertraut!“ (Jes. 26:3, Hoffnung für alle; lies Römer 5:1).
20. Warum bist du für den Rat in Römer,
Kapitel 8 dankbar?
20 Ob wir geistgesalbt sind oder hoffen, einmal ewig im Paradies auf der Erde zu leben — wir können für den Rat in Römer,
Kapitel 8 sehr dankbar sein.
Wie wertvoll doch die Aufforderung ist, unser Leben nicht von dem „Fleisch“ beherrschen zu lassen!
Wir erkennen, dass es weise ist, in Übereinstimmung mit der biblischen Zusicherung zu leben: „Das Sinnen des Geistes . . . bedeutet Leben und Frieden.“ Der Lohn dafür wird ewig sein, denn Paulus schrieb: „Der Lohn, den die Sünde zahlt, ist der Tod, die Gabe aber, die Gott gibt, ist ewiges Leben durch Christus Jesus, unseren Herrn“ (Röm. 6:23).






    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Deutsche Publikationen (2000-2017)
			
		

		
			
				
				Einstellungen
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Nutzungsbedingungen
			
			
				Datenschutzerklärung
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



