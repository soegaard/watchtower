Ñaupa tiempomanta judiosqa, lepra onqoyta mayta millachikoj kanku, chay onqoyqa Jesuspa tiemponpi may rejsisqa karqa.
Chay lepra onqoyqa may sinchʼipuni karqa, nitaj sanoyayta atikullajchu, chay onqoyniyoj runataj uj jinaman tukupoj.
Lepra onqoypaj mana jampi kasqanrayku, chay onqoyniyoj runaqa sapallan tiyakunan karqa, tukuymantaj ninan karqa lepra onqoyniyoj kashasqanta (Levítico 13:45, 46).
Leymanta yachachejkuna judíos ashkha kamachiykunata orqhorqanku, nitaj Bibliaman jinachu kasharqa.
Jinamanta chay onqoyniyoj runaspa llakiyninta yapaj kanku.
Leyesninkuman jinaqa ni pi iskay metrollatapis qayllaykunanchu karqa.
Wayramojtintaj 45 metrosta jina karunchakunanku karqa.
Ley nisqanman jina, lepra onqoyniyojkuna “jaramanta jawapi” kananku karqa, chaytataj wakin talmudistas nisqasqa, entiendej kanku leprayoj runas llajtamanta karupi kausananku kasqanta.
Chayrayku leymanta yachachejqa, leprayoj runata llajta ukhupi rikuspa rumiwan chʼanqaj, ajinatataj qhaparej: “Wasiykiman ripuy, mana wajkunaman onqoyniykita chimpachinaykipaj”, nispa.
Jesusrí mana judíos jinachu karqa.
Payqa lepra onqoyniyojkunata, mana millachikojchu, astawanqa llankhaykurej jampejtaj (Mateo 8:3).



¿Imastataj religionta kamachejkuna nej kanku uj runa divorciakuyta atinanpaj?






Kay 71, 72 watas chaynejmanta divorcio papel.




Jesuspa tiemponpeqa, religionta kamachej judíos mayta parlaj kanku divorciakuymanta.
Chayrayku uj kutipi fariseos Jesusta pantachiyta munaspa ajinata taporqanku: “Qhareqa atillanchu warminmanta tʼaqakuyta mana imallamanta?”, nispa (Mateo 19:3).
Moisespa Leyninman jina, uj runaqa warminmanta divorciakunan karqa “paypi ima pʼenqaytapis” tarispalla (Deuteronomio 24:1).
Jesuspa tiemponqeqa yachachejkunapaj escuelas nisqa kaj, chaypi kajkunaqa chay leymanta mana kikintachu yuyaj kanku.
Samay escuela nisqapi kajkunaqa, nej kanku uj runaqa divorciakuyta atisqanta warmin qhenchachakuspa khuchichakojtillan.
Chaywanpis Hilel escuela nisqapi kajkunaqa, nej kanku uj runa ima chʼajwa kajtinpis divorciakuyta atisqanta.
Arí, uj runaqa warminmanta divorciakuyta atej warmin mana allinta waykʼojtin chayri waj warmitañataj munakojtin.
¿Imatá Jesús fariseosman kuticherqa?
Payqa ajinallata nerqa: “Sichus pipis warminmanta tʼaqakun mana pay wajwan wasanchaspa khuchichakojtillan, waj warmiwantaj casarakun chayqa, khuchichakuspa qhenchachakun”, nispa (Mateo 19:6, 9).






    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Quechuapi publicaciones (2004-2017)
			
		

		
			
				
				Allinchanapaj
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Kay kamachiykunata kasukuy
			
			
				Datosta apaykachakusqanmanta
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



