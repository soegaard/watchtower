SIIRAD piibliuurijad on mõnes mõttes nagu kullaotsijad.
Neil tuleb näha vaeva ja jätkata Piibli uurimist, et leida hindamatu väärtusega tarkuseteri.
Uurigem kolme viisi, kuidas kulda võib leida, ning mõelgem, mida meie piibliuurijatena saame sellest õppida.



SA LEIAD KULLATERA

Kujutle, et kõnnid jõekaldal ning märkad üht tillukest kivi päikese käes helklemas.
Sa kummardud ning avastad enda suureks üllatuseks kullatükikese.
See on väiksem kui tikupea ning haruldasem kui kvaliteetne teemant.
Loomulikult vaatad sa ringi lootuses leida teisigi kullateri.
Ühel meeldejääval päeval võis sinu uksele koputada keegi Jehoova teenija, et jagada sinuga Piibli sõnumit.
Ehk mäletad eredalt hetke, kui sa nii-öelda avastasid Piiblist oma esimese kullatera.
Vahest nägid sa Piiblis esimest korda Jumala nime Jehoova. (Laul 83:18.)
Või said teada, et sul on võimalik saada Jehoova sõbraks. (Jaak. 2:23.)
Sa adusid sedamaid, et oled leidnud midagi kallimat kui kuld.
Ilmselt kibelesid sa otsima teisigi niisuguseid tarkuseteri.



SA LEIAD NEID VEEL

Mõnikord kogunevad väikesed kullaterad või -tükid ojadesse ja jõgedesse.
Usin kullaotsija võib ühel hooajal leida setetest isegi mitu kilogrammi kulda, mis on väärt kümneid tuhandeid eurosid.
Kui sa hakkasid mõne Jehoova tunnistajaga Piiblit uurima, võisid tunda end nagu kullaotsija, kes on sattunud kullasoone peale.
Süüvides ühte piiblisalmi teise järel, lisandus tõenäoliselt sinu teadmiste pagasisse palju vaimseid rikkusi.
Kui sa neid kallihinnalisi Piibli tõdesid lähemalt uurisid, said teada, kuidas sa võid saada Jehoovaga lähedasemaks, olla tema armastuse vääriline ning elada igavesti. (Jaak. 4:8; Juuda 20, 21.)





Kullaotsija näeb kulla leidmiseks vaeva.
Kas ka sina näed vaeva, et õppida kallihinnalisi Piibli tõdesid?




Nii nagu kullaotsija kulutab aega jõgedes tuhlamisele, oled ehk sina väga usinalt otsinud vaimseid rikkusi.
Tõenäoliselt millalgi pärast seda, kui said selgeks Piibli algõpetused, hakkasid sa astuma samme, mis viisid selleni, et sa pühendusid Jumalale ja lasid end ristida. (Matt. 28:19, 20.)



OTSI ÜHA EDASI!

Kullaotsijad võivad leida vähesel määral kulda ka tardkivimitest.
Mõnes kohas on kivimites kulla kontsentratsioon piisavalt suur, nii et seal tasub maaki kaevandada, purustada ja sellest kulda eraldada.
Kuld ei pruugi maagis kohe nähtav olla.
Miks?
Sest kvaliteetne maak võib sisaldada ühe tonni kohta vaid umbes 10 grammi kulda.
Kulla kaevandajale on seda aga piisavalt palju, et tööle asuda.
Samuti peab inimene pingutama siis, kui ta on „algõpetuse Kristusest seljataha jätnud”. (Heebr. 6:1, 2.)
Sul on tarvis näha vaeva, et leida Jumala sõnast värskeid ja õpetlikke mõtteid.
Mida sa saad teha, et piibliuurimine oleks tulemuslik, isegi kui oled Piiblit uurinud juba aastaid?
Ole õpihimuline.
Pööra hoolega tähelepanu üksikasjadele.
Kui uurid edaspidigi Piiblit usinalt, leiad sealt hindamatu väärtusega tarkuseteri ja juhatust. (Rooml. 11:33.)
Et õppida Piiblit paremini tundma, kasuta uurimistöö abivahendeid, mis on sinu keeles saadaval.
Otsi kannatlikult vajalikke juhtnööre ning vastuseid oma piibliküsimustele.
Küsi teistelt, missugused piiblisalmid ja artiklid on neid aidanud või innustanud.
Jaga teistega huvitavaid mõtteid, mida sa ise oled Jumala sõna uurides leidnud.
Muidugi pole sinu eesmärk pelgalt faktilisi teadmisi omandada.
Paulus hoiatas: „Teadmised teevad uhkeks.” (1. Kor. 8:1.)
Seega tuleb sul teha tööd selle nimel, et jääda alandlikuks ning tugevdada oma usku.
Korrapärane piibliuurimine pereringis ja iseseisvalt aitab sul elada kooskõlas Piibli põhimõtetega ning ajendab sind ka teisi aitama.
Lisaks võid sa tunda rõõmu selle üle, et oled leidnud midagi palju kallimat kui kuld. (Õpet. 3:13, 14.)






    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Eestikeelsed väljaanded (2000-2017)
			
		

		
			
				
				Eelistused
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Kasutustingimused
			
			
				Privaatsus
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



