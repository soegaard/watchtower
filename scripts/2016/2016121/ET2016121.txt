OLEN sündinud 1926. aastal Ohio osariigis Crooksville’is.
Minu ema ja isa polnud usklikud, kuid nad käskisid meil kõigil kaheksal lapsel kirikus käia.
Mina käisin metodisti kirikus.
Kui olin 14-aastane, andis kirikuõpetaja mulle auhinna selle eest, et ma polnud terve aasta üheltki pühapäevaselt jumalateenistuselt puudunud.





Margaret Walker (vasakult teine õde) aitas mul Piiblit tundma õppida




Umbes samal ajal hakkas meie naaber Margaret Walker, kes oli Jehoova tunnistaja, külastama minu ema ja rääkima talle Piiblist. Ühel päeval otsustasin ka mina nende vestlust kuulata.
Ema arvas, et ma hakkan nende arutelu segama ja käskis mul õue minna.
Mina aga püüdsin sellegipoolest nende juttu pealt kuulata.
Pärast veel mõningaid külaskäike küsis Margaret minult: „Kas sa tead, mis on Jumala nimi?” Vastasin talle: „Igaüks teab seda, see on Jumal.” Tema kostis: „Võta oma Piibel ja loe Laul 83:18.” Nii ma tegingi ja avastasin, et Jumala nimi on Jehoova.
Jooksin oma sõprade juurde ja ütlesin: „Kui te täna õhtul koju jõuate, vaadake Piiblist Laul 83:18, ja te näete, mis on Jumala nimi.” Seega võib öelda, et asusin otsekohe kuulutustööd tegema.
Uurisin Piiblit ja lasin end aastal 1941 ristida.
Varsti pärast seda määrati mind juhatama koguduse raamatu-uurimist.
Kutsusin kaasa ka oma ema ja õed-vennad ning nad kõik hakkasid käima uurimisel, mida ma juhatasin.
Isa aga ei olnud sellest huvitatud.



VASTUPANU KODUS

Mulle anti koguduses rohkem vastutust ja ma rajasin omale teokraatliku raamatukogu. Ühel päeval aga osutas isa minu raamatutele ja ütles: „Kas näed kogu seda kraami?
Ma tahan, et see siit majast kaoks, ja sina võid minna koos sellega.” Kolisin kodust välja ja sain toa Zanesville’i lähedal.
Ent ma käisin ikka oma pereliikmeid julgustamas.
Isa püüdis ema koosolekutel käimist takistada.
Mõnikord, kui ema oli juba teele asunud, läks isa talle järele ja tõi ta jõuga majja tagasi.
Ema aga jooksis teise ukse kaudu välja ja läks ikka koosolekule. Ütlesin emale: „Ära muretse, varsti väsib ta sinu järele jooksmast.” Aja möödudes lõpetas isa ema takistamise ja ema sai käia koosolekutel lahingut pidamata.
Aastal 1943 algas meie koguduses teokraatlik teenistuskool ja ma hakkasin pidama õpilaskõnesid.
Nõuanded, mida pärast nende esitamist mulle anti, aitasid mul oma kõneoskust parandada.



ERAPOOLETUS SÕJA AJAL

Selleks ajaks olid rahvad haaratud teise maailmasõtta. 1944. aastal kutsuti mind sõjaväkke.
Läksin Fort Hayesi sõjaväeossa Ohio osariigis Columbuses, läbisin tervisekontrolli ja täitsin vajalikud paberid.
Samas teatasin ametnikele, et minust ei saa sõdurit.
Seepeale lasid nad mul minna.
Mõni päev hiljem ilmus aga üks ohvitser mu ukse taha ja ütles: „Corwin Robison, mul on käsk teid arreteerida.”
Kahe nädala pärast toimus kohus ja kohtunik ütles: „Kui see oleks minu teha, määraksin sulle eluaegse vangistuse.
Kas sul on midagi öelda?” Ma vastasin: „Teie ausus, mul peaks olema sama staatus nagu vaimulikel.
Ma olen pidanud jutlust igal uksel ja kuulutanud head sõnumit Jumala kuningriigist paljudele inimestele.” Kohtunik sõnas vandekohtule: „Te pole siin selleks, et otsustada, kas see noor mees on vaimulik või mitte.
Teil tuleb otsustada, kas tema teguviisi saab pidada sõjaväest kõrvalehoidmiseks või mitte.” Vähem kui poole tunni pärast langetasid vandekohtunikud süüdimõistva otsuse.
Kohtunik määras mulle karistuseks viis aastat vangistust Ashlandi föderaalvanglas Kentucky osariigis.



JEHOOVA KAITSEB MIND VANGLAS

Esimesed kaks nädalat olin Columbuse vanglas ja istusin seal terve esimese päeva oma kongis.
Palusin Jehoovat: „Ma ei suuda jääda vangikongi viieks aastaks.
Ma ei tea, mida teha.”
Järgmisel päeval lasi valvur mind kongist välja.
Kõndisin ühe pikka kasvu laiaõlgse vangi juurde.
Seisime akna ees ja vaatasime välja.
Ta küsis minult: „Mille eest sa istud, jupats?” Mina vastasin: „Ma olen Jehoova tunnistaja.” Tema sõnas: „Oled või?
Aga millepärast sa siis siin oled?” Ütlesin: „Jehoova tunnistajad ei lähe sõdima ega tapa teisi inimesi.” Tema imestas: „Nad panid sind vangi selle pärast, et sa ei tapa inimesi!
Teised pannakse vangi just tapmise pärast.
Kas see on loogiline?” Ma vastasin: „Ei, ei ole.”
Seejärel ta lausus: „Olin 15 aastat ühes teises vanglas, kus lugesin veidi teie kirjandust.” Palvetasin mõttes Jehoova poole: „Jehoova, palun aita mul see mees enda poolele võita!” Paul (nii oli selle mehe nimi) ütles samal hetkel: „Kui keegi siin sind puudutab, siis ainult hüüa, ma tegelen temaga.” Läkski nii, et mul polnud oma rühmas oleva 50 kinnipeetavaga mingeid probleeme.





Olin koos teiste usukaaslastega erapooletuse tõttu Ashlandi vanglas Kentucky osariigis




Edasi saadeti mind Ashlandi, kust leidsin eest mitu vaimselt küpset venda.
Nendega seltsimine aitas minul ja teistel vendadel jääda usus tugevaks.
Nad andsid meile igaks nädalaks piibliosa lugeda ning me valmistasime ette küsimused ja vastused, mida hiljem ühisel koosolekul arutasime. Üks vend oli määratud territooriumisulaseks.
Meil oli suur magamisruum, mille seinte ääres asusid voodid.
Territooriumisulane ütles mulle: „Robison, sina vastutad nende voodite eest.
See on sinu territoorium.
Sinu ülesanne on kuulutada kõigile, kes neisse määratakse, enne kui nad lahkuvad.” Nii tegime organiseeritult kuulutustööd.



VANGLAST VÄLJAS

Teine maailmasõda lõppes aastal 1945, kuid mind jäeti veel mõneks ajaks vanglasse.
Olin mures oma pere pärast, sest isa oli mulle öelnud: „Kui ma sinust lahti saan, siis suudan toime tulla ka teistega.” Pärast vabanemist ootas mind aga meeldiv üllatus.
Hoolimata isa vastupanust käisid seitse meie pere liiget koosolekutel ja üks minu õdedest oli ristitud.





Kuulutustööl koos võitud venna Demetrius Papageorgega, kes sai Jehoova teenijaks aastal 1913




1950. aastal puhkes Korea sõda.
Taas sain ma käsu ilmuda Fort Hayesi sõjaväeossa.
Kui olin teinud sobivustesti, ütles üks ohvitser mulle: „Sa oled oma grupis üks parimaid.” Mina vastasin: „See on tore, aga ma ei astu sõjaväkke.” Tõin esile kirjakoha 2. Timoteosele 2:3 ja lisasin: „Ma olen juba Kristuse sõdur.” Pärast pikka vaikust ütles ta, et ma võin lahkuda.
Peagi pärast seda käisin Ohio osariigis Cincinnatis toimunud kokkutuleku ajal Peeteli-teenistusest huvitatute koosolekul.
Milton Henschel rääkis meile, et kui mõni vend on valmis kuningriigi heaks kõvasti tööd tegema, saab organisatsioon kasutada teda Peetelis.
Täitsin avalduse ja augustis 1954 kutsutigi mind Brooklyni Peetelisse.
Olen teeninud Peetelis seniajani.
Peetelis on mul alati olnud rohkesti tööd.
Palju aastaid hoolitsesin trükikojas ja büroohoonetes veeboilerite eest, töötasin mehaanikuna ja parandasin ukselukke.
Samuti töötasin New Yorgi kokkutulekusaalides.





Hoolitsesin Brooklyni Peeteli büroohoonetes veeboilerite eest




Armastan Peeteli vaimset tegevuskava, mis hõlmab hommikust piibliarutelu ja pere Vahitorni-uurimist, samuti osalemist kuulutustööl koos kogudusega.
Muidugi peaksid need tegevused kuuluma iga Jehoova tunnistajate pere ellu.
Kui vanemad ja lapsed arutavad koos päevateksti ja neil on korrapärane pere piibliõhtu, kui nad osalevad koguduse koosolekutel ja teevad agaralt kuulutustööd, siis on ilmselt kõik pereliikmed usus tugevad.
Olen leidnud Peetelis ja koguduses endale palju sõpru.
Mõned neist olid võitud kristlased, kes on oma taevase tasu juba kätte saanud.
Muidugi on kõik Jehoova teenijad, sealhulgas peetellased, ebatäiuslikud.
Kui mul tekib mõne vennaga mingi sõnavahetus, püüan alati teha rahu.
Mõtlen kirjakohale Matteuse 5:23, 24 ja sellele, mida Jehoova meilt erimeelsuste lahendamiseks ootab.
Vabandamine pole kerge, kuid ma pole just tihti näinud, et pärast sõbra ees vabandamist probleem jätkuks.



RÕÕMUTOOV TEENISTUS

Oma vanuse tõttu on mul praegu raske käia ukselt uksele, ent ma ei anna alla.
Olen õppinud veidi mandariini hiina keelt ja mulle meeldib tänaval hiinlasi kõnetada.
Mõnel hommikul olen saanud jagada 30–40 ajakirja.





Kuulutamas hiinlastele New Yorgis Brooklynis




Olen teinud korduskülastuse isegi Hiinasse. See juhtus nii, et ühel päeval naeratas mulle üks noor tüdruk, kes levitas puuviljade müügikoha reklaame.
Mina naeratasin samuti ja pakkusin talle hiinakeelseid ajakirju Vahitorn ja Ärgake!.
Ta võttis need vastu ja ütles, et tema nimi on Katie.
Hiljem tuli ta mind nähes alati minuga rääkima. Õpetasin talle puu- ja köögiviljade ingliskeelseid nimetusi ning tema kordas neid minu järel.
Selgitasin talle ka piiblitekste ja andsin raamatu „Mida Piibel meile tegelikult õpetab?”.
Ent mõne nädala pärast ta kadus.
Mõni kuu hiljem andsin ajakirjad ühele teisele tüdrukule, kes levitas samuti reklaamlehti.
Nädala pärast ulatas see tüdruk mulle tänaval telefoni ja ütles: „Sulle on kõne Hiinast.” Mina vastasin: „Ma ei tunne Hiinast kedagi.” Ta käis aga mulle peale ja nii võtsin ma telefoni ning ütlesin: „Halloo,
Robison kuuleb.” Teisest otsast kostis hääl, mis ütles: „Robby, siin on Katie.
Ma olen tagasi Hiinas.” Mina imestasin: „Hiinas?” Katie vastas: „Jah.
Tead,
Robby, see tüdruk, kes sulle telefoni andis, on mu õde.
Sa õpetasid mulle palju häid asju.
Palun õpeta teda samamoodi, nagu sa õpetasid mind.” Ma sõnasin: „Ma teen oma parima,
Katie.
Aitäh, et andsid mulle teada, kus sa oled.” Varsti pärast seda rääkisin ka Katie õega viimast korda.
Kus tahes need tüdrukud praegu on, loodan ma, et nad õpivad Jehoovat paremini tundma.
Olen olnud Jehoova pühas teenistuses 73 aastat.
Ma olen väga rõõmus, et ta aitas mul vangis olles jääda erapooletuks ja talle ustavaks.
Ka mu vennad ja õed on öelnud, et nad said julgust, kui ma isa vastupanu korral alla ei andnud.
Aja jooksul ristiti minu ema ja kuus õde-venda.
Koguni isa leebus ja käis enne oma surma mõnel koosolekul.
Kui see on Jumala tahe, siis saavad mu pereliikmed ja sõbrad, kes on surnud, uues maailmas taas elavaks.
Mõelda vaid, milline rõõm on siis teenida Jehoovat igavesti koos nendega, keda me armastame!*



Ajal, kui seda artiklit avaldamiseks ette valmistati,
Corwin Robison suri, olles lõpuni ustav Jehoovale.




    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Eestikeelsed väljaanded (2000-2017)
			
		

		
			
				
				Eelistused
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Kasutustingimused
			
			
				Privaatsus
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



