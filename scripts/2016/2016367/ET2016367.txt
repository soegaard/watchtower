KUI Kevin[1] oli õppinud tundma Piibli tõde, soovis ta üle kõige, et tal oleksid Jehoovaga lähedased suhted.
Ent kuni sinnamaani oli ta palju aastaid mänginud hasartmänge, suitsetanud, purjutanud ja tarvitanud narkootikume.
Et saavutada Jumala soosing, pidi ta need pühakirjas hukka mõistetud teod hülgama.
Jehoova ja tema sõna ümberkujundava väe abil suutiski ta seda teha. (Heebr. 4:12.)
2 Kas pärast ristimist lõpetas Kevin oma elus muudatuste tegemise?
Ei, ta pidi ikka nägema vaeva, et arendada ja täiustada kristlikke omadusi. (Efesl. 4:31, 32.)
Näiteks koges ta, et tal on üllatavalt raske taltsutada oma äkilist loomust.
Kevin möönis: „Oma käitumist kontrollima õppida oli tegelikult isegi raskem kui vanad pahed hüljata.” Ent tänu siirastele palvetele ja hoolsale piibliuurimisele suutis ta teha vajalikud muudatused.
3 Kevini sarnaselt on paljud meist teinud enne ristimist tähelepanuväärseid muudatusi, et viia oma elu kooskõlla Piibli põhinõuetega.
Pärast ristimist oleme mõistnud vajadust end aina muuta, et järjest paremini järgida Jumala ja tema poja Jeesus Kristuse eeskuju. (Efesl. 5:1, 2; 1. Peetr. 2:21.)
Näiteks oleme ehk enda juures täheldanud kriitilist vaimu, inimesekartust, kalduvust teisi taga rääkida või mõnd muud nõrkust.
Võib-olla on selliste kalduvustega võitlemine osutunud raskemaks, kui algul arvasime.
Kui nii, siis ehk imestame, et kui oleme juba teinud selliseid suuri muudatusi, siis miks on nõnda raske muuta end väiksemates asjades.
Võime mõelda ka sellele, kuidas me saaksime veel edeneda, lastes Piiblil jätkuvalt oma elu muuta.



OLE OMA OOTUSTES REALISTLIK

 4. Miks ei suuda me Jehoovale alati rõõmu valmistada?
4 Me oleme õppinud Jehoovat tundma ning me armastame teda ja soovime kogu südamest teha seda, mis valmistab talle rõõmu.
Ent kui väga me ka ei sooviks, pole me oma ebatäiuse tõttu suutelised seda alati tegema.
Me oleme samasuguses olukorras nagu apostel Paulus, kes kirjutas: „Ma küll soovin teha head, aga ei suuda.” (Rooml. 7:18; Jaak. 3:2.)
 5. Milliseid muudatusi me tegime enne ristimist, kuid milliste väärade kalduvustega tuleb meil ehk jätkuvalt võidelda?
5 Me oleme jätnud maha patused harjumused, mille tõttu poleks me olnud kõlblikud saama kristliku koguduse liikmeks. (1. Kor. 6:9, 10.)
Ent me oleme ikka ebatäiuslikud. (Kol. 3:9, 10.)
Kas oleks seega realistlik oodata, et pärast ristimist, isegi kui sellest on möödunud palju aastaid, ei tee me enam vigu, ei koge tagasilangust ega pea maadlema valede motiivide ja kalduvustega?
Tegelikult võivad mõned meie väärad kalduvused püsida aastaid.
6., 7. a) Tänu millele on meil võimalik isegi ebatäiuslikena tunda rõõmu lähedastest suhetest Jehoovaga? b) Miks me ei peaks kõhklema Jehoovalt andestust paluda?
6 Samas ei peaks päritud ebatäius takistama meil tundmast rõõmu lähedastest suhetest Jehoovaga ja teda jätkuvalt teenimast.
Mõtle sellele, et kui Jehoova tõmbas meid enda juurde, siis ta teadis, et me ikka eksime vahel. (Joh. 6:44.)
Kuna ta tunneb meie iseloomu ja südant, siis teadis ta ka seda, milliste väärade kalduvustega tuleb meil võidelda.
Samuti teadis ta, et me võime vahel siiski patustada.
Sellest hoolimata ta soovis, et saaksime tema sõbraks.
7 Armastus ajendas Jumalat andma meile väärtusliku kingituse – ta tõi lunastusohvriks omaenda armsa poja. (Joh. 3:16.)
Kui palume selle ohvri alusel Jehoovalt oma vigade pärast andestust, siis võime olla kindlad, et meie sõprus temaga on püsiv. (Rooml. 7:24, 25; 1. Joh. 2:1, 2.)
Kas peaksime kõhklema lunastusohvri alusel Jehoova poole pöörduda, kuna tunneme end ebapuhta või patusena?
Muidugi mitte!
See oleks sama, kui me keelduksime kasutamast vett mustade käte pesemiseks.
Toodi ju lunastusohver kahetsevate patuste eest.
Tänu sellele võime isegi ebatäiuslikena tunda rõõmu lähedastest suhetest Jehoovaga. (Loe 1. Timoteosele 1:15.)
 8. Miks ei tohiks me oma nõrkustesse kergelt suhtuda?
8 Muidugi ei taha me suhtuda oma nõrkustesse kergelt.
Sõprussuhete rajamine Jehoovaga hõlmab seda, et me püüame aina paremini jäljendada Jumalat ja Kristust ning olla neile meelepärased. (Laul 15:1–5.)
Samuti hõlmab see oma ebatäiuslike kalduvuste talitsemist või isegi nende kõrvaldamist.
Olgu me siis ristitud hiljuti või juba palju aastaid tagasi, tuleks meil ikka järgida üleskutset „seadke asjad korda”. (2. Kor. 13:11.)
 9. Millest me teame, et riietumine uude isiksusse on kestev protsess?
9 Meil on tarvis ka edaspidi tegutseda selle nimel, et seada asjad korda ja arendada endas uut isiksust.
Paulus kirjutas oma usukaaslastele: „Teile on õpetatud, et teil tuleb heita endalt vana isiksus, mille on kujundanud teie endine eluviis ja mis laostub oma petlike himude tõttu.
Teil tuleb üha uuendada oma mõtteviisi ning riietuda uude isiksusse, mis on loodud Jumala tahte järgi ja on kooskõlas tõelise õiguse ja ustavusega.” (Efesl. 4:22–24.)
Väljend „tuleb üha uuendada” osutab sellele, et riietumine uude isiksusse on kestev protsess.
See on innustav mõte, sest see kinnitab meile, et ükskõik kui kaua me oleme Jehoovat juba teeninud, saame ikka arendada ja täiustada kristlikke omadusi, mis kujundavad meie uut isiksust.
Tõepoolest,
Piibel võib meie elu jätkuvalt muuta.



MIKS SEE ON NII RASKE?

10. Mida meil tuleks teha, et Piibli abil end muuta, ja millised küsimused tekivad?
10 Selleks et lasta Jumala sõnal end jätkuvalt muuta, on meil tarvis vaeva näha.
Miks on selleks vaja pingutada?
Kui Jehoova õnnistab meie püüdeid, kas ei peaks meie vaimne areng toimuma märksa kergemalt?
Kas ei võiks Jehoova lihtsalt kõrvaldada meie väärad kalduvused, nii et me võiksime ilma erilise vaevata ilmutada talle meelepäraseid omadusi?
11.–13. Miks ootab Jumal, et püüaksime vääradest kalduvustest lahti saada?
11 Kui mõtleme universumile, siis mõistame, kui suur jõud on Jehooval.
Näiteks Päike muundab igas sekundis viis miljonit tonni mateeriat energiaks.
Ja kuigi vaid väike osa sellest jõuab läbi atmosfääri meieni, annab see piisaval hulgal valgust ja soojust, et elu Maa peal võiks püsida. (Laul 74:16; Jes. 40:26.)
Jehoova on meeleldi valmis andma oma teenijatele vajalikul määral jõudu. (Jes. 40:29.)
Ta võiks meile anda jõudu ka selleks, et võiksime võita kõik oma nõrkused, ilma et peaksime pingutama või enda vigadest õppima.
Miks ta siis seda ei tee?
12 Jehoova on andnud meile võrratu kingituse, vaba tahte.
Kui täidame tema tahet ja selle nimel pingutame, siis näitame sellega oma armastust Jehoova vastu ja oma soovi talle meeldida.
Samuti toetame nii tema ülemvõimu.
Saatan on seadnud kahtluse alla Jehoova õiguse olla kõrgeim valitseja.
Seepärast on meie vabatahtlikud jõupingutused tema ülemvõimu toetamisel meie armastavale taevasele isale kahtlemata väga väärtuslikud. (Iiob 2:3–5; Õpet. 27:11.)
Kui me võitleme oma nõrkustega, siis näitame, et soovime olla Jehoovale ustavad ja toetada tema ülemvõimu.
13 Niisiis ergutab Jehoova meid pingutama, et arendada talle meelepäraseid omadusi. (Loe 2. Peetruse 1:5–7; Kol. 3:12.)
Ta ootab, et teeksime kõvasti tööd, et kontrollida oma mõtteid ja tundeid. (Rooml. 8:5; 12:9.)
Kui me seda teeme ja näeme, kuidas Piibel muudab jätkuvalt meie elu, toob see meile suurt rahulolu.



LASE JUMALA SÕNAL JÄTKUVALT END MUUTA

14., 15. Mida me saame teha, et arendada Jumalale meelepäraseid omadusi? (Vaata kasti „Piibel ja palve muutsid nende elu”.)
14 Mida me saame teha, et arendada häid omadusi ja tuua Jehoovale rõõmu?
Selle asemel et ise otsustada, mida enda juures täiustada, on meil tarvis järgida Jumala juhatust.
Tekstis Roomlastele 12:2 öeldakse: „Ärge laske enam sellel ajastul end vormida, vaid muutke end teistsuguseks oma mõtteviisi uuendamise teel, et te võiksite teha endale selgeks, mis on Jumala hea, meeldiv ja täiuslik tahe.” Jehoova aitab oma sõna ja püha vaimu abil meil mõista tema tahet, seda täita ning muuta oma elu sellisel määral, et see oleks kooskõlas tema normidega.
Selleks tuleb meil näiteks iga päev Piiblit lugeda, mõtiskleda selle üle ja paluda püha vaimu. (Luuka 11:13; Gal. 5:22, 23.)
Kui järgime Jumala püha vaimu juhatust ja püüame vaadata asjadele Jehoova seisukohast, siis on meie mõtted, sõnad ja teod talle üha rohkem meelepärased.
Ent sellegipoolest on meil tarvis oma nõrkuste suhtes valvel püsida. (Õpet. 4:23.)





Oleks hea vaadata aeg-ajalt üle kirjakohti või artikleid, mis aitavad meil võidelda oma nõrkustega (vaata lõiku 15)




15 Lisaks igapäevasele piiblilugemisele on vaja uurida Jumala sõna sellel põhinevate väljaannete abil, et oskaksime jäljendada Jehoova imelisi omadusi.
Mõned on välja valinud ja aeg-ajalt üle vaadanud teatud kirjakohti või Vahitorni ja Ärgake! artikleid, mis on abiks kristlike omaduste arendamisel või mõne nõrkusega võitlemisel.
16. Miks ei peaks me masenduma, kui meie vaimne kasv näib olevat aeglane?
16 Kui sulle näib, et sinu vaimne kasv on aeglane, siis pea meeles, et see võtabki aega.
Kristlike omaduste arendamine on kestev protsess.
Meil tuleb olla kannatlik, kui laseme Piiblil oma elu muuta.
Kõigepealt on sul võib-olla tarvis end sundida, et teha seda, mis on pühakirja järgi õige.
Ent mida rohkem sa mõtled ja toimid viisil, mida Jehoova sinult ootab, seda kergem on sul arvestada tema mõtteviisiga ja teha, mis on õige. (Laul 37:31; Õpet. 23:12; Gal. 5:16, 17.)



MÕTLE MEIE SUUREPÄRASELE TULEVIKULE

17. Milline suurepärane tulevik meid ootab, kui jääme Jehoovale ustavaks?
17 Jehoova ustavad teenijad saavad tulevikus teenida teda täiuslikena igavesti.
Siis ei nõua Jumalale meelepäraste omaduste ilmutamine enam suuri pingutusi, vaid valmistab meile ülimat rõõmu.
Kuid juba praegu saame tänu lunastusohvrile teenida meie armastavat Jumalat.
Kuigi me oleme veel ebatäiuslikud, saame teda rõõmustada, kui laseme Jumala sõna ümberkujundaval väel end mõjutada.
18., 19. Miks me võime olla kindlad, et Piiblil on vägi meie elu jätkuvalt muuta?
18 Varem mainitud Kevin nägi kõvasti vaeva, et ohjeldada oma äkilist loomust.
Ta mõtiskles Piibli põhimõtete üle, rakendas neid ellu ning võttis vastu usukaaslaste abi ja nõuanded.
See nõudis küll mõned aastad, kuid ta suutis teha tähelepanuväärseid muudatusi.
Peagi oli ta kõlblik saama koguduseabiliseks ning viimased 20 aastat on ta teeninud kogudusevanemana.
Sellegipoolest tunneb ta ka praegu, et on vaja olla valvas oma nõrkade külgede suhtes.
19 Kevini lugu näitab, et Piibel aitab jumalateenijatel teha oma elus jätkuvalt positiivseid muudatusi.
Laskem siis meiegi Jumala sõna ümberkujundaval väel end ikka mõjutada ja saagem niimoodi Jehoovaga üha lähedasemaks. (Laul 25:14.)
Kui näeme, et Jehoova õnnistab meie püüdeid, on see meile kindlaks tõendiks, et Piibel saab jätkuvalt muuta meie elu. (Laul 34:8.)



[1] (1. lõik.)
Nimi on muudetud.

Piibel ja palve muutsid nende elu
  Russell oli kriitilise loomuga ja oma usukaaslaste suhtes väga nõudlik.
Maria Victoriale meeldis teisi taga rääkida.
Lindal oli kuulutustööd tehes alati suur inimesekartus.
Kõik need kolm ristitud kristlast tundsid, et on võimatu end muuta, sest nende nõrkused on nii suured.
Siiski on nad kõik teinud oma nõrkustest ülesaamiseks märkimisväärseid edusamme.
Pane tähele, mis neid on aidanud.
  Russell: „Anusin Jehoovalt abi ja lugesin iga päev Piiblit.
Samuti aitasid mind mõtisklemine kirjakoha 2. Peetruse 2:11 üle ja nõuanded kogudusevanematelt.”

  Maria Victoria: „Palusin tuliselt Jehoovat, et ta aitaks mul oma keelt talitseda.
Samuti mõistsin, et on tähtis mitte seltsida nendega, kes armastavad teisi taga rääkida. Laul 64:1–4 aitas mul aru saada, et ma ei taha olla selline, kelle eest teised paluvad kaitset.
Mõistsin ka seda, et teisi taga rääkides annan ma halba eeskuju ja toon teotust Jehoova nimele.”

  Linda: „Uurisin põhjalikult meie voldikuid, et oleksin valmis neid kuulutustööl pakkuma.
Suureks abiks on mulle olnud läbikäimine nendega, kellele meeldib osaleda teenistuse eri vormides.
Ja muidugi palun alati tuge Jehoovalt.”






    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Eestikeelsed väljaanded (2000-2017)
			
		

		
			
				
				Eelistused
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Kasutustingimused
			
			
				Privaatsus
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



