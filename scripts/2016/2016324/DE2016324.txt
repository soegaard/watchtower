Der eine oder andere erinnert sich vielleicht noch:
Als Kind ist man beim Spielen hingefallen und hat sich die Hände oder die Knie aufgeschürft.
Dann ist man schnell zur Mama gelaufen.
Die Tränen sind nur so geflossen, aber sie hat einen getröstet, die Wunde gesäubert und mit einem Pflaster versorgt.
Sobald man ihre liebe Umarmung gespürt und ihre beruhigenden Worte gehört hat, war alles wieder gut.
Als Kind konnte man so leicht getröstet werden.
Doch wenn man erwachsen wird, wird das Leben komplizierter.
Die Probleme werden größer und Trost ist oft nur schwer zu finden.
Eine Umarmung und ein Pflaster reichen jetzt nicht mehr aus.
Hier nur einige Beispiele:
  Das vernichtende Gefühl, plötzlich arbeitslos zu sein.
Für Julian war die Kündigung ein Schock und er fühlte sich völlig hilflos.
Fragen schossen ihm durch den Kopf: „Wie soll ich jetzt für meine Familie sorgen?
Ich habe so viele Jahre für die Firma gearbeitet und jetzt setzen sie mich einfach vor die Tür — wie kann das sein?“

  Tiefe Verzweiflung, weil die Ehe gescheitert ist. „Es zerriss mir das Herz, als mich mein Mann vor 18 Monaten von heute auf morgen verlassen hat.
Mich überkam eine unglaubliche Traurigkeit“, erzählt Raquel. „Ich hatte nicht nur emotionale Schmerzen, sondern auch körperliche.
Ich hatte richtig Angst.“

  Bei einer schweren Krankheit ist keine Besserung in Sicht.
Vielleicht fühlt man sich manchmal wie Hiob aus alter Zeit, der enttäuscht sagte: „Ich gebe auf!
So will ich nicht mehr weiterleben!“ (Hiob 7:16, Hoffnung für alle).
Oder man denkt genauso wie der 80-jährige Luis: „Manchmal würde ich am liebsten einfach einschlafen.“

  Man sehnt sich nach Trost, weil man einen Menschen durch den Tod verloren hat. „Als mein Sohn bei einem Flugzeugunglück ums Leben kam, konnte ich es zuerst gar nicht fassen“, erzählt Robert. „Und dann kam der Schmerz.
Der Schmerz, den die Bibel mit einem langen Schwert vergleicht, das einen durchbohrt“ (Lukas 2:35).


Trotz dieser schlimmen Erlebnisse fühlen sich Robert,
Luis,
Raquel und Julian nicht alleingelassen.
Sie haben denjenigen kennengelernt, der echten Trost bietet — Gott, den Allmächtigen.
Wie tröstet er?
Und kann man das selbst erleben?






    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Deutsche Publikationen (2000-2017)
			
		

		
			
				
				Einstellungen
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Nutzungsbedingungen
			
			
				Datenschutzerklärung
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



