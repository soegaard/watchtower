JEHOOVA TUNNISTAJAD armastavad Piiblit.
See raamat pakub usaldusväärset juhatust ning julgustab ja annab lootust. (Rooml. 15:4.)
Me ei vaatle Piiblit inimeste mõtete kogumikuna, „vaid sellena, mis see tõepoolest on:
Jumala sõnana”. (1. Tess. 2:13.)
2 Kahtlemata on igaühel meist Piiblis oma lemmikosad.
Paljudele meeldivad väga evangeeliumid, kus Jehoova isiksus ilmneb kaunilt tema poja kaudu. (Joh. 14:9.)
Teistele meeldivad Piibli prohvetlikud osad, näiteks Ilmutusraamat, mida lugedes saab aimu, „mis varsti toimub”. (Ilm. 1:1.)
Ja kes meist ei leiaks lohutust Laulude raamatust või praktilisi mõtteteri Õpetussõnadest?
Piibel on tõesti raamat kõigile inimestele.
3., 4. a)
Kuidas me suhtume meie väljaannetesse? b) Millistele sihtgruppidele mõeldud väljaandeid meil on?
3 Kuna me armastame Piiblit, siis armastame ka meie väljaandeid, mis põhinevad Piiblil.
Näiteks peame väga väärtuslikuks seda vaimset toitu, mida me saame raamatute, brošüüride, ajakirjade ja muu kirjanduse näol.
Me teame, et nende kaudu aitab Jehoova meil püsida vaimselt ärksana, hästi toidetuna ja „terved usus”. (Tiit. 2:2.)
4 Lisaks kõigile Jehoova tunnistajatele mõeldud väljaannetele on meil ka konkreetsetele sihtgruppidele suunatud infot.
Osa sellisest materjalist on mõeldud noorte aitamiseks, osa jälle nende vanematele.
Suur hulk meie trükistest ja veebimaterjalist on valmistatud avalikkust silmas pidades.
Selline vaimse toidu küllus kinnitab meile, et Jehoova on täitnud oma tõotuse valmistada „kõigile rahvastele pidusöögi rammusate roogadega”. (Jes. 25:6.)
 5. Mida Jehoova meis hindab?
5 Enamik meist sooviks ilmselt, et oleks rohkem aega lugeda Piiblit ja sellel põhinevaid väljaandeid.
Võime olla kindlad, et Jehoova hindab meie pingutusi kasutada oma „aega parimal viisil”, et saaksime Piiblit korrapäraselt lugeda ja uurida. (Efesl. 5:15, 16.)
On mõistetav, et me ei saa pöörata võrdset tähelepanu kogu meile kättesaadavale vaimsele toidule.
Me peaksime aga teadvustama endale ohtu, mida me ei pruugi märgatagi.
Mis oht see on?
 6. Mille tõttu võime jääda ilma kasulikust vaimsest toidust?
6 Me võime arvata, et osa vaimsest toidust meid ei puuduta.
Näiteks võib meile tunduda, et mingi osa Piiblist meie olukorraga ei haaku, või siis ei kuulu me mingi väljaande otsesesse sihtgruppi.
Kas meil on kalduvus heita sellisele materjalile vaid põgus pilk või see täielikult tähelepanuta jätta?
Kui nii, siis võime jääda ilma vägagi kasulikust infost.
Kuidas sellisesse lõksu langemist vältida?
Kõigepealt tuleks igaühel meist meeles hoida, et kogu meie vaimne toit tuleb Jumalalt, kes on öelnud prohvet Jesaja kaudu: „Mina,
Jehoova, olen sinu Jumal, kes õpetab sulle, mis on kasulik.” (Jes. 48:17.)
Selles artiklis vaatame kolme soovitust, mis aitavad meil ammutada kasu kõigist Piibli osadest ja kogu meie rikkalikust vaimsest toidulauast.



SOOVITUSI MÕTTEKAKS PIIBLILUGEMISEKS

 7. Miks tuleks meil lugeda Piiblit avatud meelega?
7 Loe avatud meelega. Piibel ütleb selgelt, et „kogu pühakiri on Jumalalt ning on kasulik”. (2. Tim. 3:16.)
On tõsi, et mõned Piibli osad olid algselt suunatud konkreetsele isikule või grupile.
Seepärast tuleb meil lugeda Piiblit avatud meelega. Üks vend sõnab: „Kui loen Piiblit, püüan meeles hoida, et igast osast võib leida midagi õpetlikku.
See ajendab mind kaevuma sügavamale, mitte lugema vaid pinnapealselt.” Enne Jumala sõna lugemist oleks hea paluda Jehoovalt avatud meelt ja tarkust, et panna tähele seda, mida ta soovib meile õpetada. (Esra 7:10; loe Jaakobuse 1:5.)





Kas näed Piiblit lugedes, kuidas Jehoova sind juhatab? (Vaata lõiku 7)




8., 9. a)
Mis küsimusi võiksime endale Piiblit lugedes esitada? b) Mida näitavad kogudusevanemate kõlblikkusnõuded meile Jehoova kohta?
8 Esita küsimusi. Kui loed Piiblit, siis peatu aeg-ajalt ja küsi endalt: „Mida näitab see osa Jehoova kohta?
Kuidas ma saan seda infot rakendada oma elus?
Kuidas ma saan kasutada seda infot teiste aitamiseks?” Kui mõtiskleme selliste küsimuste üle, ammutame piiblilugemisest kindlasti rohkem kasu.
Mõelgem näiteks kogudusevanemate kõlblikkusnõuetele. (Loe 1. Timoteosele 3:2–7.) Kuna enamik meist pole kogudusevanemad, siis võime esialgu arvata, et need salmid meid eriti ei puuduta.
Ent kui mõelda sellele loetelule eeltoodud küsimuste valguses, siis näeme, et seal on palju kasulikku meile kõigile.
9 Mida näitab see osa Jehoova kohta? Jehoova kõlblikkusnõuded kogudusevanematele näitavad, et tal on kõrged normid nendele, kes teenivad vastutavas ametis.
Ta ootab neilt vendadelt, et nad oleksid heaks eeskujuks, ja nõuab neilt aru selle kohta, kuidas nad kohtlevad kogudust, „kelle ta on ostnud omaenda poja verega”. (Ap. t. 20:28.)
Jehoova soovib, et tunneksime end ametisse määratud alamkarjaste käe all turvaliselt. (Jes. 32:1, 2.)
Sel moel kogudusevanemate kõlblikkusnõuetele mõeldes mõistame, kui väga Jehoova meist hoolib.
10., 11. a) Milliseid kogudusevanemate kõlblikkusnõudeid saavad rakendada ka teised kristlased? b) Kuidas me saame seda infot kasutada teiste aitamiseks?
10 Kuidas ma saan seda infot rakendada oma elus? Aeg-ajalt tuleb kogudusevanemal end nende kõlblikkusnõuete valguses läbi uurida, et näha, mis valdkonnas saaks ta edeneda.
Vend, „kes pürgib ülevaatajaks”, peab samuti neile nõuetele tähelepanu pöörama, et neid oma võimete kohaselt täita. (1 Tim. 3:1.)
Tegelikult saab igaüks meist sellest loetelust midagi õppida, sest suurem osa nendest nõuetest käib kõigi kristlaste kohta.
Näiteks peaksid kõik kristlased olema mõistlikud ja kaine mõistusega. (Filipl. 4:5; 1. Peetr. 4:7.)
Kuna kogudusevanemad on „karjale eeskujuks”, saame neilt õppida ja võtta „eeskujuks nende usku”. (1. Peetr. 5:3; Heebr. 13:7.)
11 Kuidas ma saan kasutada seda infot teiste aitamiseks? Me saame selle kõlblikkusnõuete loetelu abil aidata huvilistel või piibliõpilastel näha, kuidas Jehoova tunnistajate kogudusevanemad erinevad ristiusu vaimulikest.
Lisaks võime neid salme lugedes mõelda sellele, kui palju pingutusi nõuab meie oma koguduse vanematelt meie heaolu eest hoolitsemine.
Kui näeme, kui palju nad on valmis endast andma ja kui kõvasti nad meie heaks tööd teevad, süvendab see meis lugupidamist nende vastu. (1. Tess. 5:12.)
Mida enam me väljendame oma siirast lugupidamist nende töökate ülevaatajate vastu, seda enam tunnevad nad oma teenistusest rõõmu. (Heebr. 13:17.)
12., 13. a) Millist uurimistööd me saame meile kättesaadavate abivahenditega teha? b) Too näide, kuidas taustinfo võib aidata mõista paljutki kasulikku, mis kohe ei tule ilmsiks.
12 Tee uurimistööd. Meile kättesaadavaid abivahendeid kasutades võiksime otsida järgmist infot.
  Kes kirjutas selle pühakirja osa?

  Kus ja millal see kirjutati?

  Millised olulised sündmused toimusid selle piibliraamatu kirjutamise ajal?


Sedalaadi taustinfo aitab mõista paljutki kasulikku, mis muidu kohe ilmsiks ei tule.
13 Vaadelgem näiteks kirjakohta Hesekiel 14:13, 14, mis ütleb: „Kui maa teeb pattu minu vastu, olles truudusetu, siis tõstan ma oma käe selle vastu ja hävitan selle toiduvarud.
Ma saadan selle peale nälja ning hävitan sellelt inimesed ja loomad.
Isegi kui seal oleksid need kolm meest – Noa,
Taaniel ja Iiob –, suudaksid nad oma õigusega päästa vaid iseendid, lausub kõrgeim valitseja Jehoova.” Mõningase uurimise järel saame teada, et Hesekiel kirjutas need sõnad umbes aastal 612 e.m.a.
Selleks ajaks olid Noa ja Iiob juba sajandeid surnud ning nende ustavus oli talletatud Jumala mällu.
Ent Taaniel veel elas sel ajal.
Tegelikult võis ta olla alles hilises teismeeas või varastes 20-ndates, kui Jehoova ütles, et ta on tema silmis sama õige nagu Noa ja Iiob.
Mida me sellest õpime?
Jehoova paneb tähele ja peab kalliks kõigi oma ustavate teenijate, ka päris noorte laitmatust. (Laul 148:12–14.)



RIKKALIK VAIMNE TOIDULAUD

14. Millist abi pakuvad noorte väljaanded noortele endile ja mis kasu saavad neist ammutada kõik kristlased? (Vaata pilti artikli alguses.)
14 Me teame, et kogu Jumala sõna on kasulik.
Samamoodi me mõistame, et ka muu meile kättesaadav vaimne toit on kasulik.
Võtame paar näidet. Noortele mõeldud materjal. Viimastel aastatel on meil välja antud palju noortele suunatud kirjandust.[1] Mõned sellised väljaanded on mõeldud selleks, et aidata neil tulla toime kooliprobleemidega või noorukiea muredega.
Kuidas me kõik saaksime sellisest materjalist midagi kasulikku ammutada?
Kui loeme neid väljaandeid, siis näeme, milliste raskustega meie noored kokku puutuvad.
See võimaldab meil neid paremini aidata ja julgustada.
15. Miks peaksid ka täiskasvanud kristlased tundma huvi noortele suunatud väljaannete vastu?
15 Paljud noorteprobleemid pole sugugi võõrad ka täiskasvanutele.
Me kõik peame kaitsma oma usku, kontrollima oma emotsioone, seisma vastu kaaslaste kahjulikule survele ning vältima halba seltskonda ja meelelahutust.
Neid ja paljusid teisi teemasid käsitletakse teismelistele suunatud väljaannetes.
Kas täiskasvanud peaksid arvama, et neil ei kõlba neid lugeda?
Sugugi mitte!
Kuigi see materjal on kirjutatud eeskätt noortele, põhineb see Piibli ajatutel põhimõtetel ja on kasulik kõigile kristlastele.
16. Milles veel on meie väljaanded noortele abiks?
16 Lisaks sellele, et meie väljaanded aitavad noortel toime tulla probleemidega, aitavad need neil ka vaimselt kasvada ja saada Jehoovaga üha lähedasemaks. (Loe Koguja 12:1, 13.) Täiskasvanutel on sealt nii mõndagi õppida.
Näiteks 2009. aasta aprillikuu Ärgake! numbris oli artikkel „Noored küsivad.
Kuidas teha Piibli lugemine endale meeldivaks?” See artikkel pakkus mitmeid soovitusi ja selles oli lisa, mille sai välja lõigata ja Piibli vahel alles hoida.
Kas sellest artiklist oli kasu ka täiskasvanutele? Üks 24-aastane abielunaine ja ema kirjutas: „Mul on alati olnud piiblilugemisega raskusi.
Selles artiklis toodud soovitused meeldisid mulle väga ja nüüd kasutan seda väljalõiget kogu aeg.
Praegu ma lausa ootan, et saaksin Piiblit lugeda.
Ma näen, kuidas piibliraamatud on omavahel kooskõlas ja moodustavad otsekui kauni põimitud seinavaiba.
Piiblilugemine pole kunagi varem tundunud mulle nii põnev.”
17., 18. Miks on kasulik lugeda üldsusele suunatud väljaandeid?
Too näide.
17 Üldsusele mõeldud väljaanded. Alates 2008. aastast ilmub Vahitorni uurimisväljaanne, mis on suunatud eeskätt Jehoova tunnistajatele.
Ent meil on ka esmajoones üldsusele suunatud väljaandeid.
Kas meiegi võime sealt midagi kasulikku leida?
Toome näite.
Oletame, et ühel päeval märkad sa koosolekul enne avalikku kõnet, et kuningriigisaali on tulnud su huviline.
Kahtlemata oled väga rõõmus.
Kui avaliku kõne pidaja esitab oma kõnet, mõtled tõenäoliselt, kuidas see su huvilisele võiks meeldida, ja püüad kuulata otsekui tema kõrvadega.
Tänu sellele kuulad seda kõnet uue nurga alt ja oled kindlasti tänulik sellise teemakäsitluse eest.
18 Me võime kogeda midagi sarnast, kui loeme üldsusele suunatud väljaandeid.
Näiteks avalik Vahitorn käsitleb pühakirja teemasid sel moel, et ka need, kes pole Jehoova tunnistajad, saaksid sellest aru.
Sama võib öelda paljude artiklite kohta, mis ilmuvad meie veebisaidil jw.org, näiteks sarjad „Vastused piibliküsimustele” ja „Korduma kippuvad küsimused”.
Sellist infot lugedes süvendame oma hindamist meile juba tuttavate tõdede vastu.
Lisaks võime märgata uusi lähenemisviise, mis aitavad meil kuulutustööl oma uskumusi selgitada.
Samamoodi tugevdab Ärgake! lugemine alatasa meie veendumust, et on olemas tõeline Jumal, ning aitab meil kaitsta oma uskumusi. (Loe 1. Peetruse 3:15.)
19. Kuidas me saame näidata Jehoovale oma tänulikkust tema juhatuse eest?
19 Tõepoolest,
Jehoova on andnud meile külluslikult vaimset toitu, mis pakub meile vajalikku juhatust. (Matt. 5:3.)
Ammutagem siis edaspidigi tarkust kogu meile kättesaadavast vaimutoidust.
Sel viisil väljendame oma tänulikkust Jumalale, kes õpetab meile, mis on kasulik. (Jes. 48:17.)



[1] (14. lõik.)
Näiteks väljaanne „Noored küsivad.
Praktilisi vastuseid” (1. ja 2. köide) ning nüüd vaid veebis ilmuv sari „Noored küsivad”.




    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Eestikeelsed väljaanded (2000-2017)
			
		

		
			
				
				Eelistused
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Kasutustingimused
			
			
				Privaatsus
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



