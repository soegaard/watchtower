கி.மு. 607-ல், இரண்டாம் நேபுகாத்நேச்சார் ராஜாவின் தலைமையில், மிகப் பிரமாண்டமான பாபிலோனியப் படை எருசலேமைத் தாக்கியது. இளம் ஆண்களை அந்த ராஜா வாளால் வெட்டிக் கொன்றதாக பைபிள் சொல்கிறது. இளைஞர்கள் என்றோ கன்னிப்பெண்கள் என்றோ, வயதானவர்கள் என்றோ பலவீனமானவர்கள் என்றோ அவன் பரிதாபப்படவில்லை. கடைசியில், “அவர்கள் தேவனுடைய ஆலயத்தைத் தீக்கொளுத்தி, எருசலேமின் அலங்கத்தை இடித்து, அதின் மாளிகைகளையெல்லாம் அக்கினியால் சுட்டெரித்து, அதிலிருந்த திவ்வியமான பணிமுட்டுகளையெல்லாம் அழித்தார்கள்.”—2 நா. 36:17, 19.
 2. யெகோவா யூதர்களுக்கு என்ன எச்சரிக்கை கொடுத்திருந்தார்? யூதர்களுக்கு என்ன நடக்கவிருந்தது?
2 எருசலேமின் அழிவு யூதர்களுக்கு ஆச்சரியமாக இருந்திருக்காது. ஏனென்றால், கடவுளுடைய சட்டத்துக்குக் கீழ்ப்படியாமல் போனால், அவர்கள் பாபிலோனியர்களால் தாக்கப்படுவார்கள் என்று தீர்க்கதரிசிகள் மூலம் யெகோவா பல வருடங்களாக எச்சரிக்கை கொடுத்திருந்தார். அதோடு, யூதர்களில் நிறைய பேர் வாளால் கொல்லப்படுவார்கள் என்றும், தப்பிக்கிறவர்கள் பாபிலோனுக்குச் சிறைபிடித்துக் கொண்டுபோகப்படுவார்கள் என்றும் தீர்க்கதரிசிகள் மூலம் சொல்லியிருந்தார். (எரே. 15:2) அப்படியென்றால், சிறைபிடித்துக் கொண்டுபோகப்பட்டவர்களின் வாழ்க்கை எப்படி இருந்திருக்கும்? யூதர்கள் பாபிலோனின் கட்டுப்பாட்டுக்குள் இருந்ததைப் போன்ற ஒரு சூழ்நிலை கிறிஸ்தவர்களுக்கும் எப்போதாவது ஏற்பட்டிருக்கிறதா? அப்படியென்றால், எப்போது ஏற்பட்டது?



சிறையிருப்பில் வாழ்க்கை

 3. யூதர்கள், எகிப்தில் அடிமைகளாக இருந்தபோது அனுபவித்த வாழ்க்கைக்கும் பாபிலோனின் சிறையிருப்பில் இருந்தபோது அனுபவித்த வாழ்க்கைக்கும் என்ன வித்தியாசம்?
3 யூதர்கள் சிறைபிடித்துக் கொண்டுபோகப்பட்ட பிறகு, அந்தப் புதிய சூழ்நிலையை அவர்கள் ஏற்றுக்கொள்ள வேண்டுமென்றும், முடிந்தளவு நன்றாக வாழ வேண்டுமென்றும் யெகோவா சொல்லியிருந்தார். “வீடுகளைக் கட்டி, குடியிருந்து, தோட்டங்களை நாட்டி, அவைகளின் கனியைச் சாப்பிடுங்கள். நான் உங்களைச் சிறைப்பட்டுப் போகப்பண்ணின பட்டணத்தின் சமாதானத்தைத் தேடி, அதற்காகக் கர்த்தரை விண்ணப்பம்பண்ணுங்கள்; அதற்குச் சமாதானமிருக்கையில் உங்களுக்கும் சமாதானமிருக்கும்” என்று எரேமியா மூலம் யெகோவா சொல்லியிருந்தார். (எரே. 29:5, 7) யெகோவாவின் ஆலோசனைகளுக்குக் கீழ்ப்படிந்த யூதர்களால், சிறையிருப்பில் இருந்தபோது பெரும்பாலும் இயல்பான வாழ்க்கை வாழ முடிந்தது. அவர்களுடைய சொந்த விஷயங்களை அவர்களே பார்த்துக்கொள்வதற்கும், பாபிலோன் முழுவதும் எங்கே வேண்டுமானாலும் சுதந்திரமாகப் போய் வருவதற்கும் அவர்களுக்குப் பாபிலோனியர்கள் அனுமதி கொடுத்திருந்தார்கள். அந்தச் சமயத்தில், பாபிலோன் வியாபார மையமாக இருந்தது. பாபிலோனில் இருந்த நிறைய யூதர்கள், பொருள்களை வாங்கவும் விற்கவும் கற்றுக்கொண்டார்கள் என்றும், கைத்தொழில் செய்வதில் நன்றாகத் தேர்ச்சி பெற்றவர்களாக ஆனார்கள் என்றும் பூர்வ கால ஆவணங்கள் சொல்கின்றன. அதோடு, சில யூதர்கள் பணக்காரர்களாகவும் ஆனார்கள். பல நூறு ஆண்டுகளுக்கு முன்பு எகிப்தில் அடிமைகளாக இருந்தபோது, இஸ்ரவேலர்கள் பயங்கர கஷ்டங்களை அனுபவித்தார்கள். ஆனால், பாபிலோனின் சிறையிருப்பில் இருந்தபோது, அது போன்ற கஷ்டங்களை அவர்கள் அனுபவிக்கவில்லை.—யாத்திராகமம் 2:23-25-ஐ வாசியுங்கள்.
 4. உண்மையாக இல்லாத யூதர்களோடு சேர்ந்து வேறு யாரும் கஷ்டப்பட வேண்டியிருந்தது? கடவுளுடைய சட்டத்தை அவர்களால் ஏன் முழுமையாகக் கடைப்பிடிக்க முடியவில்லை?
4 பாபிலோனுக்குச் சிறைபிடித்துக் கொண்டுபோகப்பட்ட யூதர்களில் சிலர், கடவுளுடைய உண்மையான ஊழியர்களாக இருந்தார்கள். அவர்கள் எந்தத் தவறும் செய்யவில்லை என்றாலும், மற்ற யூதர்களோடு சேர்ந்து அவர்களும் கஷ்டப்பட வேண்டியிருந்தது. தேவையான பொருளாதார வசதிகள் யூதர்களுக்கு இருந்தது. ஆனால், அவர்கள் யெகோவாவை எப்படி வணங்குவார்கள்? ஏனென்றால், ஆலயமும் பலிபீடமும் அழிக்கப்பட்டிருந்தது. அதோடு, குருத்துவ ஏற்பாடுகளும் ஒழுங்கமைக்கப்பட்ட விதத்தில் செயல்படாமல் இருந்தன. இருந்தாலும், உண்மையுள்ள யூதர்கள், தங்களால் முடிந்தளவு கடவுளுடைய சட்டங்களின்படி நடந்தார்கள். உதாரணத்துக்கு தானியேல், சாத்ராக், மேஷாக், ஆபேத்நேகோ ஆகியவர்கள் யூதர்களுக்குத் தடை செய்யப்பட்டிருந்த உணவு வகைகளைச் சாப்பிட மறுத்தார்கள். அதோடு, தானியேல் தவறாமல் கடவுளிடம் ஜெபம் செய்ததாகவும் பைபிள் சொல்கிறது. (தானி. 1:8; 6:10) ஆனாலும், பொய்க் கடவுள்களை வணங்கியவர்களுடைய ஆட்சியின்கீழ் உண்மையுள்ள யூதர்கள் இருந்ததால், கடவுளுடைய சட்டம் சொன்ன எல்லாவற்றின்படியும் செய்வது அவர்களுக்கு முடியாத விஷயமாக இருந்தது.
 5. தன்னுடைய மக்களுக்கு யெகோவா என்ன வாக்குக் கொடுத்திருந்தார்? அவர் கொடுத்த வாக்குறுதி ஏன் குறிப்பிடத்தக்கது?
5 கடவுள் முழுமையாக ஏற்றுக்கொள்ளும் விதத்தில், மறுபடியும் இஸ்ரவேலர்களால் கடவுளை வணங்க முடியுமா? அந்தச் சமயத்தில், அது முடியாத விஷயமாகத்தான் தோன்றியது. ஏனென்றால், சிறைபிடித்துக் கொண்டுவந்தவர்களைப் பாபிலோனியர்கள் விடுதலை செய்ததே கிடையாது. ஆனால், தன்னுடைய மக்கள் விடுதலை செய்யப்படுவார்கள் என்று யெகோவா வாக்குக் கொடுத்திருந்தார். அவர் சொன்னபடியே, கடைசியில் அவர்கள் விடுதலை செய்யப்பட்டார்கள். கடவுளுடைய வாக்குறுதிகள் எப்போதுமே நிறைவேறும்!—ஏசா. 55:11.



கிறிஸ்தவர்கள் எப்போதாவது பாபிலோனின் கட்டுப்பாட்டுக்குள் இருந்திருக்கிறார்களா?

6, 7. நம்முடைய புரிந்துகொள்ளுதலில் ஏன் மாற்றம் தேவைப்படுகிறது?
6 யூதர்கள் பாபிலோனின் கட்டுப்பாட்டுக்குள் இருந்ததைப் போல, கிறிஸ்தவர்கள் எப்போதாவது பாபிலோனின் கட்டுப்பாட்டுக்குள் இருந்திருக்கிறார்களா? உண்மையுள்ள கிறிஸ்தவர்கள், 1918-ல் பாபிலோனின் கட்டுப்பாட்டுக்குள் போனதாகவும், பிறகு 1919-ல் அதிலிருந்து வெளியே வந்ததாகவும் காவற்கோபுரம் பல வருடங்களாக சொல்லி வந்தது. ஆனால், இப்போது நம்முடைய புரிந்துகொள்ளுதலில் மாற்றம் தேவைப்படுகிறது. இதைப் பற்றி இந்தக் கட்டுரையிலும் அடுத்த கட்டுரையிலும் பார்க்கலாம்.
7 இதைக் கொஞ்சம் யோசித்துப் பாருங்கள். மகா பாபிலோன், பொய் மத உலகப் பேரரசாக இருக்கிறது. 1918-ல், கடவுளுடைய மக்கள் பொய் மதத்துக்கு அடிமைகளாக ஆகவில்லை. அந்தச் சமயத்தில், பரலோக நம்பிக்கையுள்ள கிறிஸ்தவர்கள் துன்புறுத்தப்பட்டார்கள் என்பது உண்மைதான். ஆனால், பெரும்பாலும் அரசாங்கங்கள்தான் அவர்களைத் துன்புறுத்தின; பொய் மதம் அல்ல. முதல் உலகப் போர் ஆரம்பிப்பதற்கு ரொம்ப காலத்துக்கு முன்பிருந்தே, பரலோக நம்பிக்கையுள்ள கிறிஸ்தவர்கள் பொய் மதத்தில் இருந்து விலகிக்கொண்டிருந்தார்கள். அதனால், யெகோவாவின் மக்கள் 1918-ல் உண்மையிலேயே மகா பாபிலோனின் கட்டுப்பாட்டுக்குள் போனதாகத் தெரியவில்லை.



கடவுளுடைய மக்கள் எப்போது பாபிலோனின் கட்டுப்பாட்டுக்குள் போனார்கள்?

 8. அப்போஸ்தலர்கள் இறந்த பிறகு என்ன நடந்தது? (ஆரம்பப் படம்)
8 கி.பி. 33 பெந்தெகொஸ்தே நாளன்று, ஆயிரக்கணக்கான புதிய கிறிஸ்தவர்கள் கடவுளுடைய சக்தியால் அபிஷேகம் செய்யப்பட்டார்கள். அவர்கள், “தேர்ந்தெடுக்கப்பட்ட இனமாகவும், ராஜ அதிகாரமுள்ள குருமார் கூட்டமாகவும், பரிசுத்த தேசமாகவும், அவருடைய விசேஷ சொத்தாகவும்” ஆனார்கள். (1 பேதுரு 2:9, 10-ஐ வாசியுங்கள்.) அப்போஸ்தலர்கள் உயிரோடு இருந்த வரைக்கும், சபையை அவர்கள் ஜாக்கிரதையாகக் கவனித்துக்கொண்டார்கள். ஆனால், குறிப்பாக அவர்கள் இறந்த பிறகு, சபையிலிருந்த சிலர், “சீடர்களைத் தங்கள் பக்கம் இழுத்துக்கொள்வதற்காக உண்மைகளைத் திரித்து” சொன்னார்கள். அரிஸ்டாட்டில் மற்றும் பிளேட்டோவின் தத்துவங்கள் அந்த ஆட்களுக்குப் பிடித்திருந்ததால், கடவுளுடைய வார்த்தையில் இருக்கும் உண்மைகளைப் போதிப்பதற்குப் பதிலாக அந்தத் தத்துவங்களைப் போதித்தார்கள். (அப். 20:30; 2 தெ. 2:6-8) அப்படிச் செய்த பெரும்பாலான ஆட்கள், சபையில் முக்கிய பொறுப்புகளில் இருந்தார்கள்; அதோடு, சபையில் கண்காணிகளாகவும் இருந்தார்கள். “நீங்கள் எல்லாரும் சகோதரர்களாக இருக்கிறீர்கள்” என்று இயேசு தன்னைப் பின்பற்றியவர்களிடம் சொல்லியிருந்தும், குருமார் வகுப்பு உருவாக ஆரம்பித்தது.—மத். 23:8.
 9. விசுவாசதுரோக கிறிஸ்தவ மதம் எப்படி ரோம அரசாங்கத்தோடு சேர்ந்து செயல்பட ஆரம்பித்தது? அதனால் என்ன ஆனது?
9 கி.பி. 313-ல், பொய்மத ரோம சாம்ராஜ்யத்தின் பேரரசரான கான்ஸ்டன்டைன், விசுவாசதுரோகத்தில் ஈடுபட்டிருந்த கிறிஸ்தவ மதத்தை சட்டப்பூர்வமாக்கினார். அதற்குப் பிறகு, ரோம அரசாங்கத்தோடு சர்ச் கூட்டுசேர ஆரம்பித்தது. உதாரணத்துக்கு, மதத் தலைவர்களோடு கான்ஸ்டன்டைன் ஒரு கூட்டம் நடத்தினார். பிற்பாடு, அது நைசியா ஆலோசனைக் கூட்டம் என்று அழைக்கப்பட்டது. அந்தக் கூட்டத்துக்குப் பிறகு, ஆரியஸ் என்ற பாதிரியை கான்ஸ்டன்டைன் நாடுகடத்தினார்; ஏனென்றால், இயேசுதான் கடவுள் என்பதை அந்தப் பாதிரி ஏற்றுக்கொள்ள மறுத்தார். பிறகு, தியடோஷியஸ் என்பவர் ரோமப் பேரரசரானார். அப்போது, கத்தோலிக்க மதம் ரோம சாம்ராஜ்யத்தின் அதிகாரப்பூர்வ மதமாக ஆனது. பேரரசரான தியடோஷியஸ் காலத்தில் (கி.பி. 379-395), பொய்மத ரோம சாம்ராஜ்யம் கிறிஸ்தவ நாடாக ஆனதென்று சரித்திர ஆசிரியர்கள் சொல்கிறார்கள். ஆனால், உண்மை என்னவென்றால், அந்தச் சமயத்துக்குள், விசுவாசதுரோக கிறிஸ்தவர்கள் பொய்மத போதனைகளை ஏற்றுக்கொண்டதன் மூலம் ஏற்கெனவே மகா பாபிலோனின் பாகமாக ஆகியிருந்தார்கள். இருந்தாலும், பரலோக நம்பிக்கையுள்ள சில உண்மையுள்ள கிறிஸ்தவர்கள் இன்னும் இருக்கத்தான் செய்தார்கள். இயேசு சொல்லியிருந்த கோதுமையைப் போல அவர்கள் இருந்தார்கள். கடவுளை வணங்குவதற்குத் தங்களால் முடிந்ததையெல்லாம் அவர்கள் செய்தார்கள். ஆனால், அவர்கள் சொன்னதை சிலர்தான் கேட்டார்கள். (மத்தேயு 13:24, 25, 37-39-ஐ வாசியுங்கள்.) அதனால், அவர்கள் உண்மையிலேயே பாபிலோனின் கட்டுப்பாட்டுக்குள்தான் இருந்தார்கள்!
10. சர்ச் போதனைகளை எதிர்த்து சிலர் ஏன் கேள்வி கேட்க ஆரம்பித்தார்கள்?
10 கிறிஸ்துவுக்குப் பின் ஒருசில நூற்றாண்டுகள்வரை, நிறைய மக்களால் கிரேக்க அல்லது லத்தீன் மொழியில் பைபிளைப் படிக்க முடிந்தது. பைபிள் போதனைகளையும் சர்ச் போதனைகளையும் அவர்களால் ஒப்பிட்டுப் பார்க்க முடிந்தது. சர்ச் போதனைகள் பொய் என்பதைப் புரிந்துகொண்ட சிலர், அந்தப் போதனைகளை ஒதுக்கித்தள்ளினார்கள். ஆனால், அதைப் பற்றி மற்றவர்களிடம் வெளிப்படையாகச் சொல்வது அவர்களுக்கு ஆபத்தானதாக இருந்தது, ஏன், கொலை செய்யப்படும் சூழ்நிலைகூட இருந்தது.
11. பைபிள் எப்படிப் பாதிரிமார்களின் கட்டுப்பாட்டுக்குள் வந்தது?
11 காலங்கள் போகப்போக, கிரேக்க அல்லது லத்தீன் மொழியை சிலர் மட்டும்தான் பேசினார்கள். மக்கள் பேசிய பொதுவான மொழிகளில் பைபிளை மொழிபெயர்ப்பதற்கு சர்ச் தலைவர்கள் அனுமதி கொடுக்கவில்லை. அதனால், குருமார் வகுப்பாலும் நன்றாகப் படித்தவர்களாலும்தான் பைபிளைப் படிக்க முடிந்தது. ஆனால், அந்த மொழிகளில் எல்லா குருமார்களுக்கும் நன்றாக எழுதப்படிக்கத் தெரியவில்லை. சர்ச் போதனைகளை ஏற்றுக்கொள்ளாதவர்கள் கடுமையாகத் தண்டிக்கப்பட்டார்கள். பரலோக நம்பிக்கையுள்ள உண்மையுள்ள கிறிஸ்தவர்கள் சிறிய சிறிய தொகுதிகளாக ஒன்றுகூடி வர வேண்டியிருந்தது. ஆனால், அப்படி ஒன்றுகூடி வர சிலரால் முடியவில்லை. பாபிலோனின் சிறையிருப்பில் இருந்த யூதர்களைப் போல, ‘ராஜ அதிகாரமுள்ள குருமார் கூட்டத்தால்’ ஒழுங்கமைக்கப்பட்ட விதத்தில் யெகோவாவை வணங்க முடியவில்லை. மகா பாபிலோன் அந்தளவுக்கு மக்களைத் தன்னுடைய கட்டுப்பாட்டுக்குள் வைத்திருந்தது!



நம்பிக்கைக்கான காரணங்கள்

12, 13. வெளிப்படையாகவும் கடவுள் ஏற்றுக்கொள்ளும் விதமாகவும் கடவுளை வணங்க முடியும் என்ற நம்பிக்கை உண்மைக் கிறிஸ்தவர்களுக்கு வருவதற்கு என்ன இரண்டு காரணங்கள் இருந்தன?
12 உண்மைக் கிறிஸ்தவர்களால் வெளிப்படையாகவும் கடவுள் ஏற்றுக்கொள்ளும் விதமாகவும் கடவுளை வணங்க முடியுமா? அப்படி வணங்க முடியும் என்ற நம்பிக்கை வந்ததற்கு இரண்டு முக்கியமான காரணங்கள் இருந்தன. சுமார் கி.பி. 1450-ல் அச்சடிக்கும் இயந்திரம் கண்டுபிடிக்கப்பட்டதுதான் முதல் காரணம். அந்த இயந்திரத்தை எங்கே வேண்டுமானாலும் எடுத்துக்கொண்டுபோக முடிந்தது. அது கண்டுபிடிக்கப்படுவதற்கு முன்பு, பைபிள் கைப்படதான் நகல் எடுக்கப்பட்டது; அது அவ்வளவு சுலபமாக இருக்கவில்லை! திறமையாக நகல் எடுப்பவருக்குக்கூட ஒரே ஒரு பைபிளை நகல் எடுக்க 10 மாதங்கள் ஆனது! அதோடு, அதை நகல் எடுத்தவர்கள், மிருகங்களின் தோலால் செய்த சுருள்களில் நகல் எடுத்தார்கள். அதனால், கொஞ்சம் பைபிள்கள்தான் அப்போது இருந்தன. அதுவும், அவற்றின் விலை ரொம்பவே அதிகமாக இருந்தது. அச்சடிக்கும் இயந்திரமும் பேப்பரும் கண்டுபிடிக்கப்பட்டதற்குப் பிறகு, திறமையான ஒருவரால் ஒரே நாளில் 1,300-க்கும் அதிகமான பக்கங்களை அச்சடிக்க முடிந்தது!





அச்சடிக்கும் இயந்திரத்தின் கண்டுபிடிப்பும் தைரியமான பைபிள் மொழிபெயர்ப்பாளர்களின் வைராக்கியமும் பாபிலோனின் பிடியிலிருந்து விடுபட உதவின (பாராக்கள் 12, 13)




13 பதினாறாம் நூற்றாண்டின் ஆரம்பத்தில், தைரியமாக இருந்த சிலர், மக்கள் பேசுகிற பொதுவான மொழிகளில் பைபிளை மொழிபெயர்த்தார்கள்; இது நம்பிக்கை வந்ததற்கான இரண்டாவது காரணம். வெளியே தெரிந்தால் அவர்கள் கொல்லப்படுவார்கள் என்று தெரிந்திருந்தும் அவர்கள் பைபிளை மொழிபெயர்த்தார்கள். அதைக் கேள்விப்பட்ட சர்ச் தலைவர்கள் பயங்கர அதிர்ச்சி அடைந்தார்கள். ஏன்? நேர்மையான ஆண்களும் பெண்களும் தங்களுடைய சொந்த மொழியில் பைபிளைப் படித்தால், தங்களைக் கேள்வி கேட்க ஆரம்பித்துவிடுவார்கள் என்பதை நினைத்து அவர்கள் அதிர்ச்சி அடைந்தார்கள். ஒருவேளை அவர்கள் இந்தக் கேள்விகளையெல்லாம் கேட்பார்களோ என்று நினைத்திருக்கலாம்: ‘உத்தரிக்கும் ஸ்தலத்தைப் பற்றி பைபிளில் எங்கே சொல்லப்பட்டிருக்கிறது? ஒருவர் இறந்துவிட்டால், அவரை அடக்கம் செய்வதற்காகப் பாதிரிமார்களுக்கு பணம் தரவேண்டும் என்று பைபிளில் எங்கே சொல்லப்பட்டிருக்கிறது? போப்புகளைப் பற்றியும் கார்டினல்களைப் பற்றியும் பைபிளில் எங்கே சொல்லப்பட்டிருக்கிறது?’ சர்ச்சின் நிறைய பொய் போதனைகள், கிறிஸ்துவுக்குச் சில நூற்றாண்டுகளுக்கு முன்பு வாழ்ந்த அரிஸ்டாட்டில் மற்றும் பிளேட்டோவின் தத்துவங்களை அடிப்படையாகக் கொண்டிருந்தன. மக்கள் கேள்வி கேட்டதால், சர்ச் தலைவர்களுக்குப் பயங்கர கோபம் வந்தது. அவர்களுடைய போதனைகளை ஒதுக்கித்தள்ளிய ஆண்களுக்கும் பெண்களுக்கும் மரண தண்டனை கொடுக்கப்பட்டது. மக்கள் பைபிளைப் படிப்பதையும், கேள்விகள் கேட்பதையும் சர்ச் தலைவர்கள் தடுக்க நினைத்தார்கள்; நிறைய சமயங்களில், அவர்கள் நினைத்ததைச் சாதித்தார்கள். ஆனால் தைரியமாக இருந்த சிலர், மகா பாபிலோன் தங்களைக் கட்டுப்படுத்துவதற்கு அனுமதிக்கவில்லை. கடவுளுடைய வார்த்தையில் இருக்கிற உண்மைகளை அவர்கள் கண்டுபிடித்திருந்தார்கள், இன்னும் நிறைய தெரிந்துகொள்ள ஆசைப்பட்டார்கள். பொய் மதத்திலிருந்து விடுதலை அடையும் காலம் நெருங்கிக்கொண்டிருந்தது!
14. (அ) பைபிளைப் படிக்க விரும்பியவர்கள் என்ன செய்தார்கள்? (ஆ) பைபிள் உண்மைகளைத் தெரிந்துகொள்ள சகோதரர் ரஸல் எடுத்த முயற்சிகளைப் பற்றி சொல்லுங்கள்.
14 பைபிளை வாசிக்கவும் ஆழமாகப் படிக்கவும், படித்த விஷயங்களை மற்றவர்களிடம் சொல்லவும் நிறைய மக்கள் ஆசைப்பட்டார்கள். எதை நம்ப வேண்டும், எதை நம்பக் கூடாது என்று சர்ச் தலைவர்கள் தங்களுக்குச் சொல்வதை அவர்கள் விரும்பவில்லை. அதனால், எந்த நாட்டில் சுதந்திரமாக பைபிளைப் படிக்க முடியுமோ அந்த நாட்டுக்கு அவர்கள் போனார்கள். அந்த நாடுகளில் ஒன்றுதான் அமெரிக்கா. அங்கே, 1870-ல் சார்ல்ஸ் டேஸ் ரஸலும் அவரோடு இருந்த சிலரும் பைபிளைப் பற்றி ஆழமாகப் படிக்க ஆரம்பித்திருந்தார்கள். முதலில், எந்த மதம் உண்மையைப் போதிக்கிறது என்று கண்டுபிடிக்க ரஸல் ஆசைப்பட்டார். வெவ்வேறு கிறிஸ்தவ மதங்களின் போதனைகளையும், கிறிஸ்தவம் அல்லாத மற்ற மதங்களின் போதனைகளையும் பைபிள் போதனைகளோடு கவனமாக ஒப்பிட்டுப் பார்த்தார். ஒரு மதம்கூட பைபிளை முழுமையாகப் பின்பற்றவில்லை என்று அவர் சீக்கிரமாகவே தெரிந்துகொண்டார். ஒரு கட்டத்தில், உள்ளூர் சர்ச் தலைவர்கள் நிறைய பேரிடம் அவர் பேசினார். அவரும் அவரோடு சேர்ந்து பைபிளை ஆராய்ச்சி செய்தவர்களும் கண்டுபிடித்த பைபிள் உண்மைகளை, அந்த சர்ச் தலைவர்கள் ஏற்றுக்கொள்வார்கள் என்றும் அவர்களுடைய சபையில் அந்த விஷயங்களைப் போதிப்பார்கள் என்றும் சகோதரர் ரஸல் எதிர்பார்த்தார். ஆனால், அந்த மதத் தலைவர்கள் அதைக் காதில்கூட வாங்கவில்லை. பொய் மதத்தின் பாகமாக இருந்தவர்களோடு சேர்ந்து கடவுளை வணங்க முடியாது என்பதை பைபிள் மாணாக்கர்கள் சீக்கிரத்திலேயே புரிந்துகொண்டார்கள்.—2 கொரிந்தியர் 6:14-ஐ வாசியுங்கள்.
15. (அ) உண்மைக் கிறிஸ்தவர்கள் எப்போது பாபிலோனின் கட்டுப்பாட்டுக்குள் போனார்கள்? (ஆ) அடுத்த கட்டுரையில் என்னென்ன கேள்விகளுக்கான பதில்களைப் பார்க்கப்போகிறோம்?
15 கடைசியாக இருந்த அப்போஸ்தலர் இறந்த உடனே, உண்மைக் கிறிஸ்தவர்கள் பாபிலோனின் கட்டுப்பாட்டுக்குள் போனார்கள் என்று இந்தக் கட்டுரையிலிருந்து தெரிந்துகொண்டோம். இருந்தாலும், இன்னும் சில கேள்விகளுக்கு நமக்குப் பதில் தெரிய வேண்டியிருக்கிறது. 1914-க்கு பல வருடங்களுக்கு முன்பிருந்தே, பரலோக நம்பிக்கையுள்ள கிறிஸ்தவர்கள் மகா பாபிலோனிலிருந்து விலகிக்கொண்டிருந்தார்கள் என்பதற்கு என்ன கூடுதலான ஆதாரங்கள் இருக்கின்றன? முதல் உலகப் போர் சமயத்தில் தன்னுடைய ஊழியர்கள் பிரசங்க வேலையில் மந்தமானதால், யெகோவா அவர்களுக்குத் தயவு காட்டவில்லை என்பது உண்மையா? முதல் உலகப் போர் சமயத்தில், நம்முடைய சகோதரர்களில் சிலர் நடுநிலைமையை விட்டுக்கொடுத்து, யெகோவாவின் தயவை இழந்தார்களா? அப்போஸ்தலர்கள் இறந்த உடனே கிறிஸ்தவர்கள் பொய் மதத்தின் கட்டுப்பாட்டுக்குள் போனார்கள் என்றால், அவர்கள் எப்போது அதிலிருந்து வெளியே வந்தார்கள்? இந்த அருமையான கேள்விகளுக்கான பதில்களை அடுத்த கட்டுரையில் பார்க்கலாம்.






    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				தமிழ் உவாட்ச்டவர் லைப்ரரி (2000-2016)
			
		

		
			
				
				விருப்பங்கள்
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				விதிமுறைகள்
			
			
				தனியுரிமை
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



