IEHOVA ne-a dat cu iubire cel mai prețios dar trimițându-l pe Fiul său unic-născut pe pământ (Ioan 3:16; 1 Ioan 4:9, 10).
Apostolul Pavel a spus că acest dar de la Dumnezeu este ‘un dar de nedescris’ (2 Cor. 9:15).
De ce a folosit el această expresie?
2 Pavel știa că jertfa perfectă a lui Cristos este o garanție că toate promisiunile minunate ale lui Dumnezeu se vor împlini. (Citește 2 Corinteni 1:20.) Astfel, acest ‘dar de nedescris’ avea să includă toate dovezile de bunătate și de iubire loială pe care Iehova urma să ni le dea prin Isus. Într-adevăr, acest dar este atât de impresionant, încât nu poate fi descris în cuvinte.
Cum ar trebui să ne influențeze acest dar special? Și în ce aspecte concrete ar trebui să ne îndemne la acțiune în timp ce ne pregătim pentru a celebra Comemorarea morții lui Cristos miercuri, 23 martie 2016?



DARUL SPECIAL AL LUI DUMNEZEU

 3, 4. a) Ce simți când primești un dar? b) Cum ar putea un dar special să-ţi schimbe viața?
3 Când primim un dar, ne bucurăm foarte mult. Însă unele daruri sunt atât de speciale sau de valoroase, încât ne pot schimba viața.
De exemplu, imaginează-ţi că ai fost implicat într-o crimă și ai fost condamnat la moarte.
Dintr-odată, un om pe care nu-l cunoști se oferă să ia pedeapsa asupra lui.
El este dispus să moară în locul tău!
Ce efect ar avea un astfel de dar asupra ta?
4 Fără îndoială, acest gest de iubire altruistă te-ar obliga să-ţi reanalizezi acțiunile și chiar să-ţi schimbi modul de viață.
Probabil că te-ar motiva să fii mai generos și mai iubitor cu alții și chiar să-i ierți pe cei care ți-au greșit.
Pentru tot restul vieții te-ai simți îndatorat față de cel care a fost dispus să se sacrifice pentru tine.
 5. În ce sens este darul răscumpărării primit de la Dumnezeu mult superior oricărui alt dar?
5 Totuși, darul pe care ni l-a dat Iehova prin Cristos este mult superior darului descris în acest exemplu (1 Pet. 3:18).
Să vedem de ce.
Din cauza păcatului moștenit, toți suntem condamnați la moarte (Rom. 5:12).
Plin de iubire,
Iehova l-a trimis pe Isus pe pământ ca „să guste moartea pentru fiecare om” (Evr. 2:9).
Jertfa lui Isus ne influențează nu doar viața din prezent.
Pe baza acesteia,
Iehova va înlătura cauza morții pentru totdeauna (Is. 25:7, 8; 1 Cor. 15:22, 26).
Toți cei care manifestă credință în Isus vor trăi veșnic în pace și fericire, fie ca regi împreună cu Cristos în cer, fie ca supuși ai Regatului lui Dumnezeu pe pământ (Rom. 6:23; Rev. 5:9, 10).
Ce alte binecuvântări sunt incluse în acest dar al lui Iehova?
 6. a) Ce binecuvântări pe care le aduce darul lui Iehova aștepți cel mai mult? b) Menționați trei lucruri pe care darul lui Dumnezeu ne îndeamnă să le facem.
6 Darul lui Iehova include vindecarea tuturor bolilor, transformarea pământului în paradis și învierea morților (Is. 33:24; 35:5, 6; Ioan 5:28, 29).
Cu siguranță, îi iubim pe Iehova și pe Fiul său pentru că ne-au dat acest ‘dar de nedescris’.
Totuși, ce ne îndeamnă iubirea lui Dumnezeu să facem? În continuare vom vedea cum ne îndeamnă iubirea lui Dumnezeu 1) să călcăm cât mai exact pe urmele lui Cristos, 2) să arătăm iubire față de frații noștri și 3) să-i iertăm pe alții din inimă.



„IUBIREA LUI CRISTOS NE OBLIGĂ”

 7, 8. Cum ar trebui să ne simțim având în vedere iubirea lui Cristos și ce ar trebui aceasta să ne îndemne să facem?
7 În primul rând, ar trebui să ne simțim obligați să trăim pentru Cristos.
Apostolul Pavel a spus: „Iubirea lui Cristos ne obligă”. (Citește 2 Corinteni 5:14, 15.) Pavel a înțeles că, dacă acceptăm marea iubire a lui Cristos, ne vom simți obligați, sau îndemnați, să trăim pentru el.
Când înțelegem pe deplin ce a făcut Iehova pentru noi și simțim iubirea lui, ne vom dori să trăim pentru Cristos.
Cum putem face acest lucru?
8 Cei care îl iubesc pe Iehova se simt obligați să urmeze exemplul lui Cristos, umblând așa cum a umblat el și călcând cât mai exact pe urmele lui (1 Pet. 2:21; 1 Ioan 2:6).
Când ascultăm de Dumnezeu și de Cristos, arătăm că îi iubim.
Isus a spus: „Cine are poruncile mele și le respectă, acela mă iubește, iar cel care mă iubește va fi iubit de Tatăl meu; și eu îl voi iubi și mă voi arăta clar lui” (Ioan 14:21; 1 Ioan 5:3).
 9. Cu ce presiuni ne confruntăm?
9 În perioada Comemorării ar fi bine să medităm la modul în care ne trăim viața. Întreabă-te: În ce privințe urmez îndeaproape exemplul lui Isus? În ce privințe aș putea face îmbunătățiri?
Este foarte important să ne punem aceste întrebări deoarece asupra noastră se fac în mod constant presiuni ca să urmăm modul de viață al lumii (Rom. 12:2).
Dacă nu suntem atenți, am putea ajunge discipolii filozofilor acestei lumi sau chiar ai celebrităților ori ai vedetelor din lumea sportului (Col. 2:8; 1 Ioan 2:15-17).
Cum putem face față acestor presiuni?
10. Ce ne putem întreba în perioada Comemorării și ce ne-am putea simți îndemnați să facem? (Vezi imaginea de la începutul articolului.)
10 Putem folosi perioada Comemorării ca pe o ocazie de a ne examina îmbrăcămintea, colecția de filme și de muzică, precum și fișierele de pe computer, telefon sau tabletă.
Când îți examinezi îmbrăcămintea, întreabă-te:
Dacă aș merge într-un loc unde ar fi și Isus, m-aș simți stânjenit cu aceste haine? (Citește 1 Timotei 2:9, 10.) Ar arăta îmbrăcămintea mea că sunt un continuator al lui Cristos?
Ne-am putea pune întrebări asemănătoare și cu privire la alegerile noastre în materie de filme și muzică.
De exemplu, i-ar plăcea lui Isus să vizioneze aceste filme sau să asculte această muzică?
Dacă i-aș împrumuta telefonul sau tableta, mi-ar fi rușine de ceea ce ar găsi acolo?
Când analizezi un joc video, întreabă-te:
Mi-ar fi greu să-i explic lui Isus de ce îmi place acest joc?
Iubirea noastră față de Iehova ar trebui să ne oblige să ne debarasăm de orice lucru care ar fi nepotrivit pentru un discipol al lui Cristos, indiferent cât ar costa el (Fap. 19:19, 20).
Când ne-am dedicat lui Iehova, i-am promis că nu vom mai trăi pentru noi înșine, ci pentru Cristos.
De aceea, n-ar trebui să ne atașăm de niciun lucru care ne-ar putea împiedica să călcăm cât mai exact pe urmele lui Cristos (Mat. 5:29, 30; Filip. 4:8).
11. a) Cum ne influențează iubirea față de Iehova și Isus în lucrarea de predicare? b) Cum ne-ar putea îndemna iubirea să-i ajutăm pe cei din congregație?
11 De asemenea, iubirea față de Isus ne îndeamnă să predicăm și să-i învățăm pe alții cu zel (Mat. 28:19, 20; Luca 4:43). În perioada Comemorării avem ocazia de a sluji ca pionieri auxiliari, petrecând 30 sau 50 de ore în lucrarea de predicare.
Ai putea să-ţi organizezi programul astfel încât să faci și tu pionierat auxiliar?
Un frate văduv în vârstă de 84 de ani credea că nu poate face pionierat auxiliar din cauza vârstei și a stării de sănătate.
Dar pionierii din zona în care locuia s-au oferit să-l ajute.
Ei i-au asigurat transportul și i-au ales cu atenție teritoriul în care să predice, astfel încât fratele să reușească să facă cele 30 de ore.
Ai putea să ajuți și tu pe cineva din congregația ta să simtă bucuria de a face pionierat auxiliar în perioada Comemorării?
Desigur, nu toți putem să facem pionierat auxiliar. Însă ne putem folosi timpul și energia pentru a-i aduce mai multe jertfe de laudă lui Iehova.
Astfel, vom arăta că, asemenea lui Pavel, suntem motivați de iubirea lui Cristos.
Dar ce ne mai îndeamnă iubirea lui Dumnezeu să facem?



AVEM OBLIGAȚIA SĂ NE IUBIM UNII PE ALȚII

12. Ce ne îndeamnă iubirea lui Dumnezeu să facem?
12 În al doilea rând, iubirea lui Dumnezeu ar trebui să ne îndemne să-i iubim pe frații noștri. În această privință, apostolul Ioan a scris: „Iubiții mei frați, dacă așa ne-a iubit Dumnezeu, și noi avem obligația să ne iubim unii pe alții” (1 Ioan 4:7-11).
De aceea, dacă acceptăm iubirea lui Dumnezeu, ne vom simți obligați să-i iubim pe frații noștri (1 Ioan 3:16).
Cum ne putem arăta în mod concret iubirea?
13. Ce exemplu ne-a lăsat Isus în ce privește iubirea față de oameni?
13 Să ne gândim la exemplul lui Isus. În timpul serviciului său pământesc, el le-a acordat o atenție deosebită celor de condiție umilă.
El a vindecat ologi, orbi, surzi și muți (Mat. 11:4, 5).
Lui Isus i-a făcut plăcere să-i învețe pe oamenii flămânzi din punct de vedere spiritual, pe care conducătorii religioși iudei îi considerau ‘blestemați’ (Ioan 7:49).
El i-a iubit pe acei oameni umili și a făcut tot ce a putut ca să-i ajute (Mat. 20:28).





Poți ajuta un frate sau o soră în vârstă în lucrarea de predicare? (Vezi paragraful 14)




14. Cum ți-ai putea arăta iubirea față de frații din congregația ta?
14 În perioada Comemorării avem ocazia de a urma exemplul lui Isus gândindu-ne cum i-am putea ajuta pe frații și surorile din congregația noastră, mai ales pe cei în vârstă.
De exemplu, i-ai putea vizita pe acești frați dragi?
Le-ai putea duce ceva de mâncare?
I-ai putea ajuta la unele treburi gospodărești?
I-ai putea duce cu mașina la întruniri?
Sau i-ai putea invita să meargă cu tine în predicare? (Citește Luca 14:12-14.) Trebuie să permitem ca iubirea lui Dumnezeu să ne îndemne să arătăm iubire față de frații noștri!



SĂ ARĂTĂM ÎNDURARE FAȚĂ DE FRAȚII NOȘTRI

15. Ce trebuie să recunoaștem?
15 În al treilea rând, iubirea lui Iehova trebuie să ne îndemne să-i iertăm pe frații noștri de credință. Întrucât suntem urmașii primului om,
Adam, cu toții am moștenit păcatul și moartea.
De aceea, niciunul dintre noi nu poate spune: „Eu n-am nevoie de răscumpărare”.
Chiar și cel mai fidel slujitor al lui Dumnezeu are nevoie de bunătatea nemeritată a lui Iehova arătată prin Cristos.
Fiecăruia dintre noi i s-a iertat o datorie foarte mare!
De ce este important să recunoaștem acest lucru?
Găsim răspunsul într-o ilustrare făcută de Isus.
16, 17. a) Ce învățăm din ilustrarea lui Isus despre regele care a iertat o datorie foarte mare? b) Ce ești hotărât să faci meditând la ilustrarea lui Isus?
16 Isus a făcut o ilustrare despre un rege care i-a iertat unui sclav o datorie imensă de 10 000 de talanți, sau 60 000 000 de dinari.
Totuși, mai târziu, acest sclav n-a fost dispus să-i ierte unui tovarăș de sclavie o datorie mult mai mică, de 100 de dinari.
Regele s-a înfuriat când a aflat cât de insensibil a fost sclavul pe care el îl iertase cu îndurare.
Regele a spus: „Sclav rău, eu ți-am anulat toată datoria când m-ai implorat.
Nu trebuia să te înduri și tu de tovarășul tău de sclavie, așa cum m-am îndurat și eu de tine?” (Mat. 18:23-35).
Da, îndurarea extraordinară de care a dat dovadă regele ar fi trebuit să-l îndemne pe acel sclav să-l ierte pe tovarășul său de sclavie.
Asemenea acestui rege,
Iehova ne-a iertat o datorie imensă.
Ce ar trebui să ne îndemne iubirea și îndurarea lui Dumnezeu să facem?
17 Perioada Comemorării ne oferă ocazia de a vedea dacă avem resentimente față de frații și surorile noastre.
Dacă da, acesta este un moment potrivit pentru a-l imita pe Iehova, care este ‘gata să ierte’ (Neem. 9:17; Ps. 86:5).
Dacă arătăm apreciere față de îndurarea lui Iehova, ne vom dori să-i iertăm din inimă pe alții.
Dacă nu-i iubim și nu-i iertăm pe frații noștri, nu ne putem aștepta ca Iehova să ne iubească și să ne ierte (Mat. 6:14, 15).
E adevărat, faptul de a-i ierta pe alții nu va schimba trecutul, dar, cu siguranță, ne va schimba în bine viitorul.
18. Cum a ajutat-o iubirea lui Dumnezeu pe o soră să suporte imperfecțiunile altei surori?
18 Multora dintre noi ne-ar putea fi greu ‘să-i suportăm’ zi de zi pe frații și pe surorile noastre. (Citește Coloseni 3:13, 14; Efeseni 4:32.) De exemplu,
Lily, o soră celibatară, a ajutat-o de bunăvoie pe Carla,[1] o soră văduvă.
Lily o ducea cu mașina, îi făcea unele cumpărături și arăta bunătate față de ea în multe alte feluri.
Cu toate acestea,
Carla avea mereu o atitudine critică și era dificilă. Însă Lily s-a concentrat asupra calităților Carlei.
Ea a continuat s-o ajute câțiva ani, până când Carla s-a îmbolnăvit grav și a murit.
Chiar dacă nu i-a fost ușor s-o ajute pe Carla,
Lily spune: „Abia aștept s-o văd la înviere.
Vreau s-o cunosc când va fi perfectă”.
Da, iubirea lui Dumnezeu ne poate îndemna să-i suportăm pe frații noștri de credință și să așteptăm timpul când imperfecțiunea umană va dispărea pentru totdeauna.
19. Ce te îndeamnă să faci ‘darul de nedescris’ al lui Dumnezeu?
19 Într-adevăr, noi am primit un ‘dar de nedescris’ de la Iehova.
Să nu luăm niciodată acest dar prețios de la sine înțeles. În schimb, mai ales în perioada Comemorării, să medităm cu apreciere la tot ce au făcut Iehova și Isus pentru noi.
Da, fie ca iubirea lor să ne oblige să călcăm cât mai exact pe urmele lui Isus, să arătăm iubire față de frații noștri și să-i iertăm din inimă!



[1] (paragraful 18)
Unele nume din acest articol au fost schimbate.




    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Publicații în limba română (2000-2017)
			
		

		
			
				
				Preferințe
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Termeni de utilizare
			
			
				Politică de confidențialitate
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



