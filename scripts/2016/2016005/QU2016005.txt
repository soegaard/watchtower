NACEKUSQAN WATA: 1971

SUYUN: FRANCIA

ÑAUPAJ KAUSAYNIN: KHUCHI KAUSAYNIYOJ KARQA,
SUWAKOJ,
SAJRA IMASTA RUWAJ,
DROGAKOJTAJ










ÑAUPAJ KAUSAYNIY.
 Tellancourt llajtitapi tiyakorqayku chaytaj Francia suyupi kashan.
Tatayqa Francia suyumanta karqa, mamaytaj Italia suyumanta.
Pusaj watitayoj kashajteyqa Italiaman riporqayku Roma llajtapitaj tiyakorqayku.
Chay lugarpeqa wajcha runaslla tiyakoj kanku, jinapis runasqa mana qhellaschu karqanku.
Wasiypitaj kausayqa llakiy karqa, imajtinchus tatasniy qolqerayku chʼajwaj kanku.
Chunka phishqayoj kashajtiy mamayqa calleman llojsinayta munaj amigosniyoj kanaypaj.
Chayrayku callellapiña sayarqani, mana allin runaswantaj masichakorqani.
Uj kuti uj runa chimpaykamuwarqa, paytaj kʼachaman rijchʼakorqa.
Drogasta qowasqanta japʼikorqani machu runaman rijchʼakuyta munasqayrayku.
Pisi tiemponmanqa drogakojña kani, khuchi kausayniyojñataj karqani.
Ashkha kutispi abusawarqanku.
Kausayqa qhasillaña kasqanta yuyaj kani.
Imaraykuchus noqapajqa kausaypis wañuypis kikillan karqa, sapayakorqanitaj.
Chunka sojtayoj kashajteyqa wañuchikuyta munarqani.
Chayrayku botella alcoholta tomaykorqani, uj qochamantaj choqaykukorqani.
Kinsa pʼunchayta wañusqa jina kasharqani.
Chaypiraj kausayta jatunpaj qhawarqani.
Jinapis sajra runa karqani, runastataj chʼaukiyaj kani.
Runaswanpis khuchichakoj kani.
Runaj wasinman chayaspaqa drogaykoj kani, valorniyoj kaj imasninkutataj apakapoj kani.
May rejsisqa pandillas contratawaj kanku Italia suyupi drogata vendenaypaj.
Sapa kutitaj policiawan japʼichikoj kani.
Kausayniy llakiy kajtinpis yacharqani mana qhasipajchu kausasqanchejta.
Chayrayku Diosmanta mañakorqani: “Uj watallata kusisqa, kʼachatataj kausakuyta munani”, nispa.



BIBLIAMAN JINA KAUSAYNIYTA TIJRACHISQAY.
 Iskay chunka tawayoj kashajtiytaj Inglaterra suyuman ripusharqani imaraykuchus drogata vendejkuna wañuchiyta munashawarqanku.
Niraj ripushaspaqa mamaypaman rerqani, manchharikorqanitaj Annunziato Lugarà nisqa sutiyoj runata rikuspa, payqa mamayman Bibliamanta willarishasqa.* Chay runaqa suwa karqa, chayrayku imatachus chaypi ruwashasqanta taporqani.
Paytaj Jehovaj testigon kananpaj imastachus ruwasqanmanta willariwarqa, chantapis Inglaterra suyupi Jehovaj testigosninta maskʼanayta niwarqa.
Chayta ruwanayta nispapis kikillantapuni kausasharqani.
Uj pʼunchay Jehovaj testigonta rikorqani Londres llajtaj ujnin callenpi Torremanta Qhawaj, ¡Despertad! revistaswan willashajta.
Chaypitaj yuyarikorqani Italiapi imatachus nisqayta, chayrayku chay Jehovaj testigonta nerqani Bibliamanta yacharichinawanta.
Yachakusqayqa tʼukuchiwarqa.
Chantapis 1 Juan 1:9 versiculopi nin: “Juchasninchejtachus Diosman willakunchej chayri, juchasninchejta pampachanqa, nisqanta juntʼaj, cheqantaj kasqanrayku.
Jinallataj payqa tukuy sajra kayninchejmantapis pichawasunchej”, nispa.
Bibliapi ajina versiculosta leesqayqa sonqoyman chayarqa.
Chay nisqanqa sajra kausayniyta saqepunaypaj yanapawarqa, imajtinchus ruwasqaywan chʼichichasqa jina kasharqani.
Jehovaj testigosninpa tantakuyninkuman rerqanipacha, chaypitaj kʼachas kasqankuta rikorqani.
Allinta kʼachatataj kausakusqankuta rikuspa noqapis paykuna ukhumanta kayta munarqani, imajtinchus unaymantapachapuni ajinata kausakuyta munarqani.
Pisi tiempollapi drogasta, millay kausayniyta ima saqeporqani.
Jinapis kʼacha runa kaytaqa mana atillarqanichu.
Yacharqani tukuyta respetanayta, kʼachataj kanayta.
Chaywanpis kunankama kallpachakushallanipuni.
Jehová yanapawasqanraykutaj aswan kʼacha runaña kani.
Kay 1997 watapitaj sojta killata Bibliamanta yachakuytawan bautizakuspa Jehovaj testigon karqani.








BENDICIONESTA JAPʼISQAY.
 Bautizakuytawantaj Barbarawan casarakorqani, paypis niraj unaychu Jehovaj testigonman tukusqa.
Ujnin amigoypis Bibliamanta yachakorqa manaña millaytachu kausasqayta rikuwasqanrayku.
Kunanqa amigoy, hermananpis Jehovaj testigosnin kanku.
Abuelaypa hermananpis Bibliamanta yachakullarqataj.
Payqa 80 kurajniyojña Bibliamanta yachakuyta qallarerqa, bautizakuytawantaj wañuporqa.
Kunanqa anciano kani.
Londres llajtapitaj warmiywan sapa killa 70 horasta Diosmanta willayku, italiano parlayta parlajkunamantaj Bibliamanta yachachiyku.
Wakin kuteqa llakikuni sajra kausayniyta yuyarikuspa, jinapis Barbara sonqochawan mana llakikunaypaj.
Kunanmá kʼacha familiayoj, kʼacha Tatayojtaj kani, chaytataj mayta munarqani.
Diosmanta mañakorqani, uj watallata kusisqa, kʼachatataj kausakuyta, jinapis unaytaña kusisqa kausakuni.
Kunanmá kʼacha familiayoj, kʼacha Tatayojtaj kani, chaytataj mayta munarqani




Kay Torremanta Qhawaj 1 de julio de 2014, 8, 9 paginaspi,
Annunziato Lugarà kausayninmanta leeriy, “Bibliamanta yachakusqankoqa kausayninkuta tijracherqa:
Pistolaywanpuni purej kani”, nisqa yachaqanapi.




    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Quechuapi publicaciones (2004-2017)
			
		

		
			
				
				Allinchanapaj
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Kay kamachiykunata kasukuy
			
			
				Datosta apaykachakusqanmanta
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



