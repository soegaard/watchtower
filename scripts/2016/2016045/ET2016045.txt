OLI pühapäeva hommik, kell oli umbes üheksa.
Neile, kes olid Jeruusalemmas, oli see päev väga eriline.
See oli pidupäev ja seepärast ühtlasi ka hingamispäev.
Templis oli hommikune ohvritalitus juba toimunud.
Nüüd valmistus ülempreester tooma uudseviljast ja juuretisega küpsetatud leivapätse kõigutusohvriks. (3. Moos. 23:15–20.)
See ohver märgistas nisulõikuse algust.
Oli aasta 33 m.a.j ja käes oli nädalatepüha.
2 Kõik need toimingud leidsid aset templis, kuid ühe Jeruusalemma maja ülemises toas toimus midagi palju tähtsamat.
Sinna oli kogunenud umbes 120 Kristuse jüngrit, kes „kõik olid ühel meelel püsivalt palvetamas”. (Ap. t. 1:13–15.)
Sellel, mis seal juhtuma hakkas, oli otsene seos ülempreestri iga-aastase toiminguga nädalatepühal.
Samuti täitus sel päeval ennustus, mille oli umbes 800 aastat varem kirja pannud prohvet Joel. (Joel 2:28–32; Ap. t. 2:16–21.)
Miks oli seal toimuv nii tähtis?
3 Loe Apostlite teod 2:2–4. Need, kes olid kogunenud sinna ülemisse tuppa, said täis püha vaimu. (Ap. t. 1:8.)
Nad hakkasid rääkima imelistest asjadest, mida olid näinud ja kuulnud.
Peagi tuli kokku suur hulk rahvast, kellele apostel Peetrus selgitas, mis oli juhtunud.
Seejärel ütles ta kuulajaile: „Kahetsege oma patte ja igaüks teist lasku end Jeesus Kristuse nimesse ristida, et teie patud andeks antaks, ja siis te saate kingiks püha vaimu.” Umbes 3000 inimest võtsid seda kuulda, nad ristiti ja nemadki said püha vaimu. (Ap. t. 2:37, 38, 41.)
 4. a)
Miks peaks meile pakkuma huvi see, mis toimus 33. aasta nädalatepühal? b) Milline teine tähtis sündmus võis toimuda samal päeval palju aastaid tagasi? (Vaata järelmärkust.)
4 Miks on 33. aasta nädalatepüha meile nii tähtis?
Mitte selle pärast, mis toimus Jeruusalemma templis.
See, mida ülempreester templis tegi, oli vaid prohvetlikuks eelpildiks sellele, mille ülempreester Jeesus Kristus täide viis.[1] Ülempreester ohverdas nädalatepühal Jehoovale kaks sümboolse tähendusega leiba.
Need juuretisega küpsetatud leivad sümboliseerisid püha vaimuga võitud kristlasi, keda Jumal patuse inimkonna hulgast valis, et nad saaksid tema poegadeks.
Nõnda avanes teatud hulgale kristlastest võimalus saada „uudseviljaks” inimkonna seast ja olla tulevikus taevases valitsuses, mis toob lugematuid õnnistusi kõigile kuulekatele inimestele. (Jaak. 1:18, allmärkus; 1. Peetr. 2:9.)
Olgu meil siis lootus elada taevas või maa peal, meile kõigile on sel päeval toimunud sündmustel sügav tähendus.



KUIDAS VÕIDMINE TOIMUB?

 5. Kust me teame, et kõiki võitud kristlasi pole võitud samal viisil?
5 Kui sina oleksid olnud nende seas, kelle pea kohale ilmusid tulekeeled, ei unustaks sa seda päeva ilmselt kunagi.
Sul poleks olnud mingit kahtlust, et sind võiti püha vaimuga, eriti veel kui said imelise anni rääkida võõras keeles. (Ap. t. 2:6–12.)
Ent kas kõik võitud kristlased on saanud võituks sama erilisel viisil nagu need esimesed 120 jüngrit?
Ei. Ülejäänud sel päeval uue rahva liikmeks saanud inimesed võiti püha vaimuga siis, kui nad ristiti. (Ap. t. 2:38.)
Nende pea kohal polnud tulekeeli.
Lisaks sellele ei võitud mitte kõiki kristlasi püha vaimuga ristimise ajal.
Näiteks samaarlased said püha vaimuga võituks alles mingi aeg pärast ristimist. (Ap. t. 8:14–17.)
Ent Korneelius ja tema lähedased võiti püha vaimuga erandkorras juba enne nende ristimist. (Ap. t. 10:44–48.)
 6. Mille on kõik võitud saanud ja kuidas see neid mõjutab?
6 Niisiis pole mitte kõik saanud võituks täpselt samal viisil.
Mõned on mõistnud üsna äkki, et neile on esitatud see kutse, teised jälle järk-järgult.
Ent ükskõik kuidas kellegi võidmine on toimunud, käib nende kõigi kohta Pauluse kirjeldus: „Teid [märgistati]
Kristuse kaudu pitseriga, püha vaimuga, mida teile oli tõotatud.
See on meie pärandi tagatiseks.” (Efesl. 1:13, 14.)
Selline püha vaimuga märgistamine on justkui ettemakstud käsiraha ehk tagatis mingi tulevase tehingu kohta.
Võitud kristlased on selle tagatise tõttu oma taevase elu lootuses veendunud. (Loe 2. Korintlastele 1:21, 22; 5:5.)
 7. Mida peab iga võitud kristlane tegema, et saada kätte oma taevane tasu?
7 Kas võitud kristlasele, kes on saanud sellise tagatise, on taevane tasu garanteeritud?
Ei ole.
Ta on kindel, et tal on lootus taevasele elule.
Ent kas ta saab oma tasu ka kätte, sõltub sellest, kas ta jääb elu lõpuni ustavaks.
Peetrus selgitas seda nii: „Seepärast, vennad, tehke kõik endast olenev, et jääda kutsutute ja valitutena ustavaks, sest kui teil seda kõike on, ei lange te iial ning teil on suur au saada meie isanda ja päästja Jeesus Kristuse igavesse kuningriiki.” (2. Peetr. 1:10, 11.)
Seega peab iga võitud kristlane pingutama, et jääda ustavaks.
Kui ta ei jää ustavaks, ei saa ta oma taevast tasu kätte. (Heebr. 3:1; Ilm. 2:10.)



KUIDAS KRISTLANE TEAB,
ET TA ON VÕITU?

8., 9. a) Miks on enamikul inimestel raske mõista, mis toimub kristlasega, keda võitakse püha vaimuga? b) Kuidas kristlane teab, et ta on saanud taevase kutse?
8 Enamikul Jehoova teenijatest pole tänapäeval kerge mõista, kuidas toimub püha vaimuga võidmine.
See on ka loomulik, sest nad ise ju pole võitud.
Jumala algne eesmärk oli, et inimesed elaksid igavesti maa peal. (1. Moos. 1:28; Laul 37:29.)
Algul polnud Jumalal plaanis valida inimesi kuningateks ja preestriteks taevasse valitsema, see on erandlik korraldus.
Taevase kutse saamine ja püha vaimuga võidmine muudab oluliselt inimese mõtlemist ja tulevikulootust. (Loe Efeslastele 1:18.)
9 Ent kuidas siis keegi teab, et talle on esitatud taevane kutse ja et ta on tõesti saanud selle kohta erilise tagatise?
Vastus ilmneb selgelt Pauluse sõnadest Rooma võitud kristlastele, keda ta nimetas „kutsutud pühadeks”.
Ta ütles neile: „Te pole saanud mitte vaimu, mis teeb teid orjaks ja tekitab jälle kartust, vaid vaimu, mille kaudu Jumal on teid lapsendanud oma poegadeks ja mis paneb meid hüüdma: „Abba, isa!” See vaim ise tunnistab koos meie vaimuga, et me oleme Jumala lapsed.” (Rooml. 1:7; 8:15, 16.)
Lühidalt öeldes annab Jumal püha vaimu kaudu oma teenijale selge teadmise, et ta on kutsutud valitsema koos Kristusega taevas. (1. Tess. 2:12.)
10. Mida tähendab tekstis 1. Johannese 2:27 mõte, et võitud kristlane ei vaja, et keegi teda õpetaks?
10 Need, kes on saanud Jumalalt taevase kutse, ei vaja enam muid tõendeid mingist teisest allikast.
Neil pole tarvis kellegi teise kinnitust selle kohta, mis nendega on toimunud.
Jehoova annab neile nende lootuse kohta kindla veendumuse.
Apostel Johannes ütles võitud kristlastele: „Teid on aga võidnud see, kes on püha, ja te tunnete tõde.” Ta lisas: „Teid on Jumal võidnud püha vaimuga ja see vaim püsib teis, seega pole vaja, et keegi teid õpetaks.
Jumal õpetab teile selle vaimu kaudu kõike ja see vaim on tõene ega valeta.
Jääge temaga ühte, nii nagu teid on õpetatud.” (1. Joh. 2:20, 27.)
Võitud kristlased vajavad vaimset juhatust nagu kõik teisedki.
Ent nad ei vaja kellegi teise kinnitust selle kohta, et nad on võitud.
Universumi kõige võimsam jõud on andnud neile selles kindla veendumuse.



NAD ON „UUESTI SÜNDINUD”

11., 12. Millistele küsimustele võib võitud kristlane mõelda, ent milles ta kunagi ei kahtle?
11 Kui kristlane saab püha vaimu kaudu kinnituse, et ta on võitu, tähendab see talle tohutu suurt muudatust.
Jeesus rääkis sellest sisemisest protsessist kui uuesti sündimisest või ülalt sündimisest.[2] (Joh. 3:3, 5, allmärkus.)
Ta selgitas: „Ära imesta, et ma sulle ütlesin: „Te peate uuesti sündima.” Tuul puhub, kuhu tahab, ja sa kuuled selle häält, aga ei tea, kust see tuleb ja kuhu läheb.
Nii on ka igaühega, kes on sündinud vaimust.” (Joh. 3:7, 8.)
Inimesele, kes pole ise seda kogenud, on võimatu täielikult selgitada, mida see kõik tähendab.
12 Need, kes on saanud selle erilise kutse, võivad mõelda: „Miks mind valiti?
Miks just mind ja mitte kedagi teist?” Nad võivad isegi mõelda, kas nad on selle väärilised.
Ent nad ei sea kahtluse alla tõsiasja, et nad on selle kutse saanud.
Nende süda on täis rõõmu ja tänulikkust.
Nad tunnevad sama, mida Peetrus, kes kirjutas Jumala vaimu mõjutusel: „Kiidetud olgu meie isanda Jeesus Kristuse Jumal ja isa, kelle suure halastuse tõttu me oleme uuesti sündinud, et meil oleks püsiv lootus.
See lootus on meil tänu sellele, et Jeesus Kristus äratati surnuist üles.
Me oleme uuesti sündinud, et saada kadumatu, rüvetamatu ja hävimatu pärand.
Seda hoitakse taevas teile.” (1. Peetr. 1:3, 4.)
Kui võitud neid sõnu loevad, tunnevad nad ilma igasuguse kahtluseta, et nende taevane isa räägib otse nendega.
13. Millist muutust mõtlemises inimene kogeb, kui ta võitakse püha vaimuga, ja mis seda põhjustab?
13 Enne kui võitud kristlased said Jumalalt sellise isikliku kutse, oli neil lootus elada igavesti maa peal.
Nad igatsesid aega, mil Jehoova kõrvaldab kurjuse ja taastab paradiisi, ning soovisid ise sellest kõigest osa saada.
Ehk kujutasid nad ette ka seda, kuidas nad saavad kokku oma lähedastega, kes on surmast üles äratatud.
Nad unistasid elust oma kätega ehitatud majas ja sellest, kuidas nad söövad oma aias kasvanud vilju. (Jes. 65:21–23.)
Miks aga nende mõtted muutusid?
Asi pole selles, et selline tulevik poleks pakkunud neile enam rahuldust.
Nende mõtted ei muutunud ka liigse stressi või sisemise segaduse tõttu.
Samuti ei loobunud nad seostamast oma tulevikku eluga maa peal seetõttu, otsekui oleksid nad äkitselt tundnud, et igavene elu maa peal on igav või et nad lihtsalt tahavad proovida, missugune on elu taevas.
Selline muutus toimus neis hoopis seepärast, et Jumala vaim kutsus neid ning muutis nende mõtlemist ja tulevikulootust.
14. Kuidas suhtuvad võitud oma praegusesse ellu maa peal?
14 Kas me peaks aga järeldama, et võitud soovivad surra?
Paulus vastas sellele: „Meie, kes oleme selles telgis, ägame rõhutuna, kuna me ei taha seda telki endalt ära heita, kuid tahame ometi riietada end taevasesse, nii et igavene elu neelaks ära sureliku keha.” (2. Kor. 5:4.)
Nad pole kaotanud huvi praeguse elu vastu ega soovi, et sellele tuleks kiirelt lõpp.
Vastupidi, nad kasutavad innukalt igat päeva Jehoova teenistuses koos oma sõprade ja sugulastega.
Ent mida nad ka ei teeks, on neil alati meeles nende suurepärane tulevikulootus. (1. Kor. 15:53; 2. Peetr. 1:4; 1. Joh. 3:2, 3; Ilm. 20:6.)



KAS SINA OLED SAANUD TAEVASE KUTSE?

15. Mis asjaolud ei tõesta, et inimene on võitud püha vaimuga?
15 Võib-olla sa mõtled endamisi, kas ka sina oled saanud selle erilise kutse.
Sel juhul võiksid kaaluda mõningaid tähtsaid küsimusi.
Kas tunned endas suurt indu teha kuulutustööd?
Kas uurid agaralt Jumala sõna ja armastad süüvida „Jumala tarkuse sügavustesse”? (1. Kor. 2:10.)
Kas oled kogenud, et Jehoova õnnistab sinu teenistust kuidagi eriliselt?
Kas sul on tuline soov täita Jehoova tahet?
Kas tunned sisimas suurt vastutust teisi vaimselt aidata?
Kas oled näinud tõendeid, et Jehoova on sind erilisel viisil aidanud?
Kui sinu vastus kõigile nendele küsimustele on kindel jah, siis kas see näitab, et oled saanud taevase kutse?
Ei, seda see ei näita.
Mispärast?
Sest selliseid tundeid võivad kogeda kõik jumalateenijad, olgu nad võitud või mitte.
Jehoova vaim tegutseb sama jõuliselt ka neis, kel on väljavaade elada igavesti maa peal.
Tegelikult võib öelda, et kui murrad pead selle üle, kas sa oled saanud taevase kutse, näitab see juba iseenesest, et sa pole seda saanud.
Need, keda Jehoova on kutsunud, ei kahtle, kas nad on kutsutud või mitte.
Nad teavad seda!
16. Kust me teame, et mitte kõik, kes on saanud Jumala püha vaimu, pole saanud taevast kutset?
16 Pühakirjas on palju näiteid ustavatest jumalateenijatest, keda püha vaim jõuliselt mõjutas.
Neil inimestel polnud aga lootust elada tulevikus taevas. Üks selline mees oli Ristija Johannes.
Jeesus pidas temast väga lugu, kuid teatas, et Johannes pole kutsutud taevasesse valitsusse. (Matt. 11:10, 11.)
Ka Taavetit mõjutas Jumala püha vaim. (1. Saam. 16:13.)
Ta oli väga vaimne mees ja kirjutas isegi mõned Piibli osad. (Mark. 12:36.)
Siiski ütles Peetrus nädalatepühal: „Taavet ju ei läinud üles taevasse.” (Ap. t. 2:34.)
Kuigi Jehoova andis oma püha vaimuga neile meestele väe teha hämmastavaid tegusid, ei valinud ta neid välja taevaseks eluks.
See aga ei tähenda, nagu poleks nad selleks kõlvanud või olnuks neil midagi vajaka.
Jehoova äratab need ustavad jumalateenijad ellu paradiislikul maal. (Joh. 5:28, 29; Ap. t. 24:15.)
17., 18. a) Millist tasu ootab tänapäeval enamik jumalateenijaid? b) Mis küsimusi me arutame järgmises artiklis?
17 Tänapäeval pole enamik jumalateenijatest saanud taevast kutset.
Neil on samasugune lootus nagu Taavetil,
Ristija Johannesel ja teistel vanaaja usumeestel ja -naistel.
Nii nagu Aabraham, ootavad ka nemad elu Jumala kuningriigi alluvuses. (Heebr. 11:10.)
Praegusel lõpuajal on maa peal vaid väike hulk kristlasi, kes on valitud elama taevas. (Ilm. 12:17.)
See tähendab, et suuremal osal 144 000 väljavalitust on maine elutee juba lõppenud ja nad valitsevad taevas.
18 Kuidas peaksid aga need, kel on väljavaade elada igavesti maa peal, suhtuma nendesse, kes väidavad, et neil on taevase elu lootus?
Kuidas sa peaksid reageerima, kui sinu koguduses hakkab keegi mälestusõhtul leiba ja veini võtma?
Kas me peaksime muretsema selle pärast, et kasvab nende arv, kes väidavad, et nad on saanud taevase kutse?
Neid küsimusi arutame järgmises artiklis.



[1] (4. lõik.)
Võimalik, et Iisraeli rahvaga sõlmiti Siinai mäel seaduseleping samal päeval, kui hiljem hakati tähistama nädalatepüha. (2. Moos. 19:1.)
Kui see on nii, siis nagu Moosese kaudu sõlmiti sel päeval Iisraeli rahvaga seaduseleping, sõlmiti hiljem samal päeval Jeesus Kristuse kaudu uus leping ja seeläbi tuli esile uus rahvas, vaimne Iisrael.
[2] (11. lõik.)
Rohkem selgitust uuestisünni kohta võib leida ajakirjast Vahitorn 1. aprill 2009, lk 3–11.




    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Eestikeelsed väljaanded (2000-2017)
			
		

		
			
				
				Eelistused
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Kasutustingimused
			
			
				Privaatsus
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



