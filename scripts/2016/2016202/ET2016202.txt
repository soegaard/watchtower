KAS sina oled noor, kellel on soov minna ristimisele?
Kui nii, siis oled sa astumas kõige aulisemat sammu, mis inimesel võimalik.
Ent nagu me eelmises artiklis arutasime, on ristimine samas ka väga tõsine samm.
See sümboliseerib Jehoovale pühendumist, talle antud pühalikku tõotust teenida teda igavesti ja seada tema tahe kõigest muust oma elus ettepoole.
Loomulikult tuleks sul minna ristimisele vaid siis, kui oled sellise otsuse langetamiseks piisavalt küps, kui see on sinu enda soov ja kui mõistad, mida pühendumine tähendab.
2 Mida teha siis, kui sa pole kindel, kas oled ristimiseks valmis?
Või kui soovid lasta end ristida, kuid su vanemad leiavad, et peaksid veel ootama ja saama kogenumaks? Ära kaota kummalgi juhul lootust.
Püüa hoopis võtta seda kui võimalust vaimselt edeneda, et oleksid peagi valmis laskma end ristida.
Seda eesmärgiks seades mõtle, kuidas saaksid 1) tugevdada oma veendumusi, 2) näidata oma usku tegudega ja 3) kasvatada oma tänulikkust.



SINU VEENDUMUSED

3., 4. Mida saavad noored õppida Timoteose eeskujust?
3 Mõtle, kuidas sa vastaksid järgmistele küsimustele.
Miks ma usun, et Jumal on olemas?
Mis veenab mind, et Piibel on Jumala sõna?
Miks ma arvan, et Jumala moraalipõhimõtete järgimine on parem kui ilmalik eluviis?
Need küsimused pole mõeldud selleks, et sinus kahtlusi tekitada.
Pigem aitavad need sul võtta kuulda apostel Pauluse üleskutset „teha endale selgeks, mis on Jumala hea, meeldiv ja täiuslik tahe”. (Rooml. 12:2.)
Miks tuleks sul seda teha?
4 Vaadelgem üht näidet Piiblist.
Timoteos tundis hästi pühakirja.
Tema ema ja vanaema olid talle seda juba „väiksest peale” õpetanud.
Siiski õhutas Paulus Timoteost: „Püsi selles, mida oled õppinud ja milles sul aidati veendumusele jõuda.” (2. Tim. 3:14, 15.)
Kui inimene on milleski veendumusele jõudnud, tähendab see, et ta on selles täiesti kindel.
Timoteos oli veendunud, et see, mida ta usub, on tõde.
Tema veendumus ei põhinenud mitte üksnes tema ema ja vanaema õpetustel, vaid ka sellel, et ta ise oli mõtisklenud pühakirja tõdede üle. (Loe Roomlastele 12:1.)
5., 6. Miks on tähtis, et õpiksid kasutama oma mõtlemisvõimet juba noorena?
5 Kuidas on lood sinuga?
Võib-olla tead Piibli tõdesid juba pikka aega.
Kui nii, siis miks mitte seada endale sihiks uurida lähemalt, miks sa midagi usud.
See tugevdab sinu veendumust ja aitab sul mitte lasta end juhtida eakaaslastel, maailma propagandal või isegi omaenda tunnetel.
6 Kui õpid juba noorena kasutama oma mõtlemisvõimet, aitab see sul vastata oma eakaaslastele, kui nad esitavad näiteks selliseid küsimusi: „Miks sa oled nii kindel, et Jumal on olemas?
Miks Jumal lubab kurjust?
Kuidas on võimalik, et Jumalal pole algust?” Kui oled nendeks küsimusteks valmistunud, siis ei kõiguta need sinu usku, vaid ergutavad sind tegema lisauurimistööd.
7.–9. Kuidas võib aidata veebisari „Mida Piibel meile tegelikult õpetab?” sul oma veendumusi tugevdada?
7 Hoolas iseseisev piibliuurimine aitab sul leida küsimustele vastuseid, hajutada kahtlusi ja tugevdada oma veendumusi. (Ap. t. 17:11.)
Meil on selleks rohkesti abimaterjale.
Paljud on leidnud, et on kasulik uurida brošüüri „Elu päritolu.
Viis vältimatut küsimust” ja raamatut „Kas on olemas Looja, kes meist hoolib?”.
Lisaks on paljudele noortele meeldinud uurida meie veebisaidilt töölehti „Mida Piibel meile tegelikult õpetab?”.
Selle sarja töölehed leiab saidilt jw.org (PIIBLI ÕPETUSED >
NOORTELE).
Iga tööleht selles sarjas on mõeldud selleks, et saaksid tugevdada oma veendumust mõne piibliõpetuse osas.
8 Kuna sa tunned Piiblit juba päris hästi, oskad sa ilmselt mõningatele töölehtedel olevatele küsimustele kiiresti vastata.
Ent miks oled oma vastustes kindel? Töölehed kutsuvad sind üles arutlema pühakirjatekstide üle ja seejärel panema kirja oma mõtted nende kohta.
Need aitavad sul mõelda, kuidas oma uskumusi teistele selgitada.
Veebisari „Mida Piibel meile tegelikult õpetab?” on aidanud paljudel noortel oma veendumusi tugevdada.
Kui sul on võimalik neid töölehti kasutada, siis miks mitte lisada need oma iseseisvasse uurimiskavva?
9 Oma veendumusi tugevdades astud sa olulise sammu ristimise suunas. Üks teismeline õde ütles: „Enne kui otsustasin lasta end ristida, uurisin Piiblit ja mõistsin, et tegemist on õige religiooniga.
Iga päevaga mu veendumus üha tugevneb.”



SINU TEOD

10. Miks on mõistlik oodata, et ristitud kristlane tegutseks kooskõlas oma usuga?
10 Piibel ütleb: „Usk ilma tegudeta [on] surnud.” (Jaak. 2:17.)
Kui sa oled oma veendumustes kindel, siis on loogiline, et see ilmneb ka sinu tegudes.
Millistes tegudes?
Piiblis on kirjas, et me peaksime „käituma pühalt ja teenima Jumalat pühendumusega”. (Loe 2. Peetruse 3:11.)
11. Selgita, mida hõlmab püha käitumine.
11 Selleks et käituda pühalt, pead sa olema kõlbeliselt puhas.
Mõtle näiteks viimase poole aasta peale.
Kuidas on ilmnenud, et sinu eristusvõime on treenitud „vahet tegema õige ja vale vahel”? (Heebr. 5:14.)
Kas mäletad mõnd konkreetset olukorda, kui seisid vastu kiusatusele või eakaaslaste survele?
Kas sinu käitumine koolis annab head tunnistust sinu usust?
Kas oled valmis oma usku kaitsma ega püüa sulanduda klassikaaslastega vaid selleks, et vältida nende pilget? (1. Peetr. 4:3, 4.)
Muidugi pole keegi täiuslik.
Isegi kauaaegsed Jehoova teenijad võivad mõnikord tunda hirmu, kui neil tuleb avalikult oma usku kaitsta.
Ent kui inimene on pühendunud Jehoovale, võib ta olla uhke selle üle, et kannab Jumala nime, ning see avaldub ka tema käitumises.
12. Mida tähendab teenida Jumalat pühendumusega ja kuidas sa peaksid sellesse suhtuma?
12 Mida tähendab aga teenida Jumalat pühendumusega?
See hõlmab kogudusega seotud tegevusi, nagu koosolekutel käimist ja kuulutustööl osalemist, lisaks ka selliseid vaimseid tegevusi, mida teised ei näe, nagu isiklikke palveid ja iseseisvat uurimist.
Inimene, kes on pühendunud Jehoovale, ei pea selliseid tegevusi koormavaks.
Ta hoopis peegeldab kuningas Taaveti suhtumist, kes ütles: „Sinu tahet, mu Jumal, täidan ma hea meelega, su seadus on sügaval mu sees.” (Laul 40:8.)
13., 14. Mis abimaterjal võib aidata sul teenida Jumalat pühendumusega ja kuidas on see mõningaid noori aidanud?
13 Raamatu „Noored küsivad.
Praktilisi vastuseid” 2. köites on lehekülgedel 308 ja 309 tööleht, mis aitab sul endale sihte seada.
See tööleht õhutab sind panema kirja oma vastused sellistele küsimustele nagu „Kui üksikasjalikud on su palved ja mida need räägivad sinu armastusest Jehoova vastu?”, „Mida sa uurid?”, „Kas sa käid kuulutamas, isegi kui su vanemad seda ei tee?”.
Töölehel on jäetud ruumi ka selleks, et saaksid kirja panna enda eesmärgid seoses oma palvete, iseseisva uurimise ja kuulutustööga.
14 Paljud noored, kes soovivad ristimisele minna, on leidnud, et see tööleht on neile suureks abiks.
Noor õde Tilda sõnab: „Ma kasutasin seda töölehte, et endale eesmärke seada.
Saavutasin need eesmärgid järk-järgult ja umbes aasta hiljem olin valmis ristimiseks.” Noor vend Patrick ütleb: „Ma juba teadsin, mis on mu eesmärgid, kuid nende kirjapanemine ajendas mind nende saavutamiseks kõvemini tööd tegema.”





Kas jätkaksid Jehoova teenimist ka siis, kui sinu vanemad seda enam ei tee? (Vaata lõiku 15)




15. Selgita, miks on pühendumine isiklik otsus.
15 Üks mõtlemapanevamaid küsimusi sellel töölehel on „Kas teeniksid Jehoovat isegi siis, kui su vanemad ja sõbrad lakkaksid seda tegemast?”.
Pea meeles, et pühendunud ja ristitud kristlasena oled sa Jehoova ees ise vastutav.
Tema teenimine ei peaks sõltuma teistest, isegi mitte sinu vanematest.
Sinu püha käitumine ja Jumala teenimine pühendumusega annavad tunnistust, et oled veendunud Piibli õpetuste tõesuses ja arened ristimise suunas.



SINU TÄNULIKKUS

16., 17. a) Mis peaks ajendama inimest kristlaseks saama? b) Milline näide aitab meil mõista lunastusohvri väärtust?
16 Üks mees, kes tundis hästi Moosese seadust, küsis Jeesuselt: „Milline käsk Moosese seaduses on suurim?” Jeesus vastas: „Armasta Jehoovat, oma Jumalat, kogu südamest ja hingest ja kogu oma mõistusega.” (Matt. 22:35–37.)
Jeesus tõi siin välja, et kristlase tegude, sealhulgas ristimise ajendiks peaks olema südamest tulev armastus Jehoova vastu. Üks parimaid viise kasvatada oma armastust Jehoova vastu on see, kui mõtiskled Jehoova suurima anni üle, milleks on tema poja lunastusohver. (Loe 2. Korintlastele 5:14, 15; 1. Johannese 4:9, 19.) Kui mõtiskled lunastuse ja selle üle, mida see sulle isiklikult tähendab, ajendab see sind väljendama oma tänu.
17 Toome näite.
Kujutle, et keegi päästis sind uppumisest.
Kas läheksid pärast seda lihtsalt koju, kuivataksid end ära ja unustaksid kõik, mis sinu heaks tehti?
Kindlasti mitte!
Sa tunneksid end oma päästja ees võlglasena.
Sa võlgneksid talle ju oma elu!
Meie võlgneme Jumal Jehoovale ja Jeesus Kristusele tunduvalt rohkem.
Ilma lunastuseta oleksime kõik otsekui vajunud patu ja surma mülkasse.
Ent tänu sellele suurele armastuseteole on meil nüüd võrratu lootus elada igavesti paradiisis maa peal!
18., 19. a) Miks sa ei peaks kartma Jehoovale pühenduda? b) Mis mõttes teeb Jehoova teenimine sinu elu paremaks?
18 Kas oled Jehoovale tänulik selle eest, mis ta sinu heaks on teinud?
Sellisel juhul on pühendumine ja ristimine täiesti õige samm.
Pea meeles, et pühendudes annad sa Jehoovale pühaliku tõotuse täita tema tahet igavesti, mis ka ei juhtuks.
Kas sa peaksid kartma võtta enda peale sellist kohustust?
Mitte sugugi! Ära unusta, et Jehoova soovib sulle kogu südamest parimat ja ta „annab tasu neile, kes teda tõsimeeli otsivad”. (Heebr. 11:6.)
Jehoovale pühendumine ja enda ristida laskmine ei tee sinu elu halvemaks.
Vastupidi,
Jehoova teenimine teeb sinu elu paremaks. Üks 24-aastane vend, kes ristiti päris noorena, ütleb: „Ma oleksin mõistnud asju sügavamalt, kui oleksin olnud vanem, kuid otsus Jehoovale pühenduda kaitses mind maailma ahvatluste eest.”
19 Kuivõrd erineb küll Jehoova Saatanast, kes huvitub sinust vaid isekatel eesmärkidel!
Saatanal pole pakkuda mingit püsivat tasu neile, kes on tema poolel.
Ja kuidas ta saakski seda teha?
Tal endal ei ole mõelda millelegi heale ja talle ei paista ühtegi lootusekiirt.
Kuidas ta saaks anda sulle midagi sellist, mida tal endal pole?
Saatan saab pakkuda sulle vaid sama sünget tulevikku, mis ootab teda ennast. (Ilm. 20:10.)
20. Mida saab noor teha, et jõuda pühendumise ja ristimiseni? (Vaata ka kasti „Kasvata oma usku”.)
20 On selge, et Jehoovale pühendumine on õige otsus.
Kas oled valmis seda sammu astuma?
Kui oled, siis ära hoia end tagasi.
Kui sa aga vajad rohkem aega, siis kasuta selles artiklis toodud soovitusi, mis aitavad sul edeneda.
Paulus kirjutas filiplastele: „Sealt, kuhu oleme edusamme tehes jõudnud, mingem samamoodi edasi.” (Filipl. 3:16.)
Kui järgid seda nõuannet, soovid sa ilmselt peagi Jehoovale pühenduda ja lasta end ristida.



KASVATA OMA USKU
... Ärgake! rubriigi „Noored küsivad” abil
  „Kuidas oma palveid paremaks muuta?” (november 2008)

  „Kuidas teha Piibli lugemine endale meeldivaks?” (aprill 2009)

  „Kes ma olen?” (oktoober 2011)

  „Kuidas Piibli uurimine endale meeldivaks teha?” (aprill 2012)

  „Miks käia koguduse koosolekutel?” („Why Go to Christian Meetings?”) (aprill 2012, inglise keeles)


... raamatu „Noored küsivad.
Praktilisi vastuseid” abil
1. KÖIDE
  „Mis aitab Jumala teenimisest rõõmu tunda?” (ptk 38)

  „Miks ma kardan koolis oma usust rääkida?” (ptk 17)


2. KÖIDE
  „Miks elada Piibli põhimõtete järgi?” (ptk 34)

  „Kuidas saada Jumalaga sõbraks?” (ptk 35)

  „Kas peaksin end ristida laskma?” (ptk 37)







    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Eestikeelsed väljaanded (2000-2017)
			
		

		
			
				
				Eelistused
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Kasutustingimused
			
			
				Privaatsus
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



