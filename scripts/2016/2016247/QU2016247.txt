Chantapis, kunanqa ashkha ñaupa qhelqasqas tiyan, chaykunawantaj Bibliamanta yachaj runasqa yanapachikunku maypichus pantasqa kashasqanta reparanankupaj.
May chhika watasta religionta kamachejkunaqa, yachacherqanku latín parlaypi qhelqasqasninku cheqa kajta nisqanta.
Chayrayku 1 Juan 5:7 versiculoman yapaykusqanku, mayqenmantachus kay yachaqanapi parlarqanchejña.
Kay Reina-Valera nisqa Bibliapipis chay pantasqa palabrasta churaykullasqankutaj.
Chaywanpis waj ñaupa qhelqasqasta tarisqankupi, ¿imatá rikucherqa?
Bibliamanta yachaj runa Bruce Metzger qhelqarqa: “[1 Juan 5:7 versiculopi] yapasqa palabrasqa mana ñaupa qhelqasqaspi rikhurinchu, (siríaco, copto, armenio, etiópico, árabe, eslavo nisqa parlaykunapi), manaqa latín parlayllapi”, nispa.
Chayrayku Reina-Valera nisqa allinchasqa Bibliasmanta, waj Bibliasmanta ima chay palabrasta orqhorparqanku.





Papiro Chester Beatty P46, kay qhelqasqaqa 200 watamanta jina.




Ñaupa qhelqasqasqa rikuchin Bibliaj nisqanqa kikillanpuni kashasqanta.
Bibliamanta yachayniyoj runasqa, 1947 watapi Wañusqa Qochapi qhelqasqasta tarikojtin, hebreopi masoretas qhelqasqaswan kikinchayta aterqanku,
Wañusqa Qochapi qhelqasqas waranqa watasniyojña kajtinpis.
Wañusqa Qochapi Qhelqasqasta estudiajkunamanta ujnin nerqa uj rollolla, “rikuchisqanta judío qhelqeris Bibliata ashkha kutista, waranqa watas kurajpi copiasqankoqa maychus kajpuni, sumajtaj kasqanta”.
Biblioteca Chester Beatty de Dublín (Irlanda) nisqapi tiyan Griegopi Qhelqasqasmanta casi tukuynin librosmanta ashkha papiros, wakin qhelqasqasqa 101-200 watas chaynejmanta,
Bibliata qhelqayta tukuchakusqanmanta 100 watasninman jina.
Uj libropi nin jina, “papiro qhelqasqaspi Bibliamanta astawan sutʼinchajtinpis, watas pasasqanman jina chaypi nisqanqa maychus kajllapuni kasqanta rikuchin”.
“Mana iskayrayaspa nisunman ni mayqen ñaupa libropis,
Biblia jina maychus kaj qhelqasqa kasqanta”

¿IMAPÍ TUKORQA? Ashkha ñaupa qhelqasqas kasqan, unayniyojña kasqantaj Bibliaj nisqanta qʼewinanmantaqa aswan valorniyoj kananpaj yanaparqa.
Sir Frederic Kenyon sutiyoj runa qhelqarqa: “Ni mayqen ñaupa libromanta ajinata nikunchu, ashkhatataj, tukuy imata estudiajkunapis ninkuman kay tiempopi Bibliapi nisqanqa atienekunapaj jina kasqanta”, nispa.
Chantapis Hebreopi Qhelqasqasmanta parlaspa William Henry Green nerqa: “Mana iskayrayaspa nisunman ni mayqen ñaupa libropis,
Biblia jina maychus kaj qhelqasqa kasqanta”, nispa.



Astawan yachakuyta munaspaqa “Bibliata estudianapaj yanapa” folletoj 1-13 paginasninta leey, kaytaqa Internetpi, jw.org/qu nisqapi tarinki.




    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Quechuapi publicaciones (2004-2017)
			
		

		
			
				
				Allinchanapaj
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Kay kamachiykunata kasukuy
			
			
				Datosta apaykachakusqanmanta
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



