FIE că sunt de acord cu noi, fie că ni se împotrivesc cu înverșunare, puțini oameni ar nega faptul că Martorii lui Iehova, ca grup, sunt cunoscuți pentru lucrarea de predicare pe care o desfășoară.
Poate chiar ai întâlnit oameni care au spus că, deși nu sunt de acord cu convingerile noastre, ne respectă pentru lucrarea pe care o facem.
După cum știm,
Isus a prezis că vestea bună despre Regat avea să fie predicată pe tot pământul locuit (Mat. 24:14).
Dar cum putem fi siguri că lucrarea noastră constituie împlinirea profeției lui Isus?
Este oare o dovadă de îngâmfare să credem că noi suntem cei care fac lucrarea prezisă de Isus?
2 Multe grupări religioase consideră că predică Evanghelia, sau vestea bună. Însă eforturile lor se limitează în general la predici ținute la biserică, la programe transmise la televizor ori pe internet sau la mărturii personale privind modul în care au aflat despre Isus.
Alții consideră că acțiunile lor caritabile sau eforturile pe care le fac în domeniul medicinei sau al educației reprezintă modul lor de a predica.
Dar sunt aceste activități o împlinire a poruncii date de Isus discipolilor săi?
 3. Potrivit cu Matei 28:19, 20, care sunt patru lucruri pe care trebuie să le facă discipolii lui Isus?
3 Trebuia oare ca discipolii lui Isus să fie pasivi, așteptând ca oamenii să vină la ei?
Nicidecum!
După învierea sa,
Isus le-a spus la sute dintre discipolii săi: „Duceți-vă deci și faceți discipoli din oamenii tuturor națiunilor, botezându-i . . . , învățându-i să respecte tot ce v-am poruncit” (Mat. 28:19, 20).
Astfel,
Isus a arătat că trebuie să facem patru lucruri.
Trebuie să facem discipoli, să-i botezăm și să-i învățăm.
Dar care este primul lucru pe care trebuie să-l facem?
Isus a spus: „Duceți-vă”!
Cu privire la această poruncă, un biblist a explicat: „Fiecare credincios are sarcina de «a se duce», indiferent că trebuie să meargă peste drum sau peste ocean” (Mat. 10:7; Luca 10:3).
 4. Ce presupune faptul de a deveni „pescari de oameni”?
4 Se referea Isus doar la eforturile individuale ale continuatorilor săi sau avea în vedere o campanie organizată de predicare a veștii bune? Întrucât o singură persoană nu s-ar fi putut duce la ‘toate națiunile’, această lucrare presupunea ca discipolii lui să acționeze în mod organizat.
Isus a arătat acest lucru când i-a invitat pe discipoli să devină „pescari de oameni”. (Citește Matei 4:18-22.) Isus nu s-a referit aici la un singur pescar care folosește o undiță și o momeală, stând relaxat și așteptând ca peștele să muște momeala.
Mai degrabă,
Isus s-a referit la folosirea unor plase de pescuit.
Acest tip de pescuit presupunea muncă grea, organizare și eforturile conjugate ale multora (Luca 5:1-11).
 5. La ce întrebări trebuie să răspundem și de ce?
5 Pentru a ști cine predică în prezent vestea bună ca împlinire a profeției lui Isus, trebuie să răspundem la următoarele patru întrebări:
  Ce mesaj ar trebui să predice continuatorii lui Isus?

  Cu ce motivație ar trebui să fie îndeplinită această lucrare?

  Ce metode ar trebui folosite?

  Care ar trebui să fie amploarea și durata lucrării de predicare?


Răspunsurile la aceste întrebări ne vor ajuta nu numai să vedem cine face această lucrare salvatoare, ci și să ne întărim hotărârea de a continua să predicăm (1 Tim. 4:16).



CARE AR TREBUI SĂ FIE MESAJUL?

 6. De ce poți fi sigur că Martorii lui Iehova predică mesajul care trebuie?
6 Citește Luca 4:43. Isus a predicat ‘vestea bună despre Regat’ și se așteaptă ca discipolii lui să facă la fel.
Ce grup de oameni predică acest mesaj la ‘toate națiunile’?
Răspunsul este evident: numai Martorii lui Iehova.
Chiar și unii împotrivitori recunosc acest lucru.
De exemplu, un preot misionar i-a spus cândva unui Martor că a locuit în mai multe țări și că i-a întrebat pe Martorii din fiecare țară ce mesaj predicau.
Ce i-au răspuns ei?
Preotul a spus: „Au fost toți atât de proști, încât mi-au dat același răspuns: «Vestea bună despre Regat»”. Însă, departe de a fi „proști”, acei Martori vorbeau în unitate, așa cum ar trebui să facă adevărații creștini (1 Cor. 1:10).
Regatul lui Dumnezeu e și mesajul principal al revistei Turnul de veghe anunță Regatul lui Iehova. Această revistă este disponibilă în 254 de limbi și, în medie, fiecare număr este tipărit în aproape 59 000 000 de exemplare.
Astfel, Turnul de veghe este revista cu cel mai mare tiraj din lume.
 7. De unde știm că clericii creștinătății nu predică mesajul care trebuie?
7 Clericii creștinătății nu predică Regatul lui Dumnezeu.
Când vorbesc totuși despre Regat, mulți dintre ei spun că este un sentiment sau o stare a inimii (Luca 17:21).
Ei nu-i ajută pe oameni să înțeleagă că Regatul lui Dumnezeu este un guvern ceresc, condus de Isus Cristos, că este soluția la toate problemele omenirii și că va înlătura în curând toată răutatea de pe pământ (Rev. 19:11-21).
De obicei, ei își amintesc de Isus la Paște și la Crăciun.
Se pare că ei nu știu nimic despre ce va face Isus în calitate de Conducător al pământului. Și, întrucât nu înțeleg mesajul pe care ar trebui să-l predice, nu este de mirare că nu înțeleg nici de ce ar trebui să facă această lucrare.



CU CE MOTIVAȚIE AR TREBUI SĂ FIE ÎNDEPLINITĂ LUCRAREA?

 8. Care nu ar trebui să fie motivația pentru a îndeplini lucrarea de predicare?
8 Motivația pentru a îndeplini lucrarea de predicare nu ar trebui să fie dorința de a strânge bani și de a construi clădiri somptuoase.
Isus le-a spus discipolilor săi: „Fără plată ați primit, fără plată să dați” (Mat. 10:8).
Cuvântul lui Dumnezeu nu trebuie să fie comercializat (2 Cor. 2:17).
Cei care predică mesajul nu trebuie să ceară să fie plătiți pentru lucrarea pe care o fac. (Citește Faptele 20:33-35.) În pofida acestei îndrumări clare, majoritatea bisericilor au uitat care le este misiunea și caută doar să strângă bani sau să supraviețuiască pe plan financiar.
Ele trebuie să-și plătească clericii, precum și o mulțime de alți angajați. În multe cazuri, conducătorii creștinătății au acumulat mari bogății (Rev. 17:4, 5).
 9. Cum arată Martorii lui Iehova că participă la lucrarea de predicare cu motivația corectă?
9 Ce se poate spune despre Martorii lui Iehova în această privință?
Ei nu fac colectă la sala Regatului sau la congrese.
Lucrarea lor este susținută prin donații (2 Cor. 9:7).
Cu toate acestea, doar anul trecut,
Martorii lui Iehova au petrecut aproape două miliarde de ore în lucrarea de predicare a veștii bune și au condus în mod gratuit peste nouă milioane de studii biblice în fiecare lună. În mod surprinzător, pe lângă faptul că nu sunt plătiți pentru lucrarea lor, ei sunt dispuși să-și acopere singuri cheltuielile.
Referindu-se la lucrarea Martorilor lui Iehova, un cercetător a spus: „Principalul lor obiectiv este predicarea și predarea . . .
Ei nu au cler, fapt ce reduce considerabil cheltuielile”.
Atunci, cu ce motivație facem noi această lucrare?
Simplu spus, noi facem această lucrare de bunăvoie pentru că îl iubim pe Iehova și ne iubim semenii.
Acest spirit de dăruire constituie o împlinire a profeției din Psalmul 110:3. (Citește.)



CE METODE AR TREBUI FOLOSITE?






Noi le predicăm oamenilor oriunde îi putem găsi (Vezi paragraful 10)




10. Ce metode au folosit Isus și discipolii săi pentru a predica?
10 Ce metode au folosit Isus și discipolii săi pentru a predica vestea bună?
Ei mergeau la oameni oriunde îi puteau găsi, atât în locuri publice, cât și la locuințele lor.
Lucrarea de predicare presupunea și căutarea celor merituoși din casă în casă (Mat. 10:11; Luca 8:1; Fap. 5:42; 20:20).
Această metodă sistematică de predicare a reflectat imparțialitate.
11, 12. Ce fac bisericile creștinătății în comparație cu slujitorii lui Iehova în ce privește predicarea veștii bune?
11 Ce fac bisericile creștinătății în această privință? În general, membrii bisericilor lasă predicarea în seama clericilor plătiți.
Dar, în loc să fie „pescari de oameni”, clericii creștinătății par să fie mai preocupați de faptul de a nu pierde „peștii” pe care îi au deja.
E adevărat, anumiți clerici încearcă uneori să-i îndemne pe enoriașii lor să predice.
De exemplu, la începutul anului 2001, papa Ioan Paul al II-lea a spus într-o scrisoare: „De nenumărate ori am repetat în acești ultimi ani apelul la o nouă evanghelizare. Îl reiau acum . . .
Trebuie să retrăim în noi sentimentul înflăcărat al lui Paul, care exclama: «Vai mie dacă nu vestesc evanghelia!»”. În continuare, papa a spus că această misiune nu poate fi rezervată „unui grup de «specialiști», ci va trebui să implice responsabilitatea tuturor membrilor poporului lui Dumnezeu”. Însă câți au răspuns la acest apel?
12 Dar ce se poate spune despre Martorii lui Iehova?
Ei sunt singurii care le predică oamenilor că Isus domnește ca Rege din 1914.
Așa cum a poruncit Isus, ei acordă prioritate lucrării de predicare (Mar. 13:10).
O scriitoare a spus: „Pentru Martorii lui Iehova, activitatea misionară este mai importantă decât orice altă preocupare”.
Făcând referire la cuvintele unui Martor, ea a spus în continuare: „Când întâlnesc oameni flămânzi, singuri și bolnavi, ei încearcă să-i ajute, . . . dar nu uită niciodată că principala lor misiune este aceea de a transmite un mesaj de natură spirituală despre sfârșitul iminent al lumii și despre nevoia de salvare” (Pillars of Faith–American Congregations and Their Partners).
Martorii lui Iehova continuă să predice acest mesaj, folosind metodele pe care le-au folosit Isus și discipolii săi.



CARE AR TREBUI SĂ FIE AMPLOAREA ȘI DURATA LUCRĂRII DE PREDICARE?

13. Care ar trebui să fie amploarea lucrării de predicare?
13 Isus a vorbit despre amploarea acestei lucrări când a spus că vestea bună avea să fie predicată „pe tot pământul locuit” (Mat. 24:14).
Continuatorii lui Isus trebuie să facă discipoli „din oamenii tuturor națiunilor” (Mat. 28:19, 20).
Aceasta presupune o lucrare la scară mondială.
14, 15. Ce anume arată că Martorii lui Iehova împlinesc profeția lui Isus referitoare la amploarea lucrării de predicare? (Vezi imaginile de la începutul articolului.)
14 Pentru a înțelege cum împlinesc Martorii lui Iehova profeția lui Isus referitoare la amploarea lucrării de predicare, să analizăm câteva date. În Statele Unite există aproximativ 600 000 de membri ai clerului în diferite confesiuni, în timp ce numărul Martorilor lui Iehova este de aproximativ 1 200 000.
La nivel mondial există în jur de 400 000 de preoți catolici.
Să ne gândim acum la numărul Martorilor lui Iehova care participă la lucrarea de predicare a Regatului.
La nivel mondial, aproximativ opt milioane de Martori predică de bunăvoie în 240 de țări și teritorii.
Ce lucrare impresionantă se face pe tot pământul spre lauda și gloria lui Iehova! (Ps. 34:1; 51:15)
15 Ca Martori ai lui Iehova, noi dorim să ajungem la cât mai mulți oameni cu vestea bună înainte să vină sfârșitul.
Din acest motiv, noi ne deosebim de celelalte religii prin eforturile pe care le facem pentru a traduce și a produce publicații biblice.
Distribuim gratuit milioane de cărți, reviste, pliante și invitații la Comemorare și la congrese în peste 700 de limbi.
Am tipărit peste 200 de milioane de exemplare ale Sfintelor Scripturi – Traducerea lumii noi în mai bine de 130 de limbi.
Doar anul trecut am tipărit aproximativ 4,5 miliarde de publicații biblice.
Site-ul nostru oficial conține informații în peste 750 de limbi.
Ce altă grupare religioasă face o lucrare asemănătoare?
16. De unde știm că Martorii lui Iehova au spiritul lui Dumnezeu?
16 Cât timp avea să continue lucrarea de predicare?
Isus a prezis că această lucrare mondială va continua până în zilele din urmă, iar „atunci va veni sfârșitul”.
Ce altă grupare religioasă a continuat să predice vestea bună în aceste zile din urmă decisive?
Unii oameni pe care îi întâlnim în lucrarea de predicare ne spun: „Noi avem spiritul sfânt, dar voi faceți lucrarea”. Însă nu este faptul că perseverăm în lucrare o dovadă că avem spiritul lui Dumnezeu? (Fap. 1:8; 1 Pet. 4:14)
Din când în când, unele grupări religioase încearcă să facă lucrarea pe care Martorii lui Iehova o fac cu regularitate, dar, de cele mai multe ori, eforturile lor eșuează.
Alții participă la unele activități așa-zis misionare pentru o perioadă scurtă, după care se întorc la viața lor obișnuită.
Iar alții chiar încearcă să meargă din casă în casă.
Dar ce mesaj predică ei?
Răspunsul la această întrebare arată clar că ei nu îndeplinesc lucrarea pe care a început-o Cristos.



CINE PREDICĂ ÎN REALITATE VESTEA BUNĂ ÎN PREZENT?

17, 18. a) De ce putem fi siguri că Martorii lui Iehova sunt cei care predică în prezent vestea bună despre Regat? b) Cum reușim să continuăm să facem această lucrare?
17 Așadar, cine predică în prezent vestea bună despre Regat?
Putem afirma cu toată încrederea:
Martorii lui Iehova!
De ce putem fi atât de siguri?
Deoarece noi predicăm mesajul corect: vestea bună despre Regat. Și, întrucât îi căutăm pe oameni, noi folosim metodele corecte. De asemenea, noi predicăm cu motivația corectă: iubirea, nu câștigul financiar.
Lucrarea noastră are cea mai mare amploare, întrucât ajungem la oameni din toate națiunile și limbile. Și vom continua să facem această lucrare fără încetare, an după an, până va veni sfârșitul.
18 Suntem cu adevărat impresionați de ceea ce realizează poporul lui Dumnezeu în aceste zile din urmă.
Dar cum reușim să predicăm vestea bună pe tot pământul?
Apostolul Pavel dă răspunsul în scrisoarea sa către filipeni: „Dumnezeu este cel care, după buna sa plăcere, lucrează în voi ca să vreți și, totodată, să înfăptuiți” (Filip. 2:13).
Fie ca Tatăl nostru iubitor să continue să lucreze în noi și să ne întărească în timp ce facem tot ce putem pentru a ne înfăptui pe deplin serviciul! (2 Tim. 4:5)






    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Publicații în limba română (2000-2017)
			
		

		
			
				
				Preferințe
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Termeni de utilizare
			
			
				Politică de confidențialitate
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



