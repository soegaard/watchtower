Într-un dicționar biblic se spune: „Conform legii romane, semănarea neghinei pe câmpul altcuiva din răzbunare . . . era considerată infracțiune.
Existența unei astfel de legi sugerează că acest gen de situații erau frecvente”. În 533 e.n., la ordinul împăratului roman Iustinian, a fost realizată lucrarea Digeste, o culegere de hotărâri, decrete și fragmente din scrierile juriștilor romani care au trăit în perioada clasică (circa 100-250 e.n.). În această culegere (Digeste, 9.2.27.14), jurisconsultul Domitius Ulpianus face referire la un caz judecat de Publius Iuventius Celsus, jurist și politician roman din secolul al II-lea e.n.
Cineva semănase neghină pe câmpul altei persoane, compromițându-i recolta.
Lucrarea Digeste menționează mijloacele legale de reparare a prejudiciului la care putea recurge proprietarul sau arendașul pentru a obține o despăgubire de la făptaș.
Existența unor astfel de acțiuni răuvoitoare pe teritoriul Imperiului Roman arată că situația descrisă de Isus se baza, fără îndoială, pe fapte reale.



Câtă libertate acorda Roma autorităților evreiești din Iudeea în secolul I?
ÎN SECOLUL I,
Iudeea se afla sub stăpânirea Imperiului Roman, reprezentat în provincie de un guvernator care avea în subordine trupe de soldați.
Principala atribuție a guvernatorului era strângerea impozitelor pentru Roma, precum și păstrarea ordinii și a liniștii.
De asemenea, romanii reprimau orice acțiune interzisă de lege și îi pedepseau pe cei ce tulburau ordinea publică.
Sarcinile de ordin administrativ însă intrau în atribuția conducătorilor evrei.





Sanhedrinul întrunit în ședință




Instanța supremă a evreilor și autoritatea în materie de legislație iudaică era Sanhedrinul.
Pe tot teritoriul Iudeii existau instanțe inferioare.
Din câte se pare, majoritatea proceselor penale și civile erau judecate de aceste instanțe, fără intervenția autorităților romane.
Totuși, executarea infractorilor nu intra în competența instanțelor evreiești, acest drept fiind rezervat, în general, instanțelor romane.
După cum se știe, o excepție s-a făcut atunci când membrii Sanhedrinului l-au judecat pe Ștefan și au dispus uciderea lui cu pietre (Fap. 6:8-15; 7:54-60).
Prin urmare,
Sanhedrinul avea o mare putere de decizie.
Totuși, potrivit eruditului Emil Schürer, „această putere era uneori în mod considerabil limitată deoarece autoritățile romane puteau oricând să intervină și să acționeze independent, de exemplu când suspectau o infracțiune de natură politică”.
O situație de acest gen a avut loc când comandantul militar Claudiu Lisias a intervenit pentru a-l proteja pe apostolul Pavel, cetățean roman (Fap. 23:26-30).



    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Publicații în limba română (2000-2017)
			
		

		
			
				
				Preferințe
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Termeni de utilizare
			
			
				Politică de confidențialitate
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



