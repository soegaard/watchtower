Felisa: Españapi wiñakorqani.
Familiayqa allin catolicospuni karqanku.
Chunka kinsayojpuni familiaresniymanta curas karqanku chayri iglesiapi trabajarqanku.
Paykunamanta ujnintaj mamaypa primon karqa, payqa católica escuelapi yachachej.
Wañupojtintaj papa Juan Pablo ll santo kasqanta nerqa.
Tatayqa herrero karqa, mamaytaj campopi llankʼaj.
Familiaypeqa 8 wawas karqayku, noqataj kuraj kani, may wajchastaj karqayku.
Chunka iskayniyoj kashajtiy,
Españapi guerra civil karqa.
Chay guerra pasaytataj tatayta gobiernoj contranpi parlasqanrayku, carcelman wisqʼaykorqanku.
Mamaytaj mana uywayta atillawarqaykuchu.
Chayrayku sullkʼasniy Aracelita,
Laurita,
Ramonita ima Bilbao llajtapi kaj conventoman churaykamorqa.
Chaypitaj paykunaqa mikhunamanta mana pisinkumanchu karqa.
Araceli: Conventoman mamay churaykushawajtiyku 14 watasniyoj kasharqani,
Lauritaj 12 watasniyoj,
Ramonitaj 10 watasniyoj kasharqa.
Chaypitaj monjasqa llimphuchachiwaj kayku.
Iskay watanmantaj Zaragozapi kaj jatun conventoman apawarqayku.
Chaypeqa machitu runasta qhawaj kanku.
Chay conventopi cocinata llimphuchaspa llankʼasqaykurayku mayta saykʼukoj kayku.
Familiaykutaqa maytapuni faltacharqayku.
Felisa: Hermanasniy Zaragozapi kaj conventoman ripojtinku, mamaywan, tioywanqa, chay conventoman rinayta niwarqanku.
Tioyqa cura karqa.
Pay, mamay imataj noqapi interesakoj jovenmanta karunchayta munarqanku.
Diosta munakusqayrayku, uj tiempota conventopi kanaymanta kusisqa kasharqani.
Sapa día misamanpis rej kani.
Africapi kaj primoy jinataj misionera kayta munarqani.





Zaragozapi kaj convento (lloqʼe)
Nácar-Colunga Biblia (paña)




Monjasqa mana yanapawarqankuchu waj suyupi Diosta sirvinaypaj.
Wisqʼasqa jinataj karqani.
Chayrayku uj watata kaytawan tioyta qhawanaypaj wasiyman kutiporqani.
Wasipi ruwanasta ruwaytawantaj, sapa tarde rezaj kayku.
Chantá iglesiaman tʼikasta churay, virgen Mariata, santosta imataj kʼachanchay mayta gustawaj.
Araceli: Monja kanaypaj Zaragozapi ñaupaj votosta ruwarqani.
Monjasqa Madridman kachawarqanku,
Lauritataj Valenciaman,
Ramonitaj Zaragozapi qhepakorqa.
Madridpitaj monja kanaypaj waj votosta ruwallarqanitaj.
Madridpi kaj conventopeqa yachakojkuna, viejitos ima alojakoj kanku.
Chayrayku ashkha ruwanas karqa.
Chay conventoj hospitalninpitaj llankʼarqani.
Niraj monja kashaspa, conventopi Bibliata leespalla, estudiaspalla kanayta yuyarqani.
Jinapis mana jinachu kasqa.
Ni pi Bibliata leejchu, nitaj pipis Diosmanta chayri Jesusmanta parlajchu.
Latín parlaymanta, santosmanta,
Mariata yupaychaymanta imalla tumpata yachakorqani.
Astawantaj llankʼallaj kayku.
Chayrayku mayta llakikuyta qallarerqani.
Yuyarqanitaj familiayta yanapanaypaj llankʼanay kasqanta.
Conventopi wajkuna qolqechakunankupaj llankʼanaymanta nisqaqa.
Chayrayku madre superiorawan parlarqani, wasiyman kutipunaytataj nerqani.
Paytaj laqha cuartoman wisqʼaykuwarqa, manaña ajinata yuyanaypaj.
Chay cuartomanta kinsa kutita orqhomuwarqanku, ripuyta munallasqaytapunichus manachus yachanankupaj.
Ripuyta munallajtiypunitaj niwarqanku uj cartata qhelqanayta.
Chay cartapeqa “ripuyta munani Satanasta sirviyta munasqayrayku, manataj Diostachu” nispa qhelqanayta munarqanku.
Chaytaj mayta tʼukuchiwarqa.
Wasiyman ripuyta munaspapis, ni jaykʼaj chayta qhelqaymanchu karqa.
Chantá curaman confesakuyta munasqayta nerqani.
Curamantaj tukuy imata willarqani.
Paytaj Zaragozapi kaj conventoman kutinaypaj uj permisota qonawankupaj yanapawarqa.
Chaypi kinsa killata kasqaytawan wasiyman kutipojta saqewarqanku.
Pisi tiemponmantaj Lauri,
Ramoni ima conventomanta llojsiporqanku.



UJ LIBRO TʼAQANACHIWARQAYKU






Felisa




Felisa: Tiempo pasasqanman jina casarakorqani,
Cantabria nisqamantaj riporqani.
Sapa semana misaman rillajpuni kani.
Uj domingotaj curaqa La verdad que lleva a vida eterna nisqa librota orqhorqa.
May phiñasqataj, niwarqayku: “Kay libro tiyapusunkichej chayqa, noqaman apamuwaychej chayri qʼopaman wijchʼuychej”, nispa.
Chay libro mana kapuwarqachu, jinapis ujta munarqani.
Mana creenaypaj jinataj pisi pʼunchayninman Jehovaj testigosnin wasiyman jamorqanku, chay librotataj saqewarqanku.
Chay chʼisipacha leerqani.
Chay testigos kutimojtinkutaj Bibliamanta yachakuyta qallarerqani.





La Verdad libro




Diosta sirviytapuni munaj kani.
Chayrayku Jehovamanta cheqa kajta yachakuspa, payta munakuyta qallarerqani.
Tukuymantaj paymanta willayta munarqani.
Kay 1973 watapi bautizakorqani.
Familiaymanpis willariytapuni munarqani.
Jinapis familiayqa churanakuwarqa, astawantaj hermanay Araceli.
Araceli: Conventopi mayta ñakʼarichiwarqanku.
Chayraykutaj religionniywan mana kusisqachu kasharqani.
Jinapis domingospi misaman rillajpuni kani, sapa pʼunchaytaj rezaj kani.
Bibliaj yachachiynintataj entiendeyta munallarqanipuni,
Diosmantataj yanapata mañakorqani.
Uj día Felisa yachakushasqanmanta parlariwarqa.
Jinapis noqaqa mana pay jinachu yuyarqani.
Imapichus creesqanmanta maytapuni parlasqanraykutaj, locayapushasqanta yuyarqani.





Araceli




Aswan qhepamantaj Madridman llankʼaj rerqani, chaypitaj casarakorqani.
Watas pasasqanman jinataj repararqani iglesiaman rejkunaqa mana Jesús yachachisqanman jinachu kausakushasqankuta.
Chayrayku manaña iglesiaman rerqanichu.
Niñataj santospi, infiernopi, juchasninchejta cura perdonasqanpi ima creerqanichu.
Tukuy santosniytapis qʼopaman wijchʼuykorqani.
Jinapis mana yacharqanichu allintachus manachus ruwashasqayta.
Religionniymanta niña imatapis yachayta munarqanichu.
Diosman nej kani: “Rejsisuyta munani, ¡yanapaway!”, nispa.
Yuyarikuni Jehovaj testigosnin ashkha kutispi wasiyman jamusqankuta, jinapis mana uyarejchu kani.
Niña ni mayqen religionpi atienekorqanichu.
Lauri Franciapi tiyakusharqa,
Ramonitaj Españapi.
Kay 1980 wata chaynejpi paykunapis Jehovaj testigosninwan Bibliamanta yachakuyta qallarerqanku.
Yuyarqanitaj Felisata jina, paykunatapis chʼaukiyashasqankuta.
Aswan qhepamantaj Angelines sutiyoj vecinayta rejserqani, paywantaj amigas karqayku.
Paypis Jehovaj testigollantaj karqa.
Angelines, qosan ima, ashkha kutista tapuriwarqanku Bibliamanta yachakuytachus manachus munasqayta.
Religionmanta mana yachayta munasqayta nejtiypis, paykunaqa repararqanku Bibliamanta yachakuyta munasqayta.
Uj diataj nerqani: “Qankunawan Bibliata estudiasaj,
Bibliallaywan pero”, nispa.
Kapuwarqataj Nacar-Colunga nisqa Biblia.



BIBLIA UJCHAWAYKU

Felisa: Kay 1973 watapi bautizakushajtiy,
Cantabria llajtaj capitalnin Santanderpi Jehovaj testigosnin 70 jinalla karqanku.
Santanderpeqa ashkha llajtitas kan.
Chay lugaresmanta tukuy runasman willanaykupajtaj may karusta rej kayku.
Chayrayku llajtitamanta llajtita willanaykupaj jatuchaj autospi, juchʼuy autospi ima rej kayku.
Tukuy kay wataspeqa ashkha runasman Bibliamanta yachacherqani.
Casi tukuyninku católicos karqanku, 11 runastaj bautizakorqanku.
Paykunawan pacienciakunay karqa.
Paykunaqa, noqa jina tiempota necesitarqanku imapichus creesqanku mana cheqachu kasqanta reparanankupaj.
Yacharqanitaj Bibliaj yachachiynillan,
Diospa atiynillan ñaupa kausayninchejta saqenapaj,
Bibliata entiendenapaj ima yanapawasqanchejta (Heb. 4:12).
Qosayqa policía karqa, 1979 watapitaj bautizakorqa.
Mamaytaj wañupunan qayllata Bibliamanta yachakuyta qallarerqa.
Araceli: Jehovaj testigosninwan Bibliamanta yachakuyta qallarishaspa, mana paykunapi atienekojchu kani.
Tiemponmantaj paykunapi atienekuyta qallarerqani, imaraykuchus yachachisqankuman jina kausakusqankuta repararqani.
Jehovapi,
Bibliapi creesqay astawan sinchʼiyasqanrayku may kusisqa kasharqani.
Wakin vecinosniypis chayta reparaspa niwarqanku: “Araceli, ni jaykʼaj chayta saqeychu”, nispa.
Uj día Jehovaman uj mañakuypi nerqani: “Mayta agradecekuyki Jehová, mana wiñaypaj chinkasqa ovejitata jina qhawawasqaykimanta,
Bibliamanta cheqa kajta yachakunaypaj yanapawasqaykimanta ima”, nispa.
Chantapis Felisata sonqonta nanachinaypaj jina nisqaymanta perdonanawanta nerqani.
Chaymantapachataj Bibliamanta parlarispa mayta kusikuyku.
Kay 1989 watapi, 61 watasniyoj kashaspa bautizakorqani.
Felisa: Kunanqa 91 watasniyojña kani, viudataj.
Niñataj ñaupajpi jinañachu imatapis ruwani.
Jinapis sapa día Bibliata leellanipuni.
Atispataj tantakuykunaman, willayman ima rinipuni.
Araceli: Curasman, monjasman Diosmanta willay mayta gustawan, ichapis ñaupajpi monja kasqayrayku.
Paykunamanta wakinwan sumajta parlarini.
Ashkha librosta, revistasta ima saqeni.
Yuyarikuni uj curawan parlarisqaymanta, pay niwarqa: “Araceli nisqaykeqa cheqapuni.
Jinapis, ¿maymantaj risaj kay edadpiri? ¿Ima nenqankutaj iglesiamanta kajkuna, familiay ima?”, nispa.
Noqataj nerqani: “¿Imatá Dios nenqa?”, nispa.
Repararqanitaj chayta nejtiy llakikusqanta.
Jinapis manchachikorqasina ñaupa kausayninta saqeyta.
Yuyarikullanipuni qosay tantakuyman riyta munani niwasqanmanta.
Payqa ñaupaj kutirayku tantakuyman rishaspa 80 watasniyoj karqa.
Mana faltakuspataj rillajpuni.
Bibliamanta yachakuyta qallarerqa, willaymanpis llojsejña.
Iskayniyku llojsisqaykumanta yuyarikullanipuni.
Bautizakunanpaj iskay killa faltashajtintaj wañuporqa.
Felisa: Mayta kusikuni kinsantin sullkʼasniy Jehovaj testigosninman tukusqankumanta.
Tawantiyku Jehovamanta,
Bibliamanta ima parlarispa mayta kusikuyku.
Kunanmá Jehovata khuska yupaychashayku.*



Araceli 87 watayoj,
Felisa 91 watasniyoj,
Ramonitaj 83 watasniyoj.
Kinsantin tukuy sonqo Jehovata sirvishallankupuni.
Lauriqa 1990 watapi Jehovaman cheqa sonqo wañuporqa.




    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Quechuapi publicaciones (2004-2017)
			
		

		
			
				
				Allinchanapaj
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Kay kamachiykunata kasukuy
			
			
				Datosta apaykachakusqanmanta
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



