KUI pagendatud juudid muistsesse Babüloni linna viidi, nägid nad, et seal on rohkelt ebajumalaid ning inimesed on deemonite orjuses.
Ent ustavad juudid, näiteks Taaniel ja tema kolm kaaslast, ei lasknud Babülonil end vormida. (Taan. 1:6, 8, 12; 3:16–18.)
Taaniel ja tema kaaslased pidasid oma pottsepaks Jehoovat ja nende jäägitu pühendumus kuulus ainult talle.
Taaniel elas Babülonis halbade mõjutuste keskel peaaegu kogu oma elu, ometi ütles Jumala ingel, et ta on „Jumalale väga kallis”. (Taan. 10:11, 19.)
2 Piibliaegadel võis pottsepp suruda savi vormi, et see võtaks soovitud kuju.
Tõelised jumalateenijad tänapäeval peavad Jehoovat kõrgeimaks valitsejaks, kellel on voli vormida terveid rahvaid. (Loe Jeremija 18:6.) Jumalal on voli vormida ka üksikisikuid.
Samas arvestab ta meie tahtevabadusega ja soovib, et alluksime talle meeleldi.
Pöörakem nüüd tähelepanu sellele, kuidas jääda otsekui pehmeks saviks Jumala käes.
Võtame vaatluse alla kolm valdkonda. 1) Kuidas hoiduda teguritest, mis teevad meie südame kõvaks, nii et me ei taha Jumala nõu kuulda võtta. 2) Kuidas arendada omadusi, mis aitavad meil jääda pehmeks ja alandlikuks. 3) Kuidas saavad lapsevanemad alluda Jumalale, kui vormivad oma lapsi.



MIS VÕIB SÜDAME KÕVAKS TEHA?

 3. Mis võib teha südame kõvaks?
Too näide.
3 „Kaitse oma südant enam kui kõike muud, sest sellest lähtub elu,” ütleb piiblikoht Õpetussõnad 4:23.
Mille eest meil tuleb seda kaitsta?
Meie südame võivad teha kõvaks näiteks kohatu uhkus, tahtlik patustamine ja usupuudus.
Need võivad soodustada sõnakuulmatut ja mässumeelset hoiakut. (Taan. 5:1, 20; Heebr. 3:13, 18, 19.) Üks näide uhkest inimesest on Juuda kuningas Ussija. (Loe 2. Ajaraamat 26:3–5, 16–21.) Algul „Ussija tegi, mis oli Jehoova silmis õige” ja ta otsis Jumalat. „Ent kui ta oli vägevaks saanud, muutus ta süda ülbeks”, ja seda hoolimata tõsiasjast, et Jumal oli see, kes oli ta vägevaks teinud.
Ussija koguni üritas templis suitsutusrohtu põletada, mida võisid teha vaid Aaroni soost preestrid.
Kui siis preestrid Ussijat korrale kutsusid, sattus uhke kuningas raevu.
Mis oli tagajärg?
Teda tabas alandav langus Jumala käe läbi ning ta suri pidalitõbisena. (Õpet. 16:18.)
4., 5. Mis võib juhtuda, kui muutume uhkeks?
Too näide.
4 Kui me ei kaitse end uhkuse eest, võime hakata endast liiga palju arvama ning koguni keelduda kuulda võtmast Piiblil põhinevaid nõuandeid. (Õpet. 29:1; Rooml. 12:3.)
Vaadakem, mis juhtus kogudusevanem Jimiga, kes polnud ühes kogudust puudutavas küsimuses teiste vanematega sama meelt.
Ta sõnab: „Ütlesin vendadele, et nad ei käitu armastavalt, ning lahkusin vanemate koosolekult.” Umbes poole aasta pärast läks ta naaberkogudusse üle, aga teda ei määratud seal kogudusevanemaks.
Jim meenutab: „Olin löödud.
Mulle tundus, et mulle on ülekohut tehtud, ning ma lõpetasin Jehoova teenimise.” Ta oli kümme aastat mittetegev koguduseliige.
Jim tunnistab: „Mu eneseuhkus oli saanud haavata ning ma süüdistasin toimunus Jehoovat.
Aastate jooksul vennad külastasid mind ja püüdsid minuga rääkida, kuid ma keeldusin nende abist.”
5 Jimi lugu näitab, et uhkus võib panna meid oma tegusid õigustama, mistõttu meil on raske lasta end vormida. (Jer. 17:9.) „Ma lihtsalt ei suutnud peast heita seda, kui valesti olid teised minu meelest käitunud,” selgitab Jim.
Kas sind on kunagi mõni kaaskristlane haavanud?
Või kas sa oled kaotanud teenistuseesõigused?
Kui nii, siis kuidas sa reageerisid?
Kas lubasid uhkusel endas pead tõsta?
Või oli sinu esmamure vennaga rahu teha ning Jehoovale ustavaks jääda? (Loe Laul 119:165; Koloslastele 3:13.)
 6. Mis võib juhtuda, kui me tahtlikult patustame?
6 Jumala nõuandeid on raske vastu võtta ka sellel, kes tahtlikult patustab ja seda võib-olla isegi varjab.
Niisugusel juhul võib patustamine talle üha lihtsamaks muutuda. Üks vend ütles, et aja möödudes teda enam ei häirinudki enda vale käitumine. (Kog. 8:11.) Üks teine vend, kes sattus pornograafia vaatamise lõksu, tunnistas hiljem: „Märkasin, et minus arenes kriitiline suhtumine kogudusevanematesse.” Selle venna suhted Jehoovaga halvenesid.
Lõpuks tuli tema pahe päevavalgele ning ta sai hädavajalikku abi.
Muidugi mõista oleme kõik ebatäiuslikud.
Aga kui oleme kriitilised või kui õigustame valet teguviisi, selle asemel et Jumalalt andestust paluda ja abi otsida, on meie süda ehk juba kõvenema hakanud.
7., 8. a)
Kuidas oli iisraellaste puhul näha, et usupuudus tegi nende südame kõvaks? b) Mida me iisraellastega juhtunust õpime?
7 Selle kohta, kuidas usupuudus teeb südame kõvaks, võib näiteks tuua iisraellased, kelle Jehoova Egiptusest vabastas.
Iisraellased nägid, kuidas Jumal tegi nende heaks palju aukartustäratavaid imetegusid.
Ent kui nad olid tõotatud maa piiril, ilmnes nende usupuudus.
Selle asemel et Jehoovale loota, lasid nad hirmul endast võitu saada ning hakkasid Moosesega nurisema.
Nad tahtsid koguni pöörduda tagasi Egiptusse, kus nad olid olnud orjad!
See tegi Jehoovale väga haiget.
Ta küsis: „Kui kaua veel on see rahvas minu vastu lugupidamatu ...?” (4. Moos. 14:1–4, 11; Laul 78:40, 41.)
Oma südame kanguse ja usupuuduse pärast suri see põlvkond kõrbes.
8 Meid ootab ees uus maailm ning ka meie usk pannakse praegu proovile.
Meil oleks hea analüüsida, kui tugev meie usk on.
Näiteks võiksime kaaluda, kuidas me suhtume Jeesuse sõnadesse tekstis Matteuse 6:33.
Küsi endalt: „Kas minu prioriteedid ja otsused näitavad, et ma tõesti usun Jeesuse sõnu?
Kas jätaksin koosolekuid vahele või käiksin vähem kuulutamas, et suurendada oma sissetulekut?
Mida ma teeksin, kui selle maailma surve suureneks?
Kas lubaksin maailmal end oma vormi suruda ja võib-olla koguni loobuksin Jehoova teenimisest?”
 9. Miks ja kuidas meil tuleks oma usku analüüsida?
9 Meie süda võib muutuda kõvaks ka siis, kui oleme tõrksad järgima Piibli mõõdupuid, näiteks neid, mis puudutavad sõprade valikut, kogudusest eemaldamist või meelelahutust.
Kui avastame, et meil on arenemas niisugune hoiak, on vaja kiiresti oma usk läbi uurida. „Veenduge, kas te ikka elate kooskõlas kristliku usuga.
Uurige end läbi,” annab Piibel nõu. (2. Kor. 13:5.)
Meil peaks olema tavaks end Jumala sõna valguses ausalt analüüsida.



LASE END EDASPIDIGI VORMIDA

10. Mis aitab meil olla nagu pehme savi Jehoova käes?
10 Jumal aitab meil jääda otsekui pehmeks saviks.
Seepärast on ta andnud meile Piibli, koguduse ja kuulutustöö.
Nagu vesi pehmendab savi, nii teeb ka igapäevane Piibli lugemine ja selle üle mõtisklemine meid Jehoova käes voolitavaks.
Iisraeli kuningad pidid tegema endale Jumala seadusest ärakirja ning seda iga päev lugema. (5. Moos. 17:18, 19.)
Apostlid mõistsid, et pühakirja lugemine ja selle üle mõtisklemine on kuulutustööks väga oluline.
Oma kirjutistes tsiteerisid nad pühakirja heebreakeelset osa ja viitasid sellele sadu kordi.
Samuti innustasid nad neid, keda nad kuulutustööl kohtasid, pühakirja lugema ja selle üle mõtisklema. (Ap. t. 17:11.)
Tänapäeval saame ka meie aru, kui oluline on iga päev Piiblit lugeda ja loetu üle sügavalt järele mõelda. (1. Tim. 4:15.)
Nii tehes jääme Jehoova ees alandlikuks ning laseme tal end vormida.





Võta vastu Jumala pakutav abi ja lase end vormida (vaata lõike 10–13)




11., 12. Kuidas Jehoova meid koguduse kaudu vormib?
Too näide.
11 Jehoova vormib meid ka koguduse kaudu, võttes arvesse meie kui üksikisikute vajadusi.
Jim, kellest varem juttu oli, muutis tasapisi oma hoiakut, kui üks kogudusevanem tema vastu rohkem huvi tundis.
Jim räägib: „Ta ei süüdistanud ega kritiseerinud mind kordagi, vaid oli positiivne ja väljendas siirast soovi mind aidata.” Umbes kolme kuu pärast kutsus see vend Jimi koosolekule. „Kogudus võttis mind soojalt vastu,” sõnab Jim. „Nende armastus oli mulle pöördepunktiks.
Mõistsin, et minu tunded pole kõige tähtsamad.
Tänu vendadele ja oma kallile naisele, kes ei löönud usus kordagi kõikuma, sain tasapisi jälle vaimselt jalad alla.
Palju julgustust sain ka artiklitest „Jehoova ei ole süüdi” ja „Teeni Jehoovat lojaalselt”, mis ilmusid 1992. aasta 15. novembri Vahitornis.”
12 Hiljem määrati Jim taas kogudusevanemaks.
Sellest ajast alates on ta aidanud ka teistel vendadel samalaadsete katsumustega toime tulla ning vaimselt taastuda.
Jim tõdeb: „Arvasin varem, et minu suhted Jehoovaga on tugevad, aga see polnud nii.
Mul on kahju, et lasin uhkusel end pimestada ja et omistasin teiste puudustele liiga suure tähtsuse.” (1. Kor. 10:12.)
13. Milliseid omadusi aitab kuulutustöö meis kasvatada ja kellele tuleb kuulutustöö kasuks?
13 Kuidas võib kuulutustöö meid vormida?
Kui räägime teistele head sõnumit, arendame alandlikkust ja mitmesuguseid Jumala vaimu vilja tahke. (Gal. 5:22, 23.)
Mõtle, milliseid häid omadusi oled sa endas kasvatanud tänu kuulutustööle.
Ja mis veelgi tähtsam, kristlikke omadusi ilmutades kaunistame head sõnumit ja võime mõjutada oma vestluskaaslase suhtumist.
Toome selle kohta ühe näite.
Kaks Jehoova tunnistajat Austraalias kuulasid lugupidavalt üht naist, kes rääkis nendega väga ebaviisakalt.
Hiljem naine kahetses oma käitumist ja kirjutas harubüroole kirja.
Ta ütles: „Tahan paluda vabandust nende kahe ääretult kannatliku ja alandliku inimese ees.
Olin ennast täis ja üleolev.
Minust oli väga rumal saata need kaks Jumala sõnast rääkivat inimest niimoodi ära.” Kas see naine oleks sedasi kirjutanud, kui kuulutajad oleksid pisutki vihastunud?
Arvatavasti mitte.
Kuulutustöö toob tõesti häid tulemusi nii meile endile kui ka teistele.



ALLU JUMALALE,
KUI VORMID OMA LAPSI

14. Mida peavad vanemad tegema, kui tahavad laste vormimisel edukad olla?
14 Enamik lapsi on õpihimulised ja alandlikud. (Matt. 18:1–4.)
Seepärast püüavad targad lapsevanemad juba siis, kui nende lapsed on veel väikesed, juurutada neisse Jumala õpetust ja armastust Piibli tõe vastu. (2. Tim. 3:14, 15.)
Selleks et vanemaid saadaks edu, peavad nad kinnistama Piibli tõe kõigepealt enda südamesse, Jumala teenimine peaks olema nende eluviis.
Kui vanemad seda teevad, siis lapsed mitte ainult ei kuule Piibli põhimõtetest, vaid nad ka näevad, kuidas neid ellu rakendada.
Lisaks õpivad nad nägema oma vanemate manitsust nende armastuse väljendusena, milles peegeldub Jehoova armastus.
15., 16. Kuidas saavad vanemad näidata, et usaldavad Jehoovat, kui nende laps eemaldatakse?
15 Hoolimata kristlikust kasvatusest hülgavad siiski mõned lapsed hiljem tõe või nad eemaldatakse kogudusest ning see valmistab tervele perele suurt südamevalu. „Kui mu vend eemaldati, tundsin, nagu oleks ta surnud.
See oli nii valus!” sõnab üks õde Lõuna-Aafrika Vabariigist.
Mida tema ja ta vanemad tegid?
Nad järgisid Jumala sõna juhatust. (Loe 1. Korintlastele 5:11, 13.) Vanemad räägivad: „Otsustasime kindlalt järgida Piibli nõu, sest mõistsime, et kui toimime Jumala viisil, on sel kõige paremad tulemused.
Suhtusime eemaldamisse kui Jumala manitsusse ning olime veendunud, et Jehoova manitseb armastusest ja õigel määral.
Seega me ei suhelnud oma pojaga rohkem, kui oli pereasjade pärast vältimatult vajalik.”
16 Mida nende poeg sellest arvas?
Ta lausus hiljem: „Teadsin, et pereliikmed ei vihka mind, vaid nad kuuletuvad Jehoovale ja tema organisatsioonile.” Ta ütles veel: „Kui sul ei ole toetuda kellelegi muule kui ainult Jehoovale, siis mõistad, kui väga sa teda vajad.” Perekonna rõõm oli piiritu, kui see noormees kogudusse tagasi võeti.
Kui arvestame Jumalaga kõigil oma teedel, on sel parimad tulemused. (Õpet. 3:5, 6; 28:26.)
17. Miks peaks Jehoovale allumine olema meie eluviis ja kuidas see meile kasuks tuleb?
17 Prohvet Jesaja ennustas, et kui juutide pagenduses oleku aeg hakkab lõppema, tõdevad kahetsevad juudid: „Ometi oled sina,
Jehoova, meie isa.
Meie oleme savi ja sina oled pottsepp, me kõik oleme sinu kätetöö.” Seejärel nad anuvad: „Ära pea igavesti meeles meie süütegusid.
Palun vaata meid, me kõik oleme ju sinu rahvas!” (Jes. 64:8, 9.)
Kui meie kuuletume samamoodi alandlikult Jehoovale ja see on meie eluviis, peab ta meid väga kalliks, nagu ta pidas ka prohvet Taanieli.
Jehoova vormib meid ka edaspidi Piibli, püha vaimu ja oma organisatsiooni kaudu, et võiksime ühel päeval saada „Jumala laste võrratu vabaduse”. (Rooml. 8:21.)






    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Eestikeelsed väljaanded (2000-2017)
			
		

		
			
				
				Eelistused
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Kasutustingimused
			
			
				Privaatsus
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



