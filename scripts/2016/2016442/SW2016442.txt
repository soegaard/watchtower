MNAMO Novemba 2010, chombo cha udongo kutoka China cha karne ya 18 kilinunuliwa kwa karibu dola milioni 70 jijini London,
Uingereza.
Ni wazi kwamba mfinyanzi anaweza kutengeneza chombo cha thamani kubwa na maridadi kwa kutumia udongo unaopatikana kwa urahisi na usio na gharama.
Hata hivyo, hakuna mfinyanzi wa kibinadamu anayeweza kulinganishwa na Yehova.
Karibu mwisho wa siku ya sita ya uumbaji,
Mungu aliumba mwanadamu mkamilifu “kutoka katika mavumbi [udongo] ya nchi” mwenye uwezo wa kuonyesha sifa za Muumba wake. (Mwa. 2:7)
Kwa kufaa, mwanadamu huyo mkamilifu,
Adamu, aliyeumbwa kutoka katika udongo aliitwa “mwana wa Mungu.”—Luka 3:38.
2, 3. Tunaweza kuigaje mtazamo wa Waisraeli waliotubu?
2 Hata hivyo,
Adamu hakuendelea kuwa mwana wa Mungu alipomwasi Muumba wake.
Lakini kwa vizazi vyote, “wingu kubwa” la wazao wa Adamu wameunga mkono enzi kuu ya Mungu. (Ebr. 12:1)
Kwa kujitiisha kwa unyenyekevu chini ya Muumba wao walionyesha kwamba wanataka Yehova awe Baba na Mfinyanzi wao, na si Shetani. (Yoh. 8:44)
Ushikamanifu wao kwa Mungu unatukumbusha maneno haya ya Isaya kuwahusu Waisraeli waliotubu: “Ee Yehova, wewe ni Baba yetu.
Sisi ni udongo, nawe ni Mfinyanzi wetu; nasi sote ni kazi ya mkono wako.”—Isa. 64:8.
3 Leo, wote wanaomwabudu Yehova katika roho na kweli wanajitahidi kuonyesha mtazamo huo wa unyenyekevu na ujitiisho.
Wanaona kuwa heshima kubwa kumwita Yehova Baba, na kujitiisha kwake akiwa Mfinyanzi wao.
Je, unajiona kuwa udongo laini mikononi mwa Mungu, tayari kufinyangwa ili uwe chombo chenye kupendeza machoni pake?
Je, unawaona pia ndugu na dada zako kama vyombo vinavyoendelea kufinyangwa na Mungu?
Ili tuelewe jambo hilo, acheni tufikirie mambo haya matatu yanayohusu kazi ya Yehova akiwa Mfinyanzi wetu:
Jinsi anavyochagua wale atakaowafinyanga, sababu ya kuwafinyanga, na jinsi anavyofanya hivyo.



YEHOVA ANACHAGUA WALE ANAOWAFINYANGA

4. Yehova huchunguza nini anapochagua wale atakaowavuta kwake?
Toa mifano.
4 Yehova anapowatazama wanadamu, hazingatii sura yao.
Badala yake, anachunguza moyo, yule mtu wa ndani. (Soma 1 Samweli 16:7b.) Jambo hilo lilionekana wazi Mungu alipoanzisha kutaniko la Kikristo.
Alivuta watu wengi kwake na kwa Mwanaye, watu ambao walionekana kuwa hawafai machoni pa wanadamu. (Yoh. 6:44)
Mmoja wao alikuwa Farisayo aliyeitwa Sauli, ambaye alikuwa “mkufuru na mtesaji na mtu mwenye dharau.” (1 Tim. 1:13)
Hata hivyo, “mchunguzaji wa mioyo” hakumwona Sauli kama udongo usiofaa. (Met. 17:3)
Badala yake,
Mungu aliona kwamba anaweza kufinyangwa na kuwa chombo chenye thamani.
Kwa kweli, alimwona kuwa “chombo kilichochaguliwa” kutoa ushahidi “kwa mataifa na vilevile kwa wafalme na wana wa Israeli.” (Mdo. 9:15)
Wengine ambao Mungu aliwaona kuwa vyombo vilivyochaguliwa “kwa matumizi yenye kuheshimika” hapo awali walikuwa walevi, waasherati, na wezi. (Rom. 9:21; 1 Kor. 6:9-11)
Walimruhusu Yehova awafinyange kadiri walivyoendelea kupata ujuzi wa Neno la Mungu na kuimarisha imani.
5, 6. Tukiamini kwamba Yehova ni Mfinyanzi wetu, tutakuwa na mtazamo gani kuelekea (a) watu katika eneo letu? (b) ndugu na dada zetu?
5 Habari iliyotangulia inaweza kutusaidiaje?
Tukiamini kwamba Yehova ana uwezo wa kusoma mioyo na kuwavuta watu anaowachagua, hatutawahukumu wengine katika eneo letu na kutanikoni.
Fikiria mfano wa Michael.
Anakumbuka hivi: “Mashahidi wa Yehova waliponitembelea, niliwapuuza na sikujishughulisha nao.
Sikuwaheshimu hata kidogo!
Baadaye, nikiwa mahali tofauti, nilivutiwa na mwenendo wa familia fulani.
Kisha siku moja nilishtuka kujua kwamba walikuwa Mashahidi wa Yehova!
Mwenendo wao ulinichochea kuchunguza sababu zilizofanya niwabague.
Nilitambua kwamba niliwabagua kwa sababu sikuwajua vizuri na pia kwa kuwa nilisikia habari zisizo za kweli kuwahusu.” Ili ajue ukweli,
Michael alianza kujifunza Biblia.
Hatimaye, alikubali kweli na akaanza utumishi wa wakati wote.
6 Tukimkubali Yehova awe Mfinyanzi wetu, mtazamo wetu kuelekea waabudu wenzetu unaweza kubadilika pia.
Je, unawaona ndugu na dada zako kama Mungu anavyowaona? Je, unawaona kuwa vyombo vinavyoendelea kufinyangwa?
Yehova anawaona hivyo.
Anaona jinsi walivyo kwa ndani na anajua kwamba hali yao ya kutokamilika ni ya muda mfupi tu.
Pia anajua kila mmoja wao anaweza kuwa mtu wa aina gani baada ya kufinyangwa. (Zab. 130:3)
Tunaweza kumwiga kwa kuzingatia sifa nzuri za watumishi wake.
Kwa kweli, tunaweza kufanya kazi pamoja na Mfinyanzi wetu kwa kuwasaidia ndugu na dada zetu wanapojitahidi kufanya maendeleo ya kiroho. (1 The. 5:14, 15)
Wakiwa “zawadi katika wanadamu,” wazee wanapaswa kuongoza katika kufanya hivyo.—Efe. 4:8, 11-13.



KWA NINI YEHOVA ANATUFINYANGA?

7. Kwa nini unathamini nidhamu ya Yehova?
7 Huenda umewahi kusikia mtu akisema hivi: ‘Sikuthamini kikamili nidhamu niliyopokea kutoka kwa wazazi wangu hadi nilipopata watoto.’ Tunapopata uzoefu maishani, huenda tukaanza kuiona nidhamu kama Yehova anavyoiona, yaani, njia yake ya kuonyesha upendo. (Soma Waebrania 12:5, 6, 11.) Naam, kwa kuwa Yehova anawapenda watoto wake, anatufinyanga kwa subira.
Anataka tuwe na hekima na furaha na kumpenda. (Met. 23:15)
Hafurahii tunapoteseka kwa sababu ya maamuzi yetu mabaya wala hataki tufe tukiwa “watoto wa ghadhabu,” hali ambayo tulirithi kutoka kwa Adamu.—Efe. 2:2, 3.
8, 9. Yehova anatufundishaje leo, na elimu hiyo itaendeleaje wakati ujao?
8 Tulipokuwa “watoto wa ghadhabu,” tulikuwa na mazoea ambayo hayakumfurahisha Mungu, na huenda baadhi ya mazoea hayo yalikuwa kama ya wanyama!
Lakini baada ya kufinyangwa na Yehova, tulibadilika; tukawa kama wana-kondoo. (Isa. 11:6-8; Kol. 3:9, 10)
Hivyo, mazingira ambayo Yehova hutumia kutufinyanga yanaonekana kuwa paradiso ya kiroho iliyopo leo.
Tunahisi tukiwa salama licha ya ulimwengu mwovu unaotuzunguka.
Isitoshe, kuishi katika paradiso ya kiroho kumesaidia baadhi yetu kutoka familia zenye matatizo na zisizo na upendo tufurahie upendo wa kweli. (Yoh. 13:35)
Na tumejifunza kupendana.
Zaidi ya hilo, tumemjua Yehova na sasa tunajionea upendo wake akiwa Baba yetu.—Yak. 4:8.
9 Katika ulimwengu mpya tutafurahia baraka kamili za paradiso ya kiroho.
Pia tutafurahia kuishi katika paradiso halisi chini ya utawala wa Ufalme wa Mungu.
Kadiri dunia itakavyoendelea kugeuzwa kuwa paradiso,
Yehova atazidi kuwafinyanga wanadamu kwa kuwaelimisha kwa kiwango ambacho hatuwezi kuelewa kikamili sasa. (Isa. 11:9)
Zaidi ya hilo,
Mungu atafanya tuwe wakamilifu kimwili na kiakili ili tuweze kuelewa mafundisho yake na kufanya mapenzi yake kikamili.
Hivyo, acheni tuazimie kujitiisha kwa Yehova kwa kuonyesha kwamba tunatambua kuwa anatufinyanga kwa sababu anatupenda.—Met. 3:11, 12.



JINSI YEHOVA ANAVYOTUFINYANGA

10. Yesu aliigaje subira na ustadi wa Mfinyanzi Mkuu?
10 Kama mfinyanzi stadi sana,
Yehova anajua sisi ni “udongo” wa aina gani na anatufinyanga kulingana na uwezo wetu. (Soma Zaburi 103:10-14.) Kwa kweli, anashughulika nasi, huku akizingatia udhaifu, mipaka, na kiwango chetu cha ukuzi wa kiroho.
Mwana wake alionyesha mtazamo wa Mungu kuelekea watumishi wake wasio wakamilifu.
Fikiria jinsi Yesu alivyoshughulika na udhaifu wa mitume wake hasa mwelekeo wao wa kubishania cheo.
Ikiwa ungeshuhudia mitume hao wakibishana vikali, je, ungewaona kuwa wanaume wanyenyekevu na wanaoweza kufinyangwa?
Yesu hakuwa na mtazamo usiofaa kuwaelekea.
Alijua kwamba mitume wake waaminifu wangeweza kufinyangwa kupitia ushauri wa fadhili, subira, na kwa kutazama kielelezo chake cha unyenyekevu. (Marko 9:33-37; 10:37, 41-45; Luka 22:24-27)
Yesu alipofufuliwa na roho takatifu kumiminwa, mitume hawakukazia fikira cheo au umashuhuri, bali kazi aliyowapa.—Mdo. 5:42.
11. Daudi alionyeshaje kwamba alikuwa udongo laini, na tunaweza kumwigaje?
11 Leo,
Yehova anatumia Neno lake, roho yake takatifu, na kutaniko la Kikristo kuwafinyanga watumishi wake.
Neno la Mungu linaweza kutufinyanga tunapolisoma kwa makini, tunapolitafakari, na kumwomba Yehova atusaidie kulitumia maishani.
Daudi aliandika hivi: “Wakati ninapokukumbuka kitandani mwangu, wakati wa makesha ya usiku natafakari juu yako.” (Zab. 63:6)
Pia aliandika hivi: “Nitambariki Yehova, ambaye amenipa shauri.
Kwa kweli, nyakati za usiku figo zangu zimenirekebisha.” (Zab. 16:7)
Naam,
Daudi aliruhusu ushauri kutoka kwa Mungu upenye ndani zaidi ya “figo” zake, ili ufinyange mawazo na hisia zake za ndani kabisa, hata aliposhauriwa vikali. (2 Sam. 12:1-13)
Daudi alituwekea mfano mzuri sana wa unyenyekevu na ujitiisho!
Je, wewe pia hutafakari Neno la Mungu na kuliruhusu lipenye ndani zaidi ya moyo wako?
Je, unapaswa kufanya hivyo hata zaidi?—Zab. 1:2, 3.
12, 13. Yehova anatufinyanga jinsi gani kupitia roho takatifu na kutaniko la Kikristo?
12 Roho takatifu inaweza kutufinyanga katika njia mbalimbali.
Kwa mfano, inaweza kutusaidia kusitawisha utu kama wa Kristo, ambao unatia ndani tunda la roho ya Mungu. (Gal. 5:22, 23)
Sifa moja ya tunda hilo ni upendo.
Kwa kuwa tunampenda Mungu na tunataka kumtii na kufinyangwa naye, tunatambua kwamba amri zake si mzigo mzito.
Pia roho takatifu inaweza kutupatia nguvu za kukataa kufinyangwa na roho mbaya ya ulimwengu. (Efe. 2:2)
Mtume Paulo, ambaye alipokuwa kijana aliongozwa na roho ya kiburi ya viongozi wa dini ya Kiyahudi, aliandika hivi: “Kwa mambo yote nina nguvu kupitia yeye anayenipa nguvu.” (Flp. 4:13)
Hivyo, kama Paulo, acheni tuendelee kuomba tupewe roho takatifu.
Yehova hatapuuza maombi hayo ya unyoofu ya watu wapole.—Zab. 10:17.





Yehova anatumia wazee Wakristo kutufinyanga, lakini lazima tufanye sehemu yetu (Tazama fungu la 12 na 13)




13 Yehova anatumia kutaniko la Kikristo na waangalizi ili kutufinyanga tukiwa mtu mmoja-mmoja.
Kwa mfano, wazee wanapotambua kwamba tuna matatizo ya kiroho wanajaribu kutusaidia kwa kumwomba Mungu kwa unyenyekevu awape ufahamu na hekima.
Hawatumii kamwe hekima kutoka kwa wanadamu. (Gal. 6:1)
Kwa kufikiria hali zetu, wanatenda kulingana na sala zao kwa kufanya utafiti katika Neno la Mungu na machapisho yetu ya Kikristo.
Hilo linaweza kuwasaidia kutoa msaada unaofaa mahitaji yetu.
Wazee wakikupa ushauri kwa fadhili na upendo, kwa mfano kuhusiana na mtindo wa mavazi, je, utaukubali na kuuona kuwa njia ya Mungu ya kuonyesha kwamba anakupenda?
Ukifanya hivyo, utathibitisha kwamba wewe ni udongo laini mikononi mwa Yehova na kwamba uko tayari kufinyangwa ili uwe bora.
14. Ingawa Yehova ana mamlaka juu ya udongo, anaonyeshaje kwamba anaheshimu uhuru wetu wa kuchagua?
14 Kuelewa jinsi Mungu anavyotufinyanga kunaweza kutusaidia kuboresha mahusiano yetu pamoja na waabudu wenzetu na kuwa na mtazamo mzuri kuelekea watu katika eneo letu wakiwemo wanafunzi wetu wa Biblia.
Katika nyakati za Biblia, mfinyanzi hakuchimba udongo na kuanza kuufinyanga mara moja.
Kwanza aliutayarisha kwa kuondoa mawe na vitu vingine visivyofaa.
Katika maana ya kiroho,
Mungu huwatayarisha watu ili aweze kuwafinyanga.
Hawalazimishi kubadilika, bali hufunua viwango vyake vya uadilifu ili waendelee kufinyangwa na kufanya mabadiliko kwa hiari.
15, 16. Wanafunzi wa Biblia wanaonyeshaje kwamba wanataka Yehova awafinyange?
Toa mfano.
15 Mfikirie Tessie, dada kutoka Australia.
Dada aliyejifunza naye anasema hivi: “Tessie alielewa kweli za Biblia kwa urahisi.
Lakini hakufanya maendeleo yoyote ya kiroho na hata hakuhudhuria mikutano ya Kikristo!
Hivyo, baada ya kufikiria na kusali sana kuhusu jambo hilo, niliamua kuacha kujifunza naye.
Kisha jambo la kushangaza likatokea.
Nikifikiri kwamba ninajifunza naye Biblia kwa mara ya mwisho,
Tessie alinimiminia moyo wake.
Alisema kwamba alihisi kuwa mnafiki kwa sababu alipenda kucheza kamari.
Lakini sasa alikuwa ameazimia kuacha zoea hilo.”
16 Muda mfupi baadaye,
Tessie alianza kuhudhuria mikutano ya Kikristo na kuonyesha utu wa Kikristo licha ya dhihaka kutoka kwa rafiki zake.
Dada huyo aliongeza hivi: “Baada ya muda,
Tessie alibatizwa na baadaye akawa painia wa kawaida ingawa watoto wake walikuwa wadogo.” Naam, wanafunzi wa Biblia wanapoanza kubadili maisha yao ili wamfurahishe Mungu, atawakaribia na kuwafinyanga wawe vyombo vyenye kupendeza kwelikweli.
17. (a)
Ni nini kinachokuvutia kumhusu Yehova akiwa Mfinyanzi wako? (b) Tutachunguza mambo gani mengine kuhusu ufinyanzi?
17 Hadi leo, vyombo fulani vya udongo hutengenezwa kwa mkono, mfinyanzi akifinyanga udongo huo moja kwa moja.
Vivyo hivyo, kwa subira Mfinyanzi wetu anatufinyanga moja kwa moja, akitushauri na kuona jinsi tunavyopokea ushauri huo. (Soma Zaburi 32:8.) Je, unaona jinsi Yehova anavyopendezwa nawe kibinafsi?
Je, unajiona ukiwa mikononi mwake akikufinyanga kwa upendo?
Ikiwa ndivyo, ni sifa gani nyingine ambazo zitakusaidia uendelee kuwa udongo laini unaoweza kufinyangwa na Yehova?
Ni mazoea gani unayopaswa kuepuka ili usiwe mgumu au asiyeweza kufinyangwa?
Na wazazi wanawezaje kushirikiana na Yehova katika kuwafinyanga watoto wao?
Habari hii itazungumziwa katika makala inayofuata.






    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Machapisho ya Kiswahili (2000-2017)
			
		

		
			
				
				Mapendezi
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Masharti ya Utumiaji
			
			
				Sera ya Faragha
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



