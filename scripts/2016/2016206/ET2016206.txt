„PEATU,
VAATA,
KUULA.” See hoiatus on päästnud lugematul hulgal inimelusid.
Rohkem kui 100 aastat tagasi olid Põhja-Ameerikas raudteeülesõidukohtade ees suured tahvlid nende sõnadega.
Miks?
Selleks, et sõidukid ei ületaks raudteed valel ajal ega satuks rongi ette.
Selle hoiatuse järgimine päästis tõesti elusid.
2 Jehoova teeb aga midagi palju enamat, kui lihtsalt seab üles hoiatusmärke.
Ta suunab oma rahvast, näidates neile teed igavesse ellu ja aidates neil vältida ohte.
Ta on oma rahvale otsekui armastav karjane, kes juhatab lambaid õiges suunas ja hoiatab neid, et nad ei kalduks ohtlikule rajale. (Loe Jesaja 30:20, 21.)



JEHOOVA ON ALATI OMA RAHVAST JUHATANUD

 3. Kuidas sattus inimkond hukatuse teele?
3 Läbi kogu inimajaloo on Jehoova andnud juhatust ja konkreetseid juhendeid.
Näiteks Eedeni aias andis Jehoova selged juhised, mida järgides oleks inimpere elanud õnnelikult igavesti. (1. Moos. 2:15–17.)
Kui Aadam ja Eeva oleksid Jumalale kuuletunud, oleksid nad vältinud traagilisi tagajärgi.
Kuna nad aga seda ei teinud, oli nende edasine elu täis kannatusi ja nad surid ilma ülesäratamislootuseta.
Jumalale kuuletumise asemel kuulas Eeva hoopis nõu, mis näis tulevat alamat liiki loodolendilt.
Aadam omakorda kuulas Eevat, teist surelikku inimest.
Nad mõlemad pöörasid selja oma armastavale isale.
Selle tagajärjel sattus kogu inimkond hukatuse teele.
 4. a)
Miks oli pärast veeuputust vaja lisajuhendeid? b) Kuidas tuli uues olukorras esile Jumala suhtumine?
4 Noa päevil andis Jumal juhendid, mille järgimine tagas elu.
Pärast veeuputust andis Jehoova konkreetse keelu seoses verega.
Miks oli seda vaja?
Sellepärast, et olukord oli muutunud.
Jehoova lubas inimestel hakata tarvitama toiduks loomade liha, ent samas ta ütles: „Te ei tohi süüa liha koos verega, see tähendab eluga.” (1. Moos. 9:1–4.)
See uus olukord tõi selgelt esile, kuidas suhtub Jumal ellu, mille ta on andnud.
Loojana ja eluandjana on tal õigus elu kohta reegleid kehtestada.
Näiteks ütles ta, et inimesel pole õigust võtta teise inimese elu.
Jumala silmis on elu ja veri pühad ning ta peab vastutavaks igaüht, kes rikub selles osas tema seadusi. (1. Moos. 9:5, 6.)
 5. Mida me arutama hakkame ja miks?
5 Vaadelgem nüüd veel mõnda näidet, kuidas Jumal on sajandite jooksul juhatust andnud.
See tugevdab meie otsustavust järgida Jehoova juhatust teel uude maailma.



UUS RAHVAS,
UUED JUHENDID

 6. Miks oli Jumala rahval vaja kuuletuda Moosese seadusele ja millist suhtumist see eeldas?
6 Moosese päevil oli vaja selgeid juhendeid õige käitumise ja jumalateenimise kohta.
Miks?
Olukord oli taas muutunud. Üle kahesaja aasta olid Jaakobi järeltulijad elanud Egiptuses, mille elanikud kummardasid surnuid, austasid ebajumalaid ja tegid palju muud, mis teotas Jumalat.
Kui Jumala rahvas Egiptuse orjusest pääses, oli vaja uusi juhendeid.
Iisraellased polnud enam vangid, vaid vaba rahvas, kes pidi järgima üksnes Jehoova seadust.
Mõned teatmeteosed ütlevad, et heebrea sõna, mille vasteks on „seadus”, tuleneb sõnajuurest, mille tähendus on juhtima, suunama, juhatust andma.
Moosese seadus oli otsekui kaitsev müür Jumala rahva ning moraalselt ja religioosselt laostunud rahvaste vahel.
Kui iisraellased kuuletusid Jumalale, kogesid nad õnnistusi.
Kui nad aga tema juhatust ei järginud, olid sel kohutavad tagajärjed. (Loe 5. Moosese 28:1, 2, 15.)
 7. a)
Mis oli veel üks põhjus, miks Jehoova andis oma rahvale juhendeid? b) Mis mõttes oli Moosese seadus Iisraeli rahvale otsekui järelevaatajaks?
7 Juhendeid oli vaja ka ühel teisel põhjusel.
Moosese seadus valmistas iisraellasi ette tähtsaks sündmuseks, mis oli seotud Jehoova eesmärgiga.
Selleks oli messia ehk Jeesus Kristuse tulemine.
Seadus tõi selgelt esile iisraellaste ebatäiuse.
Samuti rõhutas see neile vajadust lunastuse järele, täiusliku ohvri järele, mis kataks täielikult nende patud. (Gal. 3:19; Heebr. 10:1–10.)
Peale selle aitas seadus hoida alal suguvõsaliini, mis viis messiani ja võimaldas hiljem teda ära tunda.
Tõepoolest, seadus oli otsekui ajutine kasvataja või järelevaataja, mis juhatas Kristuse juurde. (Gal. 3:23, 24.)
 8. Miks tuleks meil juhinduda Moosese seaduse põhimõtetest?
8 Iisraeli rahvale antud Moosese seadus on kasulik ka kristlastele.
Mis mõttes?
Meil on hea peatuda ja uurida põhimõtteid, millel see seadus rajaneb.
Kuigi me ei ole selle seaduse all, on selles palju usaldusväärseid juhendeid, mida me saame järgida igapäevaelus ja meie püha Jumala Jehoova teenimisel.
Ta on lasknud selle seaduse Piiblisse kirja panna, et me võiksime sellest õppida, juhinduda selle põhimõtetest ja olla tänulikud kõrgemate kristlike moraalinormide eest.
Tuleta näiteks meelde Jeesuse nõuannet: „Te olete kuulnud, et on öeldud: „Sa ei tohi abielu rikkuda.” Aga mina ütlen teile, et igaüks, kes jääb naist ihaldavalt vaatama, on temaga juba abielu rikkunud oma südames.” Seega pole meil vaja hoiduda mitte üksnes abielurikkumisest, vaid ka mistahes väärast seksuaalsest ihalusest. (Matt. 5:27, 28.)
 9. Millises uues olukorras oli Jumalal taas vaja anda uusi juhendeid?
9 Kui Jeesus sai messiaks, oli tarvis uusi juhendeid ja täpsemaid selgitusi Jehoova eesmärgi kohta, sest taas oli tegemist uue olukorraga.
Aastal 33 m.a.j hülgas Jehoova Iisraeli rahva ja valis oma uueks rahvaks kristliku koguduse.



JUHENDID UUELE VAIMSELE RAHVALE

10. Miks anti kristlikule kogudusele uusi juhendeid ja kuidas erinesid need iisraellastele antud seadusest?
10 Esimesel sajandil sai kristlikus koguduses olev Jumala rahvas uusi või täpsustatud juhendeid käitumise ja jumalateenimise kohta.
Need pühendunud jumalateenijad olid uue lepingu osalised.
Moosese seadus oli antud sünnipärasele Iisraeli rahvale.
Ent vaimse Iisraeli hulka pidid kuuluma eri rahvuse ja taustaga inimesed.
Tõesti, „Jumal ei ole erapoolik, vaid iga rahva hulgast on talle meelepärane see, kes on jumalakartlik ja teeb, mis on õige”. (Ap. t. 10:34, 35.)
Iisraeli rahvast juhtis tõotatud maal Moosese seadus koos käskudega, mis olid uuristatud kivisse.
Vaimne Iisrael järgis aga „Kristuse seadust” ja see rajanes peamiselt põhimõtetel, mis olid kirjutatud nende südamesse.
Kõik kristlased, sõltumata elukohast, pidid järgima „Kristuse seadust”. (Gal. 6:2.)
11. Millist kahte kristliku elu valdkonda hõlmas „Kristuse seadus”?
11 Vaimsele Iisraelile tõid suurt kasu juhendid, mida Jumal Jeesuse kaudu andis.
Enne uue lepingu jõustumist andis Jeesus kaks tähtsat käsku. Üks neist puudutas kuulutustööd.
Teine käis selle kohta, kuidas peaksid Jeesuse järelkäijad käituma ja oma usukaaslasi kohtlema.
Need juhendid olid mõeldud kõigile kristlastele.
Seega tuleb nende järgi tegutseda ka kõigil tõelistel jumalateenijatel tänapäeval, olgu neil siis lootus elada tulevikus maa peal või taevas.
12. Milline muutus toimus seoses kuulutustööga?
12 Mõelgem esmalt kuulutustööle, mida Jeesus oma järelkäijatel teha käskis.
Varem, kui võõramaalased soovisid kummardada Jehoovat, pidid nad tulema Iisraeli. (1. Kun. 8:41–43.)
See oli enne seda, kui Jeesus andis käsu, mis on kirjas Matteuse 28:19, 20 (loe). Nüüd pidid Jeesuse jüngrid ise minema kõigi inimeste juurde. 33. aasta nädalatepühal sai kinnitust see, et Jehoova on muutnud kuulutustöö strateegiat: head sõnumit pidi levitatama kogu maailmas.
Tänu püha vaimu väljavalamisele said umbes 120 uue koguduse liiget võime kuulutada juutidele ja proselüütidele eri keeltes. (Ap. t. 2:4–11.)
Edasi pidid head sõnumit kuulma samaarlased.
Ning seejärel, aastal 36 hakati kuulutama ümberlõikamata mittejuutidele.
Piltlikult võib öelda, et kui algul käis kuulutustöö otsekui väikses tiigis ainult juutidele, siis hiljem pidi selle töö territooriumiks saama suur ookean ehk kogu inimkond.
13., 14. a) Mida hõlmab Jeesuse antud „uus käsk”? b) Mida me õpime Jeesuse eeskujust?
13 Mõelgem ka sellele, kuidas meil tuleks kohelda oma usukaaslasi vastavalt Jeesuse „uuele käsule”. (Loe Johannese 13:34, 35.) See käsk hõlmab üleskutset ilmutada üksteise vastu armastust, kuid seda mitte ainult igapäevaelus, vaid olles valmis usukaaslase eest isegi surema.
Moosese seaduses sellist nõuet ei olnud. (Matt. 22:39; 1. Joh. 3:16.)
14 Jeesus ise oli armastuse ilmutamises ülimaks eeskujuks.
Ta armastas oma jüngreid ennastohverdavalt ning oli valmis nende eest surema.
Ja ta ootas, et ka tema jüngritel, kaasa arvatud meil, oleks samasugune armastus.
Me peame olema valmis oma usukaaslaste pärast kannatusi taluma ja nende eest ehk isegi surema. (1. Tess. 2:8.)



JUHENDID TÄNAPÄEVAKS JA TULEVIKUKS

15., 16. Millises uues olukorras me praegu oleme ja kuidas Jehoova meid juhatab?
15 Eriti just „ustava ja aruka orja” ametissemääramisest alates on Jeesus andnud oma järelkäijatele vaimset toitu õigel ajal. (Matt. 24:45–47.)
See toit on hõlmanud väga olulist juhatust vastavalt muutuvatele oludele.
16 Me elame praegu ajal, mida Piiblis nimetatakse viimseteks päevadeks, ning peagi on tulemas enneolematu viletsus. (Mark. 13:19; 2. Tim. 3:1.)
Lisaks on Saatan koos deemonitega taevast välja heidetud ja nad saavad tegutseda vaid maa läheduses, põhjustades inimestele suuri kannatusi. (Ilm. 12:9, 12.)
Samas on meile antud ülesanne osaleda ajaloo suurimas hea sõnumi kuulutamise hoogtöös, mille käigus püütakse järjest rohkemates keeltes jõuda võimalikult paljude inimesteni.
17., 18. Kuidas meil tuleks suhtuda meile antavasse juhatusse?
17 Kuulutustöö tegemiseks on Jumala organisatsioon andnud meile häid tööriistu.
Kas sina kasutad neid?
Kas oled varmas panema tähele koosolekutel antavaid juhendeid, kuidas neid tööriistu kasutada ja kuidas teha seda tõhusalt?
Kas suhtud neisse kui Jumalalt tulevatesse juhenditesse?
18 Et meil oleks Jumala õnnistus, on meil vaja kuulda võtta kõiki juhendeid, mis kristliku koguduse kaudu antakse.
Kui oleme kuulekad praegu, aitab see meil järgida juhatust „suure viletsuse” ajal, mil kogu Saatana kuri maailm hävitatakse. (Matt. 24:21.)
Seejärel on meil vaja uusi juhendeid eluks uuel maal, mis on täiesti vaba Saatana mõjust.





Paradiisis avatakse rullraamatud, et anda meile juhendeid eluks uuel maal (vaata lõike 19, 20)




19., 20. Mida sisaldavad rullraamatud, mis tulevikus avatakse, ja mis tulemus on nende uurimisel?
19 Moosese päevil vajas Iisraeli rahvas uusi juhendeid ja Jumal andis neile seaduskogu.
Hiljem tuli kristlikul kogudusel järgida „Kristuse seadust”.
Piibel näitab, et uues maailmas avatakse rullraamatud, mille kaudu antakse meile uusi juhendeid eluks uuel maal. (Loe Ilmutus 20:12.) Nendes rullraamatutes on ilmselt Jehoova nõuded, mis on mõeldud just sel ajal elavatele inimestele.
Neid uurides saavad kõik inimesed, sealhulgas ülesäratatud, teada, mida Jumal neilt ootab.
Kahtlemata aitavad need rullraamatud veelgi paremini mõista Jehoova mõtteviisi.
Tänu üha paremale arusaamisele Jumala sõnast ja tänu rullraamatutest saadavale infole kohtlevad paradiisliku maa elanikud üksteist armastuse, lugupidamise ja väärikusega. (Jes. 26:9.)
Kujutle, milline võimas haridusprogramm ootab meid ees meie kuninga Jeesus Kristuse juhtimisel!
20 Need, kes järgivad rullraamatutes olevaid juhendeid ja jäävad lõplikus katses Jumalale ustavaks, saavad igavese elu ning Jehoova kirjutab nende nimed alatiseks „eluraamatusse”.
Selline tasu võib oodata meist igaüht!
Seega on meil tarvis PEATUDA, et uurida Jumala sõna;
VAADATA selle sisse, et mõista, kuidas see meid puudutab; ning KUULATA Jumala juhatust ja tegutseda selle järgi.
Nii tehes võime elada üle suure viletsuse ning õppida kogu igaviku jooksul üha paremini tundma meie tarka ja armastavat Jumalat Jehoovat. (Kog. 3:11; Rooml. 11:33.)






    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Eestikeelsed väljaanded (2000-2017)
			
		

		
			
				
				Eelistused
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Kasutustingimused
			
			
				Privaatsus
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



