Kay revistaj iskay kaj yachaqananpi Gail sutiyoj warmimanta parlarerqanchej.
Payqa yuyan, qosanmanta yuyarikunallantapuni.
Chaywanpis mayta suyakushan Diospa mosoj pachanpi qosan kausarimunanta.
Pay nin: “Apocalipsis 21:3, 4 versículos nisqan mayta gustawan”, nispa.
Chaypi nin: “Diostaj paykunaj Diosninku kanqa.
Payqa tukuy waqayta ñawisninkumanta pichanqa, manañataj wañuy kanqachu, nitaj waqaypis, nitaj qhapariypis, nitaj nanaypis, imaraykuchus ñawpaj kaj imasqa manaña kanqachu”, nispa.
Gail nillanpuni: “Kay versiculospi nisqanqa cheqapuni.
Ashkha runasqa mana yachankuchu wañusqas kausarimunankuta, chayrayku paykunamanta llakikuni”.
Gailqa chaypi creesqanta rikuchin, vecinosninman chaymanta yachachinanpaj tiempochakuspa, imajtinchus tumpamantawan niña “wañuy kanqachu”.





Jobqa Dios kausarichimunanmanta mana iskayrayarqachu.




Ichá chayqa mana creenapaj jina.
Chaywanpis Jobpa kausayninpi tʼukuriy, payqa sinchʼita onqoykorqa (Job 2:7).
Wañupullaytaña munaspapis,
Dios Jallpʼapi kausarichinanpi atienekorqa.
Chayrayku nerqa: “Allin kanman ukhu pachaj atiynimpi pakaykunawayki [...].
Qan wajyariwankiman, noqataj uyarisuykiman.
Munakuywan qhawawankiman, imaraykuchus ruwawajniy kanki”, nispa (Job 14:13, 15).
Rikunchej jina,
Jobqa Dios kausarichimunanmanta mana iskayrayarqachu.
Diosqa, tumpamantawan Jobta chantá ashkha runasta ima kausarichimonqa, kay pachatataj uj paraisoman tukuchenqa (Lucas 23:42, 43).
Bibliaqa nin tukuy wañusqas kausarimunankuta (Hechos 24:15).
Jesuspis nillarqataj: “Ama chay nisqaymanta tʼukuychejchu.
Hora chayamonqa, chaypachataj tukuy pʼampasqa kajkunaqa Runaj Churimpa parlasqanta uyarenqanku [...], jatarimonqanku[taj]”, nispa (Juan 5:28, 29).
Jobqa chayta juntʼakojta rikonqa.
Chantá “imaynachus wayna kashaspa karqa, chay kikinman” tukonqa, cuerponpis “llampʼuman tukonqa wawaj aychanmantapis aswan llampʼuman” (Job 33:24, 25).
Dios, wañusqasta Jallpʼapi kausarichimunanpi creejkunapis chayta rikullanqankutaj.
Familiaykimanta pillapis wañuporqa chayqa kaypi yachakusqanchejqa, ichá llakiyniykita mana chinkachenqachu.
Jinapis,
Bibliaj nisqasninpi tʼukureyqa wañusqas kausarimunankuta suyakunaykipaj, llakisqa kaspapis ñaupajllamanpuni rinaykipaj yanapasonqa (1 Tesalonicenses 4:13).
Llakisqa kaspapis imastawanchus ruwanayki kasqanta, ¿yachayta munawajchu?
Ichapis kayta tapurikorqanki: “¿Imajtintaj Dios sajra kaj, ñakʼariy ima kananta saqen?”, nispa.
Jina kajtenqa,
Internetpi www.jw.org nisqaman yaykuspa,
Biblia nisqanman jina chay tapuyman kutichisqanta, yuyaychasqantataj tarinki.






    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Quechuapi publicaciones (2004-2017)
			
		

		
			
				
				Allinchanapaj
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Kay kamachiykunata kasukuy
			
			
				Datosta apaykachakusqanmanta
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



