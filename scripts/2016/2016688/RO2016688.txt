„NOI credem în Iehova, dar asta nu înseamnă neapărat că și copiii noștri vor crede”, a spus un cuplu căsătorit din Franța.
Ei au explicat: „Credința nu se moștenește.
Copiii noștri o cultivă puțin câte puțin”.
Un frate din Australia a scris: „A-ți ajuta copilul să cultive credință este, probabil, cel mai greu lucru pe care-l vei avea de făcut.
Trebuie să te folosești de toate metodele și instrumentele pe care le ai la dispoziție.
Ai putea crede că răspunsul pe care i l-ai dat la o anumită întrebare l-a mulțumit pe copilul tău.
Mai târziu însă, te trezești că îți pune din nou aceeași întrebare.
Răspunsurile care satisfac curiozitatea copilului tău astăzi s-ar putea să nu mai fie suficiente mâine.
Este posibil să fie nevoie să reiei cu regularitate unele subiecte”.
2 Dacă ești părinte, te simți uneori nepregătit pentru responsabilitatea de a-ți învăța și a-ți modela copiii ca să devină bărbați sau femei cu credință?
Adevărul e că, dacă ne-am baza pe propria înțelepciune, niciunul dintre noi n-am reuși să ne achităm bine de această sarcină (Ier. 10:23).
Dar putem avea succes dacă ne bazăm pe îndrumarea lui Dumnezeu.
Să analizăm patru lucruri pe care le puteți face pentru a vă ajuta copiii să cultive credință: 1) Străduiți-vă să vă cunoașteți bine copiii. 2) Învățați-i din inimă. 3) Folosiți ilustrări eficiente. 4) Fiți răbdători și rugați-vă pentru spirit sfânt.



CUNOAȘTEȚI-VĂ BINE COPIII

 3. Cum pot imita părinții exemplul lui Isus când își învață copiii?
3 Isus nu s-a temut să-i întrebe pe discipolii săi ce credeau (Mat. 16:13-15).
Imitați exemplul său. În timp ce vorbiți cu copiii voștri într-o atmosferă relaxată, invitați-i să-și exprime sentimentele.
Poate că au unele îndoieli.
Un frate de 15 ani din Australia a scris: „Tata vorbește de multe ori cu mine despre credința mea și mă ajută să gândesc logic.
El mă întreabă: «Ce spune Biblia?
Crezi ce spune ea?
De ce?».
El vrea să răspund cu propriile cuvinte, nu să repet pur și simplu ce spune el sau mama.
Pe măsură ce am crescut, a trebuit să dau răspunsuri mai ample”.
 4. De ce este important să luați în serios întrebările copilului vostru?
Dați un exemplu.
4 Dacă copilul vostru nu este sigur cu privire la o anumită învățătură, încercați să nu reacționați dur sau să intrați în defensivă.
Cu răbdare, ajutați-l să găsească răspunsuri la întrebările lui. „Luați în serios întrebările copilului vostru, a spus un tată.
Nu le considerați fără importanță și nu evitați un subiect doar pentru că vă vine greu să vorbiți despre el.” Considerați întrebările sincere ale copilului un indiciu că îl interesează un anumit lucru și că vrea să-l înțeleagă.
Când avea doar 12 ani,
Isus a pus întrebări profunde. (Citește Luca 2:46.) Un tânăr de 15 ani din Danemarca își amintește: „Când le-am spus că mă întrebam dacă avem religia adevărată, părinții mei au rămas calmi, chiar dacă și-au făcut, probabil, griji pentru mine.
Ei mi-au răspuns la toate întrebările cu ajutorul Bibliei”.
 5. Chiar dacă copiii par să aibă credință, ce ar trebui să facă părinții?
5 Străduiți-vă să vă cunoașteți bine copiii, adică să știți ce gândesc, ce simt și ce îi preocupă.
Nu presupuneți că ei au credință doar pentru că vin la întruniri și merg cu voi în predicare.
Purtați discuții spirituale în timp ce vă ocupați de activitățile zilnice.
Rugați-vă cu ei și pentru ei.
Căutați să aflați cu ce încercări ale credinței se confruntă și ajutați-i să le depășească.



ÎNVĂȚAȚI-I DIN INIMĂ

 6. De ce este important ca părinții să-și întipărească adevărul în inimă?
6 Ca învățător,
Isus a sensibilizat inima celor ce-l ascultau deoarece îl iubea pe Iehova, iubea Cuvântul lui Dumnezeu și îi iubea pe oameni (Luca 24:32; Ioan 7:46).
Aceeași iubire îi va ajuta și pe părinți să sensibilizeze inima copiilor lor. (Citește Deuteronomul 6:5-8; Luca 6:45.) Prin urmare, dragi părinți, studiați cu atenție și cu regularitate Biblia și publicațiile noastre.
Meditați la creație și citiți articolele din publicații care vorbesc despre acest subiect (Mat. 6:26, 28).
Astfel, veți dobândi mai multă cunoștință, vă veți spori aprecierea față de Iehova și veți fi mai bine pregătiți să vă învățați copiii (Luca 6:40).
7, 8. Care este rezultatul când un părinte are adevărul Bibliei în inima lui?
Dați un exemplu.
7 Când adevărul Bibliei este în inima voastră, veți dori să vorbiți despre el cu membrii familiei.
Nu faceți aceasta doar la închinarea în familie sau când vă pregătiți pentru întruniri, ci cu orice ocazie.
Aceste discuții nu trebuie să fie forțate, ci trebuie să aibă loc în mod firesc și spontan, ca parte a comunicării voastre de zi cu zi.
Doi părinți din Statele Unite vorbesc despre Iehova cu copiii lor când admiră natura sau când mănâncă ceva delicios. „Le amintim copiilor noștri de iubirea și de grija cu care Iehova a făcut tot ce ne-a oferit”, au spus ei.
Când lucrează în grădină împreună cu cele două fiice ale lor, doi părinți din Africa de Sud le vorbesc despre creație.
Ei discută despre unele lucruri uimitoare, cum ar fi modul în care germinează semințele și apoi cresc plantele. „Încercăm să sădim în inima fiicelor noastre un respect profund pentru viață și pentru uluitoarea ei complexitate”, spun acești părinți.
8 Un tată din Australia a profitat de o vizită la muzeu pentru a-l ajuta pe fiul său, care avea în jur de zece ani, să-și întărească credința în Dumnezeu și în creație. „Am văzut o expoziție de animale marine preistorice, numite amoniți și trilobiți, a spus tatăl.
Am fost uimiți să vedem că aceste animale dispărute erau frumoase, complexe și complete, cu nimic mai prejos decât animalele din prezent.
Deci, dacă viața a evoluat de la forme simple la forme mai complexe, de ce erau aceste animale preistorice atât de complexe?
Acest lucru m-a impresionat profund și am vorbit despre el cu fiul meu.”



FOLOSIȚI ILUSTRĂRI EFICIENTE

 9. De ce sunt eficiente ilustrările și cum a demonstrat o mamă lucrul acesta?
9 Isus a folosit deseori ilustrări.
Acestea stimulează gândirea, sensibilizează inima și sunt un ajutor pentru memorie (Mat. 13:34, 35).
Copiii au, de obicei, o imaginație bogată.
De aceea, folosiți cât mai multe ilustrări când vă instruiți copiii.
O mamă din Japonia a făcut lucrul acesta.
Când cei doi băieți ai ei aveau opt, respectiv zece ani, ea a vrut să-i învețe că modul în care a creat Iehova atmosfera pământului arată câtă grijă are el de noi. În acest sens, ea le-a dat lapte, zahăr și cafea.
Apoi l-a rugat pe fiecare să-i facă o cafea.
Ea spune: „Au pregătit-o cu multă grijă.
Când i-am întrebat de ce au fost atât de atenți, ei mi-au spus că au vrut să iasă exact așa cum îmi place mie.
Le-am explicat că Dumnezeu a amestecat gazele din atmosferă cu o grijă asemănătoare, ca să fie exact așa cum avem noi nevoie”.
Mama a folosit o ilustrare potrivită pentru vârsta lor și i-a implicat într-un mod în care probabil n-ar fi reușit dacă ar fi folosit o metodă de învățare pasivă.
Ei n-au uitat niciodată această lecție!





Vă puteți folosi de lucruri obișnuite pentru a vă ajuta copiii să creadă că există un Creator (Vezi paragraful 10)




10, 11. a) Ce ilustrare ai putea folosi pentru a-ți ajuta copilul să cultive credință în Dumnezeu? (Vezi imaginea de la începutul articolului.) b) Ce ilustrări ți se par eficiente?
10 Ce ilustrare ai putea folosi pentru a-ți ajuta copilul să cultive credință în Dumnezeu?
Ai putea face împreună cu el o prăjitură folosind o rețetă.
Explică-i de ce este important să respectați rețeta întocmai.
După aceea, dă-i un fruct, cum ar fi un măr, și întreabă-l: „Știai că mărul acesta a fost făcut după o «rețetă»?”.
Apoi taie mărul în două și dă-i o sămânță.
I-ai putea spune că rețeta a fost „scrisă” în sămânță, însă într-un limbaj mult mai complex decât cel dintr-o carte de bucate.
L-ai putea întreba: „Dacă rețeta pentru prăjitură a fost scrisă de cineva, atunci cine a scris rețeta mult mai complexă pentru măr?”.
Unui copil mai mare i-ai putea spune că rețeta pentru măr și, de fapt, pentru întregul pom în care a crescut acest fruct, a fost scrisă în ADN.
Apoi ați putea vedea împreună unele imagini și exemple de la paginile 10 la 20 ale broșurii Originea vieții – Cinci întrebări care merită să fie analizate.
11 Mulți părinți citesc cu copiii lor unele articole din revista Treziți-vă!, de la rubrica „Opera unui Proiectant?”.
Sau, dacă copiii sunt foarte mici, părinții folosesc această rubrică pentru a le transmite unele idei într-un mod simplu.
De exemplu, un cuplu din Danemarca a comparat avioanele cu păsările. „Avioanele arată exact ca niște păsări, au spus ei.
Dar pot avioanele să facă ouă, din care să iasă avioane micuțe?
Au nevoie păsările de piste de aterizare? Și cum este sunetul unui avion în comparație cu ciripitul unei păsări?
Așadar, cine este mai inteligent, proiectantul avioanelor sau Creatorul păsărilor?” Când porți astfel de discuții cu copilul tău și îi pui întrebări potrivite, îl poți ajuta să-și dezvolte „capacitatea de gândire” și să cultive credință în Dumnezeu (Prov. 2:10-12).
12. Cum îi pot ajuta ilustrările pe copii să cultive credință în Biblie?
12 Ilustrările eficiente te pot ajuta, de asemenea, să-i întărești copilului tău credința în Biblie.
De exemplu, citește-i Iov 26:7. (Citește.) Cum i-ai putea dovedi că acest verset a fost inspirat de Dumnezeu?
Nu te mulțumi să-i spui acest lucru.
Ar fi mai bine să-i stimulezi imaginația.
Spune-i că Iov a trăit cu mult înainte să existe telescoape și nave spațiale.
Apoi roagă-l să explice de ce i-ar putea fi greu cuiva să creadă că un obiect mare, cum este pământul, nu se sprijină pe nimic.
Copilul ar putea folosi o minge sau o piatră pentru a ilustra această idee, arătând că obiectele care au masă trebuie să se sprijine pe ceva.
O astfel de abordare l-ar putea ajuta pe copil să înțeleagă că Iehova a făcut ca în Biblie să fie consemnate unele realități cu mult înainte ca oamenii să le poată dovedi (Neem. 9:6).



AJUTAȚI-I SĂ ÎNȚELEAGĂ VALOAREA PRINCIPIILOR BIBLIEI

13, 14. Cum i-ar putea învăța părinții pe copiii lor valoarea principiilor Bibliei?
13 Este foarte important să-i învățați pe copiii voștri valoarea principiilor Bibliei. (Citește Psalmul 1:1-3.) Există mai multe modalități prin care puteți face acest lucru.
De exemplu, i-ați putea ruga pe copii să-și imagineze că vor merge să locuiască pe o insulă îndepărtată și că vor trebui să aleagă mai mulți oameni care să locuiască acolo împreună cu ei.
Apoi întrebați-i: „Ce calități ar trebui să aibă fiecare persoană, astfel încât toți să trăiască în pace și armonie?”.
După aceea, le-ați putea citi Galateni 5:19-23 ca să vadă ce fel de oameni dorește Iehova în lumea nouă.
14 În felul acesta, i-ați putea învăța pe copii două lucruri importante. În primul rând, normele lui Dumnezeu promovează adevărata pace și armonie. În al doilea rând, prin instruirea pe care ne-o oferă în prezent,
Iehova ne pregătește pentru viața în lumea nouă (Is. 54:13; Ioan 17:3).
I-ați putea ajuta să înțeleagă aceste idei alegând o experiență din publicațiile noastre.
De exemplu, căutați o relatare din seria de articole „Biblia schimbă viața oamenilor”, apărută în Turnul de veghe. Sau, dacă cineva din congregația voastră a făcut schimbări mari pentru a-i fi plăcut lui Iehova, ați putea să-l invitați la voi acasă și să-l rugați să vă povestească experiența lui.
Astfel de exemple dau viață principiilor Bibliei (Evr. 4:12).
15. Ce vă poate ajuta să vă instruiți copiii?
15 Când vă instruiți copiii, este foarte important să nu intrați într-o rutină. Încercați să vă folosiți imaginația.
Stimulați-le gândirea, ținând cont de vârsta lor.
Folosiți metode de predare interesante, astfel încât copiii să găsească plăcere în ceea ce învață și să-și întărească credința.
Un tată a spus: „Căutați mereu modalități noi de a aborda subiecte vechi”.



FIȚI RĂBDĂTORI, ÎNTĂRIȚI-VĂ CREDINȚA ȘI RUGAȚI-VĂ PENTRU SPIRIT SFÂNT

16. De ce este esențial să aveți răbdare când vă instruiți copiii?
Dați un exemplu.
16 Pentru a cultiva o credință puternică, avem nevoie de spiritul lui Dumnezeu (Gal. 5:22, 23).
La fel ca un fruct, credința are nevoie de timp pentru a crește.
Prin urmare, va trebui să fiți răbdători și perseverenți când vă instruiți copiii.
Un tată din Japonia, care are doi copii, a spus: „Eu și soția mea le-am acordat multă atenție copiilor noștri. Încă de când erau foarte mici, studiam cu ei câte 15 minute în fiecare zi, cu excepția zilelor în care aveam întruniri.
Cincisprezece minute nu erau prea mult nici pentru noi, nici pentru ei”.
Un supraveghetor de circumscripție a scris: „În adolescență am avut mult mai multe întrebări și îndoieli decât am exprimat în cuvinte.
De-a lungul timpului, la multe dintre ele am primit răspuns la întruniri ori cu ocazia studiului personal sau în familie.
Iată de ce este important ca părinții să continue să-i instruiască pe copiii lor”.





Dacă vreți să fiți învățători eficienți,
Cuvântul lui Dumnezeu trebuie să fie mai întâi în inima voastră (Vezi paragraful 17)




17. a) De ce este important ca părinții să-și întărească propria credință? b) Cum au reușit doi părinți să le dea un bun exemplu fiicelor lor?
17 Desigur, exemplul vostru de credință este foarte important.
Copiii vor observa ce faceți, iar aceasta va avea o influență bună asupra lor.
De aceea, continuați să vă întăriți propria credință.
Arătați-le copiilor voștri că Iehova este real pentru voi.
Când un soț și o soție din Insulele Bermude se îngrijorează de ceva, ei se roagă lui Iehova pentru îndrumare împreună cu copiii lor și îi îndeamnă și pe ei să se roage singuri. „De asemenea, îi spunem fiicei noastre mai mari: «Pune-ți toată încrederea în Iehova, fii mereu activă în serviciul său și nu te îngrijora peste măsură!».
Când vede cum se rezolvă lucrurile, ea este convinsă că Iehova ne ajută.
Acest lucru i-a întărit foarte mult credința în Dumnezeu și în Biblie.”
18. Ce nu ar trebui să uite părinții?
18 Părinți, nu uitați niciodată că nu vă puteți obliga copiii să aibă credință.
Voi puteți să plantați și să udați, dar numai Dumnezeu poate face să crească (1 Cor. 3:6).
Așadar, rugați-vă pentru spirit sfânt și străduiți-vă din răsputeri să-i instruiți pe dragii voștri copii. Și puteți fi siguri că Iehova vă va binecuvânta din plin! (Ef. 6:4)






    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Publicații în limba română (2000-2017)
			
		

		
			
				
				Preferințe
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Termeni de utilizare
			
			
				Politică de confidențialitate
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



