Diese außergewöhnlichen Ereignisse sind in den vier Evangelien des Neuen Testaments überliefert.
Aber hat sich das alles wirklich so abgespielt?
Diese Frage ist von großer Tragweite.
Denn wenn es sich nie ereignet hat, dann ist der christliche Glaube bedeutungslos.
Und auf ein ewiges Leben im Paradies zu hoffen ist nichts weiter als nur ein schöner Traum (1. Korinther 15:14).
Doch wenn dieser Bericht wahr ist, dann kann jeder einzelne Mensch einer großartigen Zukunft entgegensehen.
Daher die Frage:
Sind die Evangelien Tatsachenberichte oder doch nur frei erfundene Geschichten?



DIE BEWEISLAGE

Im Gegensatz zu Sagen und Legenden sind die Evangelien sogar bis ins kleinste Detail genau.
Die vielen Orte, die darin erwähnt werden, gab es wirklich und gibt es teilweise heute noch.
Außerdem berichten die Evangelien von realen Personen, deren Existenz von Historikern bestätigt wird (Lukas 3:1, 2, 23).
Jesus wird auch in nichtbiblischen Aufzeichnungen des 1. und 2. Jahrhunderts erwähnt.* Was die Evangelien über seinen Tod berichten, stimmt mit den Hinrichtungsmethoden der Römer in dieser Zeit überein.
Außerdem werden sämtliche Ereignisse sehr sachlich und offen geschildert, sogar wenn es um negative Seiten der Jünger geht (Matthäus 26:56; Lukas 22:24-26; Johannes 18:10, 11).
All das zeigt deutlich, dass die Verfasser der Evangelien sehr ehrlich und präzise über Jesus und sein Leben berichteten.



WURDE JESUS WIRKLICH AUFERWECKT?

Es ist zwar allgemein anerkannt, dass Jesus eine historische Person ist, doch es wird oft infrage gestellt, ob er auch wirklich auferweckt wurde.
Selbst die Apostel wollten erst nicht glauben, dass Jesus wieder lebte (Lukas 24:11).
Doch all ihre Zweifel verschwanden, als sie und andere Jünger den auferstandenen Jesus bei verschiedenen Gelegenheiten mit eigenen Augen sahen.
Einmal gab es sogar mehr als 500 Augenzeugen (1. Korinther 15:6).
Mutig erzählten die Jünger allen, dass Jesus wieder lebte — selbst denen, die für seine Hinrichtung verantwortlich waren.
Dabei riskierten sie, eingesperrt oder sogar getötet zu werden (Apostelgeschichte 4:1-3, 10, 19, 20; 5:27-32).
Wären so viele Jünger so mutig gewesen, wenn sie sich nicht hundertprozentig sicher gewesen wären?
Die Tatsache, dass Jesus auferstanden ist, bildet die treibende Kraft hinter dem großen Einfluss, den das Christentum auf die ganze Welt hatte und noch hat.
Was in den Evangelien über den Tod und die Auferstehung Jesu steht, weist alle Merkmale auf, die man von einem glaubwürdigen historischen Bericht erwartet.
Wer sie aufmerksam liest, kann sich selbst davon überzeugen, dass sich alles genau so abgespielt hat.
Und zu verstehen, warum es so kommen musste, gibt noch mehr Sicherheit.
Damit beschäftigt sich der nächste Artikel.



So schrieb Tacitus, der im Jahr 55 geboren wurde: „Dieser Name [Christen] stammt von Christus, der unter Tiberius vom Prokurator Pontius Pilatus hingerichtet worden war.“ Auch Sueton (1. Jh.), der jüdische Historiker Josephus (1. Jh.) und Plinius der Jüngere (frühes 2. Jh.),
Statthalter von Bithynien, erwähnen Jesus.

Warum gibt es außerhalb der Bibel so wenige Belege?
Jesus hat die Weltgeschichte entscheidend geprägt.
Müsste es da nicht noch mehr außerbiblische Belege für sein Leben geben?
Nicht unbedingt.
Einerseits muss man bedenken, dass die Evangelien vor fast 2 000 Jahren geschrieben wurden.
Nicht viele Schriften aus dieser Zeit haben so lange überdauert (1. Petrus 1:24, 25).
Andererseits hätten seine vielen Gegner sicher nichts aufgeschrieben, was die Berichte über ihn glaubwürdig gemacht hätte.
Petrus, einer seiner Apostel, erklärte: „Diesen hat Gott am dritten Tag auferweckt und ihn offenbar werden lassen, nicht dem ganzen Volk, sondern den von Gott zuvor bestimmten Zeugen, uns, die wir mit ihm aßen und tranken, nachdem er von den Toten auferstanden war“ (Apostelgeschichte 10:40, 41).
Warum nicht dem ganzen Volk?
Wie aus dem Matthäusevangelium hervorgeht, wollten Jesu religiöse Gegner seine Auferstehung unbedingt totschweigen (Matthäus 28:11-15).
Wollte Jesus selbst seine Auferstehung geheim halten?
Nein, denn Petrus sagte weiter: „Jesus gab uns den Auftrag, allen Menschen zu sagen und zu bezeugen, dass Gott ihn als Richter über die Lebenden und die Toten eingesetzt hat“ (Apostelgeschichte 10:42, Hoffnung für alle).
Das taten sie, und das tun echte Nachfolger Jesu heute noch.





    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Deutsche Publikationen (2000-2017)
			
		

		
			
				
				Einstellungen
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Nutzungsbedingungen
			
			
				Datenschutzerklärung
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



