Im Lukasevangelium wird erwähnt, dass Jesus eine Buchrolle mit dem Bibelbuch Jesaja öffnete, daraus vorlas und wieder zusammenrollte.
Und auch Johannes spricht von einer Buchrolle.
Er sagt gegen Ende seines Evangeliums, dass er nicht alle Zeichen und Wunder Jesu in seiner Schriftrolle aufschreiben konnte (Lukas 4:16-20; Johannes 20:30; 21:25).
Wie wurden Buchrollen hergestellt?
Man klebte Leder-,
Pergament- oder Papyrusstücke zu einem Streifen zusammen.
Dieser Streifen konnte dann um einen Stab gewickelt werden, wobei die beschriebene Seite nach innen zeigte. Über die ganze Länge der Rolle wurde in schmalen Spalten geschrieben.
Lange Buchrollen konnten auch an beiden Enden Stäbe zum Rollen haben.
Um die gesuchte Passage zu finden, rollte man die Schriftrolle mit der einen Hand ab und mit der anderen Hand auf.
In The Anchor Bible Dictionary wird gesagt: „Der Vorteil einer Buchrolle war, dass sie einerseits lang genug war [oftmals bis zu 10 m], um ein ganzes Buch zu fassen, andererseits aber auch kompakt, wenn sie zusammengerollt war.“ Das Lukasevangelium wäre zum Beispiel ungefähr 9,5 Meter lang gewesen.
Bei manchen Schriftrollen wurden die Ränder gerade geschnitten, mit einem Bimsstein geglättet und anschließend gefärbt.



Wer war wahrscheinlich mit den „Oberpriestern“ gemeint, die in den Christlichen Griechischen Schriften erwähnt werden?






Von dem Zeitpunkt an, als im Volk Israel die Priesterschaft eingesetzt wurde, amtierte jeweils nur ein Mann als Hoher Priester.
Ursprünglich war es ein Amt auf Lebenszeit (4. Mose 35:25).
Der erste Hohe Priester war Aaron.
Gewöhnlich ging dann diese ehrenvolle Stellung auf den Erstgeborenen über (2. Mose 29:9).
Viele der Nachkommen Aarons dienten als Priester, doch nur relativ wenige als Hohe Priester.
Als Israel unter Fremdherrschaft geriet, setzten nichtisraelitische Herrscher die jüdischen Hohen Priester willkürlich ein oder auch ab.
Anscheinend wurden die Nachfolger jedoch fast immer aus einer Handvoll privilegierter Familien ausgesucht, von denen die meisten Nachkommen Aarons waren.
Mit der Bezeichnung „Oberpriester“ waren offensichtlich führende Mitglieder der Priesterschaft gemeint.
Zu den Oberpriestern gehörten wohl die Vorsteher der 24 Priesterabteilungen, angesehene Mitglieder aus den Familien der Hohen Priester, aber auch abgesetzte Hohe Priester, wie zum Beispiel Annas (1. Chronika 24:1-19; Matthäus 2:4; Markus 8:31; Apostelgeschichte 4:6).






    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Deutsche Publikationen (2000-2017)
			
		

		
			
				
				Einstellungen
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Nutzungsbedingungen
			
			
				Datenschutzerklärung
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



