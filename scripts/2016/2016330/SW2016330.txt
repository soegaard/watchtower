“Mungu wa mbinguni atausimamisha ufalme ambao hautaharibiwa kamwe. . . .
Utazivunja na kuzikomesha falme hizi zote [za wanadamu].” (Danieli 2:44)
Ufalme wa Mungu ni serikali halisi.
MAMBO MENGINE TUNAYOJIFUNZA KATIKA BIBLIA
  Ufalme wa Mungu unatawala kutoka mbinguni. —Mathayo 10:7; Luka 10:9.

  Mungu anatumia Ufalme huo kutimiza mapenzi yake mbinguni na duniani.—Mathayo 6:10.





Ufalme wa Mungu utakuja lini?

UNGEJIBUJE?
  Hakuna anayejua

  Hivi karibuni

  Hautakuja kamwe


BIBLIA INASEMA HIVI:
“Hii habari njema ya Ufalme itahubiriwa katika dunia yote inayokaliwa ili kuwa ushahidi kwa mataifa yote; na ndipo ule mwisho utakapokuja.” (Mathayo 24:14)
Baada ya habari njema kuhubiriwa kikamili,
Ufalme huo utakuja na kukomesha mfumo huu mwovu.
NI NINI KINGINE TUNACHOJIFUNZA KATIKA BIBLIA?
  Hakuna hata mtu mmoja duniani anayejua kikamili wakati ambao Ufalme wa Mungu utakuja.—Mathayo 24:36.

  Unabii wa Biblia unaonyesha kwamba Ufalme huo utakuja hivi karibuni.—Mathayo 24:3, 7, 12.





Kwa habari zaidi, soma sura ya 8 ya kitabu hiki, Biblia Inafundisha Nini Hasa?, kilichochapishwa na Mashahidi wa Yehova
Pia kinapatikana kwenye www.jw.org/sw




    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Machapisho ya Kiswahili (2000-2017)
			
		

		
			
				
				Mapendezi
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Masharti ya Utumiaji
			
			
				Sera ya Faragha
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



