ÎN ARTICOLUL precedent am înțeles cum au ajuns creștinii fideli captivi în Babilonul cel Mare.
Vestea bună este că ei nu aveau să rămână pentru totdeauna în această captivitate. În Biblie,
Dumnezeu i-a poruncit poporului său ‘să iasă’ din imperiul mondial al religiei false.
Această poruncă ar fi fost lipsită de sens dacă ei nu s-ar fi putut elibera din Babilonul cel Mare. (Citește Revelația 18:4.) Suntem nerăbdători să aflăm când s-a eliberat definitiv poporul lui Dumnezeu de sub jugul babilonian! Înainte de toate însă, trebuie să aflăm răspunsul la următoarele întrebări:
Ce poziție au adoptat Studenții în Biblie față de Babilonul cel Mare înainte de 1914?
Cât de zeloși au fost frații noștri în predicarea veștii bune pe parcursul Primului Război Mondial?
A existat vreo legătură între necesitatea ca ei să fie corectați în acea perioadă și captivitatea lor în Babilonul cel Mare?



„CĂDEREA BABILONULUI”

 2. Ce poziție au luat primii Studenți în Biblie față de religia falsă înainte de Primul Război Mondial?
2 În deceniile premergătoare Primului Război Mondial, Charles Taze Russell și alți Studenți în Biblie și-au dat seama că organizațiile creștinătății nu predau adevărul biblic.
Prin urmare, s-au hotărât să rupă orice legătură cu religia falsă.
Astfel, încă din noiembrie 1879, în Turnul de veghe al Sionului ei și-au exprimat cu fermitate poziția față de Babilonul cel Mare: „Trebuie să condamnăm orice biserică ce se pretinde a fi o fecioară castă căsătorită cu Cristos, dar care, în realitate, se unește cu lumea (fiara) și este susținută de aceasta, deoarece, în limbaj scriptural, ea este o biserică prostituată”. (Citește Revelația 17:1, 2.)
 3. Ce au făcut Studenții în Biblie pentru a arăta că înțeleseseră necesitatea de a ieși din religia falsă? (Vezi imaginea de la începutul articolului.)
3 Bărbații și femeile cu teamă de Dumnezeu știau ce aveau de făcut. Înțeleseseră că nu puteau avea binecuvântarea lui Dumnezeu dacă continuau să susțină organizații religioase false.
De aceea, mulți Studenți în Biblie au trimis scrisori de retragere din biserică.
Unii dintre ei chiar au citit public scrisorile cu ocazia slujbelor.
Când li se interzicea să facă acest lucru, ei trimiteau câte un exemplar tuturor enoriașilor.
Ei voiau să se știe că nu mai aveau nimic de-a face cu religia falsă! Într-o altă perioadă istorică, o astfel de acțiune îndrăzneață i-ar fi costat scump.
Dar, spre sfârșitul secolului al XIX-lea, în multe țări,
Biserica începuse să piardă susținerea Statului, iar cetățenii erau liberi să discute despre religie și să-și exprime dezacordul față de Biserică fără să se teamă de represalii.
 4. Ce poziție a adoptat poporul lui Dumnezeu față de Babilonul cel Mare în timpul Primului Război Mondial?
4 Studenții în Biblie au înțeles că nu era suficient să le spună rudelor, prietenilor apropiați și membrilor bisericii lor ce atitudine adoptaseră față de religia falsă.
Ei voiau ca întreaga lume să știe că Babilonul cel Mare era, din punct de vedere religios, o prostituată.
De aceea, în decembrie 1917 și în primele luni din 1918, cele câteva mii de Studenți în Biblie au distribuit cu zel 10 000 000 de exemplare ale unei publicații care conținea un articol intitulat „Căderea Babilonului”.
Acest articol demasca vehement creștinătatea.
Așa cum ne putem imagina, clerul s-a înfuriat, dar Studenții în Biblie nu s-au lăsat intimidați, ci au continuat această lucrare importantă, fiind hotărâți să asculte „mai mult de Dumnezeu ca stăpânitor decât de oameni” (Fap. 5:29).
Așadar, ce concluzie putem trage?
Că, în timpul războiului, acești creștini nu au intrat în captivitatea Babilonului cel Mare, ci se eliberau de sub jugul lui, ajutându-i și pe alții să facă același lucru.



ZELOȘI ÎN TIMPUL PRIMULUI RĂZBOI MONDIAL

 5. Ce dovezi avem că frații au fost foarte zeloși pe parcursul Primului Război Mondial?
5 Până acum am crezut că Iehova a fost nemulțumit de slujitorii săi întrucât ei nu au participat cu zel la predicarea veștii bune pe parcursul Primului Război Mondial. Și am tras concluzia că, din acest motiv,
Iehova a permis Babilonului cel Mare să-i ia în captivitate o scurtă perioadă.
Totuși, frații și surorile fidele care i-au slujit lui Dumnezeu între anii 1914 și 1918 au explicat mai târziu că au făcut tot ce au putut pentru a continua să predice.
Există dovezi concludente în sprijinul mărturiei lor.
O cunoaștere mai exactă a istoriei noastre teocratice ne ajută să înțelegem mai bine anumite evenimente consemnate în Biblie.
 6, 7. a) Ce obstacole au trebuit să depășească Studenții în Biblie pe parcursul Primului Război Mondial? b) Dați exemple care să evidențieze zelul lor.
6 De fapt, în timpul Primului Război Mondial (1914-1918),
Studenții în Biblie au predicat cu zel, dar au întâmpinat și obstacole.
De exemplu, ei nu erau obișnuiți să predice doar cu Biblia, lucrarea lor constând, în principal, în distribuirea de literatură biblică.
De aceea, la începutul anului 1918, când autoritățile laice au interzis cartea Taina împlinită, multor frați le-a fost greu să predice fără ea, deoarece erau obișnuiți să lase această carte să vorbească în locul lor.
Un alt obstacol a fost gripa spaniolă, care a luat proporții epidemice în 1918, făcând ravagii.
Amploarea acestei epidemii ucigătoare a îngreunat munca vestitorilor, limitându-le libertatea de a se deplasa dintr-un loc în altul. În pofida acestor obstacole, precum și a altora,
Studenții în Biblie, ca grup, au făcut tot ce-au putut ca să predice vestea bună.





Studenții în Biblie erau plini de zel! (Vezi paragrafele 6 și 7)




7 În 1914,
Studenții în Biblie, puțini la număr, au prezentat „Foto-Drama Creațiunei” la peste 9 000 000 de oameni.
Foto-Drama, o producție realizată din imagini filmate și diapozitive, sincronizate cu o coloană sonoră, prezenta istoria omenirii de la creare până la sfârșitul Domniei de O Mie de Ani.
Pentru acea vreme,
Foto-Drama era o capodoperă. În plus, numărul oamenilor care au vizionat-o în 1914 depășește numărul proclamatorilor Regatului din prezent!
De asemenea, rapoartele arată că, în 1916, numărul celor care au participat la întrunirile publice din Statele Unite a fost de 809 393, iar în 1918, a crescut la 949 444.
Da,
Studenții în Biblie din acea vreme au fost cu adevărat zeloși!
 8. Cum au fost satisfăcute necesitățile spirituale ale fraților în Primul Război Mondial?
8 În timpul Primului Război Mondial, frații cu răspundere au făcut tot ce-au putut pentru ca Studenții în Biblie să beneficieze de hrană spirituală și de încurajare.
Astfel, aceștia au primit puterea de a continua să predice.
Richard Barber, care a predicat cu mult zel în acea perioadă, își amintește: „Am reușit să păstrăm în teritoriu câțiva supraveghetori itineranți, să distribuim în continuare Turnul de veghe și să-l trimitem și în Canada, unde era interzis.
Am avut privilegiul să trimit prin poștă exemplare în format de buzunar ale cărții Taina împlinită câtorva frați cărora le fuseseră confiscate exemplarele personale.
Fratele Rutherford a cerut să organizăm congrese în mai multe orașe din vestul Statelor Unite și să trimitem vorbitori care să-i încurajeze cât mai mult pe frați”.



ERA NEVOIE DE CORECTARE

 9. a) De ce a avut nevoie poporul lui Dumnezeu de corectare între anii 1914 și 1919? b) Chiar dacă Studenții în Biblie au avut nevoie de corectare, ce concluzie ar fi greșit să tragem?
9 Dar nu tot ce au făcut Studenții în Biblie între anii 1914-1919 s-a armonizat cu principiile biblice.
Deși sinceri, frații nu au avut o înțelegere clară cu privire la supunerea față de autoritățile laice (Rom. 13:1).
Iar, ca grup, ei nu și-au păstrat mereu neutralitatea în timpul războiului.
De exemplu, când președintele de atunci al Statelor Unite a decretat ziua de 30 mai 1918 zi de rugăciune pentru pace, Turnul de veghe i-a îndemnat pe Studenții în Biblie să susțină acest demers.
Unii au cumpărat obligațiuni pentru a susține financiar războiul, în timp ce alții chiar au mers în tranșee cu puști și baionete.
Deși Studenții în Biblie aveau nevoie de corectare, ar fi greșit să tragem concluzia că, din acest motiv, ei ar fi fost luați captivi în acea perioadă în Babilonul cel Mare. În realitate, ei înțeleseseră că era necesar să se separe de religia falsă, iar pe parcursul Primului Război Mondial ieșiseră aproape de tot de sub jugul imperiului mondial al religiei false. (Citește Luca 12:47, 48.)
10. Cum au arătat Studenții în Biblie respect față de caracterul sacru al vieții?
10 Deși nu înțelegeau semnificația neutralității creștine așa cum o înțelegem noi astăzi,
Studenții în Biblie știau clar că Biblia interzice să iei viața cuiva.
De aceea, chiar și acei frați, puțini la număr, care au pus mâna pe arme și au mers în tranșee în Primul Război Mondial au refuzat să le folosească pentru a curma vieți omenești.
Unii dintre aceștia au fost trimiși în primele linii ale frontului ca să fie omorâți.
11. Cum au reacționat autoritățile guvernamentale când Studenții în Biblie au refuzat să lupte în război?
11 Văzând poziția fraților față de război, chiar dacă, pe atunci, ei nu înțelegeau în totalitate ce însemna neutralitatea,
Diavolul s-a înfuriat și ‘a urzit nenorociri prin decrete’ (Ps. 94:20).
Un general din armata Statelor Unite, pe nume James Franklin Bell, le-a spus fraților Rutherford și Van Amburgh că Departamentul de Justiție al Statelor Unite a înaintat spre aprobarea Congresului american un proiect de lege privind aplicarea pedepsei capitale în cazul persoanelor care refuzau să lupte în război.
Acest proiect îi viza cu precădere pe Studenții în Biblie.
Clocotind de furie, generalul Bell i-a spus fratelui Rutherford: „Proiectul de lege n-a fost aprobat pentru că Wilson [președintele SUA] l-a respins; dar știm cum să vă prindem și o vom face!”.
12, 13. a) De ce au fost condamnați cei opt frați la ani mulți de închisoare? b) Le-a slăbit acestor frați închiși hotărârea de a asculta de Iehova?
Explicați.
12 Autoritățile și-au dus la îndeplinire amenințarea.
Frații Rutherford,
Van Amburgh și alți șase reprezentanți ai Societății Watch Tower au fost arestați.
Când a pronunțat sentința, judecătorul a spus: „Propaganda religioasă făcută de acești bărbați este mai periculoasă decât o divizie de soldați germani.
Atitudinea lor este o ofensă nu numai pentru magistrați și pentru Serviciul Secret de Informații al Armatei Statelor Unite, ci și pentru toți miniștrii religioși. Trebuie să fie aspru pedepsiți” (Faith on the March, de A. Macmillan, p. 99). Și așa s-a întâmplat!
Cei opt Studenți în Biblie au fost condamnați la ani grei de închisoare în penitenciarul federal din Atlanta,
Georgia. Însă, la sfârșitul războiului, ei au fost eliberați, iar acuzațiile împotriva lor au fost retrase.
13 Deși se aflau în închisoare, acești opt frați au fost hotărâți să respecte Scripturile, atât cât le înțelegeau ei la vremea aceea. Într-o cerere de grațiere adresată președintelui Statelor Unite, ei au scris: „Așa cum reiese din Scripturi, voința lui Dumnezeu este «să nu ucizi».
Prin urmare, orice membru al Asociației [Studenților Internaționali în Biblie] care încalcă intenționat legământul consacrării va pierde favoarea lui Dumnezeu, atrăgându-și chiar distrugerea.
Deci membrii Asociației nu pot să participe în mod voit sau conștient la uciderea ființelor umane”.
Ce cuvinte îndrăznețe!
Era clar că frații nu aveau intenția de a face compromis!



ÎN SFÂRȘIT,
LIBERTATE!

14. Explicați cu ajutorul Scripturilor evenimentele petrecute în perioada 1914-1919.
14 În cartea Maleahi este descris timpul când „fiii lui Levi”, unși de spirit, aveau să fie purificați. (Citește Maleahi 3:1-3.) Pe parcursul acestei perioade (1914 – începutul lui 1919),
Iehova Dumnezeu, „adevăratul Domn”, însoțit de Isus Cristos, „mesagerul legământului”, a venit la templul spiritual pentru a-i inspecta pe cei care slujeau acolo.
După ce a primit disciplinarea necesară, poporul curățat al lui Iehova a fost gata să-și asume o nouă responsabilitate în lucrarea lui Iehova.
De asemenea, în 1919,
Isus a numit ‘un sclav fidel și prevăzător’ care să furnizeze hrană spirituală casei credinței (Mat. 24:45).
Poporul lui Dumnezeu se eliberase, în sfârșit, de sub jugul Babilonului cel Mare.
De atunci încoace, prin bunătatea nemeritată a lui Iehova, poporul său înțelege tot mai bine voința lui Dumnezeu și îl iubește din ce în ce mai mult pe Tatăl ceresc.
Cât de recunoscători sunt slujitorii lui Iehova pentru aceste binecuvântări![1]
15. Cum putem să ne arătăm recunoștința față de Iehova pentru că ne-a eliberat din Babilonul cel Mare?
15 Suntem foarte bucuroși că am fost eliberați din captivitatea Babilonului cel Mare! Încercarea lui Satan de a șterge adevăratul creștinism de pe fața pământului a eșuat în mod lamentabil.
Totuși, nu ar trebui să pierdem din vedere motivul pentru care Iehova ne-a acordat această libertate (2 Cor. 6:1). În prezent, milioane de oameni sinceri se află în captivitatea religiei false.
Ei au nevoie de ajutorul nostru pentru a se elibera.
Așadar, urmând exemplul fraților noștri din secolul trecut, să facem tot ce ne stă în putință ca să-i ajutăm!



[1] (paragraful 14)
Există multe asemănări între cei 70 de ani de captivitate a evreilor în Babilon și ceea ce li s-a întâmplat creștinilor după apariția apostaziei.
Cu toate acestea, nu putem considera captivitatea evreilor o prefigurare a situației prin care au trecut creștinii.
Un motiv ar fi acela că cele două perioade de captivitate au durate diferite.
Prin urmare, nu ar trebui să căutăm paralele profetice pentru fiecare aspect al captivității evreilor, ca și cum aceste aspecte ar trebui să se regăsească în ceea ce li s-a întâmplat creștinilor unși în perioada premergătoare anului 1919.




    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Publicații în limba română (2000-2017)
			
		

		
			
				
				Preferințe
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Termeni de utilizare
			
			
				Politică de confidențialitate
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



