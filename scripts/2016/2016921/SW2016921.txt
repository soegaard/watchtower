MUDA fulani kabla ya hapo, nilikuwa nimeanza kuipenda kweli ya Biblia.
Mama alikuwa na machapisho ya Biblia, na niliyafurahia sana, hasa picha za machapisho hayo.
Baba hakutaka mama anieleze mambo aliyokuwa akijifunza.
Hata hivyo, nilikuwa mdadisi na niliuliza maswali, kwa hiyo mama alinifundisha nyakati ambazo baba hakuwa nyumbani.
Kwa sababu hiyo, mimi pia nilitaka kujiweka wakfu kwa Yehova.
Nilibatizwa huko Blackpool,
Uingereza, mwaka 1943 nikiwa na umri wa miaka kumi.



NAANZA KUMTUMIKIA YEHOVA

Baada ya kubatizwa, mimi na mama tulishiriki kwa ukawaida katika utumishi wa shambani.
Tulitumia gramafoni kutangaza ujumbe wa Biblia.
Vinanda hivyo vilikuwa vikubwa na vilikuwa na uzito wa kilo nne na nusu hivi.
Hebu wazia mvulana mdogo kama mimi akivuta mzigo mzito hivyo!
Nilitaka kuwa painia hata kabla ya kufikisha umri wa miaka 14.
Mama aliniambia nizungumze kwanza na mtumishi wa akina ndugu (sasa anaitwa mwangalizi wa mzunguko).
Mwangalizi huyo alipendekeza kwamba nijaribu kupata ustadi fulani ili nijitegemeze katika utumishi wa painia, nami nikafanya hivyo.
Baada ya kufanya kazi kwa miaka miwili, nilimwambia mwangalizi mwingine wa mzunguko kuhusu tamaa yangu.
Mwangalizi huyo aliniambia, “Usisite, anza mara moja!”
Hivyo, katika Aprili 1949, mimi na mama tuliuza baadhi ya samani na kuwagawia watu vitu vilivyosalia katika nyumba tuliyokuwa tumekodi. Kisha tukahamia Middleton, karibu na jiji la Manchester, ambako tulianza kufanya upainia.
Baada ya miezi minne, nilitafuta mwandamani wa kufanya upainia naye.
Ofisi ya tawi ilituambia tuhamie kutaniko jipya lililokuwa limeanzishwa huko Irlam.
Lakini mama na dada mwingine walifanya upainia katika kutaniko lingine.
Ingawa nilikuwa na umri wa miaka 17 tu, mimi na mwandamani wangu tulipewa jukumu la kuongoza mikutano kwa sababu ndugu waliostahili walikuwa wachache katika kutaniko hilo jipya.
Baadaye, niliombwa nihamie kutaniko la Buxton, ambalo lilikuwa na wahubiri wachache na lilihitaji msaada.
Sikuzote, mimi huona kwamba mambo hayo yalikuwa mazoezi kwa ajili ya migawo iliyofuata.





Tukiwa pamoja na wengine, tunatangaza hotuba ya watu wote huko Rochester,
New York, 1953




Mnamo 1951, nilijaza ombi la kuhudhuria Shule ya Gileadi.
Hata hivyo, katika Desemba 1952, niliambiwa nijiunge na jeshi.
Kwa kuwa nilikuwa mhudumu wa wakati wote, niliomba nisijiunge na jeshi, lakini mahakama haikukubali kwamba nilikuwa mhudumu wa kidini na hivyo nikahukumiwa kifungo cha miezi sita gerezani.
Nikiwa huko, nilipata mwaliko wa kuhudhuria darasa la 22 la Shule ya Gileadi.
Kwa hiyo katika Julai 1953, niliabiri meli iliyoitwa Georgic, kuelekea jiji la New York.
Nilipofika, nilihudhuria kusanyiko la New World Society Assembly la mwaka 1953.
Kisha nikasafiri kwa gari-moshi hadi South Lansing,
New York, ambako shule hiyo ilikuwa ikifanyiwa.
Kwa kuwa nilikuwa nimetoka gerezani hivi karibuni, nilikuwa na pesa kidogo.
Niliposhuka kutoka kwenye gari-moshi, nilihitaji kupanda basi la kunipeleka South Lansing.
Sikuwa na pesa za kulipa nauli ndogo ya basi, hivyo ilinibidi nimwombe msafiri mwenzangu nauli.



MGAWO KATIKA NCHI NYINGINE

Shule ya Gileadi iliandaa mazoezi mazuri ya kutusaidia “[kuwa] mambo yote kwa watu wa namna zote” tukiwa katika utumishi wa umishonari. (1 Kor. 9:22)
Tukiwa watatu, mimi,
Paul Bruun, na Raymond Leach, tulipewa mgawo wa kutumikia nchini Ufilipino.
Tulihitaji kusubiri miezi kadhaa kabla ya kupata viza.
Kisha tukaanza safari ya siku 47 kwa meli kupitia jiji la Rotterdam,
Bahari ya Mediterania,
Mfereji wa Suez,
Bahari ya Hindi,
Malaysia, na Hong Kong.
Mwishowe tulifika jiji la Manila Novemba 19, 1954.





Mimi na mmishonari mwenzangu Raymond Leach, tulisafiri siku 47 kwa meli hadi Ufilipino




Kisha tukaanza kuzoea watu, nchi, na lugha mpya.
Mwanzoni tulipewa mgawo katika kutaniko la Quezon City ambako wakaaji wengi walizungumza Kiingereza.
Kwa hiyo, baada ya miezi sita, tulijua maneno machache tu katika lugha ya Tagalog.
Mgawo wetu uliofuata ulitatua tatizo hilo.
Siku moja katika Mei 1955 tulipofika nyumbani baada ya utumishi, mimi na Ndugu Leach tulipata barua nyingi chumbani mwetu.
Tulikuwa tumepata mgawo mpya wa kutumika tukiwa waangalizi wa mzunguko.
Nilikuwa na umri wa miaka 22 tu, lakini mgawo huo ulinipa nafasi za “[kuwa] mambo yote kwa watu wa namna zote” katika njia ambazo sikutarajia.





Nikitoa hotuba ya watu wote katika kusanyiko la mzunguko la lugha ya Bicol




Kwa mfano, nilitoa hotuba yangu ya kwanza ya hadharani nikiwa mwangalizi wa mzunguko mbele ya duka la kijijini.
Punde si punde, nikajua kwamba siku hizo nchini Ufilipino kulikuwa na desturi ya kutoa hotuba za watu wote katika maeneo ya hadharani!
Nilipotembelea makutaniko mbalimbali katika mzunguko, nilitoa hotuba katika maeneo yaliyotengwa kwa ajili ya tafrija, sokoni, mbele ya majumba ya manispaa, viwanja vya mpira wa kikapu, bustanini, na mara nyingi mitaani.
Pindi moja nikiwa katika jiji la San Pablo City, mvua kubwa ilinizuia kutoa hotuba sokoni, kwa hiyo nikawapendekezea ndugu wenye madaraka nitoe hotuba hiyo kwenye Jumba la Ufalme.
Baadaye, ndugu hao waliniuliza ikiwa hotuba hiyo ingehesabiwa kuwa ya hadharani kwa kuwa haikutolewa hadharani!
Sikuzote niliishi pamoja na akina ndugu.
Ingawa nyumba zao zilikuwa za hali ya chini, nyakati zote zilikuwa safi.
Mara nyingi nililalia mkeka kwenye sakafu ya mbao.
Bafu hazikuwa na faragha ya kutosha, kwa hiyo nilijifunza kuoga kwa busara licha ya kufanya hivyo hadharani.
Nilisafiri kwa jeepney na basi, na nyakati fulani kwa mashua nilipoenda kwenye visiwa vingine.
Katika miaka yangu yote ya utumishi, sijawahi kununua gari.
Nilifaulu kujifunza lugha ya Tagalog kwa kuhubiri na kutembelea makutaniko.
Hakukuwa na mpango rasmi wa kunifunza lugha hiyo, kwa hiyo nilijifunza kwa kuwasikiliza akina ndugu tulipokuwa kwenye utumishi wa shambani na kwenye mikutano.
Ndugu walitaka kunisaidia nijifunze lugha hiyo, nami nilithamini maelezo waliyotoa kwa unyoofu na jinsi walivyonifundisha kwa subira.
Kadiri miaka ilivyosonga, ndivyo nilivyohitaji kufanya marekebisho zaidi kwa sababu ya kupewa migawo mipya.
Mnamo 1956 Ndugu Nathan Knorr alipotutembelea, nilipewa mgawo katika idara ya uhusiano na umma katika kusanyiko la kitaifa.
Sikufahamu chochote kuhusu kazi hiyo, hivyo wengine walijitolea kunisaidia.
Miezi kadhaa baadaye, kusanyiko lingine la kitaifa lilifanywa, na Ndugu Frederick Franz akatutembelea kutoka makao makuu ya ulimwengu.
Nilipotumika nikiwa mwangalizi wa kusanyiko, nilijifunza kutoka kwa Ndugu Franz jinsi ya kubadilika kulingana na hali za watu.
Ndugu wa Ufilipino walifurahi kumwona Ndugu Franz akiwa amevalia mavazi ya kitamaduni yanayoitwa barong Tagalog, alipokuwa akitoa hotuba ya watu wote.
Nilihitaji kufanya marekebisho zaidi nilipowekwa rasmi kuwa mwangalizi wa wilaya.
Wakati huo, tuliwaonyesha watu sinema ya The Happiness of the New World Society, na mara nyingi tulifanya hivyo hadharani.
Nyakati nyingine, tulisumbuliwa na wadudu.
Walivutiwa na mwanga wa projekta na kukwama ndani ya projekta.
Ilikuwa vigumu kuisafisha baadaye!
Haikuwa rahisi kupanga kuwaonyesha watu sinema hiyo, hata hivyo tuliridhika kuona jinsi walivyofurahi kuona namna tengenezo la Yehova linavyofanya kazi ulimwenguni pote.
Makasisi Wakatoliki waliwashinikiza wenye mamlaka wasitupatie vibali vya kufanya makusanyiko.
Au wangewazuia watu kusikia programu yetu kwa kupiga kengele za kanisa kila mara hotuba zilipotolewa karibu na makanisa yao.
Hata hivyo, kazi iliendelea, na watu wengi katika maeneo hayo sasa ni waabudu wa Yehova.



MIGAWO ZAIDI,
MAREKEBISHO ZAIDI

Mwaka 1959, nilipokea barua ya kunijulisha kwamba nimepata mgawo wa kutumika katika ofisi ya tawi.
Nikiwa huko nimejifunza mambo mengi hata zaidi.
Baada ya muda fulani, niliombwa nitembelee nchi mbalimbali nikiwa mwangalizi wa eneo.
Katika mojawapo ya safari hizo, nilikutana na Janet Dumond, dada mmishonari aliyetumika nchini Thailand.
Tuliwasiliana kupitia barua kwa muda fulani na hatimaye tukafunga ndoa.
Tumefurahi kutumikia pamoja tukiwa wenzi wa ndoa kwa miaka 51.





Tukiwa na Janet katika mojawapo ya visiwa vingi vya Ufilipino




Kwa ujumla, nimekuwa na pendeleo la kuwatembelea watu wa Yehova katika nchi 33.
Ninathamini sana kwamba migawo yangu ya kwanza ilinitayarisha kukabiliana na changamoto za pekee za kushughulika na watu kutoka maeneo mbalimbali!
Ziara hizo zilinisaidia kupanua akili yangu zaidi na kuona jinsi Yehova anavyowapenda watu wa namna zote.—Mdo. 10:34, 35.





Tunahakikisha kwamba tunahubiri kwa ukawaida







BADO NINAFANYA MAREKEBISHO

Nimefurahi sana kutumika pamoja na ndugu zetu nchini Ufilipino!
Sasa idadi ya wahubiri ni mara kumi zaidi ya ilivyokuwa nilipotumwa huku.
Mimi na Janet tunaendelea kutumikia pamoja katika ofisi ya tawi ya Ufilipino iliyoko Quezon City.
Hata baada ya kutumikia katika mgawo huu kwa miaka zaidi ya 60, bado ninahitaji kuwa tayari kufanya marekebisho yoyote ambayo Yehova atataka nifanye.
Kwa sababu ya mabadiliko ambayo yamefanywa hivi karibuni katika tengenezo, bado tunahitaji kuendelea kubadilikana na hali tunapomtumikia Mungu na ndugu zetu.





Tunafurahi daima tunapoona idadi ya Mashahidi ikiongezeka




Tumejitahidi kufanya yote tuwezayo kufuata mwongozo wa Yehova, na tumeridhika sana kwa kufanya hivyo.
Pia, tumefanya marekebisho yanayohitajiwa na hivyo kuwatumikia ndugu zetu kwa njia inayofaa.
Naam, maadamu ni mapenzi ya Yehova, tumeazimia kuwa “mambo yote kwa watu wa namna zote.”








Bado tunatumikia katika ofisi ya tawi iliyoko Quezon City







    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Machapisho ya Kiswahili (2000-2017)
			
		

		
			
				
				Mapendezi
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Masharti ya Utumiaji
			
			
				Sera ya Faragha
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



