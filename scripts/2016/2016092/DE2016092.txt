„Man erkenne, dass du, dessen Name Jehova ist, du allein, der Höchste bist über die ganze Erde“ (Psalm 83:18).
DIE BIBEL SAGT NOCH MEHR DAZU
  Gott trägt zwar viele Titel, aber er hat sich nur einen Eigennamen gegeben (2. Mose 3:15).

  Gott versteckt sich nicht.
Er will, dass wir ihn kennenlernen (Apostelgeschichte 17:27).

  Gottes Namen zu kennen ist der erste Schritt, sich mit ihm anzufreunden (Jakobus 4:8).





Darf man Gottes Namen aussprechen?

WAS MEINEN SIE?
  Ja

  Nein

  Kommt darauf an


WAS IN DER BIBEL STEHT
„Du sollst den Namen Jehovas, deines Gottes, nicht in unwürdiger Weise gebrauchen“ (2. Mose 20:7).
Es ist nur dann falsch, wenn man Gottes Namen respektlos verwendet (Jeremia 29:9).
DIE BIBEL SAGT NOCH MEHR DAZU
  Jesus kannte Gottes Namen und gebrauchte ihn (Johannes 17:25, 26).

  Gott freut sich, wenn wir ihn mit Namen ansprechen (Psalm 105:1).

  Gottes Gegner wollen, dass sein Name vergessen wird (Jeremia 23:27).





Mehr dazu in Kapitel 1 des Buches Was lehrt die Bibel wirklich? (herausgegeben von Jehovas Zeugen)
Auch online auf www.jw.org




    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Deutsche Publikationen (2000-2017)
			
		

		
			
				
				Einstellungen
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Nutzungsbedingungen
			
			
				Datenschutzerklärung
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



