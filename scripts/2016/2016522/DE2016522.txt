EIN Weinbauer geht eines Morgens zum Marktplatz, um Arbeiter für seinen Weinberg zu suchen.
Die Männer, die er findet, sind mit dem angebotenen Lohn einverstanden und machen sich an die Arbeit.
Doch der Weinbauer benötigt noch mehr Helfer.
Deshalb kehrt er während des Tages mehrmals zum Marktplatz zurück.
Auch denen, die er am späten Nachmittag einstellt, bietet er einen fairen Lohn an.
Am Abend ruft er alle zusammen, um sie auszuzahlen.
Er gibt jedem die gleiche Summe, ob sie viele Stunden oder nur eine gearbeitet haben.
Als die Männer, die zuerst eingestellt wurden, das bemerken, beschweren sie sich.
Der Weinbauer erwidert einem von ihnen sinngemäß: „Du warst doch mit dem vereinbarten Lohn einverstanden, oder?
Kann ich meinen Arbeitern nicht so viel geben, wie ich möchte?
Bist du neidisch, weil ich großzügig bin?“ (Mat. 20:1-15).
2 Jesu Gleichnis erinnert uns an ein Merkmal der Persönlichkeit Jehovas, das oft in der Bibel erwähnt wird — seine „unverdiente Güte“.[1] (Lies 2. Korinther 6:1.) Die Arbeiter, die nur eine Stunde gearbeitet hatten, schienen es nicht verdient zu haben, den vollen Lohn zu bekommen.
Doch der Weinbauer zeigte ihnen außergewöhnliche Güte.
Das Wort für „unverdiente Güte“ wird in vielen Bibelübersetzungen mit „Gnade“ wiedergegeben.
Ein Gelehrter schrieb dazu: „Das Wort vermittelt den Grundgedanken von einer freien und unverdienten Gabe, etwas, was dem Menschen unverdienterweise gegeben wird.“



DAS GRÖSSTE GESCHENK JEHOVAS

 3, 4. Warum und wie hat Jehova der Menschheit unverdiente Güte erwiesen?
3 Die Bibel spricht von „der freien Gabe der unverdienten Güte Gottes“ (Eph. 3:7).
Warum und wie gewährt Jehova diese „freie Gabe“?
Könnten wir alle Erfordernisse Jehovas vollkommen erfüllen, würden wir seine Güte verdienen.
Aber leider können wir das nicht.
Deshalb schrieb der weise König Salomo: „Da ist kein Mensch gerecht auf der Erde, der ständig Gutes tut und nicht sündigt“ (Pred. 7:20).
Der Apostel Paulus sagte dazu: „Alle haben gesündigt und erreichen nicht die Herrlichkeit Gottes“, und: „Der Lohn, den die Sünde zahlt, ist der Tod“ (Röm. 3:23; 6:23a).
Das ist das, was wir verdienen.
4 Doch aus Liebe zur Menschheit zeigte Jehova seine unverdiente Güte auf ganz besondere Weise.
Als größtes Geschenk überhaupt sandte er seinen „einziggezeugten Sohn“ auf die Erde, um für uns zu sterben (Joh. 3:16).
Paulus schrieb deshalb über Jesus, dass er jetzt „mit Herrlichkeit und Ehre gekrönt [ist], damit er durch Gottes unverdiente Güte für jedermann den Tod schmecke“ (Heb. 2:9).
Ja, „die Gabe . . . , die Gott gibt, ist ewiges Leben durch Christus Jesus, unseren Herrn“ (Röm. 6:23b).
 5, 6. Wie wirkt es sich aus, wenn wir (a) von Sünde oder (b) von unverdienter Güte beherrscht werden?
5 Wie sind wir Menschen in den sündigen, sterblichen Zustand gelangt, der uns allen so zusetzt?
Die Bibel erklärt, dass „durch die Verfehlung des e i n e n Menschen [Adam] der Tod als König“ über Adams Nachkommen regiert hat (Röm. 5:12, 14, 17).
Wir können uns jedoch dafür entscheiden, nicht länger von Sünde regiert oder beherrscht zu werden.
Wenn wir Glauben an Jesu Loskaufsopfer ausüben, begeben wir uns unter die Herrschaft der unverdienten Güte Jehovas.
In der Bibel heißt es: „Wo . . .
Sünde überströmte, da strömte die unverdiente Güte noch mehr über.
Wozu?
Damit so, wie die Sünde als König mit dem Tod regiert hat, so auch die unverdiente Güte als König regiere durch Gerechtigkeit zum ewigen Leben durch Jesus Christus“ (Röm. 5:20, 21).
6 Wir bleiben zwar in unserem sündigen Zustand, müssen uns aber nicht damit abfinden, dass Sünde unser Leben bestimmt.
Haben wir gesündigt, bitten wir Jehova um Vergebung und kommen dadurch unter die Herrschaft der unverdienten Güte.
Paulus schrieb warnend: „Sünde soll nicht Herr über euch sein, da ihr nicht unter Gesetz seid, sondern unter unverdienter Güte“ (Röm. 6:14).
Wie wirkt sich das aus?
Paulus sagte: „Die unverdiente Güte Gottes . . . unterweist uns,
Gottlosigkeit und weltliche Begierden von uns zu weisen und inmitten dieses gegenwärtigen Systems der Dinge mit gesundem Sinn und Gerechtigkeit und Gottergebenheit zu leben“ (Tit. 2:11, 12).



WIE DIE UNVERDIENTE GÜTE ZUM AUSDRUCK KOMMT

 7, 8. Was bedeutet es, dass sich Jehovas unverdiente Güte auf unterschiedliche Weise zeigt? (Siehe Anfangsbild.)
7 Der Apostel Petrus schrieb: „In dem Verhältnis, wie jeder eine Gabe empfangen hat, gebraucht sie, indem ihr einander als vortreffliche Verwalter der unverdienten Güte Gottes dient, die auf mannigfaltige Weise zum Ausdruck kommt“ (1. Pet. 4:10).
Was bedeutet das?
Ganz gleich, was wir durchmachen,
Jehova kann uns helfen, damit fertigzuwerden (1. Pet. 1:6).
Für jede schwierige Situation gibt es einen passenden Ausdruck der Güte Gottes.
8 Jehovas Güte zeigt sich wirklich auf unterschiedliche Weise.
Der Apostel Johannes sagte: „Wir alle haben aus seiner Fülle empfangen, ja unverdiente Güte über unverdiente Güte“ (Joh. 1:16).
Daraus ergeben sich für uns viele Segnungen.
Welche zum Beispiel?
 9. Wie kommt uns Jehovas unverdiente Güte zugute, und wie können wir unsere Dankbarkeit dafür zeigen?
9 Vergebung unserer Sünden. Dank der unverdienten Güte Jehovas werden uns unsere Sünden vergeben, vorausgesetzt, wir bereuen und kämpfen weiter mit aller Kraft gegen unsere sündigen Neigungen. (Lies 1. Johannes 1:8, 9.) Gottes Barmherzigkeit sollte uns dankbar machen und uns veranlassen, ihn zu verherrlichen.
In einem Brief an gesalbte Christen schrieb Paulus: „[Jehova] hat uns von der Gewalt der Finsternis befreit und uns in das Königreich des Sohnes seiner Liebe versetzt, durch den wir unsere Befreiung durch Lösegeld haben, die Vergebung unserer Sünden“ (Kol. 1:13, 14).
Diese Vergebung öffnet uns die Tür zu vielen weiteren schönen Segnungen.
10. Was wird uns durch Gottes unverdiente Güte ermöglicht?
10 Frieden mit Gott. Der Mensch ist in seinem sündigen Zustand von Geburt an ein Feind Gottes.
Paulus räumte ein: „Als wir Feinde waren, [wurden wir] mit Gott durch den Tod seines Sohnes versöhnt“ (Röm. 5:10).
Diese Versöhnung ermöglicht uns Frieden mit Jehova.
Paulus bringt ihn mit der unverdienten Güte Jehovas in Verbindung, wenn er zu seinen gesalbten Brüdern sagt: „Lasst uns, da wir nun zufolge des Glaubens gerechtgesprochen worden sind, uns des Friedens mit Gott erfreuen durch unseren Herrn Jesus Christus, durch den wir auch durch Glauben unseren Zutritt erlangt haben zu dieser unverdienten Güte, in der wir jetzt stehen“ (Röm. 5:1, 2).
Was für ein Geschenk!





Jehovas Güte zeigt sich auf unterschiedliche Weise:
Das Vorrecht, von der guten Botschaft zu erfahren (Siehe Absatz 11)




11. Wie bringen die Gesalbten die „anderen Schafe“ zur Gerechtigkeit?
11 Ein gerechter Stand vor Gott. Wir alle sind von Natur aus ungerecht.
Für die Zeit des Endes sagte der Prophet Daniel voraus, dass „die, die Einsicht haben“, der gesalbte Überrest, „die vielen zur Gerechtigkeit führen“ werden. (Lies Daniel 12:3.) Durch das Predigen und Lehren haben sie Millionen „anderer Schafe“ in einen gerechten Stand vor Jehova gebracht (Joh. 10:16).
Das ist jedoch nur durch Jehovas unverdiente Güte möglich geworden.
Paulus erklärte: „Als freie Gabe werden sie durch seine [Gottes] unverdiente Güte gerechtgesprochen aufgrund der Befreiung durch das von Christus Jesus bezahlte Lösegeld“ (Röm. 3:23, 24).





Die Gabe des Gebets (Siehe Absatz 12)




12. Was hat das Gebet mit Gottes unverdienter Güte zu tun?
12 Zugang zu Gott durch das Gebet. In seiner unverdienten Güte gewährt uns Jehova, zu ihm zu beten und uns so seinem Thron im Himmel zu nähern.
Interessanterweise nennt Paulus den Thron Jehovas „Thron der unverdienten Güte“ und lädt uns ein, uns diesem „mit Freimut der Rede“ zu nahen (Heb. 4:16a).
Jehova ermöglicht uns das durch seinen Sohn — durch ihn haben wir „diesen Freimut der Rede und Zutritt . . . mit Zuversicht durch unseren Glauben an ihn“ (Eph. 3:12).
Es ist wirklich ein Zeichen der unverdienten Güte Jehovas, sich jederzeit ungehindert im Gebet an ihn wenden zu dürfen.





Hilfe zur rechten Zeit (Siehe Absatz 13)




13. Wie kann uns Jehova durch seine unverdiente Güte „zur rechten Zeit“ helfen?
13 Hilfe zur rechten Zeit. Paulus ermunterte Christen, sich freimütig im Gebet an Jehova zu wenden.
Dann würden „wir Barmherzigkeit erlangen und unverdiente Güte finden . . . als Hilfe zur rechten Zeit“ (Heb. 4:16b).
Wann immer wir schwere Zeiten durchmachen, können wir zu Jehova flehen und ihn bitten, uns barmherzigerweise zu helfen.
Obwohl wir es nicht verdienen, reagiert er auf unseren Hilferuf — oft durch Glaubensbrüder.
Wir können guten Mutes sein und sagen: „Jehova ist mein Helfer; ich will mich nicht fürchten.
Was kann mir ein Mensch antun?“ (Heb. 13:6).
14. Was bewirkt Jehova durch seine unverdiente Güte in unserem Herzen?
14 Trost für unser Herz. Ein weiterer großer Segen der unverdienten Güte Jehovas ist Trost für ein bekümmertes Herz (Ps. 51:17).
An Christen in Thessalonich, die verfolgt wurden, schrieb Paulus: „Mögen . . . unser Herr Jesus Christus selbst und Gott, unser Vater, der uns geliebt hat und durch unverdiente Güte ewigen Trost und gute Hoffnung gegeben hat, eure Herzen trösten und euch . . . befestigen“ (2. Thes. 2:16, 17).
Es ist sehr tröstlich zu wissen, dass sich Jehova in seiner großzügigen Güte liebevoll um uns kümmert!
15. Welche Hoffnung haben wir dank der unverdienten Güte Gottes?
15 Aussicht auf ewiges Leben. Wären wir auf uns gestellt, hätten wir als Sünder keine Hoffnung. (Lies Psalm 49:7, 8.) Doch Jehova verheißt uns eine herrliche Zukunft.
Jesus versprach seinen Nachfolgern: „Dies ist der Wille meines Vaters, dass jeder, der den Sohn sieht und Glauben an ihn ausübt, ewiges Leben habe“ (Joh. 6:40).
Die Hoffnung auf ewiges Leben ist wirklich ein Geschenk, ein besonders schöner Ausdruck der unverdienten Güte Gottes.
Paulus sagte voller Wertschätzung: „Die unverdiente Güte Gottes, die allen Arten von Menschen Rettung bringt, ist offenbar geworden“ (Tit. 2:11).



GOTTES UNVERDIENTE GÜTE HAT GRENZEN

16. Wozu missbrauchten einige der ersten Christen die unverdiente Güte Gottes?
16 Durch die unverdiente Güte Jehovas genießen wir zwar viele Segnungen, aber wir sollten nicht vermessen sein und denken, er sehe über jedes Verhalten hinweg.
Unter den ersten Christen gab es einige, die versuchten, die unverdiente Güte Gottes als eine Entschuldigung für dreistes Verhalten zu missbrauchen (Jud. 4).
Diese untreuen Christen dachten offenbar, sie könnten sündigen und dann auf die Vergebung Jehovas zählen.
Und was noch schlimmer ist:
Sie wollten ihre Brüder zu dem gleichen Verhalten verleiten.
Auch heute verletzt jemand, der sich so verhält, „den Geist der unverdienten Güte“ (Heb. 10:29).
17. Welchen ernsten Rat gab Petrus?
17 Satan hat einige zu dem Trugschluss verleitet, sie könnten mit Gottes Barmherzigkeit rechnen und ungestraft sündigen.
Es stimmt zwar, dass Jehova bereit ist, reumütigen Sündern zu vergeben.
Aber er erwartet von uns, entschlossen gegen unsere sündigen Neigungen anzukämpfen.
Jehova ließ Petrus Folgendes aufschreiben: „Ihr daher,
Geliebte, die ihr dies im Voraus wisst, hütet euch, damit ihr nicht durch den Irrtum derer, die dem Gesetz trotzen, mit ihnen fortgerissen werdet und aus eurem eigenen festen Stand fallt.
Nein, sondern wachst weiterhin in der unverdienten Güte und an Erkenntnis unseres Herrn und Retters Jesus Christus“ (2. Pet. 3:17, 18).



UNSERE VERANTWORTUNG

18. Welche Verantwortung bringt die unverdiente Güte Jehovas mit sich?
18 Aus Wertschätzung für Gottes Güte sehen wir unsere Verantwortung, unsere Gaben zur Ehre Jehovas und zum Nutzen anderer zu gebrauchen.
Auf welche Weise?
Paulus sagt: „Da wir nun Gaben haben, die gemäß der uns verliehenen unverdienten Güte verschieden sind, es sei . . . ein Dienstamt, so widmen wir uns diesem Dienstamt; oder wer lehrt, der widme sich seinem Lehren; oder wer ermahnt, der widme sich seinem Ermahnen; . . . wer Barmherzigkeit erweist, der tue es mit Fröhlichkeit“ (Röm. 12:6-8).
Die unverdiente Güte, die Jehova uns erweist, nimmt uns in die Pflicht, uns eifrig im Dienst Jehovas einzusetzen, andere mit der Bibel vertraut zu machen,
Glaubensbrüdern Mut zuzusprechen und jedem zu vergeben, der uns vielleicht gekränkt hat.
19. Worum geht es im nächsten Artikel?
19 Weil wir für Jehovas großzügige Liebe dankbar sind, fühlen wir uns bestimmt gedrängt, mit ganzer Kraft „gründlich Zeugnis abzulegen für die gute Botschaft von der unverdienten Güte Gottes“ (Apg. 20:24).
Auf diese Verantwortung geht der nächste Artikel ein.



[1] (Absatz 2)
Siehe „Unverdiente Güte“ in Einsichten über die Heilige Schrift, Band 1,
Seite 779.




    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Deutsche Publikationen (2000-2017)
			
		

		
			
				
				Einstellungen
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Nutzungsbedingungen
			
			
				Datenschutzerklärung
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



