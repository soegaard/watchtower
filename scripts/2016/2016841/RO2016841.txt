„FEMEIE.” Isus li s-a adresat uneori persoanelor de sex opus cu acest apelativ.
De exemplu, după ce a vindecat o femeie care era gârbovă de 18 ani, el i-a spus: „Femeie, ești eliberată de slăbiciunea ta” (Luca 13:10-13).
Isus i s-a adresat chiar și mamei lui cu apelativul „femeie”.
Acest cuvânt se folosea des în timpurile biblice, fiind un termen de adresare politicoasă (Ioan 19:26; 20:13).
Exista însă un cuvânt care era mai mult decât respectuos.
„Fiică.” Acest cuvânt este folosit în Biblie cu referire la anumite femei și exprimă multă bunătate și sensibilitate.
Isus a folosit acest termen când a vorbit cu o femeie care de 12 ani avea o scurgere de sânge.
Modul în care l-a abordat femeia contravenea Legii lui Dumnezeu, întrucât o persoană aflată în situația ei era considerată necurată și trebuia să stea departe de mulțime (Lev. 15:19-27). Însă ea era cuprinsă de disperare deoarece „suferise multe de la mulți doctori și cheltuise tot ce avea, dar fără folos, ba chiar îi era mai rău” (Mar. 5:25, 26).
Furișându-se printre oameni, ea s-a apropiat de Isus pe la spate și i-a atins franjurii veșmântului.
Imediat, scurgerea de sânge i s-a oprit.
Deși femeia spera să scape neobservată,
Isus a întrebat: „Cine m-a atins?” (Luca 8:45-47).
Tremurând de frică, ea a căzut înaintea lui Isus și ‘i-a spus tot adevărul’ (Mar. 5:33).
Ca s-o liniștească,
Isus i-a zis cu blândețe: „Curaj, fiică!” (Mat. 9:22).
Unii bibliști consideră că termenul ebraic și cel grecesc redați prin „fiică” pot fi folosiți în mod metaforic ca expresie a „bunătății și a sensibilității”.
Isus a continuat să o consoleze prin cuvintele: „Credința ta te-a vindecat.
Du-te în pace și fii vindecată de boala ta chinuitoare” (Mar. 5:34).
Termenul „fiică” a fost folosit și de Boaz, un israelit bogat, când i s-a adresat moabitei Rut. Și ea se simțea neliniștită, întrucât aduna spice de pe câmpul unui necunoscut.
Boaz i-a spus: „Ascultă, fiica mea!”.
Apoi a îndemnat-o să adune în continuare spice de pe câmpurile lui.
Rut a căzut cu fața la pământ înaintea lui Boaz și l-a întrebat de ce era atât de bun cu ea, o străină.
Continuând s-o liniștească,
Boaz a răspuns: „Mi s-a istorisit tot ce-ai făcut pentru soacra ta [văduva Naomi] . . . .
Iehova să te răsplătească pentru ceea ce faci” (Rut 2:8-12).
Isus și Boaz sunt exemple extraordinare pentru bătrânii de congregație.
Există situații când doi bătrâni trebuie să-i ofere unei surori ajutor spiritual și încurajare.
După ce îi cer îndrumare lui Iehova în rugăciune și o ascultă cu atenție pe sora respectivă, bătrânii o pot liniști și consola cu ajutorul Cuvântului lui Dumnezeu (Rom. 15:4).






    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Publicații în limba română (2000-2017)
			
		

		
			
				
				Preferințe
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Termeni de utilizare
			
			
				Politică de confidențialitate
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



