Într-o zi, unul dintre vizitatori l-a întrebat: „Nu știi că moneda de aur face cât două monede de argint?”.
Băiețelul a zâmbit și a zis: „Ba da, știu”. „Atunci de ce iei moneda de argint?”, l-a întrebat musafirul. „Dacă ai lua moneda de aur, ai avea de două ori mai mulți bani.” Copilul i-a răspuns: „Da, dar atunci n-ar mai vrea nimeni să se joace cu mine. Știi cât de multe monede de argint am strâns până acum?”.
Băiețelul din această istorioară a dat dovadă de înțelepciune practică, o calitate necesară și adulților.
În Biblie se spune: „Păzește înțelepciunea practică și capacitatea de gândire.
Atunci vei umbla în siguranță pe drumul tău și piciorul nu ți se va lovi de nimic” (Prov. 3:21, 23).
Așadar, este important să știm ce e „înțelepciunea practică” și cum o putem manifesta în diferite aspecte ale vieții, întrucât siguranța noastră depinde de acești doi factori.
Dacă dăm dovadă de înțelepciune practică, ‘piciorul nu ni se va lovi de nimic’, adică nu ne vom pierde echilibrul spiritual.



CE ESTE ÎNȚELEPCIUNEA PRACTICĂ?

Înțelepciunea practică este mai mult decât cunoaștere sau pricepere. Cunoașterea presupune acumularea de informații, iar priceperea înseamnă înțelegerea conexiunilor dintre informațiile deja cunoscute.
O persoană care are înțelepciune îmbină cunoștința cu priceperea și le folosește într-un mod practic.
Să presupunem că o persoană reușește să citească și să înțeleagă relativ repede cartea Ce ne învață în realitate Biblia?. De asemenea, răspunde corect în timpul studiului, asistă la întrunirile congregației și chiar participă cu răspunsuri.
Toate acestea pot fi un indiciu că persoana progresează spiritualicește.
Dar înseamnă oare și că a dobândit înțelepciune?
Nu neapărat.
Poate că, pur și simplu, are o minte ageră.
Pentru a dobândi înțelepciune, ea trebuie să pună în practică învățăturile Bibliei, folosindu-și în mod potrivit cunoștințele și priceperea. În plus, când ia decizii care dau rezultate bune – ceea ce demonstrează că au fost luate în urma unei atente chibzuiri – , persoana respectivă dovedește că manifestă înțelepciune practică.
În Matei 7:24-27,
Isus a făcut o ilustrare despre doi bărbați care au construit câte o casă.
Despre unul dintre ei,
Isus spune că a fost „prevăzător”.
Anticipând eventualele pericole, acest bărbat și-a construit casa pe stâncă.
Astfel, el a fost prevăzător și a dat dovadă de simț practic.
El nu s-a gândit că ar fi mai ieftin sau că i-ar lua mai puțin timp dacă ar construi-o pe nisip.
Manifestând înțelepciune, a avut în vedere consecințele pe termen lung ale acțiunilor sale.
Prin urmare, când a venit furtuna, casa lui nu s-a prăbușit.
Dar cum putem să dobândim și ‘să păzim’ această calitate prețioasă, înțelepciunea practică?



CUM PUTEM SĂ O DOBÂNDIM?

În primul rând, să remarcăm că în Mica 6:9 se spune: „Omul cu înțelepciune practică se teme de numele [lui Dumnezeu]”.
A ne teme de numele lui Dumnezeu înseamnă a manifesta venerație față de Iehova, adică a avea un respect profund pentru ceea ce reprezintă numele său și pentru normele sale.
Ajungem să respectăm și să prețuim o persoană după ce îi cunoaștem modul de gândire.
Apoi, având încredere în ea, ne străduim să-i urmăm exemplul, astfel încât deciziile noastre să aibă rezultate la fel de bune ca ale ei.
Așadar, când reflectăm la consecințele acțiunilor noastre asupra relației cu Iehova și când luăm decizii pe baza normelor sale, dobândim înțelepciune practică.
În al doilea rând, în Proverbele 18:1 se spune: „Cel ce se izolează caută plăcerea sa egoistă și se împotrivește oricărei înțelepciuni practice”.
Dacă nu suntem atenți, ne putem îndepărta de Iehova și de poporul său.
Pentru a nu ajunge în această situație, trebuie să stăm în compania unor persoane care se tem de numele lui Dumnezeu și respectă normele sale.
Este bine să facem tot posibilul să fim prezenți la întruniri, asociindu-ne cu regularitate cu membrii congregației. În timpul întrunirilor, trebuie să ne deschidem mintea și inima, lăsându-ne sensibilizați de ceea ce auzim.
În plus, faptul de a ne vărsa inima înaintea lui Iehova în rugăciune ne ajută să ne apropiem și mai mult de el (Prov. 3:5, 6).
Dacă, în timp ce citim Biblia și publicațiile puse la dispoziție de organizația lui Iehova, ne deschidem mintea și inima, înțelegem care sunt consecințele pe termen lung ale faptelor noastre și acționăm în consecință.
De asemenea, este necesar să fim receptivi la sfaturile oferite de frații maturi (Prov. 19:20).
Astfel, în loc ‘să ne împotrivim oricărei înțelepciuni practice’, ne vom strădui să o cultivăm.



CUM ÎMI VA AJUTA FAMILIA?

Înțelepciunea practică ocrotește familia.
De exemplu,
Biblia o îndeamnă pe soție „să aibă respect profund” față de soțul ei (Ef. 5:33).
Cum poate un soț să câștige respectul soției?
Dacă pretinde cu duritate să fie respectat, va obține rezultate doar pe termen scurt.
Pentru a evita certurile, soția unui astfel de bărbat îi va arăta un oarecare respect când el este lângă ea.
Dar va arăta ea că îl respectă și în lipsa lui?
Puțin probabil.
Prin urmare, soțul trebuie să se gândească ce ar putea face ca să obțină rezultate pe termen lung.
Dacă manifestă rodul spiritului, fiind iubitor și bun, el va câștiga respectul profund al soției sale.
Bineînțeles, o soție creștină trebuie să manifeste respect față de soțul ei chiar dacă acesta nu-l merită (Gal. 5:22, 23).
De asemenea,
Biblia spune că soțul trebuie să-și iubească soția (Ef. 5:28, 33).
Dorind să nu piardă iubirea soțului ei, o soție s-ar putea gândi că ar fi mai bine dacă i-ar ascunde unele lucruri neplăcute, deși soțul ar avea tot dreptul să le cunoască.
Dar reflectă înțelepciune practică acest mod de gândire?
Ce se va întâmpla mai târziu, când soțul va afla că i s-au ascuns acele lucruri?
O va iubi el mai mult?
Greu de imaginat. În schimb, dacă soția îi va vorbi cu calm despre acele lucruri într-un moment potrivit, soțul va aprecia onestitatea ei și o va iubi și mai mult.





Modul în care vă disciplinați în prezent copiii influențează modul în care veți comunica mai târziu cu ei




Copiii trebuie să asculte de părinți și să fie disciplinați potrivit căilor lui Iehova (Ef. 6:1, 4).
Dar înseamnă aceasta că părinții trebuie doar să le stabilească o listă lungă de reguli?
Un părinte care manifestă înțelepciune practică știe că nu este suficient ca un copil să cunoască regulile casei ori pedeapsa pentru neascultare.
Este important ca acesta să înțeleagă și motivul pentru care trebuie să respecte regulile.
Să presupunem că un copil le vorbește părinților într-un mod lipsit de respect.
Dacă părinții îl ceartă cu asprime sau îl pedepsesc imediat, copilul se va simți, poate, rușinat sau obligat să tacă. Însă, în inima lui vor apărea, probabil, resentimente, ceea ce l-ar putea îndepărta de părinți.
Părinții care cultivă înțelepciune practică se vor gândi cum să-și disciplineze copilul și care vor fi efectele de durată ale disciplinării.
Chiar dacă sunt puși într-o situație jenantă de copilul lor, ei nu ar trebui să reacționeze sub impulsul momentului, ci să-i explice cu calm și iubire, probabil în particular, că Iehova dorește ca el să-și respecte părinții și că acest lucru va fi spre binele lui veșnic.
Apoi, când copilul va arăta respect față de părinți, va înțelege că îl onorează, de fapt, pe Iehova (Ef. 6:2, 3).
Această abordare plină de bunătate ar putea sensibiliza inima copilului.
El va simți grija sinceră a părinților și îi va respecta și mai mult.
Iar mai târziu, când vor apărea probleme mai serioase, nu va ezita să le ceară ajutorul.
Unii părinți consideră că, disciplinându-și copilul, îi vor răni sentimentele.
De aceea, evită să-l corecteze.
Dar ce se va întâmpla când copilul va crește?
Se va teme el de Iehova și va înțelege că este înțelept să respecte normele divine?
Va fi receptiv la sfaturile lui Iehova sau se va îndepărta de adevăr? (Prov. 13:1; 29:21)
Un sculptor iscusit se gândește cu mult înainte la ceea ce vrea să sculpteze.
El nu cioplește la întâmplare, sperând să obțină o operă de artă.
Părinții cu înțelepciune practică petrec multe ore învățând și aplicând normele lui Iehova.
Astfel, ei arată că se tem de numele lui.
Ei nu se îndepărtează de Iehova și de organizația sa, ci dobândesc înțelepciune practică, pe care o folosesc pentru a-și consolida familia.
În fiecare zi trebuie să luăm decizii care ne pot influența cursul vieții. În loc să recurgem la soluții rapide și să luăm decizii pripite, noi cugetăm la consecințele pe termen lung ale acțiunilor noastre.
Să căutăm îndrumarea lui Iehova și să manifestăm înțelepciune divină.
Atunci vom putea spune că păzim înțelepciunea practică, iar ea va fi „viață pentru sufletul” nostru (Prov. 3:21, 22).






    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Publicații în limba română (2000-2017)
			
		

		
			
				
				Preferințe
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Termeni de utilizare
			
			
				Politică de confidențialitate
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



