JE,
UMEWAHI kufikiria matatizo ya kutoelewana yanayotajwa katika Biblia?
Fikiria sura chache za kwanza za kitabu cha Mwanzo.
Kaini amuua Abeli (Mwa. 4:3-8);
Lameki amuua kijana kwa sababu ya kumpiga (Mwa. 4:23); wachungaji wa Abrahamu (Abramu) na Loti wagombana (Mwa. 13:5-7);
Hagari amdharau Sara (Sarai), naye Sara amkasirikia Abrahamu (Mwa. 16:3-6);
Ishmaeli amwinukia kila mtu, na mkono wa kila mtu wamwinukia yeye.—Mwa. 16:12.
2 Kwa nini Biblia inataja matatizo hayo ya kutoelewana?
Sababu moja ni kwamba masimulizi hayo yanawasaidia wanadamu wasio wakamilifu wajifunze umuhimu wa kudumisha amani.
Pia, yanatusaidia kujua jinsi ya kufanya hivyo.
Tunanufaika tunaposoma masimulizi ya Biblia kuhusu watu halisi waliokabili matatizo halisi.
Tunajifunza kuhusu matokeo ya jitihada zao na hivyo tunaweza kutumia habari hizo katika hali fulani tunazokabili maishani.
Kwa kweli, mambo hayo yote yanatusaidia kujua kile tunachopaswa au tusichopaswa kufanya tunapokabili hali kama hizo.—Rom. 15:4.
 3. Tutazungumzia nini katika makala hii?
3 Makala hii inaeleza ni kwa nini watumishi wa Yehova wanahitaji kusuluhisha kutoelewana na jinsi wanavyoweza kufanya hivyo.
Zaidi ya hilo, itazungumzia kanuni za Maandiko zinazoweza kuwasaidia kusuluhisha kutoelewana na kudumisha uhusiano mzuri na jirani zao na Yehova Mungu.



KWA NINI WATUMISHI WA MUNGU WANAHITAJI KUSULUHISHA KUTOELEWANA?

 4. Ni mtazamo gani ambao umeenea ulimwenguni pote, na matokeo yamekuwa nini?
4 Shetani ndiye chanzo kikuu cha ugomvi na kutoelewana kati ya wanadamu.
Katika Edeni, alidai kwamba kila mtu anaweza na anapaswa kujiamulia mema na mabaya, bila kumtegemea Mungu. (Mwa. 3:1-5)
Matokeo ya kufanya hivyo yanaonekana wazi.
Ulimwengu umejaa watu na mashirika yanayochochewa na ubinafsi ambao unasitawisha kiburi, majivuno, na mashindano.
Mtu yeyote mwenye mtazamo huo, atakuwa akiunga mkono dai la Shetani kwamba ni jambo la hekima kufanya mambo ya kibinafsi bila kujali jinsi yanavyowaathiri wengine.
Roho hiyo ya ubinafsi husababisha ugomvi.
Na ni vizuri tukumbuke kwamba “mtu mwenye hasira huchochea ugomvi, na mtu yeyote mwenye mwelekeo wa ghadhabu ana makosa mengi.”—Met. 29:22.
 5. Yesu alifundisha nini kuhusu kushughulikia hali ya kutoelewana?
5 Tofauti na hilo,
Yesu aliwafundisha watu watafute amani, hata ikiwa kufanya hivyo kungeonekana kwamba kunawaumiza.
Katika Mahubiri yake ya Mlimani,
Yesu alitoa ushauri bora sana wa jinsi ya kushughulikia kutoelewana au hali inayoweza kusababisha ugomvi.
Kwa mfano, aliwahimiza wanafunzi wake wawe na tabia-pole, wafanye amani, waondoe sababu za kuwa na hasira, wasuluhishe matatizo haraka, na wawapende adui zao.—Mt. 5:5, 9, 22, 25, 44.
6, 7. (a)
Kwa nini ni muhimu tusuluhishe haraka hali ya kutoelewana? (b) Watu wote wa Yehova wanapaswa kujiuliza maswali gani?
6 Jitihada zetu za kumtumikia Mungu, yaani kusali, kuhudhuria mikutano, kuhubiri, na mambo mengine yanayohusiana na ibada ni za bure ikiwa hatutaki kufanya amani na wengine. (Marko 11:25)
Hatuwezi kuwa rafiki za Mungu ikiwa hatuko tayari kuwasamehe wengine.—Soma Luka 11:4; Waefeso 4:32.
7 Kila Mkristo anahitaji kufikiria kwa makini na kwa unyofu kuhusu kuwasamehe wengine na kuwa na mahusiano yenye amani pamoja nao.
Je, unawasamehe waabudu wenzako kwa hiari?
Je, unafurahia kushirikiana nao?
Yehova anatarajia watumishi wake wawe tayari kusamehe.
Ikiwa dhamiri yako inakuambia kwamba unahitaji kuboresha sifa hiyo, mwombe Yehova akusaidie kufanya hivyo!
Baba yetu wa mbinguni atasikiliza na kujibu sala hizo zinazotolewa kwa unyenyekevu.—1 Yoh. 5:14, 15.



JE,
UNAWEZA KUPUUZA KOSA?

8, 9. Tunapaswa kufanya nini tunapokosewa?
8 Kwa kuwa hakuna mwanadamu aliye mkamilifu, wakati wowote mtu anaweza kusema au kutenda jambo litakalokukasirisha.
Hali hiyo haiepukiki. (Mhu. 7:20; Mt. 18:7)
Utatendaje?
Fikiria kisa fulani:
Katika tafrija moja iliyohudhuriwa na Mashahidi kadhaa, ndugu wawili walisalimiwa na dada fulani kwa njia ambayo mmoja wao aliiona kuwa haifai.
Dada huyo alipoondoka, ndugu aliyeudhika alianza kumchambua kwa sababu ya mambo aliyosema.
Hata hivyo, mwenzake alimkumbusha kwamba dada huyo alikuwa amemtumikia Yehova kwa ushikamanifu chini ya hali ngumu kwa miaka 40, na alikuwa na uhakika kwamba dada huyo hakuwa na nia mbaya.
Baada ya kufikiria jambo hilo kwa muda mfupi, ndugu huyo alisema hivi: “Umesema kweli.” Matokeo ni kwamba jambo hilo liliishia hapo.
9 Kisa hicho kinaonyesha nini?
Wewe ndiye mwenye mamlaka ya kuamua jinsi utakavyotenda unapokabili hali inayoweza kusababisha kutoelewana.
Mtu mwenye upendo hufunika makosa madogo. (Soma Methali 10:12; 1 Petro 4:8.) Yehova anaona kuwa ‘jambo lenye kupendeza unapopita kosa.’ (Met. 19:11; Mhu. 7:9)
Hivyo, unapohisi kwamba umetendewa bila fadhili au heshima, kwanza jiulize hivi: ‘Je, ninaweza kupuuza kosa hili?
Je, kweli ninahitaji kuendelea kulifikiria?’
10. (a)
Mwanzoni dada mmoja alitendaje wengine walipomchambua? (b) Ni Andiko gani lililomsaidia adumishe amani yake ya moyoni?
10 Wengine wanapotuchambua, inaweza kuwa vigumu kupuuza jambo hilo.
Fikiria kisa cha painia mmoja ambaye tutamwita Lucy.
Watu walisema mambo yasiyofaa kuhusu huduma yake na namna alivyotumia wakati wake.
Kwa kuwa jambo hilo lilimuudhi,
Lucy alitafuta ushauri kutoka kwa ndugu wakomavu.
Anaeleza hivi: “Ushauri wao wa Kimaandiko ulinisaidia kuendelea kuwa na mtazamo unaofaa kuhusu maoni ya wengine na kumkazia fikira zaidi Yehova.” Lucy alitiwa moyo kwa kusoma Mathayo 6:1-4. (Soma.) Mistari hiyo ilimkumbusha kwamba anapaswa kuwa na lengo la kumfurahisha Yehova.
Anaongeza hivi: “Hata wengine wanaposema mambo yasiyofaa kuhusu huduma yangu, ninaendelea kuwa mwenye furaha, kwa sababu najua kwamba ninafanya yote niwezayo ili nipate kibali cha Yehova.” Baada ya kufikia mkataa huo,
Lucy alichagua kwa hekima kupuuza maneno hayo yenye kuvunja moyo.



UNAPOSHINDWA KUPUUZA KOSA

11, 12. (a)
Mkristo anapaswa kutendaje akihisi kwamba ndugu yake “ana jambo fulani juu” yake? (b) Tunaweza kujifunza nini kutokana na jinsi Abrahamu alivyosuluhisha tatizo? (Tazama picha mwanzoni mwa makala hii.)
11 “Sisi sote hujikwaa mara nyingi.” (Yak. 3:2)
Unapaswa kufanya nini ukitambua kwamba ulisema au kutenda jambo ambalo limemkasirisha ndugu yako?
Yesu alisema hivi: “Ikiwa unaleta zawadi yako kwenye madhabahu na hapo ukumbuke kwamba ndugu yako ana jambo fulani juu yako, acha zawadi yako hapo mbele ya madhabahu, na uende zako; kwanza fanya amani yako pamoja na ndugu yako, na ndipo, ukiisha kurudi, itoe zawadi yako.” (Mt. 5:23, 24)
Kulingana na ushauri wa Yesu, zungumza na ndugu yako.
Unapofanya hivyo, lengo lako halipaswi kuwa kumtupia lawama ndugu yako, bali kukubali kosa lako na kufanya amani. Kuwa na amani pamoja na waabudu wenzetu ndilo jambo muhimu zaidi.
12 Simulizi la Biblia lililotajwa mwanzoni kuhusu Abrahamu na mpwa wake,
Loti, linaonyesha jinsi watumishi hao wa Mungu walivyosuluhisha kwa amani hali ambayo ingesababisha mgawanyiko mkubwa.
Wote wawili walikuwa na mifugo, na inaonekana wachungaji wao waligombania eneo la malisho.
Kwa kuwa alitaka sana kusuluhisha tatizo hilo,
Abrahamu alimpa Loti nafasi ya kwanza achague mahali ambapo angependa kuishi na familia yake. (Mwa. 13:1, 2, 5-9)
Huo ni mfano mzuri sana!
Abrahamu alitafuta amani, si faida zake. Je, alipata hasara kwa sababu ya kuwa mkarimu?
La hasha.
Muda mfupi baada ya tukio hilo,
Yehova alimwahidi Abrahamu baraka nyingi. (Mwa. 13:14-17)
Mungu hawezi kamwe kuwaacha watumishi wake wapate hasara ya kudumu kwa kutenda kulingana na kanuni zake na kusuluhisha mambo kwa upendo.[1]
13. Mwangalizi mmoja alitendaje alipoambiwa maneno makali, na tunaweza kujifunza nini kutokana na mfano wake?
13 Fikiria mfano mmoja wa kisasa.
Mwangalizi mpya wa idara ya kusanyiko alipompigia simu ndugu fulani kumwomba ikiwa angeweza kujitolea, ndugu huyo alisema maneno yenye kuumiza na kisha akakata simu.
Alifanya hivyo kwa sababu bado alikuwa na kinyongo na mwangalizi aliyetangulia.
Mwangalizi huyo mpya hakukasirishwa na maneno hayo, lakini pia alishindwa kuyapuuza.
Saa moja baadaye, alimpigia simu tena na akasema kwamba hawajawahi kuonana, na akapendekeza wasuluhishe tatizo hilo.
Juma moja baadaye, walikutana kwenye Jumba la Ufalme.
Baada ya kusali, walizungumza kwa saa moja, ambapo ndugu huyo alijieleza.
Baada ya kumsikiliza kwa huruma, mwangalizi huyo alimwonyesha Maandiko kadhaa, na wote wawili waliondoka kwa amani.
Baadaye, ndugu huyo alijitolea kusanyikoni na sasa anamshukuru mwangalizi huyo kwa kushughulika naye kwa utulivu na fadhili.



JE,
UWAHUSISHE WAZEE?

14, 15. (a)
Ni wakati gani tunaweza kutumia ushauri ulio kwenye Mathayo 18:15-17? (b) Yesu alitaja hatua gani tatu, na tunapaswa kuwa na lengo gani tunapozitumia?
14 Matatizo mengi ya kutoelewana kati ya Wakristo yanaweza na yanapaswa kutatuliwa faraghani na wahusika.
Hata hivyo, Yesu alisema kwamba hali fulani zingehitaji kuhusisha kutaniko. (Soma Mathayo 18:15-17.) Kungekuwa na matokeo gani ikiwa mkosaji angekataa kumsikiliza ndugu yake, mashahidi, na kutaniko?
Angetendewa “kama mtu wa mataifa na kama mkusanya-kodi.” Leo tunaweza kusema kwamba anapaswa kutengwa na ushirika.
Kwa kuwa hatua hiyo ni nzito, “dhambi” inayotajwa hapo haihusishi matatizo madogo ya kutoelewana.
Badala yake, ilikuwa (1) dhambi ambayo inaweza kuzungumziwa na kusuluhishwa kati ya wahusika lakini pia ilikuwa (2) dhambi nzito inayoweza kufanya mtu atengwe ikiwa haisuluhishwi.
Dhambi hizo zinaweza kuhusisha ulaghai au uchongezi ambao unaharibu sifa ya mtu.
Hatua tatu ambazo Yesu alitaja zitatumika tu ikiwa hali iko kama ilivyotajwa hapo juu.
Kosa hilo halihusishi dhambi kama vile uzinzi, ngono kati ya watu wa jinsia moja, uasi imani, ibada ya sanamu, au dhambi nyingine nzito ambazo wazee wa kutaniko wanapaswa kujulishwa.





Huenda ukahitaji kuzungumza mara kadhaa na ndugu yako ili umpate (Tazama fungu la 15)




15 Lengo la ushauri wa Yesu lilikuwa kumsaidia ndugu kwa upendo. (Mt. 18:12-14)
Kwanza, unapaswa kujitahidi kusuluhisha tatizo hilo bila kuwahusisha wengine.
Huenda ukahitaji kuzungumza mara kadhaa na mkosaji.
Ikiwa bado hampatani, zungumza naye mbele ya mashahidi wa kosa lililofanywa au wengine wanaoweza kukusaidia kujua ikiwa kwa kweli kosa lilifanyika.
Ukifanikiwa kusuluhisha tatizo kwa msaada wa mashahidi hao, basi utakuwa “umempata ndugu yako.” Unapaswa kuwajulisha wazee tatizo ikiwa tu umejitahidi kumsaidia mkosaji mara kadhaa bila mafanikio.
16. Ni nini kinachoonyesha kwamba kufuata ushauri wa Yesu ni njia bora na yenye upendo ya kusuluhisha matatizo?
16 Katika visa vingi, hatuhitaji kuchukua hatua zote tatu zilizotajwa katika Mathayo 18:15-17.
Hilo linatia moyo kwa sababu tatizo husuluhishwa kabla hali haijafikia hatua ambayo mtenda dhambi asiyetubu anaondolewa kutanikoni.
Mara nyingi, mkosaji hutambua kosa lake na kurekebisha mambo.
Aliyekosewa naye anamsamehe kwa kuwa anatambua kwamba hakuna sababu ya kuendelea kushikilia kosa.
Vyovyote vile, maneno ya Yesu yanaonyesha kwamba kutaniko halipaswi kuhusishwa mapema.
Wazee wanaweza kuingilia kati ikiwa tu hatua mbili zilizotangulia zimechukuliwa na kuna uthibitisho wa wazi kwamba kosa lilifanywa.
17. Tutafurahia baraka gani ‘tukitafuta amani’ pamoja na wengine?
17 Wanadamu wataendelea kuwa wasio wakamilifu na kuwakosea wengine maadamu mfumo huu unaendelea kuwapo.
Mwanafunzi Yakobo aliandika hivi: “Ikiwa yeyote hajikwai katika neno, huyo ni mtu mkamilifu, anayeweza kuuongoza kwa lijamu mwili wake wote pia.” (Yak. 3:2)
Ili tusuluhishe kutoelewana, ni lazima ‘tutafute amani, na kuifuatia.’ (Zab. 34:14)
Tukiwa wenye kufanya amani, tutafurahia mahusiano mazuri pamoja na waabudu wenzetu na tutachangia umoja wa kutaniko. (Zab. 133:1-3)
Zaidi ya yote, tutakuwa na uhusiano mzuri na Yehova, “Mungu anayetoa amani.” (Rom. 15:33)
Wale wanaosuluhisha kwa upendo hali ya kutoelewana ndio wanaofurahia baraka hizo.



[1] (fungu la 12)
Wengine waliosuluhisha matatizo kwa amani ni pamoja na:
Yakobo, na Esau (Mwa. 27:41-45; 33:1-11);
Yosefu, na ndugu zake (Mwa. 45:1-15); na Gideoni, na watu wa Efraimu. (Amu. 8:1-3)
Huenda ukafikiria mifano mingine kama hiyo iliyoandikwa katika Biblia.




    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Machapisho ya Kiswahili (2000-2017)
			
		

		
			
				
				Mapendezi
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Masharti ya Utumiaji
			
			
				Sera ya Faragha
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



