UBONGO wa mwanadamu una uzito wa karibu kilogramu 1.4 tu.
Hata hivyo, inaelezwa kwamba ubongo ndicho “kitu tata zaidi ambacho tumepata kugundua katika ulimwengu wetu.” Kwa kweli unastaajabisha sana!
Kadiri tunavyojifunza kuhusu ubongo, ndivyo tunavyothamini zaidi kazi za “ajabu” za Yehova. (Zab. 139:14)
Fikiria jambo moja ambalo ubongo wetu unaweza kufanya, yaani kuwazia.
Ni nini maana ya kuwazia?
Kamusi moja inasema kwamba kuwazia “ni uwezo tulionao wa kupiga picha akilini au kutafakari mambo mapya na yenye kusisimua, au mambo ambayo hatujawahi kuyaona.” Unapofikiria maana hiyo utagundua kwamba sisi hutumia sana uwezo wetu wa kuwazia.
Kwa mfano, umewahi kusoma au kusikia kuhusu eneo ambalo hujawahi kutembelea?
Je, hilo lilikuzuia kuwazia jinsi eneo hilo lilivyo?
Kwa kweli, kila mara tunapofikiria kitu ambacho hatuwezi kukiona, kukisikia, kukionja, kukigusa, au kukinusa, uwezo wetu wa kuwazia unaanza kufanya kazi.
Biblia inatusaidia kuelewa kwamba wanadamu waliumbwa kwa mfano wa Mungu. (Mwa. 1:26, 27)
Je, jambo hilo halionyeshi kwamba, kwa njia fulani,
Yehova mwenyewe ana uwezo wa kuwazia?
Kwa kuwa aliamua kutuumba tukiwa na uwezo huo, bila shaka anatarajia tuutumie kuelewa mapenzi yake. (Mhu. 3:11)
Tunaweza kutumiaje kwa hekima uwezo wetu wa kuwazia, na tunapaswa kuepuka matumizi gani yasiyofaa ya uwezo huo?



MATUMIZI YASIYOFAA YA UWEZO WA KUWAZIA






(1)
Ndoto za mchana wakati usiofaa au kuhusu mambo yasiyofaa.
Kwa ujumla ndoto za mchana si mbaya.
Kwa kweli, kuna uthibitisho kwamba ndoto za mchana zinaweza kuwa na manufaa.
Hata hivyo, andiko la Mhubiri 3:1 linasema kwamba kuna “wakati wa kila jambo,” hivyo huenda tukafanya mambo fulani kwa wakati usiofaa.
Kwa mfano, ikiwa tutaruhusu akili yetu itangetange tunapokuwa mikutanoni au tunapojifunza kibinafsi, je uwezo wetu wa kuwazia utakuwa msaada au hasara?
Yesu alitoa onyo zito kuhusu hatari ya kuruhusu akili zetu zifikirie mambo yasiyofaa, kama vile ukosefu wa maadili. (Mt. 5:28)
Huenda tukaruhusu akili yetu iwazie mambo ambayo yanamchukiza sana Yehova.
Kuwazia mambo machafu kunaweza kuwa hatua ya kwanza ya kufanya mambo hayo.
Azimia kabisa kutoruhusu uwezo wako wa kuwazia ukutenge na Yehova!
(2)
Kuwazia kwamba vitu vya kimwili vitakupa usalama wa kudumu.
Vitu vya kimwili ni vya lazima na vina faida.
Hata hivyo, tutavunjika moyo sana ikiwa tutafikiri kwamba usalama na furaha ya kweli inatokana na vitu hivyo.
Mfalme Sulemani mwenye hekima alisema hivi: “Vitu vyenye thamani vya tajiri ni mji wake wenye nguvu, navyo ni kama ukuta wa ulinzi katika mawazo yake.” (Met. 18:11)
Kwa mfano, fikiria kile kilichotokea wakati asilimia 80 ya jiji la Manila,
Ufilipino, ilipofurika maji kutokana na mvua kubwa iliyonyesha mnamo Septemba 2009.
Je, watu wenye vitu vingi vya kimwili waliokoka?
Tajiri mmoja aliyepoteza mali nyingi alisema hivi: “Mafuriko yamefanya sote tuwe sawa, yamesababisha hali ngumu na mateso kwa matajiri na maskini.” Inaweza kuwa rahisi kufikiri kwamba vitu vya kimwili vinatoa ulinzi na usalama wa kudumu.
Ukweli ni kwamba haviwezi kufanya hivyo.
(3)
Kuwa na wasiwasi kuhusu mambo ambayo huenda yasitokee kamwe.
Yesu alitushauri ‘tusihangaike’ kupita kiasi. (Mt. 6:34)
Kuwa na wasiwasi mwingi hufanya uwezo wetu wa kuwazia ufanye kazi sana.
Ni rahisi kupoteza nguvu nyingi kufikiria matatizo ya kuwaziwa, yaani, matatizo ambayo bado hayajatokea au ambayo huenda hayatatokea kamwe.
Maandiko yanaonyesha kwamba mahangaiko yanaweza kutukatisha tamaa na hata kutufanya tushuke moyo. (Met. 12:25)
Ni muhimu sana kutumia ushauri wa Yesu kwa kutokuwa na wasiwasi kupita kiasi, na kushughulikia mahangaiko ya kila siku kulingana na hali ya siku hiyo.



MATUMIZI YANAYOFAA YA UWEZO WA KUWAZIA






(1)
Kuwazia hali hatari na kuziepuka.
Maandiko yanatutia moyo tuwe werevu na tufikiri kabla ya kutenda. (Met. 22:3)
Tunaweza kutumia uwezo wetu wa kuwazia kufikiria matokeo ya maamuzi tunayotaka kufanya.
Kwa mfano, ikiwa umealikwa kwenye tukio fulani, uwezo wako wa kuwazia unaweza kukusaidiaje kuamua kwa hekima kama utahudhuria au la?
Baada ya kufikiria mambo kama vile ni nani watakaohudhuria, ukubwa wa tukio lenyewe, na vilevile mahali na wakati litakapofanyika, jiulize hivi: ‘Ni nini kinachoweza kutendeka katika tukio hilo?’ Unapofikiria mambo hayo, je unaona kwamba tukio hilo litaenda sambamba na kanuni za Biblia?
Mchakato huo utakusaidia kuvuta taswira ya tukio hilo akilini mwako.
Kutumia uwezo wako wa kuwazia kufanya maamuzi ya hekima kutakusaidia uepuke hali zinazoweza kusababisha madhara ya kiroho.
(2)
Kuwazia jinsi ya kutatua hali ngumu.
Kuwazia kunahusisha pia “uwezo wa kukabili na kushughulikia tatizo.” Wazia kwamba umekosana na mtu fulani kutanikoni.
Utamfikiaje ndugu au dada yako ili kujaribu kurudisha amani?
Kuna mambo mengi ya kufikiria.
Ndugu au dada huyo huwasiliana jinsi gani?
Ni wakati gani mzuri zaidi wa kuzungumzia tatizo hilo?
Nitatumia maneno gani na nitazungumza naye jinsi gani?
Ukitumia uwezo wako wa kuwazia, unaweza kufikiria mapema njia mbalimbali za kushughulikia hali hiyo na kuchagua ile unayohisi itafaa zaidi na itapokewa vizuri. (Met. 15:28)
Kushughulikia hali ngumu kwa njia hiyo kutaendeleza amani kutanikoni.
Kwa kweli, hiyo ni njia nzuri sana ya kutumia uwezo wetu wa kuwazia. 
(3)
Kuboresha usomaji wako wa Biblia na funzo lako la kibinafsi.
Ni muhimu sana kusoma Biblia kila siku.
Hata hivyo, kuna mengi zaidi yanayohusika mbali tu na kusoma.
Tunahitaji kutambua mambo tunayojifunza katika Biblia na kuyatumia maishani mwetu.
Usomaji wetu wa Biblia unapaswa kutuchochea tuthamini zaidi njia za Yehova.
Uwezo wetu wa kuwazia unaweza kutusaidia kufanya hivyo.
Jinsi gani?
Fikiria kitabu Igeni Imani Yao. Kusoma masimulizi ya kitabu hicho kunaweza kuchochea uwezo wetu wa kuwazia kwa kutusaidia tuvute taswira ya maeneo na hali za kila mhusika katika Biblia.
Kinatusaidia kuona mazingira yao, kusikia sauti, kunusa harufu, na kutambua hisia za wahusika.
Matokeo yake, tunapata masomo mazuri sana na mambo yenye kutia moyo katika masimulizi ya Biblia ambayo huenda tulifikiri tunayajua vizuri.
Kutumia uwezo wetu wa kuwazia kwa njia hiyo tunaposoma Biblia na kujifunza kibinafsi kutatunufaisha sana.
(4)
Kusitawisha uwezo wa kuelewa hisia za wengine.
Kuelewa hisia za wengine kunahusisha kuhisi maumivu yao moyoni mwetu.
Kwa kuwa Yehova na Yesu wanaonyesha sifa hiyo, ni vizuri tuwaige. (Kut. 3:7; Zab. 72:13)
Tunaweza kusitawishaje sifa hiyo?
Moja ya njia bora za kufanya hivyo inahusisha uwezo wetu wa kuwazia.
Huenda hatujawahi kupitia mambo ambayo ndugu au dada yetu anakabili.
Hata hivyo, unaweza kujiuliza hivi: ‘Ikiwa ningekuwa katika hali hii, ningehisije?
Ningehitaji nini?’ Tukitumia uwezo wetu wa kuwazia kujibu maswali hayo tunaweza kuelewa vizuri zaidi hisia za wengine.
Kufanya hivyo kutatusaidia katika kila sehemu ya maisha yetu ya Kikristo, ikiwemo katika huduma na uhusiano wetu na Wakristo wenzetu.
(5)
Kuwazia jinsi maisha yatakavyokuwa katika ulimwengu mpya.
Maandiko yamejaa habari nyingi zinazofunua jinsi maisha yatakavyokuwa katika ulimwengu mpya ulioahidiwa na Mungu. (Isa. 35:5-7; 65:21-25; Ufu. 21:3, 4)
Machapisho yetu huzungumzia mambo hayo kwa kutumia picha nyingi maridadi.
Kwa nini?
Picha huchochea uwezo wetu wa kuwazia na kutusaidia tuvute taswira ya jinsi tutakavyofurahia baraka hizo za wakati ujao.
Yehova, aliyetuumba tukiwa na uwezo wa kuwazia, anajua vizuri sana nguvu za uwezo huo.
Kuutumia kutafakari ahadi zake kunaweza kutufanya tuwe na uhakika zaidi kwamba zitatimia na hilo litatusaidia tuendelee kuwa waaminifu hata tunapokabili changamoto maishani.
Kwa upendo,
Yehova ametupatia uwezo wa pekee sana wa kuwazia.
Unaweza kutusaidia tunapomtumikia siku baada ya siku.
Na tuonyeshe kwamba tunampenda sana Yule aliyetupatia zawadi hiyo ya pekee kwa kuitumia kwa hekima kila siku.






    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Machapisho ya Kiswahili (2000-2017)
			
		

		
			
				
				Mapendezi
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Masharti ya Utumiaji
			
			
				Sera ya Faragha
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



