RUNAJ ñojtun uj kilo khuskanniyojlla pesajtinpis, “yachakusqanman jina Diospa tukuy ima ruwasqasninmanta aswan tʼukunapaj jina”. Ñojtunchej imayna ruwasqachus kasqanmanta astawan yachaspaqa,
Jehovaman astawan agradecekusun tukuy ima ruwasqasninmanta (Sal. 139:14).
Imastachus ñojtunchej ruwayta atisqanpi tʼukurina. Ñojtunchej kasqanrayku yuyayta atinchej tukuy imapi, mana rikusqanchej imaspi ima.
Kunantaj chayta qhawarina.
¿Imapajtaj yuyayninchej yanapawasunman?
Mosoj chayri sumaj imasta rikushasunmanpis jina yuyayninchejpi qhawanapaj, jinallataj manaraj rikunchejchu chay imastapis.
Kaypi tʼukurispa reparanchej sapa kuti rikushasunmanpis jina yuyasqanchejta.
Sutʼincharinapaj, mana rejsisqanchej lugarmanta leejtinchej chayri parlajta uyarejtinchej, chay lugar imaynachus kasqanpi tʼukurinchej.
Chantá imatachus mana rikuyta, uyariyta, muskhiyta, moqʼeriyta, llankhayta ima atisqanchejpi tʼukurispaqa, ñojtunchejta llankʼachinchej.
Bibliaqa Diospa rijchʼayninman jina ruwasqa kasqanchejta nin (Gén. 1:26, 27).
Chayrayku nisunman Jehovapis rikushanmanpis jina yuyayta atisqanta.
Pay ruwawarqanchej rikushasunmanpis jina yuyanapaj.
Chayrayku munan chaywan yanapachikunanchejta imachus munaynin kasqanta entiendenapaj (Ecl. 3:11).
Chayta ruwanapaj, ¿imaynatá yuyayninchej yanapawasunman? ¿Imaspitaj mana allinchu kanman yuyananchej?



SAJRA IMASPI AMA YUYANACHU






1)
Sajra imaspi yuyayqa mana allinchu.
Rikushasunmanpis jina yuyayqa, wakin kutis may allin kanman.
Chaywanpis ichá pillapis uj imata ruwashaspa, waj imaspi yuyanman.
Jinapis Eclesiastés 3:1 versiculoqa “tukuy imapis tiempoyojlla” kasqanta entiendenapaj yanapawanchej.
Sutʼincharinapaj, tantakuypi chayri sapallanchej Bibliata estudiashaspa, waj imaspi yuyayqa mana yanapawasunmanchu.
Jesuspis yuyaychawarqanchej sajra imaspi chayri khuchichakuy imaspi yuyayqa mana allinchu kasqanta (Mat. 5:28).
Chantapis wakin imaspi yuyasqanchejqa Jehovaj sonqonta nanachinman.
Millay khuchichakuy imaspi yuyasun chayqa, ichá khuchichakuy juchapi urmasunman.
Chayrayku ni jaykʼaj saqenachu imastachus yuyasqanchej Jehovamanta karunchanawanchejta.
2)
Qolqenchej tukuy imamanta jarkʼanawanchejta yuyayqa qhasilla.
Qolqeqa jarkʼakuna escudo jina.
Chaywanpis kusisqa kanapaj yanapanawanchejta, tukuy imapipuni jarkʼanawanchejta yuyaspaqa, ichá sonqo pʼakisqas qhepakusunman.
Rey Salomón nerqa: “Qhapaj runapaj kapuynenqa jarkʼachasqa llajta jina, yuyantaj uj alto perqa jina kasqanta”, nispa (Pro. 18:11).
Sutʼincharinapaj, septiembre de 2009 watapi sinchʼi para casipuni pʼampaykorqa Filipinaspa capitalnin Manila llajtata.
Chay kutipi, ¿qhapaj runas salvakorqankuchu?
Uj qhapaj runaqa ashkha kapuyninta chinkacherqa, pay nerqa: “Yaku wasaykamusqanqa, qhapajkunatapis wajchastapis kikillanta ñakʼarichiwayku”, nispa.
Kapuyninchej tukuy imamanta jarkʼanawanchejta yuyayqa, qhasilla.
Imaraykuchus chayqa mana cheqachu.
3)
Ama llakikunachu imaschus ichapis mana kanqachu chay imaspi.
Jesús yuyaychawarqanchej aswan qhepaman imachus kananmanta mana anchata phutikunata (Mat. 6:34).
Anchata phutikojkunaqa, astawan yuyanku.
Imaschus niraj kanchu chayri ni jaykʼaj kanqachu chay imasmanta llakikoyqa, kallpanchejta pisiyachinman.
Bibliaqa anchata phutikuy mana allinman tukuchinawanchejta, onqochinawanchejta ima nin (Pro. 12:25).
Chayrayku Jesuspa yuyaychasqanta kasukuna, nitaj imachus qʼaya kananmanta anchata llakikunachu.
Astawanpis sapa pʼunchay llakiyninchejta atipanapaj kallpachakuna.



ALLIN IMASPI YUYANA






1)
Niraj imatapis ajllashaspa tʼukurinaraj.
Bibliaqa yuyayniyoj kananchejta niwanchej (Pro. 22:3).
Imaraykuchus yuyayta atisqanchejqa, niraj imatapis ajllashaspa imaspichus rikukunanchejpi tʼukurinapaj yanapawanchej.
Sutʼincharinapaj, uj fiestaman wajyariwajtinchej, yuyayta atisqanchej yanapawasunchej chay fiestaman rinatachus manachus ajllanapaj.
Chay fiestaman pikunachus rinankupi, mashkhachus kanankupi, maypichus ruwakunanpi, maykʼajchus kananpi ima tʼukuriytawantaj, tapurikuna: “¿Imaynataj chay fiesta kanqa? ¿Chay fiesta Bibliaj nisqanman jinachu ruwakonqa?”, nispa.
Ajinata tapukoyqa sumajta ajllanapaj yanapawasun.
Nitaj Diosmanta karunchaj imasta ruwasunchu.
2)
Imaynatachus uj chʼampayta allinchaypi tʼukurina.
Yuyayta atisqanchejqa, chʼampaykunata atipanapaj, allinchanapaj ima yanapawanchej.
Tʼukuriytaj, mayqen hermanopis qhasimanta imatapis nisusqanpi.
Chay hermanowan chay chʼampayta allinchanaykipaj, ¿imatataj ruwayta yuyanki?
Chaypajqa kaykunata tapurikuy: ¿Chay hermanowan parlariy atikullanchu? ¿Maykʼajtaj sumaj kanman chay chʼampaymanta parlariy? ¿Imaynatá parlariyman?
Yuyayta atisqanchejqa, yanapawasunchej imaynasmantachus chay chʼampayta allinchanapi tʼukurinapaj.
Imaynatachus allinchayta yachaspañataj chay chʼampayta allinchana (Pro. 15:28).
Uj chʼampayta sumajta tʼukurispa allinchasun chayqa, congregación ujchasqa kananpaj yanapasun.
Ajinamanta yuyayta atisqanchejta sumajta apaykachasun.
3)
Bibliata aswan sumajta leena, estudiana ima.
Bibliata sapa día leeyqa may sumajpuni.
Jinapis mana ashkha paginasta leeyllachu.
Manaqa leesqanchejta sumajta entiendenanchej tiyan, chayman jinataj kausakunanchej tiyan.
Bibliata leesqanchejqa Jehovaman tukuy ima ruwasqanmanta astawan agradecekunapaj yanapawasun.
Yuyayta atisqanchejqa chayta ruwanapaj yanapawasunchej. ¿Imaynatá? Creeyninkumanta Yachakunachej nisqa libropi tʼukurina.
Chay libroqa ñaupa tiempomanta Diospa kamachisninmanta parlashan.
Chay librota leeyqa yanapawasunchej Diospa kamachisninta rikushasunmanpis jina yuyanapaj, chaypipis kashasunman jina yuyanapaj ima.
Chantapis yanapallawasuntaj chay tiempopi imachus kashasqanta qhawanapaj, imachus qʼapashasqanta reparanapaj, parlashasqankutapis uyarinapaj, imaynachus sientekusqankuta reparanapaj ima.
Ajinamanta ñaupa tiempomanta Diospa kamachisninmanta mayta yachakusun.
Mayqenkunamantachus ichapis yuyarqanchej sumajtaña yachasqanchejta.
4)
Wajkunaj llakiyninkumanta llakikuna.
Wajkunaj llakiyninkumanta sonqonchejpi llakikoyqa, may sumaj.
Jehovawan,
Jesuswantaj entiendenku wajkuna llakikuspa imaynachus kashasqankuta.
Noqanchejpis paykuna jina ruwananchej tiyan (Éxo. 3:7;
Sal. 72:13). ¿Imaynatataj wajkunaj llakiyninkumanta llakikusqanchejta rikuchisunman?
Ima llakiykunapichus hermanosninchej rikukusqankupi tʼukurispa.
Chay llakiykunapi ni jaykʼaj rikukuspapis, tapurikusunman: “¿Imaynataj sientekuyman chay llakiypi rikukuspa? ¿Imatá necesitayman?”, nispa.
Chay tapuykunapi tʼukurisun chayqa, hermanosninchej imaynachus kashasqankuta astawan sumajta entiendesun.
Wajkunaj llakiyninkumanta llakikusun chayqa, aswan sumaj cristianos kasun.
Nisunman aswan sumajta willamusunchej, hermanoswanpis aswan sumajta kausakusunchej.
5)
Mosoj pachapi imaynachus kausay kananpi tʼukurina.
Bibliaqa imaynapunichus paraisopi kausay kananmanta willawanchej (Isa. 35:5-7; 65:21-25;
Apo. 21:3, 4).
Publicacionestaj kʼachitu dibujoswan imaynapunichus paraíso kananta astawanraj rikuchiwanchej.
Chay dibujosqa imaynachus paraíso kananpi,
Diospa bendicionesninta japʼishasunmanñapis jina yuyanapaj ima yanapawanchej.
Jehová yuyayta atinapaj jina ruwawasqanchejrayku, yachan imaspichus yuyayta atisqanchejta.
Chayrayku Diospa nisqasninpi yuyasun chayqa, tukuy ima nisqan juntʼakunanpi atienekusun.
Chaytaj yanapawasun llakiykunapi kaspapis Jehovaman cheqa sonqollapuni kanapaj.
Jehovaqa yuyayta atinapaj jina ruwawarqanchej.
Chaytaj payta sumajta sirvinapaj yanapawanchej.
Sapa día allin imaspi yuyajtinchejqa, yuyayta atinapaj jina ruwawasqanchejmanta Jehovaman agradecekusqanchejta rikuchisun.






    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Quechuapi publicaciones (2004-2017)
			
		

		
			
				
				Allinchanapaj
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Kay kamachiykunata kasukuy
			
			
				Datosta apaykachakusqanmanta
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



