JUMALA SÕNA käsib meil kuuletuda inimvalitsustele, kuid see õpetab ka seda, et me peame kuuletuma eelkõige Jumalale, mitte inimestele. (Ap. t. 5:29; Tiit. 3:1.)
Kas siin on mingi vastuolu? Üldsegi mitte!
Jeesus sõnastas suhtelise allumise põhimõtte, mis aitab meil neid käske mõista ja neile kuuletuda.
Ta ütles: „Andke siis keisrile, mis kuulub keisrile, ja Jumalale, mis kuulub Jumalale.”[1] (Matt. 22:21.)
Kuidas seda juhendit järgida?
Me allume riigivõimule, kuuletudes seadustele, pidades lugu riigi ametnikest ja makstes makse. (Rooml. 13:7.)
Kui aga valitsus käsib meil teha midagi, mis on Jumala silmis vale, siis me keeldume lugupidavalt.
 2. Kuidas me saame jääda erapooletuks poliitikas?
2 Üks viis, kuidas me anname Jumalale, mis talle kuulub, on see, et jääme erapooletuks poliitikas. (Jes. 2:4.)
Me ei osuta vastupanu inimvalitsustele, kellel Jehoova laseb tegutseda; samuti ei osale me patriootlikes või natsionalistlikes tegevustes. (Rooml. 13:1, 2.)
Me ei tee lobitööd, ei hääleta valimistel, ei kandideeri poliitilistele ametikohtadele ega osale valitsusvastastes aktsioonides.
 3. Miks meil on tarvis jääda erapooletuks?
3 Piiblis on kirjas mitmeid põhjusi, miks Jumal nõuab meilt erapooletust.
Näiteks me ei osale poliitikas ja sõdades, sest järgime Jumala poega Jeesust, kes ütles, et ta „ei kuulu maailma”. (Joh. 6:15; 17:16.)
Me peame jääma selle maailma tegemistes erapooletuks ka seetõttu, et oleme Jumala kuningriigi ustavad alamad.
Kuidas muidu saaksime puhtast südamest kuulutada head sõnumit, et ainult Jumala kuningriik suudab lahendada kõik inimkonna probleemid?
Ja erinevalt muudest religioonidest, mis on killustunud selle liikmete poliitiliste vaadete tõttu, aitab õige jumalateenimisviis meie rahvusvahelisel vennaskonnal jääda ühtseks ja poliitilistes küsimustes erapooletuks. (1. Peetr. 2:17.)
 4. a)
Miks on ootuspärane, et erapooletuks jäämine muutub järjest raskemaks? b) Miks on meil tarvis just praegu tugevdada oma otsusekindlust jääda erapooletuks?
4 Riigis, kus me elame, võib poliitiline olukord olla üsna rahulik, nii et saame takistamatult Jehoovat teenida.
Kuna me aga teame, et Saatana maailma lõpp on lähedal, siis on ootuspärane, et erapooletuks jäämine muutub järjest raskemaks.
Tänapäeval on paljud inimesed järeleandmatud ja põikpäised ning see tekitab ühiskonnas aina rohkem lõhesid. (2. Tim. 3:3, 4.)
Viimasel ajal on meie vendade erapooletus pandud mõnel maal eriliselt proovile, sest poliitiline olukord on järsult muutunud.
Kas sa mõistad, miks on meil tarvis just praegu tugevdada oma otsusekindlust jääda erapooletuks?
Kui ootame sellega seni, kuni satume mingisse keerulisse olukorda, võib juhtuda, et teeme oma erapooletuses järeleandmisi.
Kuidas me saame end ette valmistada, et jääda selles lõhenenud maailmas erapooletuks?
Võtame vaatluse alla neli abinõu.



SUHTU INIMVALITSUSTESSE NAGU JEHOOVA

 5. Kuidas suhtub inimvalitsustesse Jehoova?
5 Esimene abinõu, mis aitab meil jääda erapooletuks, on see, kui suhtume inimvalitsustesse nii nagu Jehoova.
Kuigi mõni valitsus võib tegutseda üsna õiglaselt, pole see kunagi olnud Jehoova eesmärk, et inimene valitseks inimese üle. (Jer. 10:23.)
Inimvalitsused edendavad natsionalismi, mis inimperet lõhestab.
Isegi parimad juhid ei suuda lahendada kõiki probleeme.
Lisaks on inimvalitsustest saanud vaenlased Jumala kuningriigile, mis rajati aastal 1914 ning mis viib peagi täide kohtuotsuse kõikide inimvalitsuste üle ja hävitab need. (Loe Laul 2:2, 7–9.)
 6. Kuidas meil tuleks võimukandjatesse suhtuda?
6 Jumal lubab inimvalitsustel tegutseda, sest need tagavad teatud stabiilsuse, mis omakorda võimaldab meil kuulutada head sõnumit. (Rooml. 13:3, 4.)
Ta ergutab meid võimukandjate eest isegi palvetama, eriti kui nende otsused võivad mõjutada meie jumalateenimist. (1. Tim. 2:1, 2.)
Nagu apostel Paulus, nii soovime ka meie, et valitsused kohtleksid meid õiglaselt. (Ap. t. 25:11.)
Kuigi Piiblis selgitatakse, et Jumala vaenlasel Saatanal on võim poliitiliste süsteemide üle, ei öelda seal seda, et ta kontrollib igat juhti või ametnikku. (Luuka 4:5, 6.)
Seepärast ei tohiks me ühegi konkreetse ametniku kohta väita, et teda juhib Saatan.
Piibel hoiatab meid, et me „ei räägiks kellestki halvustavalt”. (Tiit. 3:1, 2.)
 7. Millisest mõtteviisist tuleks meil hoiduda?
7 Kuna me kuuletume Jumalale, ei eelista me kunagi üht poliitikut või parteid teisele, sõltumata sellest, kas need on meie poolt või vastu.
Kuidas võib see meie erapooletuse proovile panna?
Oletame, et inimesed protestivad millegi vastu, mis on toonud kannatusi ka Jumala rahvale.
Kuigi me ei lähe koos nendega tänavale, kas võib juhtuda, et pooldame nende ettevõtmisi? (Efesl. 2:2.)
Meil tuleb jääda erapooletuks mitte ainult oma sõnade ja tegudega, vaid ka oma südames.



„OLGE SIIS ETTEVAATLIKUD ...
JA SÜÜTUD”

 8. Mida tähendab olla ettevaatlik ja süütu, kui seisame silmitsi millegagi, mis paneb proovile meie erapooletuse?
8 Teine abinõu, mis aitab meil jääda erapooletuks, on see, kui oleme keerulistes olukordades „ettevaatlikud nagu maod ja süütud nagu tuvid”. (Loe Matteuse 10:16, 17.) Me saame olla ettevaatlikud, kui paneme ohtu juba varakult tähele, ja süütud, kui ei tee järeleandmisi Piibli põhimõtetes.
Mõelgem nüüd, millistes valdkondades võivad meid varitseda ohud, ja kuidas jääda neis erapooletuks.
 9. Mille suhtes meil tuleks kellegagi vesteldes olla ettevaatlik?
9 Vestlused. Meil on tarvis olla ettevaatlik, kui keegi tõstatab mingi poliitilise küsimuse.
Näiteks kui räägid head sõnumit, peaksid hoiduma mõne partei või poliitiku ideede kiitmisest või kritiseerimisest.
Püüa leida vestluspartneriga ühine alus, keskendudes probleemile, mitte mõnele poliitilisele abinõule selle lahendamiseks.
Seejärel näita Piibli abil, kuidas Jumala kuningriik selle probleemi täielikult ja alatiseks lahendab.
Kui jutuks tuleb mingi päevakajaline teema, näiteks samasooliste abielu või abort, siis too selgelt esile Jumala põhimõtted ja selgita, kuidas Jehoova tunnistajad neid rakendavad.
Arutelu ajal jää kindlalt erapooletuks selles osas, mida mingi seadus kõne all oleva küsimuse kohta ütleb.
Me ei taha võtta mingit seisukohta, kas mõni seadus tuleks jõustada, tühistada või ümber teha, ja samuti ei soovi me avaldada teistele survet, et nad oma vaateid muudaksid.
10. Kuidas me saame end kaitsta meedia propaganda eest?
10 Meedia. Tihti käsitletakse mõnd uudist eelarvamuslikult või nii, et on tugevalt tunda selle uudise edastaja eelistus.
Mõnikord on meedia poliitikute tööriistaks.
Maades, kus riik kontrollib meediakanaleid, võivad uudislood olla tugevasti kallutatud, ent isegi need kristlased, kes elavad nii-öelda vabal maal, peavad olema ettevaatlikud, et nad ei võtaks omaks mõne ajakirjaniku seisukohta.
Oleks hea endalt küsida, kas sulle meeldib kedagi kuulata seetõttu, et sa oled tema poliitiliste vaadetega sama meelt.
Kui see on nii, siis on ehk vaja otsida informatsiooni mõnest objektiivsemast allikast.
Igal juhul on tark piirata info omandamist sellistest meediakanalitest, mis propageerivad mingit poliitilist suunda, ja võrrelda kuuldut Piibli õige õpetusega. (2. Tim. 1:13.)
11. Kuidas võib ainelise vara tähtsaks pidamine ohustada meie erapooletust?
11 Materialism. Kui meie vara on meile väga tähtis, siis on oht, et me ei suuda proovilepanevas olukorras jääda erapooletuks. 1970. aastatel pidid Malawis paljud Jehoova tunnistajad andma ära kogu oma vara, kuna nad keeldusid liitumast parteiga.
Kahjuks aga ei suutnud mõned leppida mugavast elust loobumisega. Õde nimega Ruth meenutab: „Mõned põgenesid koos meiega, kuid ühinesid hiljem parteiga ja läksid koju tagasi, sest nad ei olnud valmis taluma põgenikelaagri ebamugavusi.” Siiski on enamik Jumala rahvast jäänud erapooletuks ka siis, kui neil on olnud majanduslikke raskusi või isegi kui nad on kaotanud kogu oma vara. (Heebr. 10:34.)
12., 13. a) Kuidas suhtub Jehoova inimestesse? b) Kuidas me saame hoiduda kohatust uhkusest oma päritolu üle?
12 Kohatu uhkus. Inimesed ülistavad tihti oma rassi, rahvust, kogukonda, kultuuri, linna või kodumaad.
Kuid Jehoova ei pea üht inimest või inimrühma teisest paremaks.
Muidugi ta ka ei oota, et me salgaksime oma kultuuri maha.
Kultuurilised erinevused ilmestavad inimpere imepärast mitmekesisust.
Siiski peaksime pidama meeles, et Jumala silmis on kõik inimesed võrdsed. (Rooml. 10:12.)
13 Kui laseme endas pead tõsta kohatul uhkusel oma päritolu üle, tekitab see natsionalismivaimu ja võib olla esimeseks sammuks erapoolikusele.
Kristlased pole sellise uhkuse suhtes immuunsed.
Näiteks esimesel sajandil ei kohelnud mõned heebrea vennad õiglaselt kreeka keelt kõnelevaid leski. (Ap. t. 6:1.)
Mida teha, kui meis hakkab tekkima kohatu uhkus?
Oletame, et mõni välismaalt pärit vend või õde annab sulle nõu.
Kas lükkad selle otsekohe tagasi, mõeldes, et meie siin teeme asju palju paremini kui nemad?
Meil tuleks hoopis rakendada Piibli nõuannet: „Pidage alandlikult teisi endast ülemaks.” (Filipl. 2:3.)



OTSI ABI JEHOOVALT

14. Kuidas aitab meid palve ja milline näide Piiblist seda kinnitab?
14 Kolmas abinõu erapooletuks jäämisel on paluda tuge Jehoovalt.
Palu temalt püha vaimu, mis aitab sul olla kannatlik ja end valitseda.
Neid omadusi on eriti tarvis riigis, kus on palju korruptsiooni või ebaõiglust.
Samuti võid paluda Jehoovalt tarkust, et tunda ära olukord, mis võib panna sind tegema erapooletuses järeleandmisi, ning paluda abi sellega toimetulekuks. (Jaak. 1:5.)
Kui sa oled Jehoova teenimise pärast vangis või sind on karistatud muul moel, siis palu Jehoovalt jõudu, et suudaksid jääda usus kindlaks ja pidada vastu mistahes tagakiusamises. (Loe Apostlite teod 4:27–31.)
15. Kuidas aitab Piibel meil erapooletuks jääda? (Vaata ka kasti „Jumala sõna tugevdas nende otsusekindlust”.)
15 Jehoova saab sind tugevdada oma sõna kaudu.
Mõtiskle piiblitekstide üle, mis aitavad sul jääda erapooletuks.
Talleta need oma mällu, et ammutada neist jõudu siis, kui sul pole võimalik Piiblit lugeda.
Samuti kinnitab Piibel sinu lootust, et Jumala tõotused tuleviku kohta täituvad.
Selline kindel lootus on eriti oluline, kui peame taluma tagakiusamist. (Rooml. 8:25.)
Vali välja piiblisalmid, mis kirjeldavad neid õnnistusi, mida sa eriti ootad, ja kujutle end paradiisis neist rõõmu tundmas.



USTAVATE JUMALATEENIJATE EESKUJU

16., 17. Mida me võime õppida nende ustavate Jehoova teenijate eeskujust, kes on jäänud erapooletuks? (Vaata pilti artikli alguses.)
16 Ustavate Jehoova teenijate eeskuju on neljas abinõu, mis aitab meil jääda erapooletuks.
Nende eeskuju võib anda meile vastupidamiseks vajalikku tarkust ja jõudu.
Näiteks Sadrak,
Meesak ja Abednego keeldusid kummardamast kuju, mis sümboliseeris Babüloonia riiki. (Loe Taaniel 3:16–18.) Nende kindlameelne hoiak on andnud tänapäeval Jehoova tunnistajatele julgust mitte tervitada riigilippu.
Ka Jeesus hoidus kindlameelselt sekkumast poliitikasse ja maailma tüliküsimustesse.
Teades, millist mõju tema eeskuju teistele avaldab, ergutas ta oma jüngreid: „Olge julged, mina olen maailma ära võitnud!” (Joh. 16:33.)
17 Ka nüüdisajal on paljud Jehoova teenijad jäänud erapooletuks.
Mõned on oma usu pärast pidanud taluma füüsilist väärkohtlemist ja vangistust või isegi surema.
Nende eeskuju võib sind julgustada, nagu see julgustas Türgi venda nimega Barış.
Ta ütleb: „Franz Reiter oli noor vend, kes hukati seetõttu, et ta keeldus liitumast Hitleri sõjaväega.
Kirjast, mille ta kirjutas emale õhtul enne oma surma, ilmneb tema tugev usk ja usaldus Jehoova vastu ning ma tahtsin tema eeskuju järgida, kui mu ees seisid sarnased usuproovid.”[2]
18., 19. a) Kuidas saavad koguduseliikmed aidata meil jääda erapooletuks? b) Mida meil on tarvis teha?
18 Veel võid saada tuge oma koguduse vendadelt ja õdedelt.
Räägi kogudusevanematele katsumustest, millega sa silmitsi seisad, ja kuula nende küpseid Piiblil põhinevaid nõuandeid.
Teised koguduseliikmed võivad samuti sind julgustada, kui nad teavad, milliseid raskusi sul on.
Palu neil sinu eest palvetada.
Muidugi, kui me soovime usukaaslastelt sellist tuge, peaksime ka ise tegema sedasama nende heaks. (Matt. 7:12.)
Veebisaidil jw.org olev artikkel „Kui palju Jehoova tunnistajaid on eri maades usu pärast vangis” võib aidata sul olla oma palvetes konkreetne (vaata PRESSINURK >
KOHTUASJAD).
Seal on ka lingid, kust avanevad nimekirjad Jehoova tunnistajatest, kes on usu pärast vangis.
Vali mõned nimed ja palveta, et need meie usukaaslased võiksid olla julged ja jääda laitmatuks. (Efesl. 6:19, 20.)
19 Kuna inimvalitsuste lõpp on lähedal, ei üllata meid see, et nad sallivad üha vähem meie ustavust Jehoovale ja tema kuningriigile.
Seepärast on meil tarvis tugevdada oma otsusekindlust, et jääda erapooletuks praeguses lõhenenud maailmas.



[1] (1. lõik.)
Rääkides keisrist, kes oli tolle aja kõrgeim valitseja, pidas Jeesus silmas riigivõimu üldiselt.
[2] (17. lõik.)
Vaata raamatust „Jehoova tunnistajad – Jumala kuningriigi kuulutajad” („Jehovah’s Witnesses – Proclaimers of God’s Kingdom”) lk 662 ning raamatust „Jumala kuningriik valitseb!” ptk 14 olevat kasti „Ta suri Jumala auks”.

Jumala sõna tugevdas nende otsusekindlust
  „Piiblitekstide Õpetussõnad 27:11, Matteuse 26:52 ja Johannese 13:35 üle mõtisklemine tugevdas minu otsusekindlust keelduda väeteenistusest.
Need salmid aitasid mul ka võimuesindajate ees rahulikuks jääda.” (Andri Ukrainast)

  „Piiblitekst Jesaja 2:4 aitas mul jääda erapooletuks.
Kujutlesin oma mõtteis elu uues maailmas, kus keegi ei kanna enam relva, et teistele kahju teha.” (Wilmer Colombiast)






    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Eestikeelsed väljaanded (2000-2017)
			
		

		
			
				
				Eelistused
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Kasutustingimused
			
			
				Privaatsus
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



