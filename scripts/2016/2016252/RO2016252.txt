TE CONSIDERI creștin?
Dacă da, te numeri printre cei peste 2 miliarde de oameni de pe pământ care susțin că sunt continuatori ai lui Cristos. În prezent există mii de confesiuni religioase care se declară creștine, deși se deosebesc foarte mult în privința doctrinelor și a concepțiilor.
Prin urmare, convingerile tale religioase ar putea fi foarte diferite de ale altor creștini.
Dar este important ce crezi?
Bineînțeles, dacă vrei să practici creștinismul susținut de Biblie.
Primii continuatori ai lui Isus Cristos au fost cunoscuți drept „creștini” (Faptele 11:26).
Nu erau necesare alte nume prin care să fie identificați, întrucât exista o singură credință creștină.
Toți creștinii respectau învățăturile și instrucțiunile lui Isus Cristos, fondatorul creștinismului.
Ce se poate spune despre biserica din care faci parte?
Crezi că aceasta îi învață pe oameni ce a predat Isus și ce credeau primii săi discipoli?
Cum poți verifica acest lucru?
Există o singură metodă: folosind Biblia drept etalon.
Isus Cristos a avut un respect profund față de Scripturi, considerându-le Cuvântul lui Dumnezeu.
El i-a dezaprobat pe cei care încurajau respectarea unor tradiții omenești și nu acordau importanța cuvenită învățăturilor Bibliei (Marcu 7:9-13).
Așadar, fără teama de a greși, putem trage concluzia că adevărații discipoli ai lui Isus ar trebui să-și bazeze convingerile pe Biblie.
Fiecare creștin trebuie să se întrebe:
Sunt învățăturile pe care le predă biserica mea în armonie cu Biblia?
Pentru a afla răspunsul la această întrebare, compară ce învață biserica ta cu ce învață Biblia.
Isus a spus că închinarea pe care i-o aducem lui Dumnezeu trebuie să se bazeze pe adevărul din Biblie (Ioan 4:24; 17:17).
Iar apostolul Pavel a spus că salvarea noastră depinde de faptul de a ajunge la „cunoștința exactă a adevărului” (1 Timotei 2:4).
De aceea, este vital să ne bazăm convingerile pe ce spune cu adevărat Biblia. În joc este propria salvare!



CUM SĂ NE ANALIZĂM CONVINGERILE

Te invităm să citești următoarele șase întrebări din chenarul de mai jos și să observi ce răspunsuri oferă Biblia.
Caută în Biblie versetele indicate și meditează la răspunsuri.
Apoi întreabă-te:
Există vreo asemănare între învățăturile bisericii mele și ce învață Biblia?
Aceste întrebări te vor ajuta să-ți faci cea mai importantă analiză.
Ai vrea să compari și alte învățături ale bisericii tale cu Biblia?
Martorii lui Iehova te pot ajuta să faci lucrul acesta.
Poți solicita un studiu biblic gratuit sau poți vizita site-ul nostru, jw.org.



1. ÎNTREBARE: Cine este Dumnezeu?
RĂSPUNS: Iehova,
Tatăl lui Isus, este Dumnezeul etern și Creatorul atotputernic al tuturor lucrurilor.

CE SPUNE BIBLIA:
„Îi mulțumim întotdeauna lui Dumnezeu,
Tatăl Domnului nostru Isus Cristos, când ne rugăm pentru voi.” (Coloseni 1:3)
„Tu ești demn,
Iehova, da,
Dumnezeul nostru, să primești gloria, onoarea și puterea, pentru că tu ai creat toate lucrurile.” (Revelația 4:11)
Vezi și Romani 10:13; 1 Timotei 1:17.


2. ÎNTREBARE: Cine este Isus Cristos?
RĂSPUNS: Isus este Fiul întâi născut al lui Dumnezeu.
Isus a fost creat, deci a avut început.
El este supus lui Dumnezeu și respectă voința Sa.

CE SPUNE BIBLIA:
„Tatăl este mai mare decât mine.” (Ioan 14:28)
„[Isus] este chipul Dumnezeului nevăzut, întâiul născut din toată creația.” (Coloseni 1:15)
Vezi și Matei 26:39; 1 Corinteni 15:28.


3. ÎNTREBARE: Ce este spiritul sfânt?
RĂSPUNS: Spiritul sfânt este forța pe care o folosește Dumnezeu pentru a-și duce la îndeplinire voința.
Nu este o persoană.
Oamenii pot fi umpluți de spirit sfânt și pot primi putere de la acesta.

CE SPUNE BIBLIA:
„Când Elisabeta a auzit salutul Mariei, pruncul i-a săltat în pântece. Și Elisabeta s-a umplut de spirit sfânt.” (Luca 1:41)
„Veți primi putere când spiritul sfânt va veni peste voi.” (Faptele 1:8)
Vezi și Geneza 1:2; Faptele 2:1-4; 10:38.


4. ÎNTREBARE: Ce este Regatul lui Dumnezeu?
RĂSPUNS: Regatul lui Dumnezeu este un guvern ceresc, al cărui rege este Isus. În curând, acest Regat va face ca voința lui Dumnezeu să fie îndeplinită pe tot pământul.

CE SPUNE BIBLIA:
„Al șaptelea înger a sunat din trompetă. În cer s-au auzit glasuri puternice, care ziceau: «Regatul lumii a devenit regatul Domnului nostru și al Cristosului său și el va domni ca rege pentru totdeauna și veșnic!».” (Revelația 11:15)
Vezi și Daniel 2:44; Matei 6:9, 10.


5. ÎNTREBARE: Merg toți oamenii buni la cer?
RĂSPUNS: Nu.
Un grup de oameni fideli, numit o „turmă mică”, este ales de Dumnezeu pentru a merge la cer.
Cei ce alcătuiesc acest grup vor domni împreună cu Isus ca regi peste omenire.

CE SPUNE BIBLIA:
„Nu te teme, turmă mică, pentru că Tatăl vostru a găsit de cuviință să vă dea regatul!” (Luca 12:32)
„Ei vor fi preoți ai lui Dumnezeu și ai lui Cristos și vor domni ca regi cu el.” (Revelația 20:6)
Vezi și Revelația 14:1, 3.


6. ÎNTREBARE: Care este voința lui Dumnezeu cu privire la pământ și la omenire?
RĂSPUNS: Sub domnia Regatului lui Dumnezeu, pământul va deveni un paradis, iar oamenii fideli vor avea o sănătate perfectă, pace permanentă și viață fără sfârșit.

CE SPUNE BIBLIA:
„Cei smeriți vor stăpâni pământul și își vor găsi desfătarea în belșug de pace.” (Psalmul 37:10, 11)
„El va șterge orice lacrimă din ochii lor și moartea nu va mai fi.
Nici jale, nici strigăt, nici durere nu vor mai fi.
Lucrurile de odinioară au trecut.” (Revelația 21:3, 4)
Vezi și Psalmul 37:29; 2 Petru 3:13.








    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Publicații în limba română (2000-2017)
			
		

		
			
				
				Preferințe
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Termeni de utilizare
			
			
				Politică de confidențialitate
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



