Kuna ushauri mwingi kuhusu jinsi ya kukabiliana na huzuni.
Hata hivyo, si ushauri wote unaofaa.
Kwa mfano, huenda wengine wakakushauri kwamba usilie au kuonyesha hisia zako kwa njia yoyote ile.
Huenda wengine wakakusihi uonyeshe hisia zako zote.
Biblia inatoa maoni yenye usawaziko kuhusu jambo hilo ambayo yanaungwa mkono na utafiti uliofanywa hivi karibuni.
Katika tamaduni fulani, ni aibu kwa mwanamume kulia.
Lakini, je, kuna sababu yoyote ya msingi kuona aibu kulia mbele ya watu?
Wataalamu wa akili wanakubali kwamba kulia ni njia ya kawaida ya kuonyesha huzuni.
Huenda baada ya muda, kuhuzunika kukakusaidia kukabiliana na kifo cha mpendwa wako.
Hata hivyo, kutohuzunika kunaweza kuwa na matokeo mabaya.
Biblia haiungi mkono wazo la kwamba haifai wanaume kulia wanapohuzunika.
Kwa mfano, mfikirie Yesu.
Lazaro rafiki ya Yesu alipokufa,
Yesu alilia hadharani hata ingawa alikuwa na uwezo wa kufufua wafu!—Yohana 11:33-35.
Kukasirika ni hali nyingine inayowapata wale wanaohuzunika hasa wanapofiwa ghafla na mtu wanayempenda.
Kuna sababu nyingi zinazoweza kumfanya mtu anayehuzunika awe na hasira.
Kwa mfano, huenda mtu anayeheshimiwa akasema maneno fulani bila kufikiri.
Mike kutoka Afrika Kusini anasema hivi: “Baba yangu alikufa nilipokuwa na umri wa miaka 14 tu.
Tulipokuwa katika mazishi, mtumishi wa kanisa la Anglikana alisema kwamba Mungu huwachukua mapema watu wazuri kwa kuwa anawahitaji.* Maneno hayo yalinikasirisha sana kwa kuwa tulimhitaji sana baba yetu.
Ingawa miaka 63 imepita, bado nina uchungu.”
Vipi kuhusu hisia za kujilaumu?
Ikiwa ni kifo cha ghafla, yule aliyefiwa anaweza kuwaza hivi, ‘Huenda asingekufa ikiwa ningefanya jambo hili au lile.’ Au labda mligombana na mpendwa wako kabla hajafa.
Jambo hilo linaweza kuongeza hisia za kujilaumu.
Ikiwa unajilaumu na kukasirika, jitahidi kutoficha hisia zako.
Zungumza na rafiki atakayekusikiliza na kukuhakikishia kwamba watu wengi waliofiwa huhisi hivyo.
Biblia inatukumbusha kwamba: “Rafiki wa kweli anapenda nyakati zote, naye ni ndugu aliyezaliwa kwa ajili ya wakati wa taabu.”—Methali 17:17.
Yehova Mungu,
Muumba wetu, anaweza kuwa Rafiki bora kwa yeyote aliyefiwa na mpendwa wake.
Mmiminie moyo wako kupitia sala kwa sababu ‘anakujali.’ (1 Petro 5:7)
Zaidi ya hayo, anaahidi kwamba “amani ya Mungu yenye ubora unaozidi fikira zote” itatuliza mawazo na hisia za wote wanaosali kwake. (Wafilipi 4:6, 7)
Pia, mruhusu Mungu akusaidie kupitia Neno lake linalofariji,
Biblia.
Andika orodha ya maandiko yanayokufariji. (Tazama sanduku katika ukurasa huu.) Unaweza kukariri baadhi ya maandiko hayo.
Kutafakari maandiko hayo kunaweza kukusaidia hasa usiku unapokuwa peke yako na unashindwa kupata usingizi.—Isaya 57:15.
Hivi karibuni, mwanamume mmoja mwenye umri wa miaka 40 ambaye tutamwita Jack, alifiwa na mke wake aliyeugua kansa.
Jack anasema kwamba kuna wakati ambapo anahisi upweke sana.
Lakini sala imemsaidia.
Anasema, “Ninaposali kwa Yehova, sihisi upweke.
Mara nyingi mimi huamka usiku na nyakati nyingine ninakosa usingizi mpaka asubuhi.
Baada ya kusoma na kutafakari Maandiko yenye kufariji na kisha kumimina hisia za moyo wangu kupitia sala, ninahisi utulivu na amani kwa kiasi kikubwa hivi kwamba akili na moyo wangu hutulia na kuniwezesha kulala usingizi.”
Msichana anayeitwa Vanessa alifiwa na mama yake baada ya kuugua.
Vanessa ameona nguvu ya sala.
Anasema hivi: “Hali inapokuwa ngumu, mimi hutaja jina la Mungu na kuanza kulia.
Yehova husikiliza sala zangu na mara zote hunipa msaada ninaohitaji.”
Watu wanaokabiliana na huzuni ya kufiwa hushauriwa kuwasaidia wengine au kujitolea katika kazi yoyote itakayonufaisha jamii.
Kufanya hivyo kunaweza kuwapa shangwe na kupunguza huzuni yao. (Matendo 20:35)
Wakristo wengi waliofiwa na wapendwa wao wamefarijika sana baada ya kuwasaidia wengine.—2 Wakorintho 1:3, 4.



Hilo si fundisho la Biblia.
Biblia inataja mambo matatu yanayosababisha kifo.—Mhubiri 9:11; Yohana 8:44; Waroma 5:12.

MISTARI YA BIBLIA INAYOFARIJI
  Mungu anaelewa maumivu yako. —Zaburi 55:22; 1 Petro 5:7.

  Mungu husikiliza sala za watumishi wake.—Zaburi 86:5; 1 Wathesalonike 5:17.

  Mungu huwakumbuka wale waliokufa. —Ayubu 14:13-15.

  Mungu anaahidi kwamba atawafufua wafu.—Isaya 26:19; Yohana 5:28, 29.








    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Machapisho ya Kiswahili (2000-2017)
			
		

		
			
				
				Mapendezi
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Masharti ya Utumiaji
			
			
				Sera ya Faragha
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



