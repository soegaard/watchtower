ULIKUWA wakati wa mkutano muhimu sana katika jiji la Yerusalemu.
Mfalme Daudi alikuwa amewaita wakuu, maofisa wa makao ya mfalme, na wanaume wenye nguvu.
Nao walifurahi sana kusikia tangazo la pekee.
Yehova alikuwa amemwagiza Sulemani mwana wa Daudi ajenge jumba kubwa kwa ajili ya ibada ya Mungu wa kweli.
Daudi, mfalme wa Israeli ambaye tayari alikuwa amezeeka, alipokea ramani ya ujenzi kupitia maono, na akampa Sulemani michoro hiyo.
Daudi alisema hivi: “Kazi ni kubwa; kwa kuwa jumba hilo la kifalme si la mwanadamu, bali ni la Yehova Mungu.”—1 Nya. 28:1, 2, 6, 11, 12; 29:1.
Kisha Daudi akauliza hivi: “Ni nani anayejitolea kuujaza mkono wake leo kwa zawadi kwa ajili ya Yehova?” (1 Nya. 29:5)
Ikiwa ungekuwepo wakati huo, ungetendaje baada ya kusikia maneno hayo?
Je, ungechochewa kutoa mchango kwa ajili ya kazi hiyo kubwa?
Waisraeli walichukua hatua mara moja.
Kwa kweli, “[walishangilia] kutokana na matoleo yao ya hiari waliyotoa, kwa maana walimtolea Yehova matoleo ya hiari kwa moyo kamili.”—1 Nya. 29:9.
Karne nyingi baadaye,
Yehova alianzisha mpango mkubwa zaidi wa ibada kuliko ule uliokuwa katika hekalu.
Alianzisha hekalu kubwa la kiroho, yaani, mpango ambao ungewawezesha wanadamu kumwabudu Mungu kwa msingi wa dhabihu ya Yesu. (Ebr. 9:11, 12)
Yehova anawasaidiaje watu wapatanishwe naye leo?
Ni kupitia kazi yetu ya kuhubiri na kuwafundisha watu Biblia. (Mt. 28:19, 20)
Kwa sababu ya kazi hiyo, kila mwaka wahubiri huongoza mamilioni ya mafunzo ya Biblia, maelfu ya wanafunzi hubatizwa, na mamia ya makutaniko huanzishwa.
Ukuzi huo umetokeza uhitaji mkubwa zaidi wa machapisho ya Biblia, kujenga na kudumisha Majumba ya Ufalme, na kununua viwanja kwa ajili ya majumba ya makusanyiko.
Je, si kweli kwamba kazi yetu ya kutangaza habari njema ni kubwa na yenye kuridhisha sana?—Mt. 24:14.
Kumpenda Mungu na jirani na kuwa na hisi ya uharaka kuelekea kazi ya kuhubiri Ufalme huwachochea watu wa Mungu ‘wajitolee kuujaza mkono wao zawadi kwa ajili ya Yehova’ kwa kutoa michango ya hiari.
Tunafurahi sana ‘kumheshimu Yehova kwa vitu vyetu vyenye thamani,’ na kuona jinsi mali zetu zinavyotumiwa kwa uaminifu na kwa busara kutimiza kazi kubwa zaidi katika historia ya wanadamu!—Met. 3:9.



Jinsi Ambavyo Wengine Hutoa Michango Kwa Ajili Ya Kazi Ya Ulimwenguni Pote
Watu wengi leo ‘huweka kando kitu fulani,’ au kiasi fulani cha pesa ambacho wanatumbukiza katika masanduku ya michango yaliyoandikwa “Kazi ya Ulimwenguni Pote.” (1 Kor. 16:2)
Kila mwezi, makutaniko hutuma michango hiyo katika ofisi ya tawi ya Mashahidi wa Yehova inayosimamia kazi katika nchi yao.
Unaweza pia kutuma michango yako moja kwa moja katika shirika la kisheria linalotumiwa na Mashahidi wa Yehova nchini mwenu.
Ili kujua shirika la kisheria linalotumiwa na Mashahidi wa Yehova katika nchi yenu, tafadhali wasiliana na ofisi ya tawi.
Anwani za ofisi za tawi zinapatikana katika tovuti ya www.jw.org/sw.
Michango ifuatayo inaweza kutumwa moja kwa moja:
MICHANGO YA MOJA KWA MOJA
  Michango inayotumwa kupitia mfumo wa elektroniki au kadi za benki.
Ofisi nyingine za tawi zinatumia pia tovuti ya jw.org au tovuti nyingine inayopendekezwa kupokea michango.

  Michango ya pesa taslimu, vito, au mali yoyote ya kibinafsi iliyo na thamani.
Ambatanisha barua inayoonyesha kwamba pesa taslimu au mali hizo ni michango ya moja kwa moja.


MPANGO WA MICHANGO YENYE MASHARTI
  Hii ni michango ya pesa taslimu zinazotolewa kwa masharti kwamba zinaweza kurudishwa kwa aliyechangia akizihitaji.

  Ambatanisha barua inayoonyesha kwamba ni mchango wenye masharti.


UTOAJI ULIOPANGWA
Mbali na zawadi za pesa na mali zenye thamani, kuna njia nyingine za kutoa michango kwa ajili ya kazi ya Ufalme ya ulimwenguni pote.
Njia hizo zimeorodheshwa hapa chini.
Haidhuru ni njia gani ambayo ungependa kutumia, tafadhali wasiliana kwanza na ofisi ya tawi inayosimamia kazi nchini mwenu ili kujua ni njia gani zinazotumika katika nchi yenu.
Kwa kuwa matakwa na sheria za kodi zinatofautiana, ni muhimu kuwasiliana na washauri wa mambo ya kisheria na kodi kabla ya kuchagua njia bora ya kutoa mchango.
Bima: Mchango huu unatolewa kwa kutaja kihususa shirika la Mashahidi wa Yehova kuwa ndilo litakalofaidika na bima ya maisha au malipo ya uzeeni au ya kustaafu.
Akaunti za Benki: Akaunti za benki, hati za amana, au akiba za pesa za kustaafu zinaweza kuwekwa zikiwa amana, au baada ya mtu kufa, zinaweza kulipwa kwa shirika linalotumiwa na Mashahidi wa Yehova, kulingana na matakwa ya benki za kwenu.
Hisa na Dhamana: Hisa na dhamana zinaweza kutolewa kama mchango kwa shirika la Mashahidi wa Yehova zikiwa zawadi ya moja kwa moja, au mtoaji anaweza kuandika kwamba shirika hilo lipewe hisa na dhamana zake baada ya yeye kufa.
Ardhi na Nyumba: Ardhi au nyumba zinaweza kutolewa kama mchango kwa shirika la Mashahidi wa Yehova, zikiwa zawadi ya moja kwa moja au zawadi yenye masharti, yaani, mtoaji anaweza kuendelea kuishi humo hadi kifo chake.
Malipo ya Mwaka: Pesa au amana zinazotolewa zikiwa mchango kwa shirika la Mashahidi wa Yehova chini ya makubaliano kwamba anayetoa mchango huo atapata kiasi fulani cha pesa kila mwaka katika maisha yake yote.
Huenda mtoaji akapunguziwa kodi ya serikali katika mwaka ambao mpango huo unaanzishwa.
Wosia na Amana: Mali au pesa zinaweza kuachiwa shirika la Mashahidi wa Yehova kama urithi kwa njia ya wosia halali, au linaweza kutajwa kuwa shirika ambalo litafaidika na mkataba wa amana.
Chini ya mpango huu, kodi fulani zinaweza kupunguzwa.
Maneno “utoaji uliopangwa” yanaonyesha kwamba mtu anapaswa kufanya mipango hususa ili atoe michango ya aina hiyo.
Broshua ya Kiingereza na Kihispania yenye kichwa Charitable Planning to Benefit Kingdom Service Worldwide imetayarishwa ili kuwasaidia watu wanaotaka kutegemeza kazi inayofanywa na Mashahidi wa Yehova ulimwenguni pote.
Broshua hiyo iliandikwa ili kueleza njia mbalimbali za kuchangia sasa au wakati ujao, kama vile urithi mtu anapokufa.
Huenda habari fulani katika broshua hiyo hazitumiki katika nchi yenu kwa sababu ya sheria za kodi au sheria nyingine.
Kwa kutumia njia kama hizo za kutoa michango, watu wengi wametegemeza shughuli zetu za kidini na kazi yetu ya kutoa misaada ulimwenguni pote, nao wamepunguziwa kodi.
Ikiwa broshua hiyo inapatikana katika nchi yenu, unaweza kuomba nakala kutoka kwa mwandishi wa kutaniko lenu.
Kwa habari zaidi, bofya kiunganishi “Toa Mchango kwa Ajili ya Kazi Yetu ya Ulimwenguni Pote” kwenye sehemu ya chini ya ukurasa wa kwanza wa jw.org/sw, au wasiliana na ofisi ya tawi.





    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Machapisho ya Kiswahili (2000-2017)
			
		

		
			
				
				Mapendezi
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Masharti ya Utumiaji
			
			
				Sera ya Faragha
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



