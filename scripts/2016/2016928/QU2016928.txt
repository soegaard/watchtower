¿Kay killasmanta Torremanta Qhawaj revistasta leerqankichu? ¿Kay tapuykunaman kutichiyta atiwajchu?
Mateo 18:15-17 nisqanman jina, ¿ima juchasmantataj Jesús parlasharqa?
Mayqen chʼampaykunatachus contranchejpi juchallikojwan allinchayta atisunman, chaykunamanta.
Jinapis juchallikoj mana pesachikojtenqa, congregacionmanta qharqochikunman.
Chay ‘juchallikoyqa’ kanman chʼaukiyay, llullakuy ima (w16.05, página 7).
¿Imatá ruwasunman Bibliapi leesqanchejwan aswan sumajta yanapachikunapaj?
Ashkha imasta ruwasunman.
Bibliata tukuy sonqo leena, tapurikunataj: “¿Imaynatataj kay leesqay yanapawan?, ¿Imaynatataj kay leesqaywan wajkunata yanapayman?”, nispa.
Chantá Bibliata estudianapaj publicacionesninchejwan yanapachikuna (w16.05, páginas 24-26).
Wañusqas kausarimunankuta yachasqanchejrayku, ¿juchachu anchata llakikuy?
Wañusqas kausarimunankuta yachaspapis, familianchejmanta pillapis wañupojtenqa llakikusunpuni.
Abrahampis Saramanta maytapuni waqarikorqa (Gén. 23:2).
Llakeyqa tiempo pasasqanman jina pisiyanqa (wp16.3, página 4).
Ezequiel mosqoypi jina rikusqanpi, ¿pikunawantaj ninakun chumpinpi qhelqana imasniyoj runa, armasniyoj sojta runas ima?
Chumpinpi qhelqana imasniyoj runaqa Jesuswan ninakun.
Paytaj manchay ñakʼariy tiempopi runasman uj señalta jina churanqa, mana wañunankupaj.
Armasniyoj sojta runastaj, cielopi angeleswan ninakunku.
Paykuna Jerusalén thuñikunanpaj yanaparqanku.
Paykunallataj Armagedonpi kay sajra pacha chinkachisqa kananpaj yanapanqanku (w16.06, páginas 16, 17).
¿Imastaj Bibliata mana chinkachiyta aterqachu?
Bibliaqa kashallanpuni: 1) papiro nisqapi chayri animalespa qarasninpi qhelqakojtinpis; 2) ashkha gobiernos, iglesiata kamachejkuna ima chinkachiyta munajtinkupis; 3) wakin runas Bibliaj nisqanta qʼewiyta munajtinkupis (wp16.4, páginas 4-7).
¿Imaynatá cristianos kapuyninchejta pisiyachisunman?
Imastachus necesitasqanchejmanta uj listata ruwana.
Ama rantinachu mana necesitasqanchej imasta.
Imastachus rantina kasqanta, mashkha qolqechus kapuwasqanchejta ima qhawarikuna.
Mana necesitasqanchej imasta vendena chayri wijchʼuna.
Manusninchejta pagana.
Ama llankʼayllaman qokunachu.
Imaynasmantachus Jehovapaj astawan llankʼanapitaj tʼukurina (w16.07, página 10).
¿Imataj qolqemanta chayri qorimanta nisqa aswan valorniyoj?
Job 28:12, 15 nisqanman jina,
Diospa qosqan yachayqa qolqemanta chayri qorimanta nisqa aswan valorniyoj.
Chay yachayta maskʼashaspa, llampʼu sonqollapuni kanapaj, creeyninchejta sinchʼiyachinapaj ima kallpachakuna (w16.08, páginas 18, 19).
¿Allinchu kanman hermanos chhunkayoj chayri barbayoj kananku?
Wakin lugarespi, sumaj kʼutusqa chhunkayoj qharisqa sumajpaj qhawasqa kanku, nitaj runasta miskʼachinchu.
Jinapis wakin hermanosqa mana chhunkayoj kayta ajllanku (1 Cor. 8:9).
Waj lugarespitajrí, chhunkayoj kayta mana allinpajchu qhawanku.
Chaypeqa mana allinchu kanman, uj cristiano chhunkayoj kananqa (w16.09, página 21).
¿Davidwan Goliatwan kausarqankupunichu?
Yachakusqanman jina, kay tiempomanta aswan jatun runaqa 270 centímetros kurajta miderqa.
Goliattaj 20 centímetros jinatawan miderqa.
Chantapis Davidqa kausarqapuni.
Chayta rikuchin arqueólogos uj qhelqasqata tarisqanku, maypichus Davidpa familianmanta parlan.
Jesuspis ashkha kutista Davidmanta parlarqa.
Chantá Davidwan Goliatwan maypichus maqanakorqanku, chay lugarpis kanpuni (wp16.5, página 13).
Yachakuywan, entiendeywan, yachayniyoj kaywan ima, ¿imapitaj mana kikinchu kanku?
Yachakojqa tukuy imata umanman juntʼachin.
Entiendejtaj imallamantapis yachakusqanta, imastachus yachasqanwanña kikinchan.
Yachayniyojtajrí yachakun, entiendentaj, chayman jinataj kausakun (w16.10, página 18).







    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Quechuapi publicaciones (2004-2017)
			
		

		
			
				
				Allinchanapaj
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Kay kamachiykunata kasukuy
			
			
				Datosta apaykachakusqanmanta
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



