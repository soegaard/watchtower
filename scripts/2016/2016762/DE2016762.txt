VOR über 30 Jahren verließ Osei[1], damals noch kein Zeuge Jehovas,
Ghana und kam nach Europa.
Er erinnert sich: „Ich merkte schnell, dass sich kaum einer für mich interessierte.
Außerdem war das Klima ein ziemlicher Schock.
Als ich den Flughafen verließ und zum ersten Mal in meinem Leben die Kälte spürte, kamen mir die Tränen.“ Über ein Jahr konnte Osei keine passende Arbeit finden, weil er Schwierigkeiten mit der Sprache hatte.
So weit weg von seiner Familie fühlte er sich einsam und litt unter Heimweh.
2 Überlege doch einmal:
Wie würdest du in einer vergleichbaren Situation gern behandelt werden?
Würdest du dich nicht freuen, ungeachtet deiner Nationalität oder Hautfarbe im Königreichssaal herzlich begrüßt zu werden?
Die Bibel fordert echte Christen direkt auf, „die Güte gegenüber Fremden nicht zu vergessen“ (Heb. 13:2,
Fn.).
Durchdenken wir deshalb folgende Fragen:
Wie betrachtet Jehova Menschen anderer Herkunft?
Warum müssen wir unsere Einstellung ihnen gegenüber vielleicht korrigieren?
Und wie können wir ihnen helfen, sich in unserer Versammlung wohlzufühlen?



WIE JEHOVA ÜBER FREMDE DENKT

 3, 4. Wie sollte Gottes Volk in alter Zeit gemäß 2. Mose 23:9 Fremde behandeln, und warum?
3 Nachdem Jehova die Israeliten aus Ägypten befreit hatte, gab er ihnen eine Gesetzessammlung.
Sie ließ besondere Rücksichtnahme gegenüber den vielen Nichtisraeliten erkennen, die sich ihnen angeschlossen hatten (2. Mo. 12:38, 49; 22:21).
Jehova wusste, dass Fremde oft benachteiligt sind, und sorgte deshalb liebevoll für sie, beispielsweise durch das Recht auf Nachlese (3. Mo. 19:9, 10).
4 Statt den Israeliten vorzuschreiben,
Fremde zu respektieren, appellierte Jehova an ihr Einfühlungsvermögen. (Lies 2. Mose 23:9.) Sie wussten, wie man sich als Fremder fühlt.
Wahrscheinlich wurden sie von den Ägyptern, die auf ihre Herkunft stolz waren und religiöse Vorurteile hatten, schon vor der Zeit der Sklaverei gemieden (1. Mo. 43:32; 46:34; 2. Mo. 1:11-14).
Man hatte ihnen das Leben als „ansässige Fremdlinge“ schwer gemacht.
Doch sie sollten Fremde anders behandeln.
Jehova erwartete von ihnen,
Fremde als Einheimische zu betrachten (3. Mo. 19:33, 34).
 5. Was hilft uns, wie Jehova an Personen anderer Herkunft interessiert zu sein?
5 Heute ist Jehova an Personen anderer Herkunft, die unsere Zusammenkünfte besuchen, genauso interessiert.
Davon können wir überzeugt sein (5. Mo. 10:17-19; Mal. 3:5, 6).
Machen wir uns bewusst, wie ihnen zum Beispiel Diskriminierung oder Sprachbarrieren zusetzen, werden wir nach Möglichkeiten suchen, ihnen Freundlichkeit und Mitgefühl zu erweisen (1. Pet. 3:8).



MÜSSEN WIR UNSERE ANSICHT ÜBER FREMDE KORRIGIEREN?

 6, 7. Was zeigt, dass die Christen im 1. Jahrhundert lernten, tief sitzende Vorurteile zu überwinden?
6 Die Christen im 1. Jahrhundert lernten, tief sitzende Vorurteile zu überwinden, die unter den Juden verbreitet waren.
Pfingsten 33 u. Z. erwiesen die in Jerusalem ansässigen Christen den Neubekehrten aus verschiedenen Ländern Gastfreundschaft (Apg. 2:5, 44-47).
Die liebevolle Fürsorge der Judenchristen gegenüber ausländischen Glaubensbrüdern zeigte, dass sie verstanden hatten, was Gastfreundschaft bedeutet, nämlich „Güte gegenüber Fremden“.
7 Als die frühe Christenversammlung wuchs, kam es offensichtlich zu einem Fall von Diskriminierung.
Griechisch sprechende Juden beschwerten sich, ihre Witwen würden nicht fair behandelt werden (Apg. 6:1).
Daraufhin ernannten die Apostel sieben Männer, die dafür sorgen sollten, dass niemand benachteiligt wird.
Diese Männer hatten alle griechische Namen, was andeutet, dass die Apostel den ersten Christen helfen wollten, mögliche Spannungen aufgrund ihrer unterschiedlichen Herkunft abzubauen (Apg. 6:2-6).
 8, 9. (a)
Was könnte ein Zeichen für Vorurteile oder Nationalstolz sein? (b) Was müssen wir aus unserem Herzen entfernen? (1. Pet. 1:22).
8 Ob es uns bewusst ist oder nicht:
Wir alle sind von unserer Kultur stark geprägt (Röm. 12:2).
Außerdem hören wir wahrscheinlich, wie sich Nachbarn,
Arbeitskollegen oder Mitschüler abfällig über Menschen einer anderen Herkunft oder Hautfarbe äußern.
Wie sehr lassen wir uns von solchen voreingenommenen Ansichten beeinflussen?
Und wie reagieren wir, wenn sich jemand über unsere Nationalität lustig macht — vielleicht eine Eigenheit unserer Kultur übertrieben darstellt?
9 Obwohl der Apostel Petrus eine Zeit lang Vorurteile gegenüber Nichtjuden hatte, lernte er, negative Gefühle zu überwinden (Apg. 10:28, 34, 35; Gal. 2:11-14).
Bemerken wir in uns eine Spur von Vorurteilen oder Nationalstolz, sollten auch wir uns gezielt bemühen, sie aus dem Herzen zu entfernen. (Lies 1. Petrus 1:22.) Wir könnten daran denken, dass niemand von uns Rettung verdient; wir alle sind unvollkommen, unabhängig unserer Nationalität (Röm. 3:9, 10, 21-24).
Welchen Grund haben wir also, uns irgendjemandem überlegen zu fühlen? (1. Kor. 4:7).
Wir sollten so eingestellt sein wie der Apostel Paulus, der seine gesalbten Mitchristen daran erinnerte, sie seien „nicht mehr Fremde und ansässige Fremdlinge, sondern . . .
Hausgenossen Gottes“ (Eph. 2:19).
Wenn wir uns wirklich bemühen,
Vorurteile gegenüber Fremden zu überwinden, hilft uns das bestimmt, die neue Persönlichkeit anzuziehen (Kol. 3:10, 11).



WIE MAN FREMDE FREUNDLICH BEHANDELN KANN

10, 11. Wie spiegelte sich in dem Verhalten von Boas gegenüber Ruth die Ansicht Jehovas über Fremde wider?
10 Boas spiegelte durch sein Verhalten gegenüber der Moabiterin Ruth ohne Frage Jehovas Ansicht über Fremde wider.
Als er während der Ernte seine Felder besichtigte, fiel ihm die hart arbeitende Ausländerin auf, die hinter seinen Erntearbeitern herging und Nachlese hielt.
Obwohl Ruth das Recht hatte,
Nachlese zu halten, hatte sie um Erlaubnis gebeten.
Boas hörte davon und erlaubte ihr großzügigerweise, sogar bei den Ährenbündeln zu sammeln. (Lies Ruth 2:5-7, 15, 16.)
11 Das folgende Gespräch zeigte, wie sehr Boas an Ruth und ihrer schwierigen Situation als Fremde interessiert war.
Er sagte ihr, sie könne sich an die jungen Frauen halten, damit sie nicht von den Männern auf dem Feld belästigt werde.
Auch stellte er sicher, dass sie wie die bezahlten Arbeiter mit genügend Essen und Wasser versorgt war.
Und statt von oben herab mit ihr zu sprechen, redete er ihr gut zu (Ruth 2:8-10, 13, 14).
12. Was kann Freundlichkeit bei jemandem bewirken, der erst seit Kurzem in unserem Land lebt?
12 Boas beeindruckte nicht nur Ruths selbstlose Liebe zu ihrer Schwiegermutter Noomi, sondern auch, dass sie eine Anbeterin Jehovas geworden war.
Seine Güte ihr gegenüber war in Wirklichkeit ein Ausdruck der loyalen Liebe Jehovas, denn sie war gekommen, um unter den Flügeln des „Gottes Israel . . .
Zuflucht zu suchen“ (Ruth 2:12, 20; Spr. 19:17).
Auch heute kann unser freundliches Verhalten gegenüber „allen Arten von Menschen“ dazu beitragen, dass sie die Wahrheit erkennen und spüren, wie sehr Jehova sie liebt (1. Tim. 2:3, 4).





Begrüßen wir neue Zuwanderer herzlich, wenn sie in den Königreichssaal kommen? (Siehe Absatz 13, 14)




13, 14. (a)
Warum sollten wir uns ernsthaft bemühen,
Fremde im Königreichssaal zu begrüßen? (b) Was hilft uns, über unseren Schatten zu springen und auf Personen aus einer anderen Kultur zuzugehen?
13 Behandeln wir neue Zuwanderer freundlich und begrüßen wir sie im Königreichssaal herzlich.
Vielleicht ist uns aufgefallen, dass solche Personen manchmal schüchtern und zurückhaltend sind.
Aufgrund ihrer Erziehung oder ihres sozialen Status fühlen sie sich möglicherweise Menschen anderer Herkunft oder Nationalität unterlegen.
Ergreifen wir daher die Initiative und zeigen ihnen herzliches und aufrichtiges Interesse.
Mit der JW-Language-App (sofern in unserer Sprache vorhanden) können wir lernen, sie in ihrer Muttersprache zu begrüßen. (Lies Philipper 2:3, 4.)
14 Vielleicht fällt es dir schwer, auf Personen einer anderen Kultur zuzugehen. Was kannst du dagegen tun?
Erzähle ihnen doch etwas über dich.
Möglicherweise wirst du schon bald merken, dass jede Kultur ihre Stärken und Schwächen hat und euch mehr verbindet als unterscheidet — seien es nun tatsächliche oder vermeintliche Unterschiede.



ALLEN HELFEN,
SICH ZU HAUSE ZU FÜHLEN

15. Was hilft uns, verständnisvoller mit denen umzugehen, die sich in einem neuen Land eingewöhnen müssen?
15 Damit du anderen helfen kannst, sich in der Versammlung zu Hause zu fühlen, frage dich doch: „Wie würde ich gern behandelt werden, wenn ich im Ausland wäre?“ (Mat. 7:12).
Sei geduldig mit denen, die sich an das Leben in einem neuen Land gewöhnen müssen.
Vielleicht ist es anfangs gar nicht so leicht, ihre Denkweise zu verstehen oder bestimmte Reaktionen einzuordnen.
Doch warum nehmen wir sie nicht einfach so, wie sie sind, statt von ihnen zu erwarten, freudestrahlend unsere Kultur zu übernehmen? (Lies Römer 15:7.)
16, 17. (a)
Wie können wir mit Personen einer anderen Kultur vertrauter werden? (b) Wie können wir in unserer Versammlung Brüdern helfen, die zugewandert sind?
16 Der Umgang mit Zuwanderern mag uns leichter fallen, wenn wir etwas über das Heimatland und die Kultur von ihnen erfahren.
Ist uns der Hintergrund von Zuwanderern in unserer Versammlung oder unserem Gebiet fremd, könnten wir im Rahmen des Familienstudiums etwas über sie in Erfahrung bringen.
Oder wir laden sie zum Essen nach Hause ein, um mit ihnen vertrauter zu werden.
Jehova hat „den Nationen die Tür zum Glauben geöffnet“.
Könnten wir dann nicht Fremden, die „uns im Glauben verwandt sind“, auch unsere Tür öffnen? (Apg. 14:27; Gal. 6:10; Hiob 31:32).





Erweisen wir neuen Zuwanderern liebevoll Gastfreundschaft? (Siehe Absatz 16, 17)




17 Verbringen wir mit einer zugewanderten Familie Zeit, verstehen wir besser, welche Anstrengungen es sie kostet, sich unserer Kultur anzupassen.
Vielleicht merken wir aber auch, dass sie Hilfe brauchen, um die Sprache zu lernen.
Oder wir könnten sie mit den örtlichen Behörden in Kontakt bringen, damit sie eine passende Unterkunft oder Arbeit finden.
So die Initiative zu ergreifen kann das Leben unserer Brüder entscheidend verändern (Spr. 3:27).
18. Worin ist Ruth Zuwanderern ein gutes Beispiel?
18 Zugewanderte sollten natürlich ihr Bestes tun, um sich der neuen Kultur anzupassen.
Ruth ist hierin ein gutes Beispiel.
Zum einen respektierte sie die Bräuche des neuen Landes und bat um Erlaubnis,
Nachlese zu halten (Ruth 2:7).
Sie betrachtete dieses Recht nicht als selbstverständlich, so als ob ihr andere etwas schulden würden.
Zum anderen zögerte sie nicht, sich für die Güte zu bedanken, die ihr erwiesen worden war (Ruth 2:13).
Zeigen Zuwanderer solch eine gute Einstellung, werden sie eher den Respekt von Glaubensbrüdern und Menschen in ihrem Umfeld gewinnen.
19. Welche Gründe haben wir,
Fremde bei uns willkommen zu heißen?
19 Wir freuen uns, dass Jehova in seiner unverdienten Güte Menschen aller Herkunft gewährt, von der guten Botschaft zu erfahren.
In ihrem Heimatland war es ihnen vielleicht nicht möglich, die Bibel zu studieren oder sich mit Jehovas Volk frei zu versammeln.
Jetzt haben sie die Gelegenheit, mit uns zusammenzukommen.
Bestimmt wollen wir ihnen helfen, sich in unserer Mitte nicht länger als Fremde zu fühlen.
Unsere Freundlichkeit ihnen gegenüber spiegelt Jehovas Liebe wider, selbst wenn unsere Möglichkeiten, ihnen materiell oder praktisch zu helfen, eingeschränkt sein mögen.
Heißen wir als „Nachahmer Gottes“ daher Fremde bei uns willkommen! (Eph. 5:1, 2).



[1] (Absatz 1)
Name wurde geändert.




    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Deutsche Publikationen (2000-2017)
			
		

		
			
				
				Einstellungen
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Nutzungsbedingungen
			
			
				Datenschutzerklärung
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



