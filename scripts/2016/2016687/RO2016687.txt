„PARI prea deșteaptă ca să crezi în Dumnezeu!”, i-a spus o colegă de școală unei surori din Marea Britanie.
Un frate din Germania a scris: „Profesorii mei consideră că relatarea biblică despre creație este un mit. Și li se pare normal ca elevii să creadă în evoluție”.
O soră tânără din Franța a spus: „Profesorii din școala mea sunt foarte surprinși că unii elevi mai cred în Biblie”.
2 Dacă ești un tânăr care îi slujește lui Iehova sau o persoană care învață despre El, te simți presat să te conformezi unor concepții larg răspândite, cum ar fi teoria evoluției, în loc să crezi într-un Creator?
Dacă da, există unele lucruri pe care le-ai putea face pentru a-ți întări credința și a ți-o păstra puternică.
De exemplu, ți-ai putea folosi capacitatea de gândire cu care te-a înzestrat Dumnezeu.
Aceasta „va veghea asupra ta” și te va ocroti de filozofiile lumești care îți pot distruge credința. (Citește Proverbele 2:10-12.)
 3. Ce vom analiza în acest articol?
3 Credința autentică se bazează pe cunoștința exactă despre Dumnezeu (1 Tim. 2:4).
De aceea, studiază cu atenție Biblia și publicațiile noastre; nu parcurge materialul în grabă.
Folosește-ți capacitatea de gândire pentru ‘a înțelege’ ce citești (Mat. 13:23).
Să vedem cum te poate ajuta acest lucru să-ți întărești credința în Iehova în calitate de Creator și în Biblie, subiecte cu privire la care există multe ‘dovezi convingătoare’ (Evr. 11:1, nota de subsol).



CUM SĂ-ȚI ÎNTĂREȘTI CREDINȚA

 4. De ce convingerile cu privire la Dumnezeu și la originea vieții presupun un anumit fel de credință și ce ar trebui să facă fiecare dintre noi?
4 Unii ar putea spune: „Eu cred în evoluție pentru că se bazează pe știință, în timp ce convingerea că există Dumnezeu se bazează pe credință”.
Mulți sunt de această părere. Însă este bine să nu uităm că și cei care sunt convinși că există Dumnezeu, și cei care susțin teoria evoluției au nevoie de un anumit fel de credință. În ce sens?
Niciunul dintre noi nu l-a văzut pe Dumnezeu și nici n-a fost martor la crearea vreunui lucru (Ioan 1:18).
Tot așa, nimeni, indiferent că este om de știință sau nu, n-a văzut o formă de viață evoluând în altă formă de viață.
De exemplu, nimeni n-a văzut o reptilă evoluând într-un mamifer (Iov 38:1, 4).
De aceea, fiecare dintre noi trebuie să analizeze dovezile și să tragă concluzii logice folosindu-și capacitatea de gândire.
Referitor la creație, apostolul Pavel a scris: ‘Calitățile nevăzute ale lui Dumnezeu, da, puterea sa eternă și dumnezeirea sa, se văd clar de la crearea lumii, deoarece sunt percepute prin lucrurile făcute, așa că ei sunt fără scuză’ (Rom. 1:20).





Când discuți cu alții, folosește instrumentele disponibile în limba ta (Vezi paragraful 5)




 5. Ce materiale ne ajută să ne folosim perspicacitatea?
5 „A percepe” înseamnă a recunoaște existența a ceva ce nu este vizibil sau evident imediat (Evr. 11:3).
Prin urmare, oamenii cu perspicacitate își folosesc mintea, nu doar ochii și urechile.
Pentru a ne ajuta în acest sens, organizația lui Iehova ne pune la dispoziție multe materiale bine documentate.
Acestea ne dau posibilitatea de ‘a-l vedea’ pe Creatorul nostru cu ochii credinței (Evr. 11:27).
Printre ele se numără materialul video Minunile creației dezvăluie gloria lui Dumnezeu, broșurile Viața – Opera unui Creator? și Originea vieții – Cinci întrebări care merită să fie analizate, precum și cartea Există un Creator care se interesează de voi?. De asemenea, revistele noastre conțin multe informații prețioase la care putem medita. În revista Treziți-vă! apar deseori interviuri cu oameni de știință și cu alte persoane care explică de ce acum cred în Dumnezeu.
Rubrica intitulată „Opera unui Proiectant?” prezintă multe exemple de proiecte uimitoare pe care le vedem în natură și pe care oamenii de știință se străduiesc să le imite.
 6. Cum te pot ajuta instrumentele de cercetare și ce foloase ți-au adus ele?
6 Un frate de 19 ani din Statele Unite a spus despre cele două broșuri menționate anterior: „Mi-au fost foarte utile!
Cred că am studiat aceste broșuri de peste zece ori”.
O soră din Franța a scris: „Articolele «Opera unui Proiectant?» mă uimesc!
Ele arată că cei mai buni ingineri pot să imite proiectele complexe din natură, dar nu le vor egala niciodată”.
Părinții unei fete de 15 ani din Africa de Sud au spus: „Primul lucru pe care fiica noastră îl citește din revista Treziți-vă! este rubrica «Interviu»”.
Dar ce se poate spune despre tine?
Profiți la maximum de aceste publicații?
Ele te pot ajuta și pe tine să vezi dovezile existenței unui Creator.
Astfel, vei putea să recunoști învățăturile false și să le respingi.
Credința ta se va întări și vei fi asemenea unui pom cu rădăcini adânci, care rămâne neclintit în fața vânturilor puternice (Ier. 17:5-8).



CREDINȚA TA ÎN BIBLIE

 7. De ce vrea Dumnezeu să-ți folosești puterea rațiunii?
7 Este greșit să pui întrebări sincere cu privire la Biblie?
Nicidecum!
Iehova nu vrea să crezi ceva numai pentru că și alții cred.
El vrea să-ți folosești „puterea rațiunii” ca să te convingi personal de adevăr.
De aceea, folosește-ți capacitatea de gândire pentru a dobândi cunoștință exactă.
Această cunoștință poate deveni apoi temelia solidă a unei credințe autentice. (Citește Romani 12:1, 2; 1 Timotei 2:4.) O modalitate de a dobândi o astfel de credință este aceea de a studia subiecte concrete despre care ai vrea să afli mai multe.
 8, 9. a) Ce subiecte aleg unii să studieze? b) Ce foloase au avut unii în urma meditării la ceea ce au studiat?
8 Unii aleg să studieze profețiile Bibliei sau modul în care Biblia se armonizează cu istoria, arheologia și știința.
O profeție fascinantă pe care o poți analiza este cea din Geneza 3:15.
Acest verset introduce tema Bibliei, și anume justificarea suveranității lui Dumnezeu și sfințirea numelui său prin intermediul Regatului. Într-un limbaj figurat, acest verset arată cum va pune Iehova capăt suferinței cauzate de răzvrătirea din Eden.
Cum ai putea studia Geneza 3:15?
De exemplu, ai putea face o axă a timpului.
Notează pe ea versetele-cheie care arată cum a aruncat Dumnezeu treptat lumină asupra persoanelor și a evenimentelor la care se face referire în acest verset și care dovedesc că această profeție se va împlini.
Când vei vedea că versetele alcătuiesc un tot unitar, vei ajunge la concluzia că profeții și scriitorii Bibliei au fost, în mod sigur, „purtați de spiritul sfânt” (2 Pet. 1:21).
9 Un frate din Germania a scris: „Tema Regatului străbate întreaga Biblie ca un fir roșu. Și aceasta, chiar dacă Biblia a fost scrisă de aproximativ 40 de bărbați, dintre care mulți au trăit în perioade diferite și nu s-au cunoscut unul pe altul”.
O soră din Australia a fost impresionată de un articol de studiu din Turnul de veghe din 15 decembrie 2013, care vorbea despre semnificația Paștelui evreiesc.
Această sărbătoare specială are o strânsă legătură cu Geneza 3:15 și cu venirea lui Mesia.
Ea a scris: „Studierea acelui articol m-a ajutat să înțeleg cât de minunat este Iehova.
Faptul că el a instituit această sărbătoare pentru israeliți și că ceea ce prefigura ea s-a împlinit în Isus chiar m-a impresionat.
A trebuit, pur și simplu, să mă opresc și să meditez la acea masă de Paște profetică!”.
De ce a simțit sora astfel?
Ea a meditat profund la acel articol și, ca urmare, ‘a înțeles’ sensul celor citite.
Acest lucru a ajutat-o să-și întărească credința și să se apropie mai mult de Iehova (Mat. 13:23).
10. Cum ne întărește încrederea în Biblie onestitatea scriitorilor ei?
10 O altă modalitate de a ne întări credința este să ne gândim la curajul și la onestitatea de care au dat dovadă bărbații care au scris Biblia.
Mulți scriitori din vechime și-au preamărit conducătorii și regatele.
Profeții lui Iehova însă au spus întotdeauna adevărul.
Ei au fost dispuși să scrie despre greșelile poporului lor și chiar despre cele pe care le-au făcut regii lor (2 Cron. 16:9, 10; 24:18-22). În plus, ei au vorbit despre propriile greșeli, precum și despre greșelile altor slujitori ai lui Dumnezeu (2 Sam. 12:1-14; Mar. 14:50). „O asemenea onestitate este rară, a spus un frate tânăr din Marea Britanie.
Aceasta ne sporește încrederea că Biblia provine cu adevărat de la Iehova.”
11. Cum pot tinerii să-și sporească aprecierea pentru valoarea principiilor Bibliei?
11 Aplicarea principiilor Bibliei are un efect pozitiv în viața oamenilor.
Aceasta îi convinge pe mulți că Biblia este inspirată de Dumnezeu. (Citește Psalmul 19:7-11.) O soră tânără din Japonia a scris: „Întrucât familia mea a aplicat învățăturile Bibliei, am fost cu adevărat fericiți.
Am avut parte de pace, unitate și iubire”.
Principiile Bibliei ne ocrotesc de închinarea falsă și de superstițiile care îi țin în sclavie pe mulți (Ps. 115:3-8).
Ce efect au asupra oamenilor filozofiile potrivit cărora nu există Dumnezeu?
Astfel de învățături, cum este și teoria evoluției, par să facă din natură un fel de dumnezeu, atribuindu-i puteri pe care numai Iehova le are.
Cei care spun că nu există Dumnezeu susțin că viitorul nostru este în mâinile noastre. Însă ei nu oferă o speranță sigură cu privire la un viitor mai bun (Ps. 146:3, 4).



CUM SĂ DISCUȚI CU ALȚII

12, 13. Care este o modalitate eficientă de a discuta despre creație sau despre Biblie cu colegii, cu profesorii sau cu alții?
12 Cum poți discuta în mod eficient cu alții despre creație și despre Biblie? În primul rând, nu te grăbi să presupui că știi deja ce cred ei.
Unii cred în evoluție, însă cred și în Dumnezeu.
Ei se gândesc că Dumnezeu s-a folosit de evoluție pentru a crea diferite forme de viață.
Alții cred în evoluție deoarece se gândesc că această teorie n-ar fi predată în școli dacă n-ar fi adevărată.
Iar alții nu mai cred în Dumnezeu deoarece sunt dezamăgiți de religie.
De aceea, când discuți cu cineva despre originea vieții, de obicei, este înțelept să-i pui mai întâi unele întrebări.
Află ce crede acea persoană.
Dacă ești rezonabil și dispus să asculți, probabil și ea va dori să te asculte (Tit 3:2).
13 Dar ce ai putea face dacă cineva spune că este irațional să crezi că există un Creator?
Cu tact, încearcă să arunci mingea în terenul lui, ca să zicem așa.
L-ai putea ruga să-ți explice cum ar fi putut să apară viața fără un Creator.
Pentru a continua să existe, prima formă de viață trebuia să se poată reproduce, sau multiplica.
Un profesor universitar de chimie a spus că, pentru aceasta, era nevoie printre altele de 1) o membrană protectoare, 2) capacitatea de a primi energie și de a o folosi, 3) informații genetice și 4) capacitatea de a multiplica acele informații.
El a adăugat: „Până și cea mai simplă formă de viață este de o complexitate uluitoare!”.
14. Ce poți face dacă nu te simți pregătit să vorbești despre evoluție sau despre creație?
14 Dacă nu te simți pregătit să vorbești despre evoluție sau despre creație, poți încerca să folosești raționamentul simplu prezentat de Pavel.
El a scris: „Orice casă este construită de cineva, dar cel care a construit toate lucrurile este Dumnezeu” (Evr. 3:4).
Acest raționament este foarte logic și eficient!
Da, proiectele complexe sunt opera unei minți inteligente.
De asemenea, ai putea folosi o publicație potrivită.
O soră i-a oferit cele două broșuri menționate anterior unui tânăr care a spus că nu credea în Dumnezeu și că era adeptul teoriei evoluției.
După aproximativ o săptămână, tânărul i-a spus: „Acum cred în Dumnezeu”.
S-a început un studiu biblic, iar acest tânăr a devenit fratele nostru.
15, 16. Cum ți-ai putea adapta modul de a discuta despre Biblie și cu ce obiectiv?
15 Poți folosi aceeași metodă și când vorbești cu cineva care are îndoieli cu privire la Biblie. Încearcă să afli ce crede cu adevărat și ce subiecte l-ar interesa (Prov. 18:13).
Dacă este interesat de subiecte științifice, ar putea reacționa favorabil la unele exemple care dovedesc că Biblia este exactă din punct de vedere științific.
Dacă îi place istoria, ai putea menționa exemple care arată că Biblia conține profeții exacte și relatări istorice demne de încredere.
Alții te-ar putea asculta dacă ai menționa unele principii biblice care le pot îmbunătăți viața, cum sunt cele din Predica de pe munte.
16 Amintește-ți că obiectivul tău ar trebui să fie acela de a câștiga inimi, nu dispute.
Prin urmare, fii un bun ascultător.
Pune întrebări sincere și vorbește cu blândețe și respect, în special când discuți cu oameni mai în vârstă.
Astfel, și ei îți vor respecta probabil opiniile.
De asemenea, vor vedea că ai meditat profund la convingerile tale, spre deosebire de alți tineri. Și nu uita că nu ești obligat să le răspunzi celor care vor doar să te contrazică sau să râdă de convingerile tale (Prov. 26:4).



CONVINGE-TE DE ADEVĂR

17, 18. a) Ce te poate ajuta să te convingi de adevăr? b) Ce vom analiza în articolul următor?
17 O credință puternică nu se bazează doar pe cunoașterea învățăturilor de bază ale Bibliei.
Așadar, sapă adânc în Cuvântul lui Dumnezeu, ca și cum ai căuta comori ascunse (Prov. 2:3-6).
Folosește și alte instrumente de cercetare disponibile în limba ta, cum ar fi Watchtower Library pe DVD, Watchtower – BIBLIOTECĂ ONLINE,
Indexul publicațiilor Watch Tower sau Ghidul de cercetare pentru Martorii lui Iehova.
Una dintre cele mai bune modalități de a-ți întări credința este citirea Cuvântului lui Dumnezeu.
De aceea, propune-ți să citești întreaga Biblie.
Ai putea încerca să faci aceasta pe parcursul unui an.
Amintindu-și de anii tinereții, un supraveghetor de circumscripție a spus: „Un lucru care m-a ajutat să înțeleg că Biblia este Cuvântul lui Dumnezeu a fost că am citit-o în întregime.
Astfel, am ajuns să înțeleg relatările biblice pe care le învățasem când eram mic.
Acesta a fost un punct de cotitură în progresul meu spiritual”.
18 Dragi părinți, voi aveți un rol important în creșterea spirituală a copiilor voștri.
Cum îi puteți ajuta să aibă o credință puternică?
Acest subiect va fi analizat în articolul următor.






    
    

        
        
            
            
            
        

        

            


                

                

                

                

                
                    
                      
                        
                      
                    
                
            

            
                 
            

            
            

            
            

            
                
            

        

    



    














					
				
						

		

	
	
				
	
		
			
				
				Publicații în limba română (2000-2017)
			
		

		
			
				
				Preferințe
			
		
	


	
		
							Copyright
						2017Watch Tower Bible and Tract Society of Pennsylvania		
					
				Termeni de utilizare
			
			
				Politică de confidențialitate
			
							
				 
				JW.ORG
			
									

	








	




	
	
	
	

	



